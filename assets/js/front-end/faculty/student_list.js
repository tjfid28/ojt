
var searchtimer = null;
$( "#search" ).keyup(function() {
    clearTimeout(searchtimer);
    searchtimer = setTimeout(
    function(){
       Display_Students();
    }, 2000);
});

Display_Students();
function Display_Students()
{
    $.ajax({
        url      : main_url+'front-end/faculty/Student_List_Controller/get_students',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
            "search" : $(".search").val(),
        },
        beforeSend : function(xhr){
             var label = $(".search").val() == "" ? "Please wait ..." : "Searching ...";
             $('#student_list_viewer').html('<br><br><br><center><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color:#1565c0;"></i><h5>'+label+'</h5><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){  

                var student_list = "<div class='row'>";
                var counter = 0;
                for(counter = 0;counter<data.length;counter++)
                {
                    var image = data[counter]['st_image_name']!="" ? data[counter]['st_image_name'] : "no-image.png";
                    student_list+= "<div class='col s12 m6 l6'>";
                    student_list+= "<a href='"+main_url+"faculty/student_info/"+encode(data[counter]['st_id'])+"/"+data[counter]['st_lname']+"'>";
                    student_list+= "<div class='card horizontal hoverable waves-effect waves-default' >";
                    student_list+= "<div class='card-image'>";
                    student_list+= "<img src='"+main_url+"resources/student_images/"+image+"' style='width:200px;height:200px;'>";
                    student_list+= "</div>";
                    student_list+= "<div class='card-stacked'>";
                    student_list+= "<div class='card-content black-text'>";
                    student_list+= "<p><b>Student No . "+data[counter]['st_number']+".</b></p>";
                    student_list+= "<p><b>"+data[counter]['st_lname']+", "+data[counter]['st_fname']+" "+data[counter]['st_mname'].substring(0, 1)+".</b></p>";
                    student_list+= "</div>";
                    student_list+= "</div>";
                    student_list+= "</div>";
                    student_list+= "<a >";
                    student_list+= "</div>";
                }
                if(counter==0)
                {
                    student_list += "<div class='center-align' style='margin-top:200px;margin-bottom:300px;'><h5><i class='fa fa-group'></i> No Student</h5></div>";
                }

                student_list += "</div>";
                
            $('#student_list_viewer').html(student_list);
        },
        error: function(xhr, status, error) 
        {
           alert(error);
        }         
    });
}