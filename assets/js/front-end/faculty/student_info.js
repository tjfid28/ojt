Display_Requirements();
$('select').material_select();
function Display_Requirements()
{   
    var by_type = $('#by_type').val();
    $.ajax({
        url      : main_url+'front-end/faculty/Student_Info_Controller/get_requirements',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
            'by_type' : by_type
        },
        beforeSend : function(xhr){
             $('#requirement_list_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){  
                var documents = "";
                var counter = 0;
                    for(counter = 0;counter<data.length;counter++)
                    {
                        var ext        = data[counter]['d_file_name'].substr(data[counter]['d_file_name'].lastIndexOf('.') + 1);
                        var color      = null;
                        var badgeColor = data[counter]['sr_status']=="Approved" ? "blue":"red";
                        var status     = data[counter]['sr_status']=="Approved"||data[counter]['sr_status']=="Rejected" ? "<span class='new badge "+ badgeColor +"' data-badge-caption='"+data[counter]['sr_status']+"'></span>" : "<span class='new badge' data-badge-caption='Pending'></span>";
                        get_ext = get_extention(ext);
                        documents += "<div class='card-document col s12 m4 l3' id='card-"+data[counter]['sr_id']+"'>";
                        documents += "<a onclick='show_document("+data[counter]['sr_id']+")'>";
                        documents += "<div class='card-panel waves-effect hoverable "+ext+"' style='text-align:center;height:240px;width:100%;z-index:0;'>";
                        documents += "<input type='hidden' id='req_id_"+data[counter]['sr_id']+"' value='"+data[counter]['req_id']+"' readonly>";
                        documents += "<i class='fa fa-file-"+get_ext.ext+"-o' style='font-size:100px;color:"+get_ext.color+"'></i>";
                        documents += "<input type='hidden' id='d_slug_"+data[counter]['sr_id']+"' value='"+data[counter]['d_slug']+"' readonly>";
                        documents += "<input type='hidden' id='d_file_name_"+data[counter]['sr_id']+"' value='"+data[counter]['d_file_name']+"' readonly>";
                        documents += "<h6 class='black-text truncate'>"+data[counter]['d_file_name']+"</h6>";
                        documents += "<h6 class='black-text truncate' id='req_type_name_"+data[counter]['sr_id']+"'>"+data[counter]['req_name']+"</h6>";
                        documents += "<h5 class='black-text'>"+ status +"</h5>";
                        documents += "</div>";
                        documents += "</a>";
                        documents += "</div>";
                    }
                documents += "";
                if(counter==0)
                {
                    documents += "<div class='center-align' style='margin-top:200px;margin-bottom:300px;'><h5><i class='fa fa-file-text-o'></i> No Requirement</h5></div>";
                }
                
            $('#requirement_list_viewer').html(documents);
        },
        error: function(xhr, status, error) 
        {
           alert(error);
        }         
    });
}

$( "#by_type" ).change(function() {
    Display_Requirements();
});

$( ".decision" ).click(function() {
    var decision = $(this).val();
    var sr_id    = $('#sr_id').attr('data-bind');
    var req_id   = $('#req_id_'+sr_id).val();

    swal({
      title: "Are you sure?",
      text: "This requirement will change status",
      type: decision == "Approve" ? "info" : "warning",
      showCancelButton: true,
      confirmButtonColor: decision == "Approve" ? "#0d47a1" : "#DD6B55",
      confirmButtonText: "Yes, "+decision+" it!",
      closeOnConfirm: false
    },
    function(){
        $.ajax({
            url  : main_url+'front-end/faculty/Student_Info_Controller/checking',
            type : 'POST',
            data: {
                'decision' : decision,
                'sr_id'    : sr_id,
                'req_id'   : req_id,
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Processing Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                
                swal("Success!", data, "success")
                Display_Requirements();

            },
            error: function(xhr, status, error) 
            {
                alert(error);
            }  
        });
    });
});


function show_document(sr_id)
{
    $('#checker').modal('open');
    $('#c_file_name').text($('#d_file_name_'+sr_id).val());
    $('#c_file_type').text($('#req_type_name_'+sr_id).text());
    $('#sr_id').attr('data-bind',sr_id);
}

$( ".download" ).click(function() {
    var sr_id = $('#sr_id').attr('data-bind');
    var slug  = $('#d_slug_'+sr_id).val();
    var f_name= $('#d_file_name_'+sr_id).val();
    swal({
      title: "Are you sure?",
      text: "This "+f_name+" will be downloaded",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#0d47a1",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: true,
      showCancelButton: true,
    },
    function(){
      $('#checker').modal('close');
      window.open(main_url+"front-end/student/My_Document_Controller/download_document/"+slug+"/"+f_name);
    });
});

$( ".checklist" ).change(function() {
    $.ajax({
            url  : main_url+'front-end/faculty/Student_Info_Controller/checklist',
            type : 'POST',
            data: {
                'check' : $(this).val()
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Processing Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){               
                swal("Success!", data, "success");
            },
            error: function(xhr, status, error) 
            {
                alert(error);
            }  
    });
});