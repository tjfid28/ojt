Display_Classes();
function Display_Classes()
{
    $.ajax({
        url      : main_url+'front-end/faculty/Class_List_Controller/get_classes',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
             $('#class_list_viewer').html('<br><br><br><center><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color:#1565c0;"></i><h5>Please wait ...</h5><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var class_list = "<div class='row'>";
                var counter = 0;
                for(counter = 0;counter<data.length;counter++)
                {
                    var image = data[counter]['cla_slug']!="" ? data[counter]['cla_slug'] : "no-image.png";
                    class_list += "<div class='col s12 m6'>";
                    class_list += "<div class='card'>";
                    class_list += "<div class='card-image'>";
                    class_list += "<img src='"+main_url+"resources/class_images/"+image+"' style='height:350px;'>";
                    class_list += "<span class='card-title' style='background-color:rgba(0, 0, 0, 0.5);' >"+data[counter]['sec_name']+"</span>";
                    class_list += "<a href='"+main_url+"faculty/student_list/"+encode(data[counter]['class_id'])+"/"+data[counter]['sec_name']+"' style='margin-right:70px;' class='btn-floating btn-large halfway-fab waves-effect waves-light blue darken-4'><i class='fa fa-user'></i></a>";
                    class_list += "<a href='"+main_url+"faculty/class_room/"+encode(data[counter]['class_id'])+"/"+data[counter]['sec_name']+"' class='btn-floating btn-large halfway-fab waves-effect waves-light blue darken-4'><i class='fa fa-folder-open'></i></a>";
                    class_list += "<a class='btn-floating btn-large halfway-fab waves-effect waves-light blue darken-4' style='margin-right:140px;' onclick='upload_cover("+data[counter]['cla_id']+")'><i class='material-icons'>image</i></a>";
                    class_list += "</div>";
                    class_list += "<div class='card-content'><br>";
                    class_list += "<b>"+data[counter]['sub_name']+"</b>";
                    class_list += "<p>"+data[counter]['cou_title']+"</p>";
                    class_list += "<br>";
                    class_list += "<div class='right-align'>";
                    class_list += "<p class='blue-text'>"
                        + "<a onclick='preview_reports("+data[counter]['cla_id']+", 0)' style='cursor:pointer;'><i class='fa fa-download'></i> Download Reports</a> | "
                        + "<a onclick='preview_reports("+data[counter]['cla_id']+", 1)' style='cursor:pointer;'><i class='fa fa-download'></i> Download Incomplete Reports</a> | "
                        + "<i class='fa fa-group'></i> "+data[counter]['number_of_students']+" members</p>";
                    class_list += "</div>";
                    class_list += "</div>";
                    class_list += "</div>";
                    class_list += "</div>";
                }
                if(counter==0)
                {
                    class_list += "<div class='center-align' style='margin-top:200px;margin-bottom:300px;'><h5><i class='fa fa-list-alt'></i> No Class</h5></div>";
                }
                class_list += "</div>";


            $('#class_list_viewer').html(class_list);
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function upload_cover(cla_id)
{
    $('#upload_cover').modal('open');
    $('#cla_id').val(cla_id);
}

$(document).on('submit', '#form-upload-cover', function(e){
        e.preventDefault();

        var fileType = $('#cover').val().split('.').pop().toLowerCase();
        var fileSize = $('#cover')[0].files[0].size;

        if ($.inArray(fileType, ['jpeg', 'jpg', 'png']) == -1) {
            swal("Error", "File must be in this format (jpeg, jpg, png)", "error");
            return false;
        }

        if (fileSize > 20000000) {
            swal("Error", "File size limit is 20MB", "error");
            return false;
        }

        $.ajax({
            url  : main_url+'front-end/faculty/Class_List_Controller/upload_cover',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Uploading Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "New cover photo has been uploaded!", "success")
                $('#upload_cover').modal('close');
                $('#form-upload-cover').trigger("reset");
                Display_Classes();

            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function preview_reports(cla_id, type)
{
    if(cla_id!=null)
    {
        $('#preview_report').modal('open');
        $.ajax({
        url      : main_url+'front-end/faculty/Class_List_Controller/preview_report',
        type     : 'POST',
        data     : {
            'cla_id': cla_id,
            'type': type
        },
        beforeSend : function(xhr){
             $('#preview_report_view').html('<br><br><br><center><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color:#1565c0;"></i><h5>Analyzing Please wait ...</h5><span class="sr-only">Loading...</span></center><br>');
        },
        success : function(data){
                $('#preview_report_view').html(data);
                $('#data_viewer').DataTable();
            },
            error: function(xhr, status, error)
            {
               alert(error);
            }
        });

        $('#download_reports').attr('onclick',"download_reports("+cla_id+", "+type+")");
    }
    else
    {
        swal("Failed", "This class has no students", "error")
    }
}

function download_reports(cla_id, type)
{
    swal({
      title: "Are you sure?",
      text: "A class report will be downloaded",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#1565c0;",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false
    },
    function(){
        swal.close();
        window.open(main_url+"front-end/faculty/Class_List_Controller/download_reports/"+cla_id+"/"+type);
    });
}
