Display_News();
function Display_News()
{
    $.ajax({
        url      : main_url+'back-end/News_Controller/get_news',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#news_viewer').html('<br><br><br><center><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color:#1565c0;"></i><h5>Please wait ...</h5><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){  
        		var post_card = "<div>";
        		var counter = 0;
                    for(counter = 0;counter<data.length;counter++)
                    {
                        var content = data[counter]['n_content'];
      					post_card += "<div class='col s12 m12 l12'>";
                        post_card += "<div class='card-panel white'>";
                        post_card += "<table style='width:250px;' >";
                        post_card += "<tr>";
                        post_card += "<td style='padding:0px;text-align:left;font-weight:bold;'><span>STI NOVALICHES</span></td>";
                        post_card += "</tr>";
                        post_card += "<tr>";
                        post_card += "<td style='padding:0px;text-align:left;color:gray;'><span>"+moment(data[counter]['n_date_posted']).fromNow()+"</span></td>";
                        post_card += "</tr>";
                        post_card += "</table>";
                        post_card += "<br>";
                        post_card += "<pre class='news-content'>";
                        post_card += content.replace("\n", "<br>");;
                        post_card += "</pre>";
                        post_card += "</div>";
                        post_card += "</div>";
                    }
                    post_card += "</div>";
                if(counter==0)
                {
                    post_card += "<div class='center-align' style='margin-top:200px;margin-bottom:300px;'><h5><i class='fa fa-newspaper-o'></i> No News</h5></div>";
                }
                
            $('#news_viewer').html(post_card);
        },
        error: function(xhr, status, error) 
        {
           alert(error);
        }         
    });
}