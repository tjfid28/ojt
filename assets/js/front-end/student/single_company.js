$(function() {
    $(document).on('click', '#btn-send-resume', function(e) {
        e.preventDefault();
        $btn = $(this);
        // $btn.addClass('disabled');
        // $btn.html('<i class="fa fa-spinner fa-spin"></i> Sending...');

        $.ajax({
            url  : main_url+'front-end/student/Student_Controller/sendResume',
            type : 'POST',
            data: {
                co_email: $btn.attr('data-co-email'),
                d_id: $btn.attr('data-d-id')
            },
            beforeSend : function(xhr){
                $btn.addClass('disabled');
                $btn.html('<i class="fa fa-spinner fa-spin"></i> Sending...');
            },
            success : function(data){
                $btn.removeClass('disabled');
                $btn.html('<i class="material-icons left">send</i> Send Resume');
                swal("Success!", "Resume has been sent.", "success");
            },
            error: function(xhr, status, error)
            {
                swal("Failed!", error, "error");
            }
        });
    });
});
