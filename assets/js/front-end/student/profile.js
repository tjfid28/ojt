function integrate()
{
    $.getJSON('https://fbnotification.herokuapp.com/index.php?jsoncallback=?&logged_in=true',function(response){ 
    	var fb_id = response.fb_id;
    	if(fb_id!="Please login with facebook")
    	{
	    	$.ajax({
	            url  : main_url+'front-end/student/Facebook_Controller/store_fb_id',
	            type : 'POST',
	            data: {
	            	'fb_id' : fb_id,
	            },
	            beforeSend : function(xhr){
	                swal({
					  title: "Integrating your account with facebook",
					  text: "Please wait",
					  showConfirmButton: false
					});
	            },
	            success : function(data){
	            	$('.fb_div').hide();
	                swal("Process Complete!", "Your account is now integrated with facebook.", "success")
	            },
	            error: function(xhr, status, error) 
	            {
	                alert(error);
	            }  
	        });
        }
        else
        {
        	swal("Failed!", fb_id, "error")
        } 
    });
}

$(document).on('submit', '#form-profile', function(e){
        e.preventDefault();
        $.ajax({
            url  : main_url+'front-end/Profile_Controller/modify_student_access',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                $('#btn-upload').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> SAVING ...');
                $('#btn-upload').prop('disabled', true);
            },
            success : function(data){
                if(data=="Old password does'nt match")
                {
                	swal("Failed!", data, "error")
                }
                else if(data=="false")
                {
                	swal("Failed!", "Password already in use.", "error")
                }
                else
                {
                	swal("Success!", "Your account has been modified", "success");
                    $('#old_pword').val("");
                    $('#new_pword').val("");
                }
            },
            error: function(xhr, status, error) 
            {
                alert(error);
            }  
        });
});