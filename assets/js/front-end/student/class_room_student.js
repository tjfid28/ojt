Display_Class_Announcements();
function Display_Class_Announcements()
{
    $.ajax({
        url      : main_url+'front-end/Class_Room_Controller/get_class_announcements',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#class_announcement_viewer').html('<br><br><br><center><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color:#1565c0;"></i><h5>Please wait ...</h5><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){
        		var post_card = "<div>";
        		var counter = 0;
                    for(counter = 0;counter<data.length;counter++)
                    {
                        var content = data[counter]['ann_post'];
      					post_card += "<div class='col s12 m12 l12'>";
                        post_card += "<div class='card-panel white'>";
                        post_card += "<table style='width:200px;' >";
                        post_card += "<tr>";
                        post_card += "<td rowspan='2' style='padding:0 10px 0 0;text-align:left;'><img src='"+main_url+"resources/faculty_images/"+data[counter]['fa_image_name']+"' class='circle' style='width:50px;height:50px;'></td>";
                        post_card += "<td style='padding:0px;text-align:left;font-weight:bold;'><span>"+data[counter]['fa_fname']+" "+data[counter]['fa_lname']+" </span></td>";
                        post_card += "</tr>";
                        post_card += "<tr>";
                        post_card += "<td style='padding:0px;text-align:left;color:gray;'><span>"+moment(data[counter]['ann_date_posted']).fromNow()+"</span></td>";
                        post_card += "</tr>";
                        post_card += "</table>";
                        post_card += "<br>";
                        post_card += "<pre class='news-content'>";
                        post_card += content.replace("\n", "<br>");;
                        post_card += "</pre >";
                        post_card += "<div class='left-align icon-posting'>";
                        post_card += data[counter]['number_of_comments']+" <i class='fa fa-comments'></i>";
                        post_card += "</div>";
                        post_card += "<hr>";
                        post_card += "<div class='card-action'>";
                        post_card += "<div class='"+ (data[counter]['ann_attachment'] ? 'left' : 'left-align') +"'>";
                        post_card += "<a href='"+main_url+"/student/"+encode(data[counter]['class_announcement_id'])+"/comments' class='btn-posting white' ><i class='fa fa-comments '></i> <span class='hide-on-small-only'>Comments</span></a>";
                        post_card += "</div>";

                        if (data[counter]['ann_attachment']) {
                            post_card += "<div class='right'>";
                            post_card += "<a href='"+main_url+"resources/announcement_files/"+data[counter]['ann_attachment']+"' class='waves-effect waves-light btn blue darken-3' download><i class='fa fa-download'></i> <span class='hide-on-small-only'>Attachment</span></a>";
                            post_card += "</div>";
                        }

                        post_card += "<div id='ann_attachment_"+data[counter]['class_announcement_id']+"' attachment='"+ (data[counter]['ann_attachment'] || null) +"' class='clearfix'>";
                        post_card += "</div>";
                        post_card += "</div>";
                        post_card += "</div>";
                        post_card += "</div>";
                        post_card += "</div>";
                    }
                    post_card += "</div>";
                if(counter==0)
                {
                    post_card += "<div class='center-align' style='margin-top:200px;margin-bottom:300px;'><h5><i class='fa fa-bullhorn'></i> No Announcement</h5></div>";
                }

            $('#class_announcement_viewer').html(post_card);
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}
