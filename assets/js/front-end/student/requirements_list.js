var searchtimer = null;
$( "#search" ).keyup(function() {
    clearTimeout(searchtimer);
    searchtimer = setTimeout(
    function(){
       Display_Requirements();
    }, 2000);
});

Display_Requirements();
function Display_Requirements()
{
    $.ajax({
        url  : main_url+'front-end/student/Requirement_List_Controller/get_requirements',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
            'search' : $('.search').val(),
        },
        beforeSend : function(xhr){
            var label = $(".search").val() == "" ? "Please wait ..." : "Searching ...";
            $('#requirement_viewer').html('<br><br><br><center><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color:#1565c0;"></i><h5>'+label+'</h5><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

                var documents = "<div class='row'>";
                var counter = 0;
                    for(counter = 0;counter<data.length;counter++)
                    {
                        var ext   = data[counter]['d_file_name'].substr(data[counter]['d_file_name'].lastIndexOf('.') + 1);
                        var color = null;
                        var badgeColor = data[counter]['sr_status']=="Approved" ? "blue":"red";
                        var status     = data[counter]['sr_status']=="Approved"||data[counter]['sr_status']=="Rejected" ? "<span class='new badge "+ badgeColor +"' data-badge-caption='"+data[counter]['sr_status']+"'></span>" : "<span class='new badge' data-badge-caption='Pending'></span>";
                        get_ext        = get_extention(ext);
                        documents += "<div class='card-document col s12 m4 l3' id='card-"+data[counter]['d_id']+"'>";
                        documents += "<div class='right-align toggle-btn'><br>";
                        documents += "<a class='waves-effect waves-light btn red' onclick='remove_requirement("+data[counter]['sr_id']+","+data[counter]['d_id']+")'><i class='fa fa-remove'></i></a>";
                        documents += "</div>";
                        documents += "<a onclick='download_document("+data[counter]['d_id']+")'>";
                        documents += "<div class='card-panel waves-effect hoverable "+ext+"' style='text-align:center;height:230px;width:100%;'>";
                        documents += "<i class='fa fa-file-"+get_ext.ext+"-o' style='font-size:100px;color:"+get_ext.color+"'></i>";
                        documents += "<input type='hidden' id='d_slug_"+data[counter]['d_id']+"' value='"+data[counter]['d_slug']+"' readonly>";
                        documents += "<input type='hidden' id='d_file_name_"+data[counter]['d_id']+"' value='"+data[counter]['d_file_name']+"' readonly>";
                        documents += "<h6 class='black-text truncate'>"+data[counter]['d_file_name']+"</h6>";
                        documents += "<h6 class='black-text truncate'>"+data[counter]['req_name']+"</h6>";
                        documents += "<h6 class='black-text'>"+ status +"</h6>";
                        documents += "</div>";
                        documents += "</a>";
                        documents += "</div>";
                    }
                documents += "</div>";
                if(counter==0)
                {
                    documents += "<div class='center-align' style='margin-top:200px;margin-bottom:300px;'><h5><i class='fa fa-file-text-o'></i> No documents</h5></div>";
                }

            $('#requirement_viewer').html(documents);
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}


$(document).on('submit', '#form-upload-requirement', function(e){
        e.preventDefault();

        var fileType = $('#requirement').val().split('.').pop().toLowerCase();
        var fileSize = $('#requirement')[0].files[0].size;

        if ($.inArray(fileType, ['doc', 'docx', 'xlsx', 'pdf']) == -1) {
            swal("Error", "File must be in this format (doc, docx, xlsx, pdf)", "error");
            return false;
        }

        if (fileSize > 20000000) {
            swal("Error", "File size limit is 20MB", "error");
            return false;
        }

        $.ajax({
            url  : main_url+'front-end/student/Requirement_List_Controller/upload_requirement',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                $('#btn-upload').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> UPLOADING');
                $('#btn-upload').prop('disabled', true);
            },
            success : function(data){

                if(data!=false)
                {
                    $('#btn-upload').html('<i class="fa fa-save"></i> SAVE');
                    $('#btn-upload').prop('disabled', false);
                    Materialize.toast('Requirement has been submitted', 3000)
                    $('#upload_requirement').modal('close');
                    $('#form-upload-requirement').trigger("reset");
                    Display_Requirements();
                }
                else
                {
                    $('#btn-upload').html('<i class="fa fa-save"></i> SAVE');
                    $('#btn-upload').prop('disabled', false);
                    swal("Failed!", "File size must be less than equal to 3MB", "error");
                }

            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

$(document).on('submit', '#form-upload-from-documents', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'front-end/student/Requirement_List_Controller/upload_from_documents',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                $('#btn-document').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> UPLOADING');
                $('#btn-document').prop('disabled', true);
            },
            success : function(data){
                $('#btn-document').html('<i class="fa fa-send"></i> SUBMIT');
                $('#btn-document').prop('disabled', false);
                Materialize.toast('Requirement has been submitted', 3000);
                $('#upload_from_documents').modal('close');
                $('#form-upload-from-documents').trigger("reset");
                Display_Requirements();

            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function remove_requirement(sr_id,d_id)
{
    swal({
      title: "Are you sure?",
      text: "This requirement will be removed in your requirement list",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Delete it",
      cancelButtonText: "No",
      closeOnConfirm: false
    },
    function(){
        swal.close();
        $.ajax({
            url  : main_url+'front-end/student/Requirement_List_Controller/remove_requirement',
            type : 'POST',
            data: {
                'sr_id'     : sr_id,
            },
            beforeSend : function(xhr){
                Materialize.toast('Requirement has been removed', 3000);
            },
            success : function(data){
                $('#card-'+d_id).fadeOut("slow",function(){ $(this).remove(); });
                if($('.card-document').length==1)
                {
                    Display_Requirements();
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

    });
}

function download_document(d_id)
{
    var slug_name = $('#d_slug_'+d_id).val();
    var file_name = $('#d_file_name_'+d_id).val();

    swal({
      title: "Are you sure?",
      text: "This "+file_name+" will be downloaded",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#1565c0;",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false
    },
    function(){
        swal.close();
        window.open(main_url+"front-end/student/My_Document_Controller/download_document/"+slug_name+"/"+file_name);
    });
}
