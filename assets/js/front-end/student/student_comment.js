var data_bind = $('#form-comment').attr('data-bind');
var data_identifier = $('main').attr('data-identifier');

Display_Comments();
setInterval(function(){ reload_comment(); }, 5000);
function Display_Comments()
{
    $.ajax({
        url      : main_url+'front-end/Class_Room_Controller/get_comments',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
            'data_identifier' : data_identifier
        },
        beforeSend : function(xhr){
            $('#comments_viewer').html('<br><br><br><center><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color:#1565c0;"></i><h5>Please wait ...</h5><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){  
                var post_card = "<div>";
                var counter = 0;
                    for(counter = 0;counter<data.length;counter++)
                    {
                        var content = data[counter]['ac_content'];
                        var name    = data[counter]['student_id'] == 0 ? data[counter]['fa_fname']+" "+data[counter]['fa_lname']:data[counter]['st_fname']+" "+data[counter]['st_lname'];
                        var data_binder = null;
                        post_card += "<table class='post-image-name-holder' >";
                        post_card += "<tr>";
                        if(data[counter]['student_id'] == 0)
                        {
                            data_binder = data[counter]['fa_id'];
                            post_card += "<td rowspan='2' class='post-image-holder'><img src='"+main_url+"resources/faculty_images/"+data[counter]['fa_image_name']+"' class='circle' style='width:50px;height:50px;'></td>";
                            post_card += "<td class='name-holder'><span>"+name+" </span></td>";
                        }
                        else
                        {
                            data_binder = data[counter]['st_id'];
                            var image = data[counter]['st_image_name'] != null && data[counter]['st_image_name'] != "" ? data[counter]['st_image_name'] : "no-image.png";
                            post_card += "<td rowspan='2' class='post-image-holder'><img src='"+main_url+"resources/student_images/"+image+"' class='circle' style='width:50px;height:50px;'></td>";
                            if(data_binder==data_bind)
                            {
                                post_card += "<td class='name-holder'><span>"+name+" </span> <a style='color:red;cursor:pointer;' onclick='delete_comment("+data[counter]['ac_id']+")'><i class='fa fa-remove'></i></a></td>";
                            }
                            else
                            {
                                post_card += "<td class='name-holder'><span>"+name+" </span></td>";
                            }
                        }
                        post_card += "</tr>";
                        post_card += "<tr>";
                        post_card += "<td class='time-holder grey-text'><span>"+moment(data[counter]['ac_date_posted']).fromNow()+"</span></td>";
                        post_card += "</tr>";
                        post_card += "</table>";
                        post_card += "<br>";
                        post_card += "<pre class='news-content'>";
                        post_card += content.replace("\n", "<br>");;
                        post_card += "</pre >";
                        post_card += "<hr>";
                    }
                    post_card += "</div>";
                if(counter==0)
                {
                    post_card += "<div class='center-align' style='margin-top:100px;margin-bottom:100px;'><h5><i class='fa fa-comments'></i> No Comment</h5></div>";
                }
            $('#comment_count').html(counter);   
            $('#comments_viewer').html(post_card);
        },
        error: function(xhr, status, error) 
        {
           alert(error);
        }         
    });
}

function reload_comment()
{
    $.ajax({
        url      : main_url+'front-end/Class_Room_Controller/get_comments',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
            'data_identifier' : data_identifier
        },
        beforeSend : function(xhr){
        },
        success : function(data){  
                var post_card = "<div>";
                var counter = 0;
                    for(counter = 0;counter<data.length;counter++)
                    {
                        var content = data[counter]['ac_content'];
                        var name    = data[counter]['student_id'] == 0 ? data[counter]['fa_fname']+" "+data[counter]['fa_lname']:data[counter]['st_fname']+" "+data[counter]['st_lname'];
                        var data_binder = null;
                        post_card += "<table class='post-image-name-holder' >";
                        post_card += "<tr>";
                        if(data[counter]['student_id'] == 0)
                        {
                            data_binder = data[counter]['fa_id'];
                            post_card += "<td rowspan='2' class='post-image-holder'><img src='"+main_url+"resources/faculty_images/"+data[counter]['fa_image_name']+"' class='circle' style='width:50px;height:50px;'></td>";
                            post_card += "<td class='name-holder'><span>"+name+" </span></td>";
                        }
                        else
                        {
                            data_binder = data[counter]['st_id'];
                            var image = data[counter]['st_image_name'] != null && data[counter]['st_image_name'] != "" ? data[counter]['st_image_name'] : "no-image.png";
                            post_card += "<td rowspan='2' class='post-image-holder'><img src='"+main_url+"resources/student_images/"+image+"' class='circle' style='width:50px;height:50px;'></td>";
                            if(data_binder==data_bind)
                            {
                                post_card += "<td class='name-holder'><span>"+name+" </span> <a style='color:red;cursor:pointer;' onclick='delete_comment("+data[counter]['ac_id']+")'><i class='fa fa-remove'></i></a></td>";
                            }
                            else
                            {
                                post_card += "<td class='name-holder'><span>"+name+" </span></td>";
                            }
                        }
                        post_card += "</tr>";
                        post_card += "<tr>";
                        post_card += "<td class='time-holder grey-text'><span>"+moment(data[counter]['ac_date_posted']).fromNow()+"</span></td>";
                        post_card += "</tr>";
                        post_card += "</table>";
                        post_card += "<br>";
                        post_card += "<pre class='news-content'>";
                        post_card += content.replace("\n", "<br>");;
                        post_card += "</pre >";
                        post_card += "<hr>";
                    }
                    post_card += "</div>";
                if(counter==0)
                {
                    post_card += "<div class='center-align' style='margin-top:100px;margin-bottom:100px;'><h5><i class='fa fa-comments'></i> No Comment</h5></div>";
                }
            $('#comment_count').html(counter);  
            $('#comments_viewer').html(post_card);
        },
        error: function(xhr, status, error) 
        {
           alert(error);
        }         
    });
}

$(document).on('submit', '#form-comment', function(e){
        e.preventDefault();
        $.ajax({
            url  : main_url+'front-end/Class_Room_Controller/post_comment/'+data_identifier,
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                $('.btn_comment').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Saving ...');
                $('.btn_comment').prop('disabled', true);
            },
            success : function(data){
                $('.btn_comment').html('<i class="fa fa-comment"></i> COMMENT');
                $('.btn_comment').prop('disabled', false);
                swal("Success!", data, "success");
                $('#form-comment').trigger("reset");
                reload_comment();
            },
            error: function(xhr, status, error) 
            {
                alert(error);
            }  
        });
});

function delete_comment(ac_id)
{
    swal({
      title: "Are you sure?",
      text: "This comment will be removed.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
        $.ajax({
            url  : main_url+'front-end/Class_Room_Controller/delete_comment',
            type : 'POST',
            data: {
                'ac_id' : ac_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Removing Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Deleted!", "Comment has been deleted.", "success");
                reload_comment();
            },
            error: function(xhr, status, error) 
            {
                alert(error);
            }  
        });
    });
}