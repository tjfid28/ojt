// $("#announcement_field").on('focus focusout',function(){
// 	setTimeout(function(){
// 		$('.announcement_btn').hasClass( "scale-out" )==true ? $('.announcement_btn').removeClass('scale-out hidden') : $('.announcement_btn').addClass('scale-out hidden')
// 		},
// 		100);
// });

Display_Class_Announcements();
function Display_Class_Announcements()
{
    $.ajax({
        url      : main_url+'front-end/Class_Room_Controller/get_class_announcements',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#class_announcement_viewer').html('<br><br><br><center><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color:#1565c0;"></i><h5>Please wait ...</h5><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){
        		var post_card = "<div>";
        		var counter = 0;
                    for(counter = 0;counter<data.length;counter++)
                    {
                        var content = data[counter]['ann_post'];
      					post_card += "<div class='col s12 m12 l12'>";
                        post_card += "<div class='card-panel white'>";
                        post_card += "<table style='width:200px;' >";
                        post_card += "<tr>";
                        post_card += "<td rowspan='2' style='padding:0 10px 0 0;text-align:left;'><img class='circle post-image' src='"+main_url+"resources/faculty_images/"+data[counter]['fa_image_name']+"' ></td>";
                        post_card += "<td style='padding:0px;text-align:left;font-weight:bold;'><span>"+data[counter]['fa_fname']+" "+data[counter]['fa_lname']+" </span><a style='color:red;cursor:pointer;' onclick='delete_post("+data[counter]['class_announcement_id']+")'><i class='fa fa-remove'></i></a></td>";
                        post_card += "</tr>";
                        post_card += "<tr>";
                        post_card += "<td style='padding:0px;text-align:left;color:gray;'><span>"+moment(data[counter]['ann_date_posted']).fromNow()+"</span></td>";
                        post_card += "</tr>";
                        post_card += "</table>";
                        post_card += "<br>";
                        post_card += "<pre class='news-content'>";
                        post_card += content.replace("\n", "<br>");;
                        post_card += "</pre>";
                        post_card += "<div class='left-align icon-posting'>";
                        post_card += data[counter]['number_of_comments']+" <i class='fa fa-comments'></i>";
                        post_card += "</div>";
                        post_card += "<hr>";
                        post_card += "<div class='card-action'>";
                        post_card += "<div class='"+ (data[counter]['ann_attachment'] ? 'left' : 'left-align') +"'>";
                        post_card += "<a href='"+main_url+"/faculty/"+encode(data[counter]['class_announcement_id'])+"/comments' class='btn-posting white' ><i class='fa fa-comments '></i> <span class='hide-on-small-only'>Comments</span></a>";
                        post_card += "</div>";

                        if (data[counter]['ann_attachment']) {
                            post_card += "<div class='right'>";
                            post_card += "<a href='"+main_url+"resources/announcement_files/"+data[counter]['ann_attachment']+"' class='waves-effect waves-light btn blue darken-3' download><i class='fa fa-download'></i> <span class='hide-on-small-only'>Attachment</span></a>";
                            post_card += "</div>";
                        }

                        post_card += "<div id='ann_attachment_"+data[counter]['class_announcement_id']+"' attachment='"+ (data[counter]['ann_attachment'] || null) +"' class='clearfix'>";
                        post_card += "</div>";
                        post_card += "</div>";
                        post_card += "</div>";
                        post_card += "</div>";
                        post_card += "</div>";
                    }
                    post_card += "</div>";
                if(counter==0)
                {
                    post_card += "<div class='center-align' style='margin-top:200px;margin-bottom:300px;'><h5><i class='fa fa-bullhorn'></i> No Announcement</h5></div>";
                }

            $('#class_announcement_viewer').html(post_card);
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}


$(document).on('submit', '#form-announcement', function(e){
        e.preventDefault();
        $.ajax({
            url  : main_url+'front-end/Class_Room_Controller/post_announcement',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Posting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "Announcement has been posted!", "success");
                $('#form-announcement').trigger("reset");
                Display_Class_Announcements();

            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function delete_post(cla_ann_id)
{
    swal({
      title: "Are you sure?",
      text: "This post will be removed.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
        $.ajax({
            url  : main_url+'front-end/Class_Room_Controller/delete_announcement',
            type : 'POST',
            data: {
                'cla_ann_id' : cla_ann_id,
                'ann_attachment' : $('#ann_attachment_'+cla_ann_id).attr("attachment")
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Removing Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Deleted!", "Post has been deleted.", "success");
                Display_Class_Announcements();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
    });
}
