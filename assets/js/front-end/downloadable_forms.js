Display_Forms();
function Display_Forms()
{
    $.ajax({
        url  : main_url+'back-end/ojt_management/Downloadable_Form_Controller/get_forms',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#forms_viewer').html('<br><br><br><center><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color:#1565c0;"></i><h5>Please wait ...</h5><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var forms = "<div class='row'>";
                var counter = 0;
                    for(counter = 0;counter<data.length;counter++)
                    {
                        forms += "<a onclick='download_form("+data[counter]['df_id']+")'>";
                        forms += "<div class='col s12 m4 l3'>";
                        forms += "<div class='card-panel waves-effect hoverable ' style='width:100%;text-align:center;height:230px;'>";
                        forms += "<i class='fa fa-file-text-o' style='font-size:120px;'></i>";
                        forms += "<input type='hidden' id='df_slug_"+data[counter]['df_id']+"' value='"+data[counter]['df_slug']+"' readonly>";
                        forms += "<input type='hidden' id='df_file_name_"+data[counter]['df_id']+"' value='"+data[counter]['df_file_name']+"' readonly>";
                        forms += "<h5 class='black-text truncate'>"+data[counter]['req_name']+"</h5>";
                        forms += "</div>";
                        forms += "</div>";
                        forms += "</a>";
                    }
                forms += "</div>";

            $('#forms_viewer').html(forms);
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

$(document).on('submit', '#form-add-form', function(e){
    e.preventDefault();

    var fileType = $('#document').val().split('.').pop().toLowerCase();
    var fileSize = $('#document')[0].files[0].size;

    if ($.inArray(fileType, ['doc', 'docx', 'xlsx', 'pdf']) == -1) {
        swal("Error", "File must be in this format (doc, docx, xlsx, pdf)", "error");
        return false;
    }

    if (fileSize > 20000000) {
        swal("Error", "File size limit is 20MB", "error");
        return false;
    }

    $.ajax({
        url  : main_url+'back-end/ojt_management/Downloadable_Form_Controller/add_form',
        type : 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function(xhr){
            swal({
              title: "",
              text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
              html: true,
              showConfirmButton: false
            });
        },
        success : function(data){
            swal(data, "New form has been added ", "success");
            $('#add_form').modal('close');
            $('#form-add-form').trigger("reset");
            Display_Forms();
        },
        error: function(xhr, status, error)
        {
            alert(error);
        }
    });
});

function download_form(df_id)
{
    var slug_name = $('#df_slug_'+df_id).val();
    var file_name = $('#df_file_name_'+df_id).val();

    swal({
      title: "Are you sure?",
      text: "This "+file_name+" will be downloaded",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#1565c0;",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false
    },
    function(){
        swal.close();
        window.open(main_url+"back-end/ojt_management/Downloadable_Form_Controller/download_form/"+slug_name+"/"+file_name);
    });
}
