Display_Students();

function Display_Students()
{
    $.ajax({
        url      : main_url+'back-end/Class_Student_List_Controller/get_students_in_class',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#student_student_list_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Action</th>";
                table += "<th style='display:none;'></th>";
                table += "<th>Image</th>";
                table += "<th>Student No</th>";
                table += "<th>Username</th>";
                table += "<th>First Name</th>";
                table += "<th>Middle Name</th>";
                table += "<th>Last Name</th>";
                table += "<th>Gender</th>";
                table += "<th>Course</th>";
                table += "<th>Added at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    var image_name = data[counter]['st_image_name']!=null&&data[counter]['st_image_name']!="" ? data[counter]['st_image_name'] : "no-image.png";
                    table += "<tr>";
                    table += "<td><a onclick='remove("+data[counter]['cs_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-remove'></i></a></td>";
                    table += "<td id='st_image_name_"+data[counter]['st_id']+"' style='display:none;'>"+data[counter]['st_image_name']+"</td>";
                    table += "<td id='st_image_"+data[counter]['st_id']+"'><img src='"+main_url+"resources/student_images/"+image_name+"' style='object-fit:cover;width:60px;height:70px;'></td>";
                    table += "<td id='st_number_"+data[counter]['st_id']+"'>"+data[counter]['st_number']+"</td>";
                    table += "<td id='st_uname_"+data[counter]['st_id']+"'>"+data[counter]['st_uname']+"</td>";
                    table += "<td id='st_fname_"+data[counter]['st_id']+"'>"+data[counter]['st_fname']+"</td>";
                    table += "<td id='st_mname_"+data[counter]['st_id']+"'>"+data[counter]['st_mname']+"</td>";
                    table += "<td id='st_lname_"+data[counter]['st_id']+"'>"+data[counter]['st_lname']+"</td>";
                    table += "<td id='st_gender_"+data[counter]['st_id']+"'>"+data[counter]['st_gender']+"</td>";
                    table += "<td id='st_title_"+data[counter]['st_id']+"'>"+data[counter]['cou_title']+"</td>";
                    table += "<td>"+data[counter]['st_created_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No Student</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#class_student_list_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

$(document).on('submit', '#form-add-student', function(e){
        e.preventDefault();
        var st_id_container = $(".student_list").select2('val').toString();
        var cla_id          = $("#cla_id").val();
        $.ajax({
            url  : main_url+'back-end/Class_Student_List_Controller/add_student',
            type : 'POST',
            data: {
            	'st_id_container'  : st_id_container,
              'cla_id' : cla_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal({
                  title: "Success",
                  text: "A student has been added",
                  type: "success",
                  confirmButtonText: "OK",
                },
                function(){
                  window.location.reload();
                });
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function remove(cs_id)
{
    swal({
      title: "Are you sure?",
      text: "This student will be removed in this class",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Class_Student_List_Controller/delete_student',
            type : 'POST',
            data: {
                'cs_id' : cs_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Removing Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal({
                  title: "Success",
                  text: "A student has been removed",
                  type: "success",
                  confirmButtonText: "OK",
                },
                function(){
                  window.location.reload();
                });
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Student has not yet removed", "error");
      }
    });
}
