Display_News();
function Display_News()
{
    $.ajax({
        url  : main_url+'back-end/News_Controller/get_bin',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#news_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Action</th>";
                table += "<th>Content</th>";
                table += "<th>Date Posted</th>";
                table += "<th>Deleted at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td>";
                    table += "<a onclick='restore("+data[counter]['n_id']+")' class='waves-effect waves-light btn light-green'><i class='fa fa-history'></i></a>"
                    table += "&nbsp;&nbsp;<a onclick='remove("+data[counter]['n_id']+")' class='waves-effect waves-light btn red darken-3'><i class='fa fa-trash'></i></a></td>";
                    table += "</td>";
                    table += "<td>"+data[counter]['n_content']+"</td>";
                    table += "<td>"+data[counter]['n_date_posted']+"</td>";
                    table += "<td>"+data[counter]['deleted_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='4'><center>No deleted news</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#news_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function restore(n_id)
{
    swal({
      title: "Are you sure?",
      text: "This news will be restored",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#8bc34a",
      confirmButtonText: "Yes",
      closeOnConfirm: false,
      closeOnCancel: false
    }, function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url  : main_url+'back-end/News_Controller/restore',
                type : 'POST',
                data: {
                    'n_id' : n_id
                },
                beforeSend : function(xhr){
                    swal({
                      title: "",
                      text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Restoring Please wait ...",
                      html: true,
                      showConfirmButton: false
                    });
                },
                success : function(data){
                    swal("Success!", "News has been restored", "success");
                    Display_News();
                },
                error: function(xhr, status, error)
                {
                    alert(error);
                }
            });
        } else {
            swal("Cancelled", "News has not been restored", "error");
        }
    });
}

function remove(n_id)
{
    swal({
      title: "Are you sure?",
      text: "This news will be deleted permanently",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url  : main_url+'back-end/News_Controller/delete_news',
                type : 'POST',
                data: {
                    'n_id' : n_id
                },
                beforeSend : function(xhr){
                    swal({
                      title: "",
                      text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                      html: true,
                      showConfirmButton: false
                    });
                },
                success : function(data){
                    swal("Success!", "News has been deleted permanently", "success");
                    Display_News();
                },
                error: function(xhr, status, error)
                {
                    alert(error);
                }
            });
        } else {
            swal("Cancelled", "News has not been deleted", "error");
        }
    });
}
