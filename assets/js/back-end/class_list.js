Display_Classes();
function Display_Classes()
{
    $.ajax({
        url      :  main_url+'back-end/Class_List_Controller/get_classes',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#class_list_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th style='text-align:center;'>Action</th>";
                table += "<th>Students List</th>";
                table += "<th style='display:none;'></th>";
                table += "<th>Section</th>";
                table += "<th style='display:none;'></th>";
                table += "<th>Subject</th>";
                table += "<th style='display:none;'></th>";
                table += "<th>Course</th>";
                table += "<th style='display:none;'></th>";
                table += "<th>OJT Adviser</th>";
                table += "<th>Term</th>";
                table += "<th>School year</th>";
                table += "<th>Status</th>";
                table += "<th>Created at</th>";
                table += "<th>Updated at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='edit("+data[counter]['cla_id']+")' class='waves-effect waves-light btn blue darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-pencil'></i></a><br><br><a onclick='remove("+data[counter]['cla_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "<td><a href='class_student_list/"+data[counter]['cla_id']+"/"+data[counter]['cou_id']+"/"+data[counter]['sec_name']+"' class='waves-effect waves-light btn blue darken-3'><i class='fa fa-users'></i></a></td>";
                    table += "<td id='sec_id_"+data[counter]['cla_id']+"' style='display:none;'>"+data[counter]['sec_id']+"</td>";
                    table += "<td id='sec_name_"+data[counter]['cla_id']+"'>"+data[counter]['sec_name']+"</td>";
                    table += "<td id='sub_id_"+data[counter]['cla_id']+"' style='display:none;'>"+data[counter]['sub_id']+"</td>";
                    table += "<td id='sub_name_"+data[counter]['cla_id']+"' style='text-align:center;'>"+data[counter]['sub_name']+"</td>";
                    table += "<td id='cou_id_"+data[counter]['cla_id']+"' style='display:none;'>"+data[counter]['cou_id']+"</td>";
                    table += "<td id='cou_title_"+data[counter]['cla_id']+"' style='text-align:center;'>"+data[counter]['cou_title']+"</td>";
                    table += "<td id='fa_id_"+data[counter]['cla_id']+"' style='display:none;'>"+data[counter]['fa_id']+"</td>";
                    table += "<td id='adviser_"+data[counter]['cla_id']+"' style='text-align:center;'>"+data[counter]['adviser']+"</td>";
                    table += "<td id='term_"+data[counter]['cla_id']+"'>"+data[counter]['cla_term']+"</td>";
                    table += "<td id='year_"+data[counter]['cla_id']+"'>"+data[counter]['cla_year']+"</td>";
                    table += "<td id='status_"+data[counter]['cla_id']+"'>"+data[counter]['cla_status']+"</td>";
                    table += "<td>"+data[counter]['cla_created_at']+"</td>";
                    table += "<td>"+data[counter]['cla_updated_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No Class</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#class_list_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

$(document).on('submit', '#form-create-class', function(e){
        e.preventDefault();
        $.ajax({
            url  : main_url+'back-end/Class_List_Controller/create_class',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                console.log(this);
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal(data, "New class has been created ", "success");
                $('#create_class').modal('close');
                $('#form-create-class').trigger("reset");
                swal({
                  title: "Success",
                  text: "New class has been added",
                  type: "success",
                  confirmButtonColor: "#0d47a1",
                  confirmButtonText: "OK",
                }, Display_Classes()
                );
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

$(document).on('submit', '#form-edit-class', function(e){
        e.preventDefault();
        $.ajax({
            url  : main_url+'back-end/Class_List_Controller/edit_class',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal(data, "Class has been modified ", "success");
                $('#edit_class').modal('close');
                $('#form-edit-class').trigger("reset");
                Display_Classes();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function edit(cla_id)
{
    $('#edit_class').modal('open');
    // $('#edit_sec_id').val($('#sec_id_'+cla_id).text());
    $('#edit_sec_name').html($('#sec_name_'+cla_id).text());
    $('#edit_sec_id').material_select();
    $('#edit_sub_id').val($('#sub_id_'+cla_id).text());
    $('#edit_sub_id').material_select();
    $('#edit_cou_id').val($('#cou_id_'+cla_id).text());
    $('#edit_cou_id').material_select();
    $('#edit_fa_id').val($('#fa_id_'+cla_id).text());
    $('#edit_term').val($('#term_'+cla_id).text());
    var year = $('#year_'+cla_id).text().split("-");
    $('#edit_start_year').val(year[0]);
    $('#edit_end_year').val(year[1]);
    $('#edit_term').material_select();
    $('#edit_fa_id').material_select();
    $('#edit_status').val($('#status_'+cla_id).text());
    $('#edit_status').material_select();

    $('#edit_id').val(cla_id);
}

function remove(cla_id)
{
    swal({
      title: "Are you sure?",
      text: "This class will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Class_List_Controller/softDelete',
            type : 'POST',
            data: {
                'cla_id' : cla_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal({
                  title: "Success",
                  text: "Class has been deleted and was moved to bin",
                  type: "success",
                  confirmButtonColor: "#0d47a1",
                  confirmButtonText: "OK",
                }, Display_Classes());
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Class has not been deleted", "error");
      }
    });
}
