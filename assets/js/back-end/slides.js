Display_Slides();
function Display_Slides()
{
    $.ajax({
        url  : main_url+'back-end/Slide_Controller/get_slides',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#slide_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th style='text-align:center;'>Action</th>";
                table += "<th>Image Name</th>";
                table += "<th>Caption</th>";
                table += "<th>Description</th>";
                table += "<th>Created at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='remove("+data[counter]['s_id']+")' class='waves-effect waves-light btn red darken-3'><i class='fa fa-trash'></i></a></td>";
                    table += "<td id='s_slug_image_"+data[counter]['s_id']+"'><img src='"+main_url+"resources/slide-images/"+data[counter]['s_slug_name']+"' style='object-fit:cover;width:120px;height:70px;'></td>";
                    table += "<td>"+data[counter]['s_caption']+"</td>";
                    table += "<td>"+data[counter]['s_description']+"</td>";
                    table += "<td>"+data[counter]['s_created_at']+"</td>";
                    table += "<input type='text' id='s_slug_name_"+data[counter]['s_id']+"' style='display:none;' value='"+data[counter]['s_slug_name']+"' readonly>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No Slide</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#slide_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

$(document).on('submit', '#form-slide', function(e){
        e.preventDefault();

        var fileType = $('#slide-image').val().split('.').pop().toLowerCase();
        var fileSize = $('#slide-image')[0].files[0].size;

        if ($.inArray(fileType, ['jpeg', 'jpg', 'png']) == -1) {
            swal("Error", "File must be in this format (jpeg, jpg, png)", "error");
            return false;
        }

        if (fileSize > 20000000) {
            swal("Error", "File size limit is 20MB", "error");
            return false;
        }

        $.ajax({
            url  : main_url+'back-end/Slide_Controller/create_slide',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal(data, "New slide has been created ", "success");
                $('#create_slide').modal('close');
                $('#form-slide').trigger("reset");
                Display_Slides();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function remove(s_id)
{
    swal({
      title: "Are you sure?",
      text: "This slide will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Slide_Controller/softDelete',
            type : 'POST',
            data: {
                's_id'     : s_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A slide has been deleted and was moved to bin", "success");
                Display_Slides();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Slide has not been deleted", "error");
      }
    });
}
