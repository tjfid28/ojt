Display_News();
function Display_News()
{
    $.ajax({
        url      : main_url+'back-end/News_Controller/get_news',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#news_viewer').html('<br><br><br><center><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color:#1565c0;"></i><h5>Please wait ...</h5><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){
        		var post_card = "<div>";
        		var counter = 0;
                    for(counter = 0;counter<data.length;counter++)
                    {
                        var content = data[counter]['n_content'];
      					post_card += "<div class='col s12 m12 l12'>";
                        post_card += "<div class='card-panel white'>";
                        post_card += "<table style='width:350px;' >";
                        post_card += "<tr>";
                        post_card += "<td style='padding:0px;text-align:left;font-weight:bold;'><span> STI COLLEGE NOVALICHES </span><a style='color:red;cursor:pointer;' onclick='delete_news("+data[counter]['n_id']+")'><i class='fa fa-remove'></i></a></td>";
                        post_card += "</tr>";
                        post_card += "<tr>";
                        post_card += "<td style='padding:0px;text-align:left;color:gray;'><span>"+moment(data[counter]['n_date_posted']).fromNow()+"</span></td>";
                        post_card += "</tr>";
                        post_card += "</table>";
                        post_card += "<br>";
                        post_card += "<pre class='news-content'>";
                        post_card += content.replace("\n", "<br>");;
                        post_card += "</pre>";
                        post_card += "</div>";
                        post_card += "</div>";
                    }
                    post_card += "</div>";
                if(counter==0)
                {
                    post_card += "<div class='center-align' style='margin-top:200px;margin-bottom:300px;'><h5><i class='fa fa-newspaper-o'></i> No News</h5></div>";
                }

            $('#news_viewer').html(post_card);
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

$(document).on('submit', '#form-news', function(e){
        e.preventDefault();
        $.ajax({
            url  : main_url+'back-end/News_Controller/post_news',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Posting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "News has been posted!", "success");
                $('#form-news').trigger("reset");
                Display_News();

            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function delete_news(n_id)
{
    swal({
      title: "Are you sure?",
      text: "This news will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url  : main_url+'back-end/News_Controller/softDelete',
                type : 'POST',
                data: {
                    'n_id' : n_id
                },
                beforeSend : function(xhr){
                    swal({
                      title: "",
                      text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                      html: true,
                      showConfirmButton: false
                    });
                },
                success : function(data){
                    swal("Success!", "News has been deleted and was moved to bin", "success");
                    Display_News();
                },
                error: function(xhr, status, error)
                {
                    alert(error);
                }
            });
        } else {
            swal("Cancelled", "News has not been deleted", "error");
        }
    });
}
