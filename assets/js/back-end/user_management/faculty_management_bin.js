Display_Faculty();
function Display_Faculty()
{
    $.ajax({
        url      : main_url+'back-end/user_management/Faculty_Management_Controller/get_bin',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#faculty_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){
            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Action</th>";
                table += "<th>Image</th>";
                table += "<th>Faculty No</th>";
                table += "<th>Username</th>";
                table += "<th>Password</th>";
                table += "<th>First Name</th>";
                table += "<th>Middle Name</th>";
                table += "<th>Last Name</th>";
                table += "<th>Gender</th>";
                table += "<th>Department</th>";
                table += "<th>Deleted at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='restore("+data[counter]['fa_id']+")' class='waves-effect waves-light btn light-green'><i class='fa fa-history'></i></a>&nbsp;&nbsp;<a onclick='remove("+data[counter]['fa_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "<td id='fa_image_name_"+data[counter]['fa_id']+"' style='display:none;'>"+data[counter]['fa_image_name']+"</td>";
                    table += "<td id='fa_image_"+data[counter]['fa_id']+"'><img src='"+main_url+"resources/faculty_images/"+data[counter]['fa_image_name']+"' style='object-fit:cover;width:60px;height:70px;border-radius:5px;'></td>";
                    table += "<td>"+data[counter]['fa_number']+"</td>";
                    table += "<td>"+data[counter]['fa_uname']+"</td>";
                    table += "<td>"+data[counter]['fa_pword']+"</td>";
                    table += "<td>"+data[counter]['fa_fname']+"</td>";
                    table += "<td>"+data[counter]['fa_mname']+"</td>";
                    table += "<td>"+data[counter]['fa_lname']+"</td>";
                    table += "<td>"+data[counter]['fa_gender']+"</td>";
                    table += "<td>"+data[counter]['dept_name']+"</td>";
                    table += "<td>"+data[counter]['f_deleted_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No Faculty member</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#faculty_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function restore(fa_id)
{
    swal({
      title: "Are you sure?",
      text: "This faculty member will be restored",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#8bc34a",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/user_management/Faculty_Management_Controller/restore',
            type : 'POST',
            data: {
                'fa_id' : fa_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Restoring Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A faculty member has been restored ", "success");
                Display_Faculty();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Faculty member has not been restored", "error");
      }
    });
}

function remove(fa_id)
{
    var fa_img = $('#fa_image_name_'+fa_id).text()
    swal({
      title: "Are you sure?",
      text: "This faculty member will be deleted permanently",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/user_management/Faculty_Management_Controller/delete_faculty',
            type : 'POST',
            data: {
                'fa_id' : fa_id,
                'fa_img': fa_img
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A faculty member has been deleted permanently ", "success");
                Display_Faculty();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Faculty member has not been deleted", "error");
      }
    });
}
