Display_Students();

function Display_Students()
{
    $.ajax({
        url      : main_url+'back-end/user_management/Student_Management_Controller/get_bin',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#student_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Action</th>";
                table += "<th>Image</th>";
                table += "<th>Student No</th>";
                table += "<th>Email</th>";
                table += "<th>Mobile No</th>";
                table += "<th>Username</th>";
                table += "<th>Password</th>";
                table += "<th>First Name</th>";
                table += "<th>Middle Name</th>";
                table += "<th>Last Name</th>";
                table += "<th>Gender</th>";
                table += "<th>School year</th>";
                table += "<th>Course</th>";
                table += "<th>Deleted at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    var image_name = data[counter]['st_image_name']!=null&&data[counter]['st_image_name']!="" ? data[counter]['st_image_name'] : "no-image.png";
                    table += "<tr>";
                    table += "<td><a onclick='restore("+data[counter]['st_id']+")' class='waves-effect waves-light btn light-green'><i class='fa fa-history'></i></a>&nbsp;&nbsp;<a onclick='remove("+data[counter]['st_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "<td id='st_image_name_"+data[counter]['st_id']+"' style='display:none;'>"+data[counter]['st_image_name']+"</td>";
                    table += "<td><img src='"+main_url+"resources/student_images/"+image_name+"' style='object-fit:cover;width:60px;height:70px;'></td>";
                    table += "<td>"+data[counter]['st_number']+"</td>";
                    table += "<td>"+data[counter]['st_email']+"</td>";
                    table += "<td>"+data[counter]['st_mobile_number']+"</td>";
                    table += "<td>"+data[counter]['st_uname']+"</td>";
                    table += "<td>"+data[counter]['st_pword']+"</td>";
                    table += "<td>"+data[counter]['st_fname']+"</td>";
                    table += "<td>"+data[counter]['st_mname']+"</td>";
                    table += "<td>"+data[counter]['st_lname']+"</td>";
                    table += "<td>"+data[counter]['st_gender']+"</td>";
                    table += "<td>"+data[counter]['st_school_year']+"</td>";
                    table += "<td>"+data[counter]['cou_title']+"</td>";
                    table += "<td>"+data[counter]['s_deleted_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='14'><center>No Student</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#student_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function restore(st_id)
{
    swal({
      title: "Are you sure?",
      text: "This student will be restored",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#8bc34a",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/user_management/Student_Management_Controller/restore',
            type : 'POST',
            data: {
                'st_id' : st_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Restoring Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A student has been restored", "success");
                Display_Students();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Student has not been restored", "error");
      }
    });
}

function remove(st_id)
{
    var st_img = $('#st_image_name_'+st_id).text();
    swal({
      title: "Are you sure?",
      text: "This student will be deleted permanently",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/user_management/Student_Management_Controller/delete_student',
            type : 'POST',
            data: {
                'st_id' : st_id,
                'st_img': st_img
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A student has been deleted permanently ", "success");
                Display_Students();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Student has not been deleted", "error");
      }
    });
}
