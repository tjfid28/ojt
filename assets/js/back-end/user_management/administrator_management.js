Display_Administrators();
function Display_Administrators()
{
	$.ajax({
        url      : main_url+'back-end/user_management/Administrator_Management_Controller/get_administrators',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#admin_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th style='text-align:center;'>Action</th>";
                table += "<th>Username</th>";
                table += "<th>Password</th>";
                table += "<th>First Name</th>";
                table += "<th>Middle Name</th>";
                table += "<th>Last Name</th>";
                table += "<th>Created at</th>";
                table += "<th>Updated at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td>"

                    if ($('#ad_id').val() == 0) {
                        if (data[counter]['ad_id'] == 0) {
                            table += "<a onclick='edit("+data[counter]['ad_id']+")' class='waves-effect waves-light btn blue darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-pencil'></i></a>";
                        } else {
                            table += "<a onclick='edit("+data[counter]['ad_id']+")' class='waves-effect waves-light btn blue darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-pencil'></i></a><br><br><a onclick='remove("+data[counter]['ad_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a>";
                        }
                    } else if ($('#ad_id').val() == data[counter]['ad_id']) {
                        table += "<a onclick='edit("+data[counter]['ad_id']+")' class='waves-effect waves-light btn blue darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-pencil'></i></a>";
                    }

                    table += "</td>";
                    table += "<td id='ad_uname_"+data[counter]['ad_id']+"'>"+data[counter]['ad_uname']+"</td>";
                    table += "<td id='ad_pword_"+data[counter]['ad_id']+"'>"+data[counter]['ad_pword']+"</td>";
                    table += "<td id='ad_fname_"+data[counter]['ad_id']+"'>"+data[counter]['ad_fname']+"</td>";
                    table += "<td id='ad_mname_"+data[counter]['ad_id']+"'>"+data[counter]['ad_mname']+"</td>";
                    table += "<td id='ad_lname_"+data[counter]['ad_id']+"'>"+data[counter]['ad_lname']+"</td>";
                    table += "<td>"+data[counter]['ad_created_at']+"</td>";
                    table += "<td>"+data[counter]['ad_updated_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No Admin</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#admin_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
  	});
}

$(document).on('submit', '#form-add-administrator', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/user_management/Administrator_Management_Controller/add_administrators',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data!='false')
                {
                    swal("Success!", "New administrator has been added ", "success");
                    $('#add_administrator').modal('close');
                    $('#form-add-administrator').trigger("reset");
                    Display_Administrators();
                }
                else
                {
                    swal("Failed!", "Username already exist.", "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

$(document).on('submit', '#form-edit-administrator', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/user_management/Administrator_Management_Controller/edit_administrators',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data!='false')
                {
                    swal("Success!", "An administrator has been modified ", "success");
                    $('#edit_administrator').modal('close');
                    Display_Administrators();
                }
                else
                {
                    swal("Failed!", "Username already exist.", "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function edit(ad_id)
{
    $('#edit_administrator').modal('open');
    $('#edit_uname').val($('#ad_uname_'+ad_id).text());
    $('#edit_pword').val($('#ad_pword_'+ad_id).text());
    $('#edit_first_name').val($('#ad_fname_'+ad_id).text());
    $('#edit_middle_name').val($('#ad_mname_'+ad_id).text());
    $('#edit_last_name').val($('#ad_lname_'+ad_id).text());
    $('#edit_id').val(ad_id);
}

function remove(ad_id)
{
    swal({
      title: "Are you sure?",
      text: "This administrator will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/user_management/Administrator_Management_Controller/softDelete',
            type : 'POST',
            data: {
                'ad_id' : ad_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "An administrator has been deleted and was moved to bin", "success");
                Display_Administrators();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Administrator has not been deleted", "error");
      }
    });
}
