Display_Students();

function Display_Students()
{
    $.ajax({
        url      : main_url+'back-end/user_management/Student_Management_Controller/get_students',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#student_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th style='display:none;'></th>";
                table += "<th style='text-align:center;'>Action</th>";
                table += "<th>Image</th>";
                table += "<th>Student No</th>";
                table += "<th>Email</th>";
                table += "<th>Mobile No</th>";
                table += "<th>Username</th>";
                table += "<th>Password</th>";
                table += "<th>First Name</th>";
                table += "<th>Middle Name</th>";
                table += "<th>Last Name</th>";
                table += "<th>Gender</th>";
                table += "<th>School year</th>";
                table += "<th style='display:none;'></th>";
                table += "<th>Course</th>";
                table += "<th>Created at</th>";
                table += "<th>Updated at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    var image_name = data[counter]['st_image_name']!=null&&data[counter]['st_image_name']!="" ? data[counter]['st_image_name'] : "no-image.png";
                    table += "<tr>";
                    table += "<td><a onclick='edit("+data[counter]['st_id']+")' class='waves-effect waves-light btn blue darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-pencil'></i></a><br><br><a onclick='remove("+data[counter]['st_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "<td id='st_image_name_"+data[counter]['st_id']+"' style='display:none;'>"+data[counter]['st_image_name']+"</td>";
                    table += "<td id='st_image_"+data[counter]['st_id']+"'><img src='"+main_url+"resources/student_images/"+image_name+"' style='object-fit:cover;width:60px;height:70px;'></td>";
                    table += "<td id='st_number_"+data[counter]['st_id']+"'>"+data[counter]['st_number']+"</td>";
                    table += "<td id='st_email_"+data[counter]['st_id']+"'>"+data[counter]['st_email']+"</td>";
                    table += "<td id='st_mobile_no_"+data[counter]['st_id']+"'>"+data[counter]['st_mobile_number']+"</td>";
                    table += "<td id='st_uname_"+data[counter]['st_id']+"'>"+data[counter]['st_uname']+"</td>";
                    table += "<td id='st_pword_"+data[counter]['st_id']+"'>"+data[counter]['st_pword']+"</td>";
                    table += "<td id='st_fname_"+data[counter]['st_id']+"'>"+data[counter]['st_fname']+"</td>";
                    table += "<td id='st_mname_"+data[counter]['st_id']+"'>"+data[counter]['st_mname']+"</td>";
                    table += "<td id='st_lname_"+data[counter]['st_id']+"'>"+data[counter]['st_lname']+"</td>";
                    table += "<td id='st_gender_"+data[counter]['st_id']+"'>"+data[counter]['st_gender']+"</td>";
                    table += "<td id='st_school_year_"+data[counter]['st_id']+"'>"+data[counter]['st_school_year']+"</td>";
                    table += "<td id='st_cou_id_"+data[counter]['st_id']+"' style='display:none;'>"+data[counter]['cou_id']+"</td>";
                    table += "<td id='st_title_"+data[counter]['st_id']+"'>"+data[counter]['cou_title']+"</td>";
                    table += "<td>"+data[counter]['st_created_at']+"</td>";
                    table += "<td>"+data[counter]['st_updated_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='12'><center>No Student</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#student_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

$(document).on('submit', '#form-add-student', function(e){
        e.preventDefault();

        var fileType = $('#st_img').val().split('.').pop().toLowerCase();
        var fileSize = $('#st_img')[0].files[0].size;

        if ($.inArray(fileType, ['jpeg', 'jpg', 'png']) == -1) {
            swal("Error", "File must be in this format (jpeg, jpg, png)", "error");
            return false;
        }

        if (fileSize > 20000000) {
            swal("Error", "File size limit is 20MB", "error");
            return false;
        }

        $.ajax({
            url  : main_url+'back-end/user_management/Student_Management_Controller/add_student',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data!="false")
                {
                    swal("Success!", "New student has been added ", "success");
                    $('#add_student').modal('close');
                    $('#form-add-student').trigger("reset");
                    Display_Students();
                }
                else
                {
                    swal("Failed!", "Password or Student no. already exist.", "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

$(document).on('submit', '#form-excel', function(e){
        e.preventDefault();
        $.ajax({
            url  : main_url+'back-end/user_management/Student_Management_Controller/import_student',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Importing Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                    if(data=="Error")
                    {
                        swal("Error", "Import has been aborted", "error");
                    }
                    else
                    {
                        swal("Success!", data, "success");
                        $('#excel').modal('close');
                        $('#form-excel').trigger("reset");
                        Display_Students();
                    }
            },
            error: function(xhr, status, error)
            {
                swal("Error", "Importing has been cancelled", "error");
            }
        });
});

$(document).on('submit', '#form-edit-student', function(e){
        e.preventDefault();

        if ($('#edit_st_img')[0].files[0]) {
            var fileType = $('#edit_st_img').val().split('.').pop().toLowerCase();
            var fileSize = $('#edit_st_img')[0].files[0].size;

            if ($.inArray(fileType, ['jpeg', 'jpg', 'png']) == -1) {
                swal("Error", "File must be in this format (jpeg, jpg, png)", "error");
                return false;
            }

            if (fileSize > 20000000) {
                swal("Error", "File size limit is 20MB", "error");
                return false;
            }
        }

        $.ajax({
            url  : main_url+'back-end/user_management/Student_Management_Controller/edit_student',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data!="false")
                {
                    swal("Success!", "A student has been modified ", "success");
                    $('#form-edit-student').trigger("reset");
                    $('#edit_student').modal('close');
                    Display_Students();
                }
                else
                {
                    swal("Failed!", "Password or Student no. already exist.", "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});


function edit(st_id)
{
    $('#edit_student').modal('open');
    $('#edit_st_number').val($('#st_number_'+st_id).text());
    $('#edit_email').val($('#st_email_'+st_id).text());
    $('#edit_mobile_no').val($('#st_mobile_no_'+st_id).text());
    $('#edit_uname').val($('#st_uname_'+st_id).text());
    $('#edit_pword').val($('#st_pword_'+st_id).text());
    $('#edit_image_name').val($('#st_image_name_'+st_id).text());
    $('#edit_first_name').val($('#st_fname_'+st_id).text());
    $('#edit_middle_name').val($('#st_mname_'+st_id).text());
    $('#edit_last_name').val($('#st_lname_'+st_id).text());
    $('#edit_gender').val($('#st_gender_'+st_id).text());
    $('#edit_gender').material_select();
    var year = $('#st_school_year_'+st_id).text().split("-");
    $('#edit_start_year').val(year[0]);
    $('#edit_end_year').val(year[1]);
    $('#edit_cou_id').val($('#st_cou_id_'+st_id).text());
    $('#edit_cou_id').material_select();

    $('#edit_id').val(st_id);
}

function remove(st_id)
{
    swal({
      title: "Are you sure?",
      text: "This student will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/user_management/Student_Management_Controller/softDelete',
            type : 'POST',
            data: {
                'st_id' : st_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A student has been deleted ", "success");
                Display_Students();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Student has not been deleted", "error");
      }
    });
}

$('#download_template').click(function(){
    swal({
      title: "Are you sure?",
      text: "Template will be downloaded",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#1565c0",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {
        swal.close();
        window.open(main_url+"back-end/user_management/Student_Management_Controller/download_template");
      } else {
        swal("Cancelled", "Downloading template has been cancelled", "error");
      }
    });
});
