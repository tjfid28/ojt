Display_Faculty();
function Display_Faculty()
{
    $.ajax({
        url      : main_url+'back-end/user_management/Faculty_Management_Controller/get_faculty',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#faculty_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th style='text-align:center;'>Action</th>";
                table += "<th style='display:none;'></th>";
                table += "<th>Image</th>";
                table += "<th>Faculty No</th>";
                table += "<th>Username</th>";
                table += "<th>Password</th>";
                table += "<th>First Name</th>";
                table += "<th>Middle Name</th>";
                table += "<th>Last Name</th>";
                table += "<th>Gender</th>";
                table += "<th style='display:none;'></th>";
                table += "<th>Department</th>";
                table += "<th>Created at</th>";
                table += "<th>Updated at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='edit("+data[counter]['fa_id']+")' class='waves-effect waves-light btn blue darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-pencil'></i></a><br><br><a onclick='remove("+data[counter]['fa_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "<td id='fa_image_name_"+data[counter]['fa_id']+"' style='display:none;'>"+data[counter]['fa_image_name']+"</td>";
                    table += "<td id='fa_image_"+data[counter]['fa_id']+"'><img src='"+main_url+"resources/faculty_images/"+data[counter]['fa_image_name']+"' style='object-fit:cover;width:60px;height:70px;border-radius:5px;'></td>";
                    table += "<td id='fa_number_"+data[counter]['fa_id']+"'>"+data[counter]['fa_number']+"</td>";
                    table += "<td id='fa_uname_"+data[counter]['fa_id']+"'>"+data[counter]['fa_uname']+"</td>";
                    table += "<td id='fa_pword_"+data[counter]['fa_id']+"'>"+data[counter]['fa_pword']+"</td>";
                    table += "<td id='fa_fname_"+data[counter]['fa_id']+"'>"+data[counter]['fa_fname']+"</td>";
                    table += "<td id='fa_mname_"+data[counter]['fa_id']+"'>"+data[counter]['fa_mname']+"</td>";
                    table += "<td id='fa_lname_"+data[counter]['fa_id']+"'>"+data[counter]['fa_lname']+"</td>";
                    table += "<td id='fa_gender_"+data[counter]['fa_id']+"'>"+data[counter]['fa_gender']+"</td>";
                    table += "<td id='fa_dept_id_"+data[counter]['fa_id']+"' style='display:none;'>"+data[counter]['dept_id']+"</td>";
                    table += "<td id='fa_title_"+data[counter]['fa_id']+"'>"+data[counter]['dept_name']+"</td>";
                    table += "<td>"+data[counter]['fa_created_at']+"</td>";
                    table += "<td>"+data[counter]['fa_updated_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='12'><center>No Faculty member</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#faculty_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

$(document).on('submit', '#form-add-faculty', function(e){
        e.preventDefault();

        var fileType = $('#fa_img').val().split('.').pop().toLowerCase();
        var fileSize = $('#fa_img')[0].files[0].size;

        if ($.inArray(fileType, ['jpeg', 'jpg', 'png']) == -1) {
            swal("Error", "File must be in this format (jpeg, jpg, png)", "error");
            return false;
        }

        if (fileSize > 20000000) {
            swal("Error", "File size limit is 20MB", "error");
            return false;
        }

        $.ajax({
            url  : main_url+'back-end/user_management/Faculty_Management_Controller/add_faculty',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data!="false")
                {
                    swal("Success!", "New faculty has been added ", "success");
                    $('#add_faculty').modal('close');
                    $('#form-add-faculty').trigger("reset");
                    Display_Faculty();
                }
                else
                {
                    swal("Failed!", "Username or Faculty no. already exist.", "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

$(document).on('submit', '#form-edit-faculty', function(e){
        e.preventDefault();

        if ($('#edit_fa_img')[0].files[0]) {
            var fileType = $('#edit_fa_img').val().split('.').pop().toLowerCase();
            var fileSize = $('#edit_fa_img')[0].files[0].size;

            if ($.inArray(fileType, ['jpeg', 'jpg', 'png']) == -1) {
                swal("Error", "File must be in this format (jpeg, jpg, png)", "error");
                return false;
            }

            if (fileSize > 20000000) {
                swal("Error", "File size limit is 20MB", "error");
                return false;
            }
        }

        $.ajax({
            url  : main_url+'back-end/user_management/Faculty_Management_Controller/edit_faculty',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data!="false")
                {
                    swal("Success!", "A faculty member has been modified ", "success");
                    $('#form-edit-faculty').trigger("reset");
                    $('#edit_faculty').modal('close');
                    Display_Faculty();
                }
                else
                {
                    swal("Failed!", "Password or Faculty no. already exist.", "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});


function edit(fa_id)
{
    $('#edit_faculty').modal('open');
    $('#edit_fa_number').val($('#fa_number_'+fa_id).text());
    $('#edit_uname').val($('#fa_uname_'+fa_id).text());
    $('#edit_pword').val($('#fa_pword_'+fa_id).text());
    $('#edit_image_name').val($('#fa_image_name_'+fa_id).text());
    $('#edit_first_name').val($('#fa_fname_'+fa_id).text());
    $('#edit_middle_name').val($('#fa_mname_'+fa_id).text());
    $('#edit_last_name').val($('#fa_lname_'+fa_id).text());
    $('#edit_gender').val($('#fa_gender_'+fa_id).text());
    $('#edit_gender').material_select();
    $('#edit_dept_id').val($('#fa_dept_id_'+fa_id).text());
    $('#edit_dept_id').material_select();

    $('#edit_id').val(fa_id);
}

function remove(fa_id)
{
    swal({
      title: "Are you sure?",
      text: "This faculty member will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/user_management/Faculty_Management_Controller/softDelete',
            type : 'POST',
            data: {
                'fa_id' : fa_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A faculty member has been deleted and was moved to bin ", "success");
                Display_Faculty();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Faculty member has not been deleted", "error");
      }
    });
}
