Display_Sub_Administrators();

function Display_Sub_Administrators()
{
    $.ajax({
        url      : main_url+'back-end/user_management/Sub_Administrator_Management_Controller/get_sub_administrators',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#sub_admin_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th style='text-align:center;'>Action</th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th style='display:none;'></th>";
                table += "<th>Username</th>";
                table += "<th>Password</th>";
                table += "<th>First Name</th>";
                table += "<th>Middle Name</th>";
                table += "<th>Last Name</th>";
                table += "<th>Created at</th>";
                table += "<th>Updated at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    //
                    table += "<td id='dashboard_"+data[counter]['s_ad_id']+"' style='display:none;'>"+data[counter]['s_dashboard']+"</td>";
                    table += "<td id='news_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_news']+"</td>";
                    table += "<td id='class_list_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_class_list']+"</td>";
                    table += "<td id='slides_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_slides']+"</td>";
                    table += "<td id='company_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_company']+"</td>";
                    table += "<td id='sections_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_sections']+"</td>";
                    table += "<td id='courses_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_courses']+"</td>";
                    table += "<td id='programs_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_programs']+"</td>";
                    table += "<td id='departments_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_departments']+"</td>";
                    table += "<td id='faculty_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_faculty']+"</td>";
                    table += "<td id='student_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_student']+"</td>";
                    table += "<td id='uploaded_documents_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_uploaded_documents']+"</td>";
                    table += "<td id='ojt_requirements_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_ojt_requirement']+"</td>";
                    table += "<td id='downloadable_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_downloadable_forms']+"</td>";
                    table += "<td id='ojt_policy_"+data[counter]['s_ad_id']+"' style='display:none;' >"+data[counter]['s_ojt_policy']+"</td>";
                    //
                    table += "<td><a onclick='edit("+data[counter]['s_ad_id']+")' class='waves-effect waves-light btn blue darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-pencil'></i></a><br><br><a onclick='remove("+data[counter]['s_ad_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "<td id='s_ad_uname_"+data[counter]['s_ad_id']+"'>"+data[counter]['s_ad_uname']+"</td>";
                    table += "<td id='s_ad_pword_"+data[counter]['s_ad_id']+"'>"+data[counter]['s_ad_pword']+"</td>";
                    table += "<td id='s_ad_fname_"+data[counter]['s_ad_id']+"'>"+data[counter]['s_ad_fname']+"</td>";
                    table += "<td id='s_ad_mname_"+data[counter]['s_ad_id']+"'>"+data[counter]['s_ad_mname']+"</td>";
                    table += "<td id='s_ad_lname_"+data[counter]['s_ad_id']+"'>"+data[counter]['s_ad_lname']+"</td>";
                    table += "<td>"+data[counter]['s_ad_created_at']+"</td>";
                    table += "<td>"+data[counter]['s_ad_updated_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No Sub Admin</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#sub_admin_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

$(document).on('submit', '#form-add-sub-administrator', function(e){
        e.preventDefault();
        $.ajax({
            url  : main_url+'back-end/user_management/Sub_Administrator_Management_Controller/add_sub_administrators',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                if(data!='false')
                {
                    swal("Success!", "New sub administrator has been added ", "success");
                    $('#add_sub_administrator').modal('close');
                    $('#form-add-sub-administrator').trigger("reset");
                    Display_Sub_Administrators();
                }
                else
                {
                    swal("Failed!", "Username already exist.", "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

$(document).on('submit', '#form-edit-sub-administrator', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/user_management/Sub_Administrator_Management_Controller/edit_sub_administrators',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data!='false')
                {
                    swal("Success!", "A sub administrator has been modified ", "success");
                    $('#edit_sub_administrator').modal('close');
                    Display_Sub_Administrators();
                }
                else
                {
                    swal("Failed!", "Username already exist.", "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function edit(s_ad_id)
{
    $('#edit_dashboard_permission').prop('checked', $('#dashboard_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_news_permission').prop('checked', $('#news_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_class_list_permission').prop('checked', $('#class_list_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_slides_permission').prop('checked', $('#slides_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_company_permission').prop('checked', $('#company_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_courses_permission').prop('checked', $('#courses_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_sections_permission').prop('checked', $('#sections_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_departments_permission').prop('checked', $('#departments_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_programs_permission').prop('checked', $('#programs_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_student_permission').prop('checked', $('#student_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_faculty_permission').prop('checked', $('#faculty_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_uploaded_documents_permission').prop('checked', $('#uploaded_documents_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_ojt_requirements_permission').prop('checked', $('#ojt_requirements_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_downloadable_permission').prop('checked', $('#downloadable_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_ojt_policies_permission').prop('checked', $('#ojt_policy_'+s_ad_id).text()=="1" ? true : false);
    $('#edit_sub_administrator').modal('open');
    $('#edit_uname').val($('#s_ad_uname_'+s_ad_id).text());
    $('#edit_pword').val($('#s_ad_pword_'+s_ad_id).text());
    $('#edit_first_name').val($('#s_ad_fname_'+s_ad_id).text());
    $('#edit_middle_name').val($('#s_ad_mname_'+s_ad_id).text());
    $('#edit_last_name').val($('#s_ad_lname_'+s_ad_id).text());
    $('#edit_id').val(s_ad_id);
}

function remove(s_ad_id)
{
    swal({
      title: "Are you sure?",
      text: "This sub administrator will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/user_management/Sub_Administrator_Management_Controller/softDelete',
            type : 'POST',
            data: {
                's_ad_id' : s_ad_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A sub administrator has been deleted and was moved to bin ", "success");
                Display_Sub_Administrators();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Administrator has not been deleted", "error");
      }
    });
}
