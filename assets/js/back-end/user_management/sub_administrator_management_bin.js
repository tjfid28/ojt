Display_Sub_Administrators();

function Display_Sub_Administrators()
{
    $.ajax({
        url      : main_url+'back-end/user_management/Sub_Administrator_Management_Controller/get_bin',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#sub_admin_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Action</th>";
                table += "<th>Username</th>";
                table += "<th>Password</th>";
                table += "<th>First Name</th>";
                table += "<th>Middle Name</th>";
                table += "<th>Last Name</th>";
                table += "<th>Deleted at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='restore("+data[counter]['s_ad_id']+")' class='waves-effect waves-light btn light-green'><i class='fa fa-history'></i></a>&nbsp;&nbsp;<a onclick='remove("+data[counter]['s_ad_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "<td>"+data[counter]['s_ad_uname']+"</td>";
                    table += "<td>"+data[counter]['s_ad_pword']+"</td>";
                    table += "<td>"+data[counter]['s_ad_fname']+"</td>";
                    table += "<td>"+data[counter]['s_ad_mname']+"</td>";
                    table += "<td>"+data[counter]['s_ad_lname']+"</td>";
                    table += "<td>"+data[counter]['deleted_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='7'><center>No Sub Admin</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#sub_admin_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function restore(s_ad_id)
{
    swal({
      title: "Are you sure?",
      text: "This sub administrator will be restored",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#8bc34a",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/user_management/Sub_Administrator_Management_Controller/restore',
            type : 'POST',
            data: {
                's_ad_id' : s_ad_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Restoring Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A sub administrator has been restored", "success");
                Display_Sub_Administrators();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Sub Administrator has not been restored", "error");
      }
    });
}

function remove(s_ad_id)
{
    swal({
      title: "Are you sure?",
      text: "This sub administrator will be deleted permanently",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/user_management/Sub_Administrator_Management_Controller/delete_sub_administrators',
            type : 'POST',
            data: {
                's_ad_id' : s_ad_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A sub administrator has been deleted permanently", "success");
                Display_Sub_Administrators();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Administrator has not been deleted", "error");
      }
    });
}
