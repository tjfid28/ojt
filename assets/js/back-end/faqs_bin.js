Display_Faqs();
function Display_Faqs()
{
    $.ajax({
        url  : main_url+'back-end/Faqs_Controller/get_bin',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#faq_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Action</th>";
                table += "<th>Title</th>";
                table += "<th>Description</th>";
                table += "<th>Deleted at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='restore("+data[counter]['fa_id']+")' class='waves-effect waves-light btn light-green'><i class='fa fa-history'></i></a>&nbsp;&nbsp;<a onclick='remove("+data[counter]['fa_id']+")' class='waves-effect waves-light btn red darken-3'><i class='fa fa-trash'></i></a></td>";
                    table += "<td>"+data[counter]['fa_title']+"</td>";
                    table += "<td>"+data[counter]['fa_description']+"</td>";
                    table += "<td>"+data[counter]['deleted_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='4'><center>No FAQ's</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#faq_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function restore(fa_id)
{
    swal({
      title: "Are you sure?",
      text: "This FAQ will be restored",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#8bc34a",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Faqs_Controller/restore',
            type : 'POST',
            data: {
                'fa_id' : fa_id,
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Restoring Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A FAQ has been restored", "success");
                Display_Faqs();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "FAQ has not been restored", "error");
      }
    });
}

function remove(fa_id)
{
    swal({
      title: "Are you sure?",
      text: "This FAQ will be deleted permanently",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Faqs_Controller/delete_faq',
            type : 'POST',
            data: {
                'fa_id' : fa_id,
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A FAQ has been deleted permanently", "success");
                Display_Faqs();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "FAQ has not been deleted", "error");
      }
    });
}
