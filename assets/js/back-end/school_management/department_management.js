Display_Departments();
function Display_Departments()
{
	$.ajax({
        url      : main_url+'back-end/school_management/Department_Management_Controller/get_departments',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#department_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th style='text-align:center;'>Department Name</th>";
                table += "<th style='text-align:center;'>Created at</th>";
                table += "<th style='text-align:center;'>Updated at</th>";
                table += "<th style='text-align:center;'>Edit</th>";
                table += "<th style='text-align:center;'>Delete</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td id='dept_name_"+data[counter]['dept_id']+"'>"+data[counter]['dept_name']+"</td>";
                    table += "<td>"+data[counter]['dept_created_at']+"</td>";
                    table += "<td>"+data[counter]['dept_updated_at']+"</td>";
                    table += "<td><a onclick='edit("+data[counter]['dept_id']+")' class='waves-effect waves-light btn blue darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-pencil'></i></a></td>";
                    table += "<td><a onclick='remove("+data[counter]['dept_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No Department</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#department_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
  	});
}

$(document).on('submit', '#form-add-department', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/school_management/Department_Management_Controller/add_department',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data==true)
                {
                    swal("Success!", "New department has been added ", "success");
                    $('#add_department').modal('close');
                    $('#form-add-department').trigger("reset");
                    Display_Departments();
                }
                else
                {
                    swal("Failed!", data, "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

$(document).on('submit', '#form-edit-department', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/school_management/Department_Management_Controller/edit_department',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                if(data==true)
                {
                    swal("Success!", "A department has been modified ", "success");
                    $('#edit_department').modal('close');
                    Display_Departments();
                }
                else
                {
                    swal("Failed!", data, "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function edit(dept_id)
{
    $('#edit_department').modal('open');
    $('#edit_dept_name').val($('#dept_name_'+dept_id).text());
    $('#edit_id').val(dept_id);
}

function remove(dept_id)
{
    swal({
      title: "Are you sure?",
      text: "This department will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/school_management/Department_Management_Controller/softDelete',
            type : 'POST',
            data: {
                'dept_id' : dept_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A department has been deleted and was moved to bin ", "success");
                Display_Departments();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Department has not been deleted", "error");
      }
    });
}
