Display_Courses();
function Display_Courses()
{
    $.ajax({
        url      : main_url+'back-end/school_management/Course_Management_Controller/get_bin',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#course_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Actions</th>";
                table += "<th>Course Title</th>";
                table += "<th>Deleted at</th>";
                table += "</thead>";
                table += "<tbody>";
                var counter = 0;
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='restore("+data[counter]['cou_id']+")' class='waves-effect waves-light btn light-green' ><i class='fa fa-history'></i></a>&nbsp;&nbsp;<a onclick='remove("+data[counter]['cou_id']+")' class='waves-effect waves-light btn red darken-3'><i class='fa fa-trash'></i></a></td>";
                    table += "<td>"+data[counter]['cou_title']+"</td>";
                    table += "<td>"+data[counter]['deleted_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='3'><center>No Course</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#course_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function restore(cou_id)
{
    swal({
      title: "Are you sure?",
      text: "This course will be restored",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#8bc34a",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/school_management/Course_Management_Controller/restore',
            type : 'POST',
            data: {
                'cou_id' : cou_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Restoring Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A course has been restored ", "success");
                Display_Courses();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Course has not been restored", "error");
      }
    });
}

function remove(cou_id)
{
    swal({
      title: "Are you sure?",
      text: "This course will be deleted permanently",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/school_management/Course_Management_Controller/delete_course',
            type : 'POST',
            data: {
                'cou_id' : cou_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A course has been deleted permanently ", "success");
                Display_Courses();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Course has not been deleted", "error");
      }
    });
}
