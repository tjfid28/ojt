Display_Sections();
function Display_Sections()
{
	$.ajax({
        url      : main_url+'back-end/school_management/Section_Management_Controller/get_sections',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#section_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Section Name</th>";
                table += "<th>Created at</th>";
                table += "<th>Updated at</th>";
                table += "<th style='text-align:center;'>Edit</th>";
                table += "<th style='text-align:center;'>Delete</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td id='sec_name_"+data[counter]['sec_id']+"'>"+data[counter]['sec_name']+"</td>";
                    table += "<td>"+data[counter]['sec_created_at']+"</td>";
                    table += "<td>"+data[counter]['sec_updated_at']+"</td>";
                    table += "<td><a onclick='edit("+data[counter]['sec_id']+")' class='waves-effect waves-light btn blue darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-pencil'></i></a></td>";
                    table += "<td><a onclick='remove("+data[counter]['sec_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No Section</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#section_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
  	});
}

$(document).on('submit', '#form-add-section', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/school_management/Section_Management_Controller/add_section',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                if(data==true)
                {
                    swal("Success!", "New section has been added ", "success");
                    $('#add_section').modal('close');
                    $('#form-add-section').trigger("reset");
                    Display_Sections();
                }
                else
                {
                    swal("Failed!", data, "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

$(document).on('submit', '#form-edit-section', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/school_management/Section_Management_Controller/edit_section',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data==true)
                {
                    swal("Success!", "A section has been modified ", "success");
                    $('#edit_section').modal('close');
                    Display_Sections();
                }
                else
                {
                    swal("Failed!", data, "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function edit(sec_id)
{
    $('#edit_section').modal('open');
    $('#edit_sec_name').val($('#sec_name_'+sec_id).text());
    $('#edit_id').val(sec_id);
}

function remove(sec_id)
{
    swal({
      title: "Are you sure?",
      text: "This section will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/school_management/Section_Management_Controller/softDelete',
            type : 'POST',
            data: {
                'sec_id' : sec_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A section has been deleted and was moved to bin ", "success");
                Display_Sections();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Section has not been deleted", "error");
      }
    });
}
