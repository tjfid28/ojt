Display_Subjects();
function Display_Subjects()
{
	$.ajax({
        url      : main_url+'back-end/school_management/Subject_Management_Controller/get_subjects',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#section_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Program Name</th>";
                table += "<th>Required Hours</th>";
                table += "<th>No. of Units</th>";
                table += "<th>Created at</th>";
                table += "<th>Updated at</th>";
                table += "<th style='text-align:center;'>Edit</th>";
                table += "<th style='text-align:center;'>Delete</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td id='sub_name_"+data[counter]['sub_id']+"'>"+data[counter]['sub_name']+"</td>";
                    table += "<td id='sub_req_hours_"+data[counter]['sub_id']+"' style='text-align:center;'>"+data[counter]['sub_req_hours']+"</td>";
                    table += "<td id='sub_unit_"+data[counter]['sub_id']+"' style='text-align:center;'>"+data[counter]['sub_unit']+"</td>";
                    table += "<td>"+data[counter]['sub_created_at']+"</td>";
                    table += "<td>"+data[counter]['sub_updated_at']+"</td>";
                    table += "<td><a onclick='edit("+data[counter]['sub_id']+")' class='waves-effect waves-light btn blue darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-pencil'></i></a></td>";
                    table += "<td><a onclick='remove("+data[counter]['sub_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No Program</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#section_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
  	});
}

$(document).on('submit', '#form-add-subject', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/school_management/Subject_Management_Controller/add_subject',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data==true)
                {
                    swal("Success!", "New subject has been added ", "success");
                    $('#add_subject').modal('close');
                    $('#form-add-subject').trigger("reset");
                    Display_Subjects();
                }
                else
                {
                    swal("Failed!", data, "error");
                }

            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

$(document).on('submit', '#form-edit-subject', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/school_management/Subject_Management_Controller/edit_subject',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data==true)
                {
                    swal("Success!", "A subject has been modified ", "success");
                    $('#edit_subject').modal('close');
                    Display_Subjects();
                }
                else
                {
                    swal("Failed!", data, "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function edit(sub_id)
{
    $('#edit_subject').modal('open');
    $('#edit_sub_name').val($('#sub_name_'+sub_id).text());
    $('#edit_sub_req_hours').val($('#sub_req_hours_'+sub_id).text());
    $('#edit_sub_unit').val($('#sub_unit_'+sub_id).text());
    $('#edit_id').val(sub_id);
}

function remove(sub_id)
{
    swal({
      title: "Are you sure?",
      text: "This subject will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/school_management/Subject_Management_Controller/softDelete',
            type : 'POST',
            data: {
                'sub_id' : sub_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A subject has been deleted and was moved to bin", "success");
                Display_Subjects();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Subject has not been deleted", "error");
      }
    });
}
