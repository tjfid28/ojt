Display_Courses();
function Display_Courses()
{
	$.ajax({
        url      : main_url+'back-end/school_management/Course_Management_Controller/get_courses',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#course_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Course Title</th>";
                table += "<th>Created at</th>";
                table += "<th>Updated at</th>";
                table += "<th style='text-align:center;'>Edit</th>";
                table += "<th style='text-align:center;'>Delete</th>";
                table += "</thead>";
                table += "<tbody>";
                var counter = 0;
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td id='cou_title_"+data[counter]['cou_id']+"'>"+data[counter]['cou_title']+"</td>";
                    table += "<td>"+data[counter]['cou_created_at']+"</td>";
                    table += "<td>"+data[counter]['cou_updated_at']+"</td>";
                    table += "<td><a onclick='edit("+data[counter]['cou_id']+")' class='waves-effect waves-light btn blue darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-pencil'></i></a></td>";
                    table += "<td><a onclick='remove("+data[counter]['cou_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No Course</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#course_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
  	});
}

$(document).on('submit', '#form-add-course', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/school_management/Course_Management_Controller/add_course',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data==true)
                {
                    swal("Success!", "New course has been added ", "success");
                    $('#form-add-course').trigger("reset");
                    $('#add_course').modal('close');
                    Display_Courses();
                }
                else
                {
                    swal("Failed!", data, "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

$(document).on('submit', '#form-edit-course', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/school_management/Course_Management_Controller/edit_course',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){

                if(data==true)
                {
                    swal("Success!", "A course has been modified ", "success");
                    $('#edit_course').modal('close');
                    Display_Courses();
                }
                else
                {
                    swal("Failed!", data, "error");
                }
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function edit(cou_id)
{
    $('#edit_course').modal('open');
    $('#edit_cou_title').val($('#cou_title_'+cou_id).text());
    $('#edit_id').val(cou_id);
}

function remove(cou_id)
{
    swal({
      title: "Are you sure?",
      text: "This course will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/school_management/Course_Management_Controller/softDelete',
            type : 'POST',
            data: {
                'cou_id' : cou_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Removing Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A course has been deleted and was moved to bin ", "success");
                Display_Courses();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Course has not been deleted", "error");
      }
    });
}
