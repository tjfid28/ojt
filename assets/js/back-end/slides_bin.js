Display_Slides();
function Display_Slides()
{
    $.ajax({
        url  : main_url+'back-end/Slide_Controller/get_bin',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#slide_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Action</th>";
                table += "<th>Image Name</th>";
                table += "<th>Caption</th>";
                table += "<th>Description</th>";
                table += "<th>Deleted at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='restore("+data[counter]['s_id']+")' class='waves-effect waves-light btn light-green'><i class='fa fa-history'></i></a>&nbsp;&nbsp;<a onclick='remove("+data[counter]['s_id']+")' class='waves-effect waves-light btn red darken-3'><i class='fa fa-trash'></i></a></td>";
                    table += "<td><img src='"+main_url+"resources/slide-images/"+data[counter]['s_slug_name']+"' style='object-fit:cover;width:120px;height:70px;'></td>";
                    table += "<td>"+data[counter]['s_caption']+"</td>";
                    table += "<td>"+data[counter]['s_description']+"</td>";
                    table += "<td>"+data[counter]['deleted_at']+"</td>";
                    table += "<input type='text' id='s_slug_name_"+data[counter]['s_id']+"' style='display:none;' value='"+data[counter]['s_slug_name']+"' readonly>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='5'><center>No Slide</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#slide_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function restore(s_id)
{
    swal({
      title: "Are you sure?",
      text: "This slide will be restored",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#8bc34a",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Slide_Controller/restore',
            type : 'POST',
            data: {
                's_id'     : s_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Restoring Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A slide has been restored", "success");
                Display_Slides();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Slide has not been restored", "error");
      }
    });
}

function remove(s_id)
{
    var slug_name = $('#s_slug_name_'+s_id).val();
    swal({
      title: "Are you sure?",
      text: "This slide will be deleted permanently",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Slide_Controller/delete_slide',
            type : 'POST',
            data: {
                's_id'     : s_id,
                'slug_name' : slug_name,
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A slide has been deleted permanently ", "success");
                Display_Slides();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Slide has not been deleted", "error");
      }
    });
}
