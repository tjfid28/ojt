$(document).on('submit', '#form-policy', function(e){
        e.preventDefault();

        var fileType = $('#policy_cover').val().split('.').pop().toLowerCase();
        var fileSize = $('#policy_cover')[0].files[0].size;

        if ($.inArray(fileType, ['jpeg', 'jpg', 'png']) == -1) {
            swal("Error", "File must be in this format (jpeg, jpg, png)", "error");
            return false;
        }

        if (fileSize > 20000000) {
            swal("Error", "File size limit is 20MB", "error");
            return false;
        }

        $.ajax({
            url  : main_url+'back-end/ojt_management/Ojt_Policy_Controller/save_policy',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal({
                  title: "Success",
                  text: "Policy has been modified.",
                  type: "success",
                  confirmButtonColor: "#0d47a1",
                  confirmButtonText: "OK",
                },
                function(){
                  location.reload();
                });

            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});
