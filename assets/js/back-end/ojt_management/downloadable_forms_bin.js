Display_Forms();
function Display_Forms()
{
    $.ajax({
        url  : main_url+'back-end/ojt_management/Downloadable_Form_Controller/get_bin',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#forms_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Action</th>";
                table += "<th>File Name</th>";
                table += "<th>Requirement Type</th>";
                table += "<th>Deleted at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='restore("+data[counter]['df_id']+")' class='waves-effect waves-light btn light-green' ><i class='fa fa-history'></i></a>&nbsp;&nbsp;<a onclick='remove("+data[counter]['df_id']+")' class='waves-effect waves-light btn red darken-3'><i class='fa fa-trash'></i></a></td>";
                    table += "<td>"+data[counter]['df_file_name']+"</td>";
                    table += "<td>"+data[counter]['req_name']+"</td>";
                    table += "<td>"+data[counter]['deleted_at']+"</td>";
                    table += "<td style='display:none;' id='df_slug_"+data[counter]['df_id']+">"+data[counter]['df_slug']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='4'><center>No Forms</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#forms_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function restore(df_id)
{
    swal({
      title: "Are you sure?",
      text: "This form will be restored",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#8bc34a",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {
        $.ajax({
            url  : main_url+'back-end/ojt_management/Downloadable_Form_Controller/restore',
            type : 'POST',
            data: {
                'df_id': df_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Restoring Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A form has been restored ", "success");
                Display_Forms();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Form has not been restored", "error");
      }
    });
}

function remove(df_id)
{
    swal({
      title: "Are you sure?",
      text: "This form will be deleted permanently",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {
        var slug_name = $('#df_slug_'+df_id).text();
        $.ajax({
            url  : main_url+'back-end/ojt_management/Downloadable_Form_Controller/delete_form',
            type : 'POST',
            data: {
                'df_id'     : df_id,
                'slug_name' : slug_name,
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A form has been deleted permanently ", "success");
                Display_Forms();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Form has not been deleted", "error");
      }
    });
}
