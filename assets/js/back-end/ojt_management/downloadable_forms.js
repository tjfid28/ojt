Display_Forms();
function Display_Forms()
{
    $.ajax({
        url  : main_url+'back-end/ojt_management/Downloadable_Form_Controller/get_forms',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#forms_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th style='width:100px !important;'>Action</th>";
                table += "<th>File Name</th>";
                table += "<th>Requirement Type</th>";
                table += "<th>Created at</th>";
                table += "<th style='display:none;'></th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='download_form("+data[counter]['df_id']+")' class='waves-effect waves-light btn teal lighten-1' ><i class='fa fa-download'></i></a>&nbsp;&nbsp;<a onclick='remove("+data[counter]['df_id']+")' class='waves-effect waves-light btn red darken-3'><i class='fa fa-trash'></i></a></td>";
                    table += "<td id='df_file_name_"+data[counter]['df_id']+"'>"+data[counter]['df_file_name']+"</td>";
                    table += "<td>"+data[counter]['req_name']+"</td>";
                    table += "<td>"+data[counter]['df_created_at']+"</td>";
                    table += "<td id='df_slug_"+data[counter]['df_id']+"' style='display:none;'>"+data[counter]['df_slug']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='4'><center>No Forms</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#forms_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

$(document).on('submit', '#form-add-form', function(e){
        e.preventDefault();

        var fileType = $('#document').val().split('.').pop().toLowerCase();
        var fileSize = $('#document')[0].files[0].size;

        if ($.inArray(fileType, ['doc', 'docx', 'xlsx', 'pdf']) == -1) {
            swal("Error", "File must be in this format (doc, docx, xlsx, pdf)", "error");
            return false;
        }

        if (fileSize > 20000000) {
            swal("Error", "File size limit is 20MB", "error");
            return false;
        }

        $.ajax({
            url  : main_url+'back-end/ojt_management/Downloadable_Form_Controller/add_form',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal(data, "New form has been added ", "success");
                $('#add_form').modal('close');
                $('#form-add-form').trigger("reset");
                Display_Forms();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function remove(df_id)
{
    swal({
      title: "Are you sure?",
      text: "This form will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {
        $.ajax({
            url  : main_url+'back-end/ojt_management/Downloadable_Form_Controller/softDelete',
            type : 'POST',
            data: {
                'df_id'     : df_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A form has been deleted and was moved to bin ", "success");
                Display_Forms();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Form has not been deleted", "error");
      }
    });
}

function download_form(df_id)
{
    var slug_name = $('#df_slug_'+df_id).text();
    var file_name = $('#df_file_name_'+df_id).text();
    window.open(main_url+"back-end/ojt_management/Downloadable_Form_Controller/download_form/"+slug_name+"/"+file_name);
}
