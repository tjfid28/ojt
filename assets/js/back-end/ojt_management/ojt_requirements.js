Display_Requirements();
function Display_Requirements()
{
    $.ajax({
        url      : main_url+'back-end/ojt_management/Ojt_Requirement_Controller/get_requirements',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#section_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th style='text-align:center;'>Action</th>";
                table += "<th>Requirement Name</th>";
                table += "<th>Created at</th>";
                table += "<th>Updated at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='edit("+data[counter]['req_id']+")' class='waves-effect waves-light btn blue darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-pencil'></i></a><br><br><a onclick='remove("+data[counter]['req_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "<td id='req_name_"+data[counter]['req_id']+"'>"+data[counter]['req_name']+"</td>";
                    table += "<td>"+data[counter]['req_created_at']+"</td>";
                    table += "<td>"+data[counter]['req_updated_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No Requirement</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#requirements_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

$(document).on('submit', '#form-add-requirement', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/ojt_management/Ojt_Requirement_Controller/add_requirement',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){

            },
            success : function(data){
                swal("Success!", "New requirement has been added ", "success");
                $('#add_requirement').modal('close');
                $('#form-add-requirement').trigger("reset");
                Display_Requirements();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

$(document).on('submit', '#form-edit-requirement', function(e){
        e.preventDefault();

        $.ajax({
            url  : main_url+'back-end/ojt_management/Ojt_Requirement_Controller/edit_requirement',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){

            },
            success : function(data){
                swal("Success!", "A requirement has been modified", "success");
                $('#edit_requirement').modal('close');
                $('#form-edit-requirement').trigger("reset");
                Display_Requirements();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function edit(req_id)
{
    $('#edit_requirement').modal('open');
    $('#edit_req_name').val($('#req_name_'+req_id).text());
    $('#edit_cou_id').val($('#cou_id_'+req_id).text());
    $('#edit_cou_id').material_select();
    $('#edit_sub_id').val($('#sub_id_'+req_id).text());
    $('#edit_sub_id').material_select();

    $('#edit_id').val(req_id);
}

function remove(req_id)
{
    swal({
      title: "Are you sure?",
      text: "This requirement will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/ojt_management/Ojt_Requirement_Controller/softDelete',
            type : 'POST',
            data: {
                'req_id' : req_id
            },
            beforeSend : function(xhr){

            },
            success : function(data){
                swal("Success!", "A requirement has been deleted and was moved to bin ", "success");
                Display_Requirements();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Requirement has not been deleted", "error");
      }
    });
}
