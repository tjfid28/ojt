var searchtimer = null;
$( "#search" ).keyup(function() {
    clearTimeout(searchtimer);
    searchtimer = setTimeout(
    function(){
       Display_Documents();
    }, 2000);
});

Display_Documents();
function Display_Documents()
{
    $.ajax({
        url  : main_url+'back-end/ojt_management/Uploaded_Documents_Controller/get_documents',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
            'search' : $('.search').val(),
        },
        beforeSend : function(xhr){
            var label = $(".search").val() == "" ? "Please wait ..." : "Searching ...";
            $('#documents_viewer').html('<br><br><br><center><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color:#1565c0;"></i><h5>'+label+'</h5><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var documents = "<div class='row'>";
                var counter = 0;
                    for(counter = 0;counter<data.length;counter++)
                    {
                        var ext = data[counter]['d_file_name'].substr(data[counter]['d_file_name'].lastIndexOf('.') + 1);
                        var color = null;
                        get_ext = get_extention(ext);
                        documents += "<div class='card-document col s12 m4 l3' id='card-"+data[counter]['d_id']+"'>";
                        documents += "<div class='right-align toggle-btn'>";
                        documents += "<a class='waves-effect waves-light btn red' onclick='delete_document("+data[counter]['d_id']+")'><i class='fa fa-remove'></i></a>";
                        documents += "</div>";
                        documents += "<a onclick='download_document("+data[counter]['d_id']+")'>";
                        documents += "<div class='card-panel waves-effect hoverable "+ext+"' style='text-align:center;height:250px;width:100%;'>";
                        documents += "<i class='fa fa-file-"+get_ext.ext+"-o' style='font-size:100px;color:"+get_ext.color+"'></i>";
                        documents += "<input type='hidden' id='d_slug_"+data[counter]['d_id']+"' value='"+data[counter]['d_slug']+"' readonly>";
                        documents += "<input type='hidden' id='d_file_name_"+data[counter]['d_id']+"' value='"+data[counter]['d_file_name']+"' readonly>";
                        documents += "<h6 class='black-text truncate'>"+data[counter]['d_file_name']+"</h6>";
                        documents += "<div align='left'>";
                        documents += "<h6 class='black-text truncate'><b>Owner</b></h6>";
                        documents += "<h6 class='black-text truncate'>"+data[counter]['st_number']+"</h6>";
                        documents += "<h6 class='black-text truncate'> "+data[counter]['owner']+"</h6>";
                        documents += "</div>";
                        documents += "</div>";
                        documents += "</a>";
                        documents += "</div>";
                    }
                documents += "</div>";
                if(counter==0)
                {
                    documents += "<div class='center-align' style='margin-top:200px;margin-bottom:300px;'><h5><i class='fa fa-file-text-o'></i> No documents</h5></div>";
                }

            $('#documents_viewer').html(documents);
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function download_document(d_id)
{
    var slug_name = $('#d_slug_'+d_id).val();
    var file_name = $('#d_file_name_'+d_id).val();

    swal({
      title: "Are you sure?",
      text: "This "+file_name+" will be downloaded",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#1565c0;",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false
    },
    function(){
        swal.close();
        window.open(main_url+"front-end/student/My_Document_Controller/download_document/"+slug_name+"/"+file_name);
    });
}

function delete_document(d_id)
{
    var file_name = $('#d_file_name_'+d_id).val();
    var slug_name = $('#d_slug_'+d_id).val();
    swal({
      title: "Are you sure?",
      text: "This "+file_name+" will be deleted",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url  : main_url+'back-end/ojt_management/Uploaded_Documents_Controller/softDelete',
                type : 'POST',
                data: {
                    'd_id'     : d_id
                },
                beforeSend : function(xhr){
                    $('#card-'+d_id).fadeOut("slow",function(){ $(this).remove(); });
                },
                success : function(data){
                    swal("Success!", "Document has been deleted and was moved to bin", "success");
                    if($('.card-document').length==1)
                    {
                        Display_Documents();
                    }
                },
                error: function(xhr, status, error)
                {
                    alert(error);
                }
            });
        } else {
            swal("Cancelled", "Document has not been deleted", "error");
        }
    });
}

