Display_Documents();
function Display_Documents()
{
    $.ajax({
        url  : main_url+'back-end/ojt_management/Uploaded_Documents_Controller/get_bin',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#documents_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead class='grey lighten-2'>";
                table += "<th>Action</th>";
                table += "<th>File Name</th>";
                table += "<th>Owner</th>";
                table += "<th>Deleted at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='restore("+data[counter]['d_id']+")' class='waves-effect waves-light btn light-green'><i class='fa fa-history'></i></a>&nbsp;&nbsp;<a onclick='remove("+data[counter]['d_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "<td>"+data[counter]['d_file_name']+"</td>";
                    table += "<td>"+data[counter]['owner']+"</td>";
                    table += "<td>"+data[counter]['deleted_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='4'><center>No Documents</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";

            $('#documents_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function restore(d_id)
{
    swal({
      title: "Are you sure?",
      text: "This document will be restored",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#8bc34a;",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url  : main_url+'back-end/ojt_management/Uploaded_Documents_Controller/restore',
                type : 'POST',
                data: {
                    'd_id'     : d_id
                },
                beforeSend : function(xhr){
                    swal({
                      title: "",
                      text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Restoring Please wait ...",
                      html: true,
                      showConfirmButton: false
                    });
                },
                success : function(data){
                    swal("Success!", "A document has been restored ", "success");
                    Display_Documents();
                },
                error: function(xhr, status, error)
                {
                    alert(error);
                }
            });
        } else {
            swal("Cancelled", "Document has not been restored", "error");
        }
    });
}

function remove(d_id)
{
    var slug_name = $('#d_slug_'+d_id).val();
    swal({
      title: "Are you sure?",
      text: "This document will be deleted permanently",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url  : main_url+'back-end/ojt_management/Uploaded_Documents_Controller/delete_document',
                type : 'POST',
                data: {
                    'd_id'     : d_id,
                    'slug_name' : slug_name,
                },
                beforeSend : function(xhr){
                    swal({
                      title: "",
                      text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                      html: true,
                      showConfirmButton: false
                    });
                },
                success : function(data){
                    swal("Success!", "A document has been deleted permanently ", "success");
                    Display_Documents();
                },
                error: function(xhr, status, error)
                {
                    alert(error);
                }
            });
        } else {
            swal("Cancelled", "Document has not been deleted", "error");
        }
    });
}

