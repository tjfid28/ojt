Display_Company();
function Display_Company()
{
    $.ajax({
        url  : main_url+'back-end/Partner_Company_Controller/get_bin',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#company_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Action</th>";
                table += "<th>Image Name</th>";
                table += "<th>Caption</th>";
                table += "<th>Type</th>";
                table += "<th>Email</th>";
                table += "<th>Link</th>";
                table += "<th>Deleted at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='restore("+data[counter]['co_id']+")' class='waves-effect waves-light btn light-green'><i class='fa fa-history'></i></a>&nbsp;&nbsp;<a onclick='remove("+data[counter]['co_id']+")' class='waves-effect waves-light btn red darken-3'><i class='fa fa-trash'></i></a></td>";
                    table += "<td id='s_slug_image_"+data[counter]['co_id']+"' data-image='"+data[counter]['co_image_name']+"'><img src='"+main_url+"resources/partner_companies/"+data[counter]['co_image_name']+"' style='object-fit:cover;width:120px;height:70px;'></td>";
                    table += "<td>"+data[counter]['co_name']+"</td>";
                    table += "<td>"+(data[counter]['co_type'] == 1 ? 'ICT' : 'HM')+"</td>";
                    table += "<td>"+data[counter]['co_email']+"</td>";
                    table += "<td><a href='"+data[counter]['co_link']+"' target='_blank'>"+data[counter]['co_link']+"</a></td>";
                    table += "<td>"+data[counter]['deleted_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='7'><center>No Company</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#company_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function restore(co_id)
{
    swal({
      title: "Are you sure?",
      text: "This company will be restored",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#8bc34a",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Partner_Company_Controller/restore',
            type : 'POST',
            data: {
                'co_id' : co_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Restoring Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success", "A partner company has been restored", "success");
                Display_Company();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Company has not been restored", "error");
      }
    });
}

function remove(co_id)
{
    var image_to_delete = $('#s_slug_image_'+co_id).attr('data-image');

    swal({
      title: "Are you sure?",
      text: "This company will be deleted permanently",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Partner_Company_Controller/delete_company',
            type : 'POST',
            data: {
                'co_id' : co_id,
                'image_to_delete' : image_to_delete
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success", "A partner company has been deleted permanently ", "success");
                Display_Company();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Company has not been deleted", "error");
      }
    });
}

