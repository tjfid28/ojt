Display_Faqs();
function Display_Faqs()
{
    $.ajax({
        url  : main_url+'back-end/Faqs_Controller/get_faqs',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#faq_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th style='text-align:center;'>Action</th>";
                table += "<th>Title</th>";
                table += "<th>Description</th>";
                table += "<th>Created at</th>";
                table += "<th>Updated at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='edit("+data[counter]['fa_id']+")' class='waves-effect waves-light btn blue darken-3'><i class='fa fa-pencil'></i></a> <br><br><a onclick='remove("+data[counter]['fa_id']+")' class='waves-effect waves-light btn red darken-3'><i class='fa fa-trash'></i></a></td>";
                    table += "<td id='fa_title_"+data[counter]['fa_id']+"'>"+data[counter]['fa_title']+"</td>";
                    table += "<td id='fa_description_"+data[counter]['fa_id']+"'>"+data[counter]['fa_description']+"</td>";
                    table += "<td>"+data[counter]['fa_created_at']+"</td>";
                    table += "<td>"+data[counter]['fa_updated_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No FAQ's</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#faq_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function edit(fa_id)
{
    $('#edit_faq').modal('open');
    $('#edit_faq_title').val($('#fa_title_'+fa_id).text());
    CKEDITOR.instances['edit_description'].setData($('#fa_description_'+fa_id).text());
    $('.fa_id').val(fa_id);
}

$(document).on('submit', '#form-faq', function(e){
        e.preventDefault();
        $.ajax({
            url  : main_url+'back-end/Faqs_Controller/create_faq',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal(data, "New FAQ has been added ", "success");
                $('#create_faq').modal('close');
                $('#form-faq').trigger("reset");
                Display_Faqs();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});


$(document).on('submit', '#edit-form-faq', function(e){
        e.preventDefault();
        $.ajax({
            url  : main_url+'back-end/Faqs_Controller/edit_faq',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal(data, "A FAQ has been modified ", "success");
                $('#edit_faq').modal('close');
                $('#edit-form-faq').trigger("reset");
                Display_Faqs();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function remove(fa_id)
{
    swal({
      title: "Are you sure?",
      text: "This FAQ will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Faqs_Controller/softDelete',
            type : 'POST',
            data: {
                'fa_id' : fa_id,
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success!", "A FAQ has been deleted and was moved to bin", "success");
                Display_Faqs();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "FAQ has not been deleted", "error");
      }
    });
}
