Display_Logs();
function Display_Logs()
{
    $.ajax({
        url  : main_url+'back-end/Audit_Log_Controller/get_audit_logs',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#audit_logs_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>User</th>";
                table += "<th>Module</th>";
                table += "<th>Action</th>";
                table += "<th>Logged Date</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td>"+data[counter]['ad_fname'] + " " + data[counter]['ad_lname']+"</td>";
                    table += "<td>"+data[counter]['log_module']+"</td>";
                    table += "<td>"+data[counter]['log_action']+"</td>";
                    table += "<td>"+data[counter]['log_date']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='4'><center>No logs</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#audit_logs_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}
