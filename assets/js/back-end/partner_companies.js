Display_Company();
function Display_Company()
{
    $.ajax({
        url  : main_url+'back-end/Partner_Company_Controller/get_companies',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#company_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th style='text-align:center;'>Action</th>";
                table += "<th>Image Name</th>";
                table += "<th>Caption</th>";
                table += "<th>Type</th>";
                table += "<th>Email</th>";
                table += "<th>Link</th>";
                table += "<th>Created at</th>";
                table += "<th>Updated at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='edit("+data[counter]['co_id']+")' class='waves-effect waves-light btn blue darken-3'><i class='fa fa-pencil'></i></a> <br><br><a onclick='remove("+data[counter]['co_id']+")' class='waves-effect waves-light btn red darken-3'><i class='fa fa-trash'></i></a></td>";
                    table += "<td id='s_slug_image_"+data[counter]['co_id']+"' data-image='"+data[counter]['co_image_name']+"'><img src='"+main_url+"resources/partner_companies/"+data[counter]['co_image_name']+"' style='object-fit:cover;width:120px;height:70px;'></td>";
                    table += "<td id='co_name_"+data[counter]['co_id']+"'>"+data[counter]['co_name']+"</td>";
                    table += "<td id='co_type_"+data[counter]['co_id']+"'>"+(data[counter]['co_type'] == 1 ? 'ICT' : 'HM')+"</td>";
                    table += "<td id='co_email_"+data[counter]['co_id']+"'>"+data[counter]['co_email']+"</td>";
                    table += "<td id='co_link_"+data[counter]['co_id']+"'><a href='"+data[counter]['co_link']+"' target='_blank'>"+data[counter]['co_link']+"</a></td>";
                    table += "<span class='hidden' id='co_description_"+data[counter]['co_id']+"'>"+data[counter]['co_description']+"</span>";
                    table += "<td>"+data[counter]['co_created_at']+"</td>";
                    table += "<td>"+data[counter]['co_updated_at']+"</td>";
                    table += "<input type='text' id='s_slug_name_"+data[counter]['co_id']+"' style='display:none;' value='"+data[counter]['co_image_name']+"' readonly>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='11'><center>No Company</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#company_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function edit(co_id)
{
    $('#edit_company').modal('open');
    $('#edit_name').val($('#co_name_'+co_id).text());
    $('#edit_type').find("option:contains('"+ $('#co_type_'+co_id).text() +"')").prop('selected', true);
    $("#edit_type").material_select();
    $('.edit_co_image').val($('#s_slug_image_'+co_id).attr('data-image'));
    CKEDITOR.instances['edit_description'].setData($('#co_description_'+co_id).text());
    $('#edit_company_link').val($('#co_link_'+co_id).text());
    $('#edit_company_email').val($('#co_email_'+co_id).text());
    $('.co_id').val(co_id);
}

$(document).on('submit', '#form-company', function(e){
        e.preventDefault();

        var fileType = $('#company-image').val().split('.').pop().toLowerCase();
        var fileSize = $('#company-image')[0].files[0].size;

        if ($.inArray(fileType, ['jpeg', 'jpg', 'png']) == -1) {
            swal("Error", "File must be in this format (jpeg, jpg, png)", "error");
            return false;
        }

        if (fileSize > 20000000) {
            swal("Error", "File size limit is 20MB", "error");
            return false;
        }

        $.ajax({
            url  : main_url+'back-end/Partner_Company_Controller/create_company',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal(data, "New partner company has been added ", "success");
                $('#create_company').modal('close');
                $('#form-company').trigger("reset");
                Display_Company();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});


$(document).on('submit', '#edit-form-company', function(e){
        e.preventDefault();

        var fileType = $('#edit_company-image').val().split('.').pop().toLowerCase();
        var fileSize = $('#edit_company-image')[0].files[0].size;

        if ($.inArray(fileType, ['jpeg', 'jpg', 'png']) == -1) {
            swal("Error", "File must be in this format (jpeg, jpg, png)", "error");
            return false;
        }

        if (fileSize > 20000000) {
            swal("Error", "File size limit is 20MB", "error");
            return false;
        }

        $.ajax({
            url  : main_url+'back-end/Partner_Company_Controller/edit_company',
            type : 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Saving Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal(data, "A partner company has been modified ", "success");
                $('#edit_company').modal('close');
                $('#edit-form-company').trigger("reset");
                Display_Company();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });
});

function remove(co_id)
{
    var image_to_delete = $('#s_slug_image_'+co_id).attr('data-image');

    swal({
      title: "Are you sure?",
      text: "This company will be deleted",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Partner_Company_Controller/softDelete',
            type : 'POST',
            data: {
                'co_id' : co_id,
                'image_to_delete' : image_to_delete
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Deleting Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal("Success", "A partner company has been deleted and was moved to bin ", "success");
                Display_Company();
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Company has not been deleted", "error");
      }
    });
}

$(document).ready(function() {
    $('select').material_select();
  });
