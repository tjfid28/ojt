Display_Classes();
function Display_Classes()
{
    $.ajax({
        url      :  main_url+'back-end/Class_List_Controller/get_bin',
        type     : 'POST',
        dataType : 'JSON',
        data     : {
        },
        beforeSend : function(xhr){
            $('#class_list_viewer').html('<br><br><br><center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        },
        success : function(data){

            var table = "<table id='data_viewer' class='highlight bordered mdl-data-table'>";
                table += "<thead  class='grey lighten-2'>";
                table += "<th>Action</th>";
                table += "<th>Section</th>";
                table += "<th>Subject</th>";
                table += "<th>Course</th>";
                table += "<th>OJT Adviser</th>";
                table += "<th>Term</th>";
                table += "<th>School year</th>";
                table += "<th>Status</th>";
                table += "<th>Deleted at</th>";
                table += "</thead>";
                var counter = 0;
                table += "<tbody>";
                for(counter = 0;counter<data.length;counter++)
                {
                    table += "<tr>";
                    table += "<td><a onclick='restore("+data[counter]['cla_id']+")' class='waves-effect waves-light btn light-green'><i class='fa fa-history'></i></a>&nbsp;&nbsp;<a onclick='remove("+data[counter]['cla_id']+")' class='waves-effect waves-light btn red darken-3' id='btn_delete' name='btn_delete'><i class='fa fa-trash'></i></a></td>";
                    table += "<td>"+data[counter]['sec_name']+"</td>";
                    table += "<td>"+data[counter]['sub_name']+"</td>";
                    table += "<td>"+data[counter]['cou_title']+"</td>";
                    table += "<td>"+data[counter]['adviser']+"</td>";
                    table += "<td>"+data[counter]['cla_term']+"</td>";
                    table += "<td>"+data[counter]['cla_year']+"</td>";
                    table += "<td>"+data[counter]['cla_status']+"</td>";
                    table += "<td>"+data[counter]['deleted_at']+"</td>";
                    table += "</tr>";
                }

                if(counter==0)
                {
                    table += "<tr>";
                    table += "<td colspan='9'><center>No Class</center></td>";
                    table += "</tr>";
                }
                table += "</tbody>";
                table += "</table>";


            $('#class_list_viewer').html(table);
            $('#data_viewer').DataTable();
        },
        error: function(xhr, status, error)
        {
           alert(error);
        }
    });
}

function restore(cla_id)
{
   swal({
      title: "Are you sure?",
      text: "This class will be restored",
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#8bc34a",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Class_List_Controller/restore',
            type : 'POST',
            data: {
                'cla_id' : cla_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Restoring Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal({
                  title: "Success",
                  text: "Class has been restored",
                  type: "success",
                  confirmButtonColor: "#0d47a1",
                  confirmButtonText: "OK",
                }, Display_Classes());
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Class has not been restored", "error");
      }
    });
}

function remove(cla_id)
{
    swal({
      title: "Are you sure?",
      text: "This class will be deleted permanently",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {

        $.ajax({
            url  : main_url+'back-end/Class_List_Controller/delete_class',
            type : 'POST',
            data: {
                'cla_id' : cla_id
            },
            beforeSend : function(xhr){
                swal({
                  title: "",
                  text: "<i class='fa fa-spinner fa-spin fa-3x fa-fw'></i><br><br>Removing Please wait ...",
                  html: true,
                  showConfirmButton: false
                });
            },
            success : function(data){
                swal({
                  title: "Success",
                  text: "Class has been deleted permanently",
                  type: "success",
                  confirmButtonColor: "#0d47a1",
                  confirmButtonText: "OK",
                }, Display_Classes());
            },
            error: function(xhr, status, error)
            {
                alert(error);
            }
        });

      } else {
        swal("Cancelled", "Class has not been deleted", "error");
      }
    });
}
