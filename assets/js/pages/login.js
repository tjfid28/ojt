$(document).on('submit', '#form-login', function(e) {
    e.preventDefault();
    var uname = $('#uname').val();
    var pword = $('#pword').val();
    if (uname != "" && pword != "") {
        $.ajax({
            url: 'Log_in_Controller/Log_in_function',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(xhr) {
                $('#btn-login').prop('disabled', true);
                $('#btn-login').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Processing');
            },
            success: function(data) {
                console.log(data);
                $('#btn-login').prop('disabled', false);
                if (data != "false" && data != "No Class") {
                    window.location.href = data;
                } else if (data == "No Class") {
                    swal("Failed!", "No assigned section yet", "warning")
                } else {
                    swal("Failed!", "Invalid username or password", "warning")
                }
                $('#btn-login').html("Login");
            }
        });
    } else {
        swal("Failed!", "Please fill all the fields!", "warning")
    }
})
