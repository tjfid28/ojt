$(document).on('submit', '#form_login', function(e) {
    e.preventDefault();
    var uname = $('#uname').val();
    var pword = $('#pword').val();
    if (uname != "" && pword != "") {
        $.ajax({
            url: 'Log_in_Controller/Log_in_function',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(xhr) {
                $('#btn_login').prop('disabled', true);
                $('#btn_login').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Processing');
            },
            success: function(data) {
                $('#btn_login').prop('disabled', false);
                if (data != "false" && data != "No Class") {
                    window.location.href = data;
                } else if (data == "No Class") {
                    swal("Failed!", "Your account does exist but not yet assigned to a section!", "warning")
                } else {
                    swal("Failed!", "Wrong username & password!", "warning")
                }
                $('#btn_login').html("LOG IN");
            }
        });
    } else {
        swal("Failed!", "Please fill all the fields!", "warning")
    }
})