<?php defined('BASEPATH') or exit('No direct script access allowed');

class Api
{
    protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
    }

    public function generate_slug($file_name)
    {
        $ext      = pathinfo($file_name, PATHINFO_EXTENSION);
        $get_Slug = rand(100, 999)."-".rand((int)10000000000, (int)99999999999).".".$ext;

        return $get_Slug;
    }

    public function strip_spaces($file_name)
    {
        return str_replace(" ", "_", $file_name);
    }

    public function notification_to_fb($fb_id)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://fbnotification.herokuapp.com/");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "Notify_ojt=true&User_id=".$fb_id);

        curl_exec($ch);
        return true;
    }

    public function batch_notification_to_fb($students_fb_id)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://fbnotification.herokuapp.com/");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "Batch_Notify_ojt=true&students_fb_id=".$students_fb_id);

        curl_exec($ch);

        return true;
    }

    public function system_notification($st_id, $fa_id, $n_content, $n_link, $n_show_to)
    {
        $date = date("Y-m-d H:i:s");
        $data = [
            'fa_id'         => $fa_id,
            'st_id'         => $st_id,
            'n_content'     => trim($n_content),
            'n_link'        => $n_link,
            'n_created_at'  => $date,
            'n_show_to'     => $n_show_to,
            'seen'          => 0,
        ];

        $this->CI->db->insert('notification_tbl', $data);
    }

    public function seen($n_id)
    {
        $data = [
            'seen' => 1
        ];
        $this->CI->db->where("n_id", $n_id);
        $this->CI->db->update('notification_tbl', $data);

        return true;
    }
}
