<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sms
{
    private $mobile_number;
    private $shortcode;
    private $message_id;
    private $message;
    private $client_id;
    private $secret_key;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->config->load('sms', true);
        $config = $this->CI->config->config['sms'];

        if (! empty($config)) {
            foreach ($config as $key => $val) {
                $this->{$key} = $val;
            }
        }
    }

    public function send($message, $mobile_number = null)
    {
        $arr_post_body = [
            "message_type" => "SEND",
            "mobile_number" => is_null($mobile_number) ? $this->mobile_number : $mobile_number,
            "shortcode" => $this->shortcode,
            "message_id" => $this->generateRandomString(32, '0123456789'),
            "message" => urlencode($message),
            "client_id" => $this->client_id,
            "secret_key" => $this->secret_key
        ];

        $query_string = "";
        foreach ($arr_post_body as $key => $frow) {
            $query_string .= '&'.$key.'='.$frow;
        }

        $URL = "https://post.chikka.com/smsapi/request";

        $curl_handler = curl_init();
        curl_setopt($curl_handler, CURLOPT_URL, $URL);
        curl_setopt($curl_handler, CURLOPT_POST, count($arr_post_body));
        curl_setopt($curl_handler, CURLOPT_POSTFIELDS, $query_string);
        curl_setopt($curl_handler, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handler, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl_handler);
        curl_close($curl_handler);

        $json = json_decode($response, true);
        return $json;
    }

    private function generateRandomString($length = 10, $key = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        return substr(
            str_shuffle(
                str_repeat(
                    $x = $key,
                    ceil($length/strlen($x))
                )
            ),
            1,
            $length
        );
    }
}
