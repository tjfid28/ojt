<footer class="page-footer blue darken-3" >
    <div class="container">
          <div class="row">
              <div class="col l6 s12">
                  <h5 class="white-text" >STI COLLEGE NOVALICHES</h5>
                  <p class="grey-text text-lighten-4">AIM HIGH WITH STI.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                  <ul>
                      <li><a class="grey-text text-lighten-3" href="https://www.facebook.com/novaliches.sti.edu"><i class="fa fa-facebook-official"></i> FACEBOOK</a></li>
                      <li><a class="grey-text text-lighten-3" href="https://twitter.com/stinovaliches"><i class="fa fa-twitter-square"></i> TWITTER</a></li>
                  </ul>
              </div>
          </div>
      </div>
      <div class="footer-copyright yellow accent-2">
        <div class="container blue-text text-darken-4">
          <b>© <?php echo date("Y"); ?> Copyright Text</b>
          <a class="blue-text text-darken-4 right" href="https://stinovaliches.edu.ph">STI COLLEGE NOVALICHES</a>
        </div>
    </div>
</footer>