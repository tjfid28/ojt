<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?= !isset($title) ? 'OJT STI College Novaliches' : $title ?></title>

    <link rel="icon" type="image/png" href="<?= base_url('resources/images/sti_logo.png'); ?>" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="<?= base_url('assets/css/mdb.min.css') ?>" rel="stylesheet">

    <!-- Template styles -->
    <link href="<?= base_url('assets/css/front.style.css') ?>" rel="stylesheet">
</head>

<body>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="<?= base_url('resources/images/sti-logo.png') ?>" height="60px"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="Home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Ojt_policy">OJT Policy</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Partner_companies">Partner Companies</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Faqs">FAQ's</a>
                    </li>
                </ul>
                <a class="login-link hoverable" href="Log_in">Sign In</a>
            </div>
        </div>
    </nav>
    <!--/.Navbar-->

    <!--Carousel Wrapper-->
    <div id="carousel-example-3" class="carousel slide carousel-fade white-text" data-ride="carousel">
        <!--Indicators-->
        <ol class="carousel-indicators">
            <?php for ($i=0; $i < count($slides); $i++) : ?>
                <li data-target="#carousel-example-3" data-slide-to="<?= $i ?>" class="<?= $i === 0 ? 'active' : '' ?>"></li>
            <?php endfor; ?>
        </ol>
        <!--/.Indicators-->

        <!--Slides-->
        <div class="carousel-inner" role="listbox">
            <?php foreach ($slides as $key => $slide) : ?>
                <div class="carousel-item view hm-black-light <?= $key === 0 ? 'active' : '' ?>" style="background-image: url(<?= base_url("resources/slide-images/{$slide->s_slug_name}"); ?>); background-repeat: no-repeat; background-size: cover;">

                    <!-- Caption -->
                    <div class="full-bg-img flex-center white-text">
                        <ul class="animated fadeInUp col-md-12">
                            <li>
                                <h1 class="h1-responsive flex-item font-bold"><?= $slide->s_caption; ?></h1>
                            </li>
                            <li>
                                <p class="flex-item"><?= $slide->s_description; ?></p>
                            </li>
                            </li>
                        </ul>
                    </div>
                    <!-- /.Caption -->
                </div>
            <?php endforeach; ?>
        </div>
        <!--/.Slides-->

        <!--Controls-->
        <a class="carousel-control-prev" href="#carousel-example-3" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-example-3" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->

    <!-- Main Container -->
    <div class="container">
        <div class="divider-new wow fadeIn" data-wow-delay="0.2s">
            <h2 class="h2-responsive">Partner Companies</h2>
        </div>

        <!--Section: About-->
        <section class="wow fadeIn" data-wow-delay="0.2s">
            <?php if (!$companies) : ?>
                <h4>No Partner Companies found</h4>
            <?php else : ?>
                <?php foreach ($companies as $key => $company) : ?>
                    <div class="col-md-3">
                        <div class="card card-body text-center">
                            <h4 class="card-title"><?= $company->co_name ?></h4>
                            <p>
                                <a href="<?= $company->co_link ?>" target="_blank">
                                    <img src="<?= base_url("resources/partner_companies/{$company->co_image_name}") ?>">
                                </a>
                            </p>
                        </div>
                    </div>
<?php if ($key === 3) {
    break;
} ?>
                <?php endforeach; ?>

                <?php if (count($companies) > 4) : ?>
                    <a href="Partner_companies">View All</a>
                <?php endif ?>
            <?php endif ?>
        </section>
        <!--Section: About-->
    </div>
    <!--/ Main Container -->
    <div class="pt-5"></div>
    <!--/.Main layout-->

    <!--Footer-->
    <footer class="page-footer center-on-small-only mt-0">
        <!--Footer links-->
        <div class="container-fluid">
            <div class="row">
                <!--First column-->
                <div class="col-lg-3 col-md-6 ml-auto">
                    <h5 class="title font-bold mt-3 mb-4">ABOUT US</h5>
                    <p class="">
                        One of the best and reputable institutions. The contents of our page are the property of STI Novaliches and/or its partners, contributors or site administrators and are protected by STI and local copyright laws.
                    </p>
                </div>
                <!--/.First column-->
                <hr class="w-100 clearfix d-sm-none">
                <!--Second column-->
                <div class="col-lg-2 col-md-6 ml-auto">
                    <h5 class="title font-bold mt-3 mb-4">LINKS</h5>
                    <ul>
                        <li><a href="https://www.sti.edu/">STI Official Website</a></li>
                        <li><a href="https://stinovaliches.edu.ph">Campus Official Website</a></li>
                        <li><a href="http://elms.sti.edu/">ELMS</a></li>
                    </ul>
                </div>
                <!--/.Second column-->
                <hr class="w-100 clearfix d-sm-none">
                <!--Third column-->
                <div class="col-lg-3 col-md-6 ml-auto">
                    <h5 class="title font-bold mt-3 mb-4">Find us on</h5>
                    <ul>
                        <li>
                            <h2>
                                <a href="https://www.facebook.com/novaliches.sti.edu"><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/stinovaliches"><i class="fa fa-twitter icon-4x"></i></a>
                            </h2>
                        </li>
                    </ul>
                </div>
                <!--/.Third column-->
            </div>
        </div>
        <!--/.Footer links-->

        <!--Copyright-->
        <div class="footer-copyright">
            <div class="container">
                <p class="text-center">
                     © <?= date('Y') ?> Copyright: STI Education Services Group, Inc.
                </p>
            </div>
        </div>
        <!--/.Copyright-->

    </footer>
    <!--/.Footer-->


    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/jquery.min.js') ?>"></script>

    <!-- Bootstrap dropdown -->
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/popper.min.js') ?>"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/bootstrap.min.js') ?>"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/mdb.min.js') ?>"></script>

    <script>
    new WOW().init();
    </script>

</body>

</html>
