<html lang="en">
    <?php
        $last = $this->uri->total_segments();
        $Page_Name = $this->uri->segment($last);
        $Page_Name = str_replace("%20", " ", $Page_Name);
        if ($Page_Name==null||strcmp($Page_Name, "Home")==0) {
            $Page_Name = "STI College Novaliches" ;
        }
    ?>
    <title><?php echo strtoupper(str_replace("_", " ", $Page_Name)); ?></title>
    <head>
      <link rel="icon" type="image/png" href="<?php echo base_url('resources/images/sti_logo.png'); ?>" />
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.material.min.css">
      <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/css/materialize.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/sweetalert.css');?>">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/master.css');?>">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

      <!--Import JS-->
      <span class="base_url" data-base-url="<?php echo base_url(); ?>"></span>
      <script type="text/javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      <script src="<?php echo base_url('assets/js/plugins/sweetalert.min.js');?>"></script>
      <script src="<?php echo base_url('assets/js/plugins/moment.js');?>"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.material.min.js"></script>

      <!-- Defaults -->
      <script type="text/javascript" src="<?php echo base_url('assets/js/home.js');?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/js/log_in.js');?>"></script>
      <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery.countTo.js');?>"></script>
      <script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>

      <?php if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==1): ?>
        <!-- ManyChat -->
        <script src="//widget.manychat.com/242423062959019.js" async="async">
        </script>
      <?php endif ?>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body id="main_body" class="main_body" name="main_body">
        <?php $this->load->view('layout/navbar_view'); ?>
        <?php echo $content; ?>
        <?php $this->load->view('layout/footer_view'); ?>
    </body>

    <?php $this->load->view('front-end/home/partials/log_in_form_view.php'); ?>

    <input type="hidden" id="Page_Name" value="<?php echo str_replace(" ", "_", $Page_Name); ?>" style="display:none;" readonly/>
    <script>
        $("#"+$("#Page_Name").val()).addClass("active blue darken-3");
        $("."+$("#Page_Name").val()).addClass("active");
    </script>
</html>
