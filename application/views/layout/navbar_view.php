<?php
    if ($this->session->userdata('logged_in')) {
        $dire="resources/slide-images/";
        $images = glob($dire. '*.{jpg,jpeg,png,gif}', GLOB_BRACE);
        shuffle($images);
        $randomImage=array_pop($images);
    }
?>

<?php if (!$this->session->userdata('logged_in')) {
    ?>
	<style>
		.side-nav .active{
			background-color: #e0e0e0 !important;
			color: white;
		}
		.nav-wrapper {
			border-bottom:2px solid #ffff00;
		}
	</style>
 			<!-- If not logged in -->
			<nav class="nav-extended fixed  blue darken-3" >
					<div class="nav-wrapper">
						<a class="brand-logo hide-on-small-only" style="margin-left:80px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:85px;height:54px;"/></a>
						<a class="brand-logo hide-on-med-and-up" style="margin-left:80px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:80px;height:50px;"/></a>
						<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
						<ul id="nav-mobile" class="right hide-on-med-and-down" style="padding-right:50px;">
							<li class="STI_College_Novaliches waves-effect waves-default"><a href="Home"><i class="fa fa-home"></i> HOME</a></li>
							<li class="Ojt_policy waves-effect waves-default"><a href="Ojt_policy"><i class="fa fa-info-circle"></i> OJT POLICY</a></li>
							<li class="Partner_companies waves-effect waves-default"><a href="Partner_companies"><i class="fa fa-building"></i> PARTNER COMPANIES</a></li>
							<li class="Faqs waves-effect waves-default"><a href="Faqs"><i class="fa fa-question"></i> FAQS</a></li>
							<li class="Log_in waves-effect waves-default"><a data-target="log_in_form"><i class="fa fa-sign-in"></i> LOG IN</a></li>

						</ul>
						<ul class="side-nav" id="mobile-demo">
							<li class="tab disabled" style="margin-left:30px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:85px;"/>
						</li>
							<li id="STI_College_Novaliches"><a href="Home"><i class="fa fa-home"></i> HOME</a></li>
							<li id="Ojt_policy"><a href="Ojt_policy"><i class="fa fa-info-circle"></i> OJT POLICY</a></li>
							<li id="Partner_companies"><a href="Partner_companies"><i class="fa fa-building"></i> PARTNER COMPANIES</a></li>
							<li id="Faqs"><a href="Faqs"><i class="fa fa-question"></i> FAQS</a></li>
							<li id="Log_in"><a data-target="log_in_form"><i class="fa fa-sign-in"></i> LOG IN</a></li>
						</ul>
					</div>
			</nav>
<?php
} elseif ($this->session->userdata('account_type')==4) {
        ?>
			<style>
			header, main, footer {
			      padding-left: 300px;
			}

			@media only screen and (max-width : 992px) {
			  header, main, footer {
			    padding-left: 0;
			  }
			}
			.collapsible-body > .active{
				background-color: #607d8b !important;
				color: white;
			}
			</style>
			<!-- If Administrator -->
			<nav class="nav-extended">
					<div class="nav-wrapper blue darken-3">
						<a class="brand-logo hide-on-small-only" style="margin-left:80px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:85px;height:64px;"/></a>
						<a class="brand-logo hide-on-med-and-up" style="margin-left:80px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:80px;height:56px;"/></a>
						<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
						<ul id="nav-mobile" class="right hide-on-med-and-down" style="padding-right:50px;">
							<li id="user-title"><span><i class="fa fa-database"></i> SUB ADMINISTRATION</span></li>
							<li id="Log_out"><a href="<?php echo base_url('Master_Controller/Log_out'); ?>" ><i class="fa fa-sign-out"></i> LOG OUT</a></li>
						</ul>
						<ul class="side-nav fixed  grey lighten-4" id="mobile-demo">
							<li class="tab disabled" style="margin-left:30px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:85px;"/>
						</li>
							<li class="dashboard"><a href="<?php echo base_url('Sub_Administration/dashboard'); ?>"><i class="fa fa-dashboard"></i> DASHBOARD</a></li>
							<?php if (isset($_SESSION['s_news'])&&$_SESSION['s_news']==1) {
            ?>
								<li class="news_management"><a href="<?php echo base_url('Sub_Administration/news_management') ?>"><i class="fa fa-newspaper-o"></i>  NEWS</a></li>
							<?php
        } ?>

							<?php if (isset($_SESSION['s_class_list'])&&$_SESSION['s_class_list']==1) {
            ?>
								<li class="class_list"><a href="<?php echo base_url('Sub_Administration/class_list'); ?>"><i class="fa fa-book"></i>  CLASS LISTS</a></li>
							<?php
        } ?>
							<li class="no-padding">
					        <?php if (isset($_SESSION['s_student'])&&$_SESSION['s_student']==1||isset($_SESSION['s_faculty'])&&$_SESSION['s_faculty']==1) {
            ?>
						        <ul class="collapsible collapsible-accordion">
						          <li>
						            <a class="collapsible-header sub_administrator_management administrator_management faculty_management student_management"  style="padding-left:27px;"><i class="fa fa-users" style="font-size:15px;"></i>USER MANAGEMENT <b class="pull-right">&#9660;</b></a>
						            <div class="collapsible-body">
						              <ul>
						              	<?php if (isset($_SESSION['s_faculty'])&&$_SESSION['s_faculty']==1) {
                ?>
						                	<li id="faculty_management"><a href="<?php echo base_url('Sub_Administration/faculty_management'); ?>">&#8226; FACULTY MEMBERS</a></li>
						                <?php
            } ?>
						                <?php if (isset($_SESSION['s_student'])&&$_SESSION['s_student']==1) {
                ?>
						                	<li id="student_management"><a href="<?php echo base_url('Sub_Administration/student_management'); ?>">&#8226; STUDENTS</a></li>
						                <?php
            } ?>
						              </ul>
						            </div>
						          </li>
						        </ul>
					        <?php
        } ?>
					        <?php if (isset($_SESSION['s_departments'])&&$_SESSION['s_departments']==1||isset($_SESSION['s_courses'])&&$_SESSION['s_courses']==1||isset($_SESSION['s_programs'])&&$_SESSION['s_programs']==1||isset($_SESSION['s_sections'])&&$_SESSION['s_sections']==1) {
            ?>
						        <ul class="collapsible collapsible-accordion">
						          <li>
						            <a class="collapsible-header department_management course_management program_management section_management" style="padding-left:27px;"><i class="fa fa-university" style="font-size:15px;"></i> SCHOOL MANAGEMENT <b class="pull-right">&#9660;</b></a>
						            <div class="collapsible-body">
						              <ul>
						              	<?php if (isset($_SESSION['s_courses'])&&$_SESSION['s_courses']==1) {
                ?>
						              		<li id="course_management"><a href="<?php echo base_url('Sub_Administration/course_management'); ?>">&#8226; COURSES</a></li>
						              	<?php
            } ?>
						              	<?php if (isset($_SESSION['s_departments'])&&$_SESSION['s_departments']==1) {
                ?>
						              		<li id="department_management"><a href="<?php echo base_url('Sub_Administration/department_management'); ?>">&#8226; DEPARTMENTS</a></li>
						              	<?php
            } ?>
						              	<?php if (isset($_SESSION['s_programs'])&&$_SESSION['s_programs']==1) {
                ?>
						                	<li id="program_management"><a href="<?php echo base_url('Sub_Administration/program_management'); ?>">&#8226; PROGRAMS</a></li>
						                <?php
            } ?>
						                <?php if (isset($_SESSION['s_sections'])&&$_SESSION['s_sections']==1) {
                ?>
						                	<li id="section_management"><a href="<?php echo base_url('Sub_Administration/section_management'); ?>">&#8226; SECTIONS</a></li>
						                <?php
            } ?>
						              </ul>
						            </div>
						          </li>
						        </ul>
					        <?php
        } ?>
					      </li>
					      <?php if (isset($_SESSION['s_ojt_requirement'])&&$_SESSION['s_ojt_requirement']==1||isset($_SESSION['s_uploaded_documents'])&&$_SESSION['s_uploaded_documents']==1||isset($_SESSION['s_downloadable_forms'])&&$_SESSION['s_downloadable_forms']==1||isset($_SESSION['s_ojt_policy'])&&$_SESSION['s_ojt_policy']==1) {
            ?>
						      <ul class="collapsible collapsible-accordion">
						          <li>
						            <a class="collapsible-header ojt_requirements downloadable_forms ojt_policy uploaded_documents" style="padding-left:27px;"><i class="fa fa-suitcase" style="font-size:15px;"></i>OJT MANAGEMENT <b class="pull-right">&#9660;</b></a>
						            <div class="collapsible-body">
						              <ul>
						              	<?php if (isset($_SESSION['s_uploaded_documents'])&&$_SESSION['s_uploaded_documents']==1) {
                ?>
						                	<li id="uploaded_documents" ><a href="<?php echo base_url('Sub_Administration/uploaded_documents'); ?>">&#8226; UPLOADED DOCUMENTS</a></li>
						                <?php
            } ?>
						                <?php if (isset($_SESSION['s_ojt_requirement'])&&$_SESSION['s_ojt_requirement']==1) {
                ?>
						              		<li id="ojt_requirements" ><a href="<?php echo base_url('Sub_Administration/ojt_requirements'); ?>">&#8226; OJT REQUIREMENTS</a></li>
						              	<?php
            } ?>
						              	<?php if (isset($_SESSION['s_downloadable_forms'])&&$_SESSION['s_downloadable_forms']==1) {
                ?>
						                	<li id="downloadable_forms" ><a href="<?php echo base_url('Sub_Administration/downloadable_forms'); ?>">&#8226; DOWNLOADABLE FORMS</a></li>
						                <?php
            } ?>
						                <?php if (isset($_SESSION['s_ojt_policy'])&&$_SESSION['s_ojt_policy']==1) {
                ?>
						                	<li id="ojt_policy"><a href="<?php echo base_url('Sub_Administration/ojt_policy'); ?>">&#8226; OJT POLICIES</a></li>
						                <?php
            } ?>
						              </ul>
						            </div>
						          </li>
					        	</ul>
					      <?php
        } ?>
					      <?php if (isset($_SESSION['s_company'])&&$_SESSION['s_company']==1) {
            ?>
					      	<li class="partner_companies"><a href="<?php echo base_url('Sub_Administration/partner_companies'); ?>"><i class="fa fa-building"></i> PARTNER COMPANIES</a></li>
					      <?php
        } ?>
					      <?php if (isset($_SESSION['s_slides'])&&$_SESSION['s_slides']==1) {
            ?>
					      	<li class="slides_management"><a href="<?php echo base_url('Sub_Administration/slides_management'); ?>"><i class="fa fa-sliders"></i> SLIDES</a></li>
					      <?php
        } ?>
						<?php if (isset($_SESSION['s_faqs'])&&$_SESSION['s_faqs']==1): ?>
							<li class="faqs">
								<a href="<?php echo base_url('Sub_Administration/faqs'); ?>"><i class="fa fa-question"></i> FAQ's</a>
							</li>
						<?php endif ?>

					      <li class="Log_out"><a href="<?php echo base_url('Master_Controller/Log_out'); ?>"><i class="fa fa-sign-out"></i> LOG OUT</a></li>
					</div>
			</nav>
<?php
    } elseif ($this->session->userdata('account_type')==3) {
        ?>
			<style>
			header, main, footer {
			      padding-left: 300px;
			}

			@media only screen and (max-width : 992px) {
			  header, main, footer {
			    padding-left: 0;
			  }
			}
			.collapsible-body > .active{
				background-color: #607d8b !important;
				color: white;
			}
			</style>
			<!-- If Administrator -->
			<nav class="nav-extended">
					<div class="nav-wrapper blue darken-3">
						<a class="brand-logo hide-on-small-only" style="margin-left:80px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:85px;height:64px;"/></a>
						<a class="brand-logo hide-on-med-and-up" style="margin-left:80px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:80px;height:56px;"/></a>
						<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
						<ul id="nav-mobile" class="right hide-on-med-and-down" style="padding-right:50px;">
							<li id="user-title"><span><i class="fa fa-database"></i> ADMINISTRATION</span></li>
							<li id="Log_out"><a href="<?php echo base_url('Master_Controller/Log_out'); ?>" ><i class="fa fa-sign-out"></i> LOG OUT</a></li>
						</ul>
						<ul class="side-nav fixed  grey lighten-4" id="mobile-demo">
							<li class="tab disabled" style="margin-left:30px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:85px;"/>
						</li>
							<li class="dashboard"><a href="<?php echo base_url('Administration/dashboard'); ?>"><i class="fa fa-dashboard"></i> DASHBOARD</a></li>

							<li class="news_management"><a href="<?php echo base_url('Administration/news_management') ?>"><i class="fa fa-newspaper-o"></i>  NEWS</a></li>
							<li class="class_list"><a href="<?php echo base_url('Administration/class_list'); ?>"><i class="fa fa-book"></i>  CLASS LISTS</a></li>
							<li class="no-padding">
					        <ul class="collapsible collapsible-accordion">
					          <li>
					            <a class="collapsible-header sub_administrator_management administrator_management faculty_management student_management " style="padding-left:27px;"><i class="fa fa-users" style="font-size:15px;"></i>USER MANAGEMENT<b class="pull-right">&#9660;</b></a>
					            <div class="collapsible-body">
					              <ul>
					                <li id="administrator_management"><a href="<?php echo base_url('Administration/administrator_management'); ?>">&#8226; ADMINISTRATORS</a></li>
					                 <li id="sub_administrator_management"><a href="<?php echo base_url('Administration/sub_administrator_management'); ?>">&#8226; SUB ADMINISTRATORS</a></li>
					                <li id="faculty_management"><a href="<?php echo base_url('Administration/faculty_management'); ?>">&#8226; FACULTY MEMBERS</a></li>
					                <li id="student_management"><a href="<?php echo base_url('Administration/student_management'); ?>">&#8226; STUDENTS</a></li>
					              </ul>
					            </div>
					          </li>
					        </ul>
					      </li>
					      <li class="no-padding">
					        <ul class="collapsible collapsible-accordion">
					          <li>
					            <a class="collapsible-header department_management course_management program_management section_management" style="padding-left:27px;"><i class="fa fa-university" style="font-size:15px;"></i> SCHOOL MANAGEMENT<b class="pull-right">&#9660;</b></a>
					            <div class="collapsible-body">
					              <ul>
					              	<li id="course_management"><a href="<?php echo base_url('Administration/course_management'); ?>">&#8226; COURSES</a></li>
					              	<li id="department_management"><a href="<?php echo base_url('Administration/department_management'); ?>">&#8226; DEPARTMENTS</a></li>
					                <li id="program_management"><a href="<?php echo base_url('Administration/program_management'); ?>">&#8226; PROGRAMS</a></li>
					                <li id="section_management"><a href="<?php echo base_url('Administration/section_management'); ?>">&#8226; SECTIONS</a></li>
					              </ul>
					            </div>
					          </li>
					        </ul>
					      </li>
					      <li class="no-padding">
					      <ul class="collapsible collapsible-accordion">
					          <li>
					            <a class="collapsible-header ojt_requirements downloadable_forms ojt_policy uploaded_documents" style="padding-left:27px;"><i class="fa fa-suitcase" style="font-size:15px;"></i>OJT MANAGEMENT<b class="pull-right">&#9660;</b></a>
					            <div class="collapsible-body">
					              <ul>
					                <li id="uploaded_documents" ><a href="<?php echo base_url('Administration/uploaded_documents'); ?>">&#8226; UPLOADED DOCUMENTS</a></li>
					              	<li id="ojt_requirements" ><a href="<?php echo base_url('Administration/ojt_requirements'); ?>">&#8226; OJT REQUIREMENTS</a></li>
					              	<!-- <li><a href="#!">&#8226; UPLOADED REQUIREMENTS</a></li> -->
					                <li id="downloadable_forms" ><a href="<?php echo base_url('Administration/downloadable_forms'); ?>">&#8226; DOWNLOADABLE FORMS</a></li>
					                <li id="ojt_policy"><a href="<?php echo base_url('Administration/ojt_policy'); ?>">&#8226; OJT POLICIES</a></li>
					              </ul>
					            </div>
					          </li>
					        </ul>
					      </li>
					      <li class="partner_companies"><a href="<?php echo base_url('Administration/partner_companies'); ?>"><i class="fa fa-building"></i> PARTNER COMPANIES</a></li>
					      <li class="slides_management"><a href="<?php echo base_url('Administration/slides_management'); ?>"><i class="fa fa-sliders"></i> SLIDES</a></li>
					      <li class="faqs">
							<a href="<?php echo base_url('Administration/faqs'); ?>"><i class="fa fa-question"></i> FAQ's</a>
						  </li>
					      <li class="audit_log"><a href="<?= base_url('Administration/audit_log') ?>"><i class="fa fa-book"></i> AUDIT LOG</a></li>
					      <li class="Log_out"><a href="<?php echo base_url('Master_Controller/Log_out'); ?>"><i class="fa fa-sign-out"></i> LOG OUT</a></li>
					</div>
			</nav>

<?php
    } elseif ($this->session->userdata('account_type')==2) {
        ?>
			<style>
			header, main, footer {
			      padding-left: 300px;
			      padding-top: 60px;
			}

			@media only screen and (max-width : 992px) {
			  header, main, footer {
			    padding-left: 0;
			  }
			}
			.collapsible-body > .active{
				background-color: #607d8b !important;
				color: white;
			}
			</style>
			<!-- If Faculty -->
			<nav class="nav-extended fixed" style="position:fixed;top:0px;z-index:100;">
					<div class="nav-wrapper blue darken-3">
						<a class="brand-logo hide-on-small-only" style="margin-left:-200px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:85px;height:64px;"/></a>
						<a class="brand-logo hide-on-med-and-up" style="margin-left:-50px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:80px;height:56px;"/></a>
						<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
						<ul id="nav-mobile" class="right" style="padding-right:20px;">

							<li class="hide-on-small-only"><a class="dropdown-button" href="#!" data-activates="dropdown" data-beloworigin="true" ><span class="btn btn-floating pulse red note-number" style="display:none;width:20px;height:20px;line-height:22px;font-size:10px;position:absolute;top:14px;margin-left:10px;"></span> <i class="fa fa-globe"></i></a></li>
							<li class="hide-on-med-and-up"><a href="<?php echo base_url('faculty/notifications'); ?>"><span class="btn btn-floating pulse red note-number" style="display:none;width:20px;height:20px;line-height:22px;font-size:10px;position:absolute;top:6px;margin-left:10px;"></span> <i class="fa fa-globe" style="padding-top:15px;"></i></a></li>

							<li id="Log_out" class="hide-on-med-and-down"><a href="<?php echo base_url('Master_Controller/Log_out'); ?>" ><i class="fa fa-sign-out"></i> LOG OUT</a></li>
						</ul>
					</div>
			</nav>
			<ul class="side-nav fixed  grey lighten-4" id="mobile-demo" >
				<li>
					<div class="user-view" style="margin-bottom:40px;">
						<div class="background">
					        <img src="<?php echo base_url().$randomImage; ?>" style="width:100%;height:100%;">
					    </div>
					      	<img class="materialboxed z-depth-1" style="border:5px solid white;position:relative;top:40px;left:75px;width:85px;height:85px;object-fit:cover;" src="<?php echo base_url(); ?>resources/faculty_images/<?php echo $_SESSION['fa_image_name']; ?>">
					</div>
					<a href="<?php echo base_url('faculty/profile'); ?>" class="grey lighten-4" style="text-align:center;"><span class="black-text name"><?php echo strtoupper($_SESSION['fa_fname']." ".$_SESSION['fa_lname']); ?></span></a>
				</li>
				<li><div class="divider"></div></li>
				<li class="class_list"><a href="<?php echo base_url('faculty/class_list'); ?>"><i class="fa fa-list-alt"></i> CLASS LIST</a></li>
				<li class="news"><a href="<?php echo base_url('faculty/news'); ?>"><i class="fa fa-newspaper-o"></i> NEWS</a></li>
				<li class="downloadable_forms"><a href="<?php echo base_url('faculty/downloadable_forms'); ?>"><i class="fa fa-wpforms"></i> DOWNLOADABLE FORMS</a></li>
		      	<li class="Log_out"><a href="<?php echo base_url('Master_Controller/Log_out'); ?>"><i class="fa fa-sign-out"></i> LOG OUT</a></li>
			</ul>

			<ul id="dropdown" class="dropdown-content collection notification_list">
				<li style="background-color:transparent !important;">
					<center><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Loading...</center>
				</li>
			</ul>

			<script>
				var title = $('title').html();
				get_notifications();
				function get_notifications(){
					$.ajax({
				          url      : main_url+'front-end/Notification_Controller/get_notifications',
				          type     : 'GET',
				          dataType : 'JSON',
				          cache    : false,
				          success: function(data){
				             var notification;
				             var counter   = 0;
				             var count_new = 0;
				             var notification = "";
				             notification += "<li class='collection-item' style='height:60px !important;min-height: 50px !important;'><a style='height:50px !important;margin-top: -28px !important' href='<?php echo base_url(); ?>faculty/notifications'><center><i class='fa fa-history'></i> See History</center></a></li>";
				             for(counter;counter<data.length;counter++)
				             {
				             	var nid  = encode(data[counter]['n_id']);
				             	var link = encode(data[counter]['n_link']);
				             	var image_link_split = data[counter]['profileimage'].split('/');
				             	var image = image_link_split[2]!=null&&image_link_split[2]!=''? image_link_split[2] : "no-image.png";
				             	var assemble_link    = image_link_split[0]+"/"+image_link_split[1]+"/"+image;
				             	notification += "<li class='collection-item avatar history_note'>";
				             	notification += "<a style='font-size:11px;' href='<?php echo base_url(); ?>front-end/Notification_Controller/seen/"+link+"/"+nid+"'>";
				             	notification += "<img src='<?php echo base_url(); ?>"+assemble_link+"'  class='circle'>";
				             	notification += "<span class='title' style='font-size:15px;'>"+data[counter]['name']+"</span>";
				             	notification += "<p class='truncate' style='font-size:13px;'>"+data[counter]['n_content']+"</p>";
				             	notification += "<span style='color:#9e9e9e;'>"+moment(data[counter]['n_created_at']).fromNow()+"</span>";
				             	notification += "</a>";
				             	notification += "</li>";
				             	count_new += 1;
				             }
				             if(count_new!=0)
				             {
				             	$('.note-number').html(count_new).show();
				             	$('title').html('').html("("+count_new+") "+title);
				             }
				             $('.notification_list').html(notification);
				             setTimeout(function(){ get_notifications(); },5000);
				          }
				    });
				}
			</script>
<?php
    } else {
        ?>
			<style>
			header, main, footer {
			      padding-left: 300px;
			      padding-top: 65px;
			}

			@media only screen and (max-width : 992px) {
			  header, main, footer {
			    padding-left: 0;
			  }
			}
			.collapsible-body > .active{
				background-color: #607d8b !important;
				color: white;
			}
			</style>
			<!-- If Student -->
			<nav class="nav-extended fixed" style="position:fixed;top:0px;z-index:100;">
					<div class="nav-wrapper blue darken-3">
						<a class="brand-logo hide-on-small-only" style="margin-left:-200px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:85px;height:64px;"/></a>
						<a class="brand-logo hide-on-med-and-up" style="margin-left:-50px;"><img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:80px;height:56px;"/></a>
						<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
						<ul id="nav-mobile" class="right" style="padding-right:20px;">

							<li class="hide-on-small-only"><a class="dropdown-button" href="#!" data-activates="dropdown" data-beloworigin="true" ><span class="btn btn-floating pulse red note-number" style="display:none;width:20px;height:20px;line-height:22px;font-size:10px;position:absolute;top:14px;margin-left:10px;"></span> <i class="fa fa-globe"></i></a></li>
							<li class="hide-on-med-and-up"><a href="<?php echo base_url('student/notifications'); ?>"><span class="btn btn-floating pulse red note-number" style="display:none;width:20px;height:20px;line-height:22px;font-size:10px;position:absolute;top:6px;margin-left:10px;"></span> <i class="fa fa-globe" style="padding-top:15px;"></i></a></li>

							<li id="Log_out" class="hide-on-med-and-down"><a href="<?php echo base_url('Master_Controller/Log_out'); ?>" ><i class="fa fa-sign-out"></i> LOG OUT</a></li>
						</ul>
					</div>
			</nav>
			<ul class="side-nav grey fixed lighten-4" id="mobile-demo">
				<li>
					<div class="user-view" style="margin-bottom:40px;">
						<div class="background yellow accent-3">
					        <img src="<?php echo base_url().$randomImage; ?>" style="width:100%;height:100%;">
					    </div>
					      <img class="materialboxed z-depth-1" style="border:5px solid white;position:relative;top:40px;left:75px;width:85px;height:85px;object-fit:cover;" src="<?php echo base_url(); ?>resources/student_images/<?php echo $_SESSION['st_image_name']; ?>">
					</div>
					<a href="<?php echo base_url('student/profile'); ?>" class="grey lighten-4" style="text-align:center;"><span class="black-text name"><?php echo strtoupper($_SESSION['st_fname']." ".$_SESSION['st_lname']); ?></span></a>
				</li>
				<li><div class="divider"></div></li>
				<li class="class_room"><a href="<?php echo base_url('student/class_room'); ?>"><i class="fa fa-feed"></i> CLASS ROOM</a></li>
				<li class="news"><a href="<?php echo base_url('student/news'); ?>"><i class="fa fa-newspaper-o"></i> NEWS</a></li>
				<li class="my_documents"><a href="<?php echo base_url('student/my_documents'); ?>"><i class="fa fa-folder"></i> MY DOCUMENTS</a></li>
				<li class="requirements_list"><a href="<?php echo base_url('student/requirements_list'); ?>"><i class="fa fa-list-alt"></i> REQUIREMENTS LIST</a></li>
		      	<li class="downloadable_forms"><a href="<?php echo base_url('student/downloadable_forms'); ?>"><i class="fa fa-wpforms"></i> DOWNLOADABLE FORMS</a></li>
		      	<li class="companies"><a href="<?php echo base_url('student/companies'); ?>"><i class="fa fa-building"></i> COMPANIES</a></li>
				<li class="Log_out"><a href="<?php echo base_url('Master_Controller/Log_out'); ?>"><i class="fa fa-sign-out"></i> LOG OUT</a></li>
			</ul>

			<ul id="dropdown" class="dropdown-content collection notification_list">
				<li style="background-color:transparent !important;">
					<center><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Loading...</center>
				</li>
			</ul>

			<script>
				var title = $('title').html();
				get_notifications();
				function get_notifications(){
					$.ajax({
				          url      : main_url+'front-end/Notification_Controller/get_notifications',
				          type     : 'GET',
				          dataType : 'JSON',
				          cache    : false,
				          success: function(data){
				             var notification;
				             var counter   = 0;
				             var count_new = 0;
				             var notification = "";
				             notification += "<li class='collection-item' style='height:60px !important;min-height: 50px !important;'><a style='height:50px !important;margin-top: -28px !important' href='<?php echo base_url('student/notifications'); ?>'><center><i class='fa fa-history'></i> See History</center></a></li>";
				             for(counter;counter<data.length;counter++)
				             {
				             	var nid  = encode(data[counter]['n_id']);
				             	var link = encode(data[counter]['n_link']);
				             	var image_link_split = data[counter]['profileimage'].split('/');
				             	var assemble_link    = image_link_split[0]+"/"+image_link_split[1]+"/"+(image_link_split[2]!=null||image_link_split[2]!=''? image_link_split[2] : "no-image.png");
				             	notification += "<li class='collection-item avatar history_note'>";
				             	notification += "<a style='font-size:11px;' href='<?php echo base_url(); ?>front-end/Notification_Controller/seen/"+link+"/"+nid+"'>";
				             	notification += "<img src='<?php echo base_url(); ?>"+assemble_link+"'  class='circle'>";
				             	notification += "<span class='title' style='font-size:15px;'>"+data[counter]['name']+"</span>";
				             	notification += "<p class='truncate' style='font-size:13px;'>"+data[counter]['n_content']+"</p>";
				             	notification += "<span style='color:#9e9e9e;'>"+moment(data[counter]['n_created_at']).fromNow()+"</span>";
				             	notification += "</a>";
				             	notification += "</li>";
				             	count_new += 1;
				             }
				             if(count_new!=0)
				             {
				             	$('.note-number').html(count_new).show();
				             	$('title').html('').html("("+count_new+") "+title);
				             }
				             $('.notification_list').html(notification);
				             setTimeout(function(){ get_notifications(); },5000);
				          }
				    });
				}
			</script>

<?php
    }

?>
