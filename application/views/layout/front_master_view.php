<!DOCTYPE html>
<html lang="en" class="<?= is_null($this->uri->segment(1)) || $this->uri->segment(1) == 'Home' ?: 'full-height'  ?>">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?= !isset($title) ? 'OJT STI College Novaliches' : $title ?></title>

    <link rel="icon" type="image/png" href="<?= base_url('resources/images/sti_logo.png'); ?>" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="<?= base_url('assets/css/mdb.min.css') ?>" rel="stylesheet">

    <!-- SweetAlert CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/sweetalert.css');?>">

    <!-- Template styles -->
    <link href="<?= base_url('assets/css/front.style.css') ?>" rel="stylesheet">

    <!-- Custom Styling -->
    <?php if (!is_null($this->uri->segment(1)) || !$this->uri->segment(1) == 'Home') : ?>
        <style>
            .intro-1 {
                <?php if ($this->uri->segment(1) == 'Partner_companies') : ?>
                    background: url(<?= base_url('assets/img/bg/partner-companies.jpg') ?>) no-repeat center center;
                <?php elseif ($this->uri->segment(1) == 'Ojt_policy') : ?>
                    background: url(<?= base_url("resources/images/{$policy['op_image']}") ?>) no-repeat center center;
                <?php else : ?>
                    background: #fff;
                <?php endif ?>

                background-size: cover;
            }
        </style>
    <?php endif; ?>
</head>

<body>
    <!--Navbar-->
    <?php $this->load->view('pages/partials/navbar_view'); ?>
    <!--/.Navbar-->

    <!--Content-->
    <?= $content ?>
    <!--/.Content-->

    <!--Footer-->
    <?php $this->load->view('pages/partials/footer_view'); ?>
    <!--/.Footer-->

    <!--Modal-->
    <?php $this->load->view('pages/partials/login_modal_view'); ?>
    <!--/.Modal-->


    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/jquery.min.js') ?>"></script>

    <!-- Bootstrap dropdown -->
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/popper.min.js') ?>"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/bootstrap.min.js') ?>"></script>

    <!-- SweetAlert -->
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/sweetalert.min.js') ?>"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/mdb.min.js') ?>"></script>

    <!-- Login JS -->
    <script type="text/javascript" src="<?= base_url('assets/js/pages/login.js') ?>"></script>

    <script>
        new WOW().init();
    </script>

</body>

</html>
