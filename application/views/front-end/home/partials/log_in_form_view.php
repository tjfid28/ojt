<div id="log_in_form" class="modal" >
    <div class="modal-content">
      <center>
      	<h5 class="text-center"><i class="fa fa-sign-in"></i> LOG IN</h5>
      </center>
      	<form id="form_login" class="form_login" name="form_login">
		      <div class="row">
				    <div class="input-field col s12 m12 l12">
				      <i class="material-icons prefix">person</i>
				      <input id="uname" name="uname" type="text" class="validate">
				      <label for="uname">Username</label>
				    </div>
				    <div class="input-field col s12 m12 l12">
				      <i class="material-icons prefix">lock</i>
				      <input id="pword" name="pword" type="password" class="validate">
				      <label for="pword">Password</label>
				    </div>
				    <br>
				    <center>
				    	<button class="waves-effect waves-light btn blue darken-4" id="btn_login" type="submit" >LOG IN</button>
				    </center>
			  </div>
		</form>
    </div>
</div>


