<html>
<head>
	<head>
	<title>STI COLLEGE NOVALICHES</title>
	  <link rel="icon" type="image/png" href="<?php echo base_url(); ?>resources/images/sti_logo.png" />
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/css/materialize.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/sweetalert.css');?>">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/master.css');?>">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

      <!--Import JS-->
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	  <script src="<?php echo base_url('assets/js/plugins/sweetalert.min.js');?>"></script>
	  <script src="<?php echo base_url('assets/js/plugins/moment.js');?>"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script>

	  <!-- Defaults -->
    <span class="base_url" data-base-url="<?php echo base_url(); ?>"></span>
	  <script type="text/javascript" src="<?php echo base_url('assets/js/home.js');?>"></script>
	</head>
</head>
<body  class="blue darken-4">

<main style="margin-top:13%;">
  <br>
  <center>
  	<img src="<?php echo base_url(); ?>resources/images/sti_logo.png" style="width:30%;border-radius:10px;"/>
  </center>
  <br>
  <div class="card-panel" style="margin:10px;">
  	  <center>
  		<h5>LOG IN</h5>
	  </center>
	  	<form id="form_login" class="form_login" name="form_login">
		      <div class="row">
				    <div class="input-field col s12 m12 l12">
				      <i class="material-icons prefix">person</i>
				      <input id="uname" name="uname" type="text" class="validate">
				      <label for="uname">Username</label>
				    </div>
				    <div class="input-field col s12 m12 l12">
				      <i class="material-icons prefix">lock</i>
				      <input id="pword" name="pword" type="password" class="validate">
				      <label for="pword">Password</label>
				    </div>
				    <br>
				    <center>
				    	<button class="waves-effect waves-light btn blue darken-4" id="btn_login" type="submit" ><i class="fa fa-sign-in"></i> LOG IN</button>
				    </center>
			  </div>
		</form>
  </div>
</main>

<script type="text/javascript">
	$(document).on('submit', '#form_login', function(e) {
    e.preventDefault();
    var uname = $('#uname').val();
    var pword = $('#pword').val();
    if (uname != "" && pword != "") {
        $.ajax({
            url: 'Log_in_Controller/Log_in_function',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(xhr) {
                $('#btn_login').prop('disabled', true);
                $('#btn_login').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Processing');
            },
            success: function(data) {
                $('#btn_login').prop('disabled', false);
                if (data != "false" && data != "No Class") {
                    window.location.href = data;
                } else if (data == "No Class") {
                    swal("Failed!", "Your account does exist but not yet assigned to a section!", "warning")
                } else {
                    swal("Failed!", "Wrong username & password!", "warning")
                }
                $('#btn_login').html("LOG IN");
            }
        });
    } else {
        swal("Failed!", "Please fill all the fields!", "warning")
    }
})
</script>
</body>
</html>