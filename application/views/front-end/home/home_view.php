<style>
.carousel{
height:600px !important;
}
.filler{
  height:88%;
}
@media only screen and (max-width : 992px) {
  .filler{
    height: 82%
  }
  .slider .slides li img {
    background-size:100%;
    background-repeat: no-repeat;
    background-color: black;
  }
  .caption h3,.caption h3{
    font-size: 15px;
  }
  .slider{
    height: 250px !important;
  }
  .slider .slides img{
    margin-top:-125px !important;
    object-fit:cover;
  }
}

</style>
<!-- <div class="filler"></div> -->
<div class="slider" style="z-index:-1;">
    <ul class="slides" >
      <?php $position = ['left','top','right'];?>
      <?php foreach ($slides->result() as $slide) {
    ?>
      <li>
          <div>
            <img src="<?php echo base_url('resources/slide-images').'/'.$slide->s_slug_name; ?>">
            <div style="background: rgba(0, 0, 0, 0.5);position:absolute;left:0px;top:0px;height:100%;width:100%;z-index:1;"></div>
            <div class="caption <?php echo $position[rand(0, 2)] ?>-align" style="z-index:2;">
              <h3 style="opacity:1;"><?php echo $slide->s_caption; ?></h3>
              <h5 style="opacity:1;" class="light white-text text-lighten-3"><?php echo $slide->s_description; ?></h5>
            </div>
          </div>

      </li>
      <?php
} ?>
    </ul>
</div>

<script type="text/javascript">
  $('.slides').attr('style','height:450px !important;');
</script>
