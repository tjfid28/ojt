<main>
    <div class="container">
        <h5><i class='fa fa-question'></i> Frequently Asked Questions</h5><hr>
        <div class="container-fluid" style="margin: 0 10px; padding-bottom:10%;">
            <?php if ($faqs->result()) : ?>
                    <?php foreach ($faqs->result() as $faq) : ?>
                    <ul class="collection with-header">
                        <li class="collection-header blue-text text-darken-3"><h5><?php echo $faq->fa_title; ?></h5></li>
                        <li class="collection-item"><?php echo $faq->fa_description; ?></li>
                    </ul>
                    <?php endforeach; ?>
            <?php else : ?>
                <div class="row center">
                    <h5>No FAQ's found</h5>
                </div>
            <?php endif; ?>
        </div>
    </div>
</main>
