<style>
	.ojt_policy_parallax{
		margin-left:-1px;
		height:350px;
		width: 100%;
	}
	.ojt_policy_parallax .parallax img{
		
	}
	@media only screen and (max-width: 900px)
	{
		.ojt_policy_parallax .parallax img{
			width: 100%;
			height: 130%;
		}
		.ojt_policy_parallax{
			height:250px !important;
		}
	}
	@media(max-width: 500px)
	{
		.ojt_policy_parallax{
			height:130px !important;
		}
	}
</style>
<main>
	<div class="parallax-container ojt_policy_parallax" >
      <div class="parallax"><img src="<?php echo base_url('resources/images/');?><?php echo $policy['op_image']; ?>"></div>
    </div>
	<div class="container">
		<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:10%;">
			<?php echo $policy['op_content']; ?>
		</div>
	</div>
</main>