<main>
	<div class="container">
		<h5><i class='fa fa-building'></i> PARTNER COMPANIES</h5><hr>
		<div class="container-fluid" style="margin-left:150px;margin-right:150px;padding-bottom:10%;">
			<?php foreach ($companies->result() as $company) {
    ?>
				 <div class="row">
			        <div class="card horizontal">
				      <div class="card-image">
				        <a href="<?= $company->co_link ?>" target="_blank">
				        	<img src="<?php echo base_url(); ?>resources/partner_companies/<?php echo $company->co_image_name; ?>">
			        	</a>
				      </div>
				      <div class="card-stacked">
				        <div class="card-content">
				          <b><a href="<?= $company->co_link ?>" target="_blank"><?php echo $company->co_name; ?></a></b>
				          <p><?php echo $company->co_description; ?></p>
				        </div>
				      </div>
				    </div>
			      </div>
			<?php
}?>
		</div>
	</div>
</main>
