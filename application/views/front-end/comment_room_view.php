<main data-identifier="<?php echo $cla_ann_id; ?>">
	<div class="container-fluid" style="margin-left:-1px;padding-bottom:20%;">
	    
	    <div class="row">
	    	<div class='col s12 m12 l12'>
			    <div class="card-panel white align-left">
			    	<div style="padding:10px;">
				    	<table class='post-image-name-holder'>
				    		<tr>
				    			<td rowspan='2' class='post-image-holder'><img src='<?php echo base_url("resources/faculty_images/").$class_announcement["fa_image_name"];?>' class='circle' style='width:50px;height:50px;'></td>
				    			<td class='name-holder'><span><?php echo $class_announcement['fa_fname']." ".$class_announcement['fa_lname']; ?></span></td>
				    		</tr>
				    		<tr>
				    			<td class='time-holder grey-text'><span><?php echo $class_announcement['ann_date_posted']; ?></span></td>
				    		</tr>	
				    	</table>
				    	<pre class='news-content'><?php echo $class_announcement['ann_post']; ?></pre>
				    	<div class="icon-posting">
				    		<span id='comment_count'></span> <i class='fa fa-comments'></i>
				    	</div>
			    	</div>
			    	<br>
			    </div>
			    <div class="card-panel white align-left">
			    	<form id="form-comment" class="form-comment" name="form-comment" data-bind="<?php echo $_SESSION['account_type']==2 ? $_SESSION['fa_id'] : $_SESSION['st_id']; ?>">
				        <div class="input-field">
				          <textarea id="comment_field" name="comment_field" form="form-comment" class="materialize-textarea" required></textarea>
				          <label for="comment_field"><i class="fa fa-comment"></i> Write a comment here</label>
				        </div>
				        <div class="right-align">
				        	<button type="submit" class="waves-effect waves-light btn blue darken-3 btn_comment"><i class="fa fa-comment"></i> COMMENT</button>
				      	</div>
				    </form>

				    <div class="comments_viewer" id="comments_viewer" name="comments_viewer" style="margin-left:10px;margin-right:10px;">
	    			</div>
			    </div>
			</div>
	    </div>
	</div>
	<?php $this->load->view('back_view'); ?>
</main>

<?php if(isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) { ?>
	<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/faculty/faculty_comment.js');?>"></script>
<?php } ?>
<?php if(isset($_SESSION['account_type'])&&$_SESSION['account_type']==1) { ?>
	<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/student/student_comment.js');?>"></script>
<?php } ?>