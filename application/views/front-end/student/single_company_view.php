<main>
    <div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:30%;">
        <p><br></p>

        <div class="row">
            <div class="col m12">
                <div class="card horizontal">
                    <div class="card-image">
                        <img src="<?= base_url("resources/partner_companies/{$company->co_image_name}") ?>">
                    </div>

                    <div class="card-stacked">
                        <div class="card-content">
                            <h1 class="card-title"><?= $company->co_name ?></h1>
                            <p><?= $company->co_description ?></p>
                        </div>

                        <div class="card-action">
                            <?php if ($_SESSION['st_email'] && $company->co_email && $hasResume) : ?>
                                <a id="btn-send-resume" class="waves-effect waves-light btn" data-co-email="<?= $company->co_email ?>" data-d-id="<?= $hasResume->d_id ?>">
                                    <i class="material-icons left">send</i> Send Resume
                                </a>
                            <?php endif ?>

                            <a class="waves-effect waves-light btn blue" href="<?= $company->co_link ?>" target="_blank">
                                <i class="material-icons left">pageview</i> View Website
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/student/single_company.js');?>"></script>
