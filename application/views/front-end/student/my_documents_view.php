<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
	   <h5><i class="fa fa-folder"></i> MY DOCUMENTS</h5>
	   <hr>
	    <div>
			<form>
				<div class="row">
					<div class="input-field col s12 m5 l5">
			          <input id="search" name="search" type="text" class="search">
			          <label for="search">SEARCH DOCUMENT</label>
			        </div>
				</div>
			</form>
		</div>
		<div class="documents_viewer" id="documents_viewer" name="documents_viewer">
			<center style="padding-top:50px;" id="document-list">

			</center>
		</div>
	</div>

	<div class="fixed-action-btn fab-adjust">
	    <a class="btn-floating btn-large waves-effect waves-light btn blue accent-4">
	      <i class="large material-icons">menu</i>
	    </a>
	    <ul>
	      <li><a class="front-toggle btn-floating red tooltipped toggle"  data-position="left" data-delay="20" data-tooltip="Delete document" ><i class="material-icons">delete</i></a></li>
	      <li><a class="btn-floating blue tooltipped" href="#upload_document" data-position="left" data-delay="20" data-tooltip="Upload new document"><i class="material-icons">note_add</i></a></li>
	    </ul>
    </div>


</main>

<div id="upload_document" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-file"></i> UPLOAD DOCUMENT</h5>
      <br>
      <form class="form-upload-document" name="form-upload-document" id="form-upload-document" enctype="multipart/form-data">
      		<div class="row">
      			<label>Maximum size of 20MB per file</label>
      			<div class="file-field input-field">
				      <div class="btn blue darken-3 waves-effect waves-light">
					        <span>File</span>
					        <input type="file" form="form-upload-document" id="document" name="document" accept='.doc, .docx, .xlsx, .pdf' required>
				      </div>
				      <div class="file-path-wrapper">
				        	<input class="file-path validate" type="text" >
				      </div>
			    </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3" id="btn-upload"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>


<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/student/my_documents.js');?>"></script>
