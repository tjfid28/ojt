<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:10%;">
	   <div class="row">
	   		<div class="col s12 right-align">
	   			<img class="materialboxed z-depth-1 hide-on-small-only" style="position:relative;top:40px;left:70%;border:5px solid white;width:185px;height:185px;object-fit:cover;" src="<?php echo base_url(); ?>resources/student_images/<?php echo $_SESSION['st_image_name']; ?>">
	   			<img class="materialboxed z-depth-1 hide-on-med-and-up" style="position:relative;top:40px;left:10%;border:5px solid white;width:100px;height:100px;object-fit:cover;" src="<?php echo base_url(); ?>resources/student_images/<?php echo $_SESSION['st_image_name']; ?>">  		
	   		</div>
			<div class="col s12 m12 l12" >
				<div class="card-panel" style="margin-top:-70px;">
					<div class="hide-on-med-and-up" style="padding-top:100px;"></div>
					<div class="row">
						<h6><b>Student Information</b></h6>
						<h6><?php echo $_SESSION['st_number']; ?></h6>
						<h5><?php echo $_SESSION['st_fname']." ".$_SESSION['st_lname']; ?></h5>
						<h6><?php echo $_SESSION['cou_title']; ?></h6>
						<h6><?php echo $_SESSION['sec_name']; ?></h6>
						<br>
							<div class="card-panel grey lighten-5">
							<div class="right-align">
								<b><i class="fa fa-lock"></i> Access Information</b>
							</div>
								<form class="form-profile" id="form-profile" name="form-profile">
							      <div class="row">
							        <div class="input-field col s12">
							          <i class="material-icons prefix ">account_circle</i>
							          <input type="text" id="uname" name="uname" type="text" class="validate" value="<?php echo $_SESSION['st_uname']; ?>">
							          <label for="uname" class="active">Username</label>
							        </div>
							        <div class="input-field col s12 l6">
							          <i class="material-icons prefix ">lock</i>
							          <input type="password" id="old_pword" name="old_pword" type="tel" class="validate" required>
							          <label for="old_pword">Old Password</label>
							        </div>
							        <div class="input-field col s12 l6">
							          <input type="password" id="new_pword" name="new_pword" type="tel" class="validate" required>
							          <label for="new_pword">New Password</label>
							        </div>
							      </div>
									<center>
										<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-pencil"></i> Save</button>
									</center>
							    </form>
							</div>
							<?php if($_SESSION['fb_id']==''||$_SESSION['fb_id']==null) {?>
								<div class='fb_div'>
									<iframe src = "https://fbnotification.herokuapp.com/index.php?log_in_form" width="100%" height="50" id="facebook_frame" name="facebook_frame" style="border:none;margin-left:-10px;">
									   Sorry your browser does not support inline frames.
									</iframe>
									<button class="waves-effect waves-light btn blue darken-3" onclick="integrate();">Integrate your account with facebook</button>	
								</div>
							<?php } ?>
					</div>
				</div>
			</div>
	  </div>
	</div>
</main>

<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/student/profile.js');?>"></script>