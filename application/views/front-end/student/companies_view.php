<main>
    <div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:40%;">
        <h5><i class="fa fa-building"></i> COMPANIES</h5>

        <hr>

        <div class="row">
            <div class="col m12 company_viewer">
                <table id="data_viewer" class='mdl-data-table'>
                    <thead class='grey lighten-2'>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Website</th>
                    </thead>

                    <tbody>
                        <?php if ($companies): ?>
                            <?php foreach ($companies as $key => $company) : ?>
                                <tr>
                                    <td>
                                        <a href="company/<?= $company->slug ?>">
                                            <img src="<?= base_url("resources/partner_companies/{$company->co_image_name}") ?>"
                                            style='object-fit:cover;width:120px;height:70px;' >
                                        </a>
                                    </td>
                                    <td><?= $company->co_name ?></td>
                                    <td><?= $company->co_type == 1 ? 'ICT' : 'HM' ?></td>
                                    <td>
                                        <a href="<?= $company->co_link ? $company->co_link : '#!'  ?>" target="_blank">
                                            <?= $company->co_link ? $company->co_link : '#!'  ?>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <tr>
                                <td colspan="4">No Company</td>
                            </tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<script>
    $(function() {
        $('#data_viewer').DataTable();
    });
</script>
