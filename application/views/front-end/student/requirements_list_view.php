<main>
	<style>
	.tabs .indicator {
		background-color:white ;
	}
    .active{
		font-weight: bold;
	}
	.checkbox-orange[type="checkbox"].filled-in:checked + label:after{
     border: 2px solid #0d47a1;
     background-color: #0d47a1;
	}
	</style>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:50%;">
	   <h5><i class="fa fa-list-alt"></i> REQUIREMENTS LIST</h5>
	   <hr>
	   <div class="row">
		   <div class="col s12" >
		      <ul class="tabs z-depth-1 blue darken-3" style="overflow-x:hidden;">
		        <li class="tab col s6"><a class="white-text" href="#requirement_panel">My Requirements</a></li>
		        <li class="tab col s6"><a class="white-text" href="#checklist">My Check List</a></li>
		      </ul>
		    </div>
		    <div id="requirement_panel">
		    	<form style="margin-top:20px;">
					<div class="row">
						<div class="col s12">
							<br>
						</div>
						<div class="input-field col s12 m5 l5">
				          <input id="search" name="search" type="text" class="search">
				          <label for="search" style='font-weight:normal;'>SEARCH DOCUMENT</label>
				        </div>
					</div>
				</form>
			   <div class="requirement_viewer" name="requirement_viewer" id="requirement_viewer">
			   </div>
		    </div>
		   <div class="col s12 checklist" name="checklist" id="checklist">
		   		<div class="row card-panel">
			      <div class="col s12 m12 l12" >
			          <form>
				          <?php foreach ($checklists->result() as $checklist) {
    ?>
				      		    <p class="truncate">
							      <input class="checklist filled-in checkbox-orange" type="checkbox" value="<?php echo $checklist->req_list_id ?>" id="req_type_<?php echo $checklist->req_list_id ?>" <?php echo $checklist->cl_id != null ? 'checked' : ''; ?> disabled/>
							      <label for="req_type_<?php echo $checklist->req_list_id ?>" > <?php echo $checklist->req_name ?></label>
							      <!-- <br>
							      <label><?php echo $checklist->cl_date_checked!=null ?  "Date checked : ".$checklist->cl_date_checked : ""; ?> </label> -->
							    </p>
					      <?php
}
                          ?>
					  </form>
			      </div>
			    </div>
		   </div>
	   </div>
	</div>

	<div class="fixed-action-btn">
	    <a class="btn-floating btn-large waves-effect waves-light btn blue accent-4">
	      <i class="large material-icons">menu</i>
	    </a>
	    <ul>
	      <li><a class="front-toggle btn-floating red tooltipped toggle"  data-position="left" data-delay="20" data-tooltip="Remove requirement" ><i class="material-icons">remove</i></a></li>
	      <li><a class="btn-floating blue tooltipped" href="#upload_from_documents" data-position="left" data-delay="20" data-tooltip="Get from my documents"><i class="material-icons">insert_drive_file</i></a></li>
	      <li><a class="btn-floating teal tooltipped" href="#upload_requirement" data-position="left" data-delay="20" data-tooltip="Upload from device"><i class="material-icons">computer</i></a></li>
	    </ul>
    </div>

</main>

<!-- From Documents -->
<div id="upload_from_documents" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-file"></i> MY DOCUMENTS LIST</h5>
      <br>
      <form class="form-upload-from-documents" name="form-upload-from-documents" id="form-upload-from-documents" enctype="multipart/form-data">
      		<div class="row">
      			<div class="input-field col s12 m6 l6">
				    <select id="d_id" name="d_id" class="select" required>
					      <option value="" disabled selected>Choose document</option>
					      <?php foreach ($documents->result() as $document) {
                              ?>
					      		<option value="<?php echo $document->d_id ?>"><?php echo $document->d_file_name; ?></option>
					      <?php
                          }
                          ?>
				    </select>
				    <label>My documents</label>
				</div>
			    <div class="input-field col s12 m6 l6">
				    <select id="req_id" name="req_id" class="select" required>
					      <option value="" disabled selected>Choose requirement type</option>
					      <?php foreach ($requirements->result() as $requirement) {
                              ?>
					      		<option value="<?php echo $requirement->req_id ?>"><?php echo $requirement->req_name; ?></option>
					      <?php
                          }
                          ?>
				    </select>
				    <label>Requirement type</label>
				</div>
	    	</div>

	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3" id="btn-document" style="z-index:0;"><i class="fa fa-send"></i> SUBMIT</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>
<!-- From Device -->
<div id="upload_requirement" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-file"></i> UPLOAD REQUIREMENT</h5>
      <br>
      <form class="form-upload-requirement" name="form-upload-requirement" id="form-upload-requirement" enctype="multipart/form-data">
      		<label>Maximum size of 20MB per file</label>
      		<div class="row">
      			<div class="file-field input-field col s12 m6 l6">
				      <div class="btn blue darken-3 waves-effect waves-light">
					        <span>File</span>
					        <input type="file" form="form-upload-requirement" id="requirement" name="requirement" accept='.doc, .docx, .xlsx, .pdf' required>
				      </div>
				      <div class="file-path-wrapper">
				        	<input class="file-path validate" type="text" >
				      </div>
			    </div>
			    <div class="input-field col s12 m6 l6">
				    <select id="requirement_req_id" name="requirement_req_id" class="select" required>
					      <option value="" disabled selected>Choose requirement type</option>
					      <?php foreach ($requirements->result() as $requirement) {
                              ?>
					      		<option value="<?php echo $requirement->req_id ?>"><?php echo $requirement->req_name; ?></option>
					      <?php
                          }
                          ?>
				    </select>
				    <label>Requirement type</label>
				</div>
	    	</div>

	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3" id="btn-upload" style="z-index:0;"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/student/requirements_list.js');?>"></script>
