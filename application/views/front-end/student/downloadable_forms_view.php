<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:50%;">
		<h5><i class="fa fa-wpforms"></i> DOWNLOADABLE FORMS</h5>
		<hr>
		<div class="forms_viewer" name="forms_viewer" id="forms_viewer">

		</div>
	</div>
</main>

<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/downloadable_forms.js');?>"></script>