<main>
	<div class="container-fluid" style="padding-left:20px;padding-right:20px;padding-bottom:30%;">
	   <h5><i class="fa fa-newspaper-o"></i> NEWS</h5>
	   <hr>
	   <div id="news_viewer" name="news_viewer" class="news_viewer" >
	   </div>
	</div>
</main>

<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/news.js');?>"></script>
