<main>
	<div class="container-fluid" style="margin-left:-1px;padding-bottom:20%;margin-top:-10px;">
	   <div class="parallax-container">
	      <div class="parallax grey"><img src="<?php echo base_url(); ?>resources/class_images/<?php echo $result['cla_slug']; ?>" ></div>
	    	<div class="right white-text" >
           		<h4 style="background-color:rgba(0, 0, 0, 0.5);padding:20 20 20 20;"><?php echo $result['sec_name']; ?></h4>
    		</div>
	    </div>
	    <?php if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
    ?>
		    <div class="row" style="margin:10px;">
		      <div class="col s12 m12 l12">
		        <div class="card-panel white">
		          <form id="form-announcement" class="form-announcement" name="form-announcement">
                    <div class="file-field input-field">
                          <div class="btn blue darken-3 waves-effect waves-light">
                                <span>Attachment</span>
                                <input type="file" form="form-announcement" id="ann_attachment" name="ann_attachment[]" multiple>
                          </div>
                          <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" >
                          </div>
                    </div>

			        <div class="input-field">
			          <textarea id="announcement_field" name="announcement_field" form="form-announcement" class="materialize-textarea" required></textarea>
			          <label for="announcement_field"><i class="fa fa-edit"></i> Write an announcement here </label>
			        </div>
			        <div class="right-align">
			        	<button type="submit" class="waves-effect waves-light btn blue darken-3 announcement_btn"><i class="fa fa-bullhorn"></i> POST</button>
			      	</div>
			      </form>
		        </div>
		      </div>
		    </div>
	    <?php
} ?>
	    <div class="class_announcement_viewer" id="class_announcement_viewer" name="class_announcement_viewer" style="margin-left:20px;margin-right:20px;">
	    </div>
	</div>
</main>

<?php if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
        ?>
	<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/class_room.js'); ?>"></script>
<?php
    } ?>
<?php if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==1) {
        ?>
	<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/student/class_room_student.js'); ?>"></script>
<?php
    } ?>
