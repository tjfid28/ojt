<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:30%;">
		<h5><i class="fa fa-list-alt"></i> CLASS LIST</h5>
		<hr>
		<div class="class_list_viewer" name="class_list_viewer" id="class_list_viewer">

		</div>
	</div>
</main>

<div id="upload_cover" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-file"></i> UPLOAD COVER PHOTO</h5>
      <br>
      <form class="form-upload-cover" name="form-upload-cover" id="form-upload-cover" enctype="multipart/form-data">
      		<div class="row">
      			<input type="hidden" class="cla_id" name="cla_id" id="cla_id" readonly required>
      			<div class="file-field input-field">
				      <div class="btn blue darken-3 waves-effect waves-light">
					        <span>File</span>
					        <input type="file" form="form-upload-cover" id="cover" accept='.jpeg, .jpg, .png' name="cover" required>
				      </div>
				      <div class="file-path-wrapper">
				        	<input class="file-path validate" type="text" >
				      </div>
			    </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<div id="preview_report" class="modal preview_modal" style="width:90%;min-height:80%;">
    <div class="modal-content">
      <h5><i class="fa fa-eye"></i> PREVIEW REPORT</h5>
      <hr>
      <form class="form-upload-cover" name="form-upload-cover" id="form-upload-cover" enctype="multipart/form-data">
      		<div class="preview_report_view" id="preview_report_view" name="preview_report_view">

      		</div>

      		<hr>
	    	<center>
		       	<a class="waves-effect waves-light btn blue darken-3" id="download_reports" name="download_reports"><i class="fa fa-save"></i> DOWNLOAD</a>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/faculty/class_list.js');?>"></script>
