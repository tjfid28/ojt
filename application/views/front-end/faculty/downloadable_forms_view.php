<main>
    <div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:50%;">
        <h5><i class="fa fa-wpforms"></i> DOWNLOADABLE FORMS</h5>
        <hr>
        <div class="forms_viewer" name="forms_viewer" id="forms_viewer">

        </div>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large blue modal-trigger waves-effect waves-light" href="#add_form">
              <i class="large material-icons">add</i>
            </a>
        </div>
    </div>
</main>

<div id="add_form" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> NEW FORM</h5>
      <br>
      <form class="form-add-form" name="form-add-form" id="form-add-form" enctype="multipart/form-data">
            <div class="row">
                <div class="file-field input-field">
                      <div class="btn blue darken-3 waves-effect waves-light">
                            <span>File</span>
                            <input type="file" form="form-add-form" id="document" name="document" accept='.doc, .docx, .xlsx, .pdf' required>
                      </div>
                      <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" >
                      </div>
                </div>
                <div class="input-field col s12 m12 l12">
                    <select id="req_id" name="req_id" class="select" required>
                          <option value="" disabled selected>Choose requirement type</option>
                          <?php foreach ($ojt_requirements->result() as $ojt_requirement) {
    ?>
                                <option value="<?php echo $ojt_requirement->req_id ?>"><?php echo $ojt_requirement->req_name; ?></option>
                          <?php
}
                          ?>
                    </select>
                    <label>Requirement Type</label>
                </div>
            </div>
            <center>
                <button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
            </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/downloadable_forms.js');?>"></script>
