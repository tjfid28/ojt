<main>
	<style>
	.tabs .indicator {
		background-color:white ;
	}
    .active{
		font-weight: bold;
	}
	.dropdown-content li > a, .dropdown-content li > span {
    color: #0d47a1 !important;
	}
	.checkbox-orange[type="checkbox"].filled-in:checked + label:after{
     border: 2px solid #0d47a1;
     background-color: #0d47a1;
	}	
	</style>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5><i class="fa fa-user"></i> <?php echo $result['st_lname'];?>'s information</h5>
		<hr>
		<div class="row">
			    <div class="col s12" >
			      <ul class="tabs z-depth-1 blue darken-3" style="overflow-x:hidden;">
			      	<li class="tab col s4"><a class="white-text" href="#student-profile"><?php echo $result['st_lname'];?>'s Profile</a></li>
			        <li class="tab col s4"><a class="white-text active" href="#requirement">Requirements</a></li>
			        <li class="tab col s4"><a class="white-text" href="#checklist">Check List</a></li>
			      </ul>
			    </div>
			    <div class="col 12" id="student-profile" style="width:100%;">
					<div class="row card-panel">
						<div class="col s12 m3 l2">
								<img class="materialboxed z-depth-1" style="border:5px solid white;width:170px;height:170px;object-fit:cover;position:relative;left:7px;" src="<?php echo base_url(); ?>resources/student_images/<?php echo $result['st_image_name']!=null&&$result['st_image_name']!='' ? $result['st_image_name'] : 'no-image.png'; ?>"> 
						</div>
						<div class="col s12 m9 l10 right-align" style="font-size:15px;">
							<h4><b><?php echo $result['st_lname']; ?>, <?php echo $result['st_fname']; ?> <?php echo $result['st_mname'];?></b></h4>
							<br><br>
							<b>Student No . <?php echo $result['st_number']; ?></b>
							<br>
							<b><?php echo $result['cou_title']; ?> </b>
						</div>
					</div>
				</div>
				<!-- requirement -->
				<div id="requirement" >
					<div class="row">
				      <div class="col s12 m12">
				        <div class="card-panel">
				        	<label>Sort by type</label>
						    <select class="by_type" id="by_type" name="by_type">
						      <option value="" disabled selected>Choose type</option>
						      <option value="0">All</option>
						      <?php foreach ($requirements->result() as $requirement)
							  {
						      ?>
						      		<option class="black-text" value="<?php echo $requirement->req_id ?>"><?php echo $requirement->req_name; ?></option>
						      <?php
						      	}
						      ?>
						    </select>
				        </div>
				      </div>
					</div>
					<div name="requirement_list_viewer" id="requirement_list_viewer" class="row requirement_list_viewer" style="margin-top:-18px;">
							<!-- document content -->
					</div>
				</div>
				<!-- end requirement -->
				<div id="checklist" class="col s12" style="width:100%;">
			    	<div class="row card-panel">
				      <div class="col s12 m12 l12" >
				          <form>
					          <?php foreach ($checklists->result() as $checklist)
							  {
						      ?>
					      		    <p>
								      <input class="checklist filled-in checkbox-orange" type="checkbox" value="<?php echo $checklist->req_list_id ?>" id="req_type_<?php echo $checklist->req_list_id ?>" <?php echo $checklist->cl_id != null ? 'checked' : ''; ?> />
								      <label for="req_type_<?php echo $checklist->req_list_id ?>" > <?php echo $checklist->req_name ?></label>
								      <!-- <br>
								      <label><?php echo $checklist->cl_date_checked!=null ?  "Date checked : ".$checklist->cl_date_checked : ""; ?> </label> -->
								    </p>	
						      <?php
						      }
						      ?>
						  </form>
				      </div>
				    </div>
			    </div>
		 </div>
	</div>
	<?php $this->load->view('back_view'); ?>
</main>

<div id="checker" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-file"></i> <span id="c_file_name"></span></h5>
      <br>
      <form class="form-checker" name="form-checker" id="form-checker" enctype="multipart/form-data">
      		<div class="row">
      			<center>
      				<span id="sr_id" > </span>
      				<span style="font-size:20px;">
      					What action do you want to do in this <b id="c_file_type"></b> ? 
      				</span>
      			</center>
	    	</div>
	    	<center>
	    		<button type="button" class="waves-effect waves-light btn download" ><i class="fa fa-download"></i> Download</button>
		       	<button type="button" class="waves-effect waves-light btn red darken-3 decision" value="Reject" ><i class="fa fa-remove"></i> Reject</button>
		    	<button type="button" class="waves-effect waves-light btn blue darken-3 decision" value="Approve"><i class="fa fa-check"></i> Approve</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/faculty/student_info.js');?>"></script>
