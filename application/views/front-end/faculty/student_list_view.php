<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:50%;">
		<h5><i class="fa fa-group"></i> <?php echo $result['sec_name']; ?> STUDENT LIST</h5>
		<hr>
		<div>
			<form>
				<div class="row">
					<div class="input-field col s12 m5 l5">
			          <input id="search" name="search" type="text" class="search">
			          <label for="search">SEARCH STUDENT</label>
			        </div>
				</div>
			</form>
		</div>
		<div class="student_list_viewer" name="student_list_viewer" id="student_list_viewer">
			
		</div>
		
	</div>
</main>

<script type="text/javascript" src="<?php echo base_url('assets/js/front-end/faculty/student_list.js');?>"></script>