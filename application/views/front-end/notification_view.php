<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:30%;">
	   <h5><i class="fa fa-history"></i> Notifications</h5>
	   <hr>
	   <div class="new_notification_panel" style='display:none;'>
	   	  <h6><b>New Notifications</b></h6>
		   <ul id="new_notification" name="new_notification" class="new_notification collection" style="margin-left:10px;margin-right:10px;background-color:white;">
		   	<li style="background-color:transparent !important;">
				<center><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Loading...</center>
			</li>
		   </ul>
	   </div>
	   <h6><b>Old Notifications</b></h6>
	   <ul id="notification_history_list" name="notification_history_list" class="notification_history_list collection" style="margin-left:10px;margin-right:10px;background-color:white;">
	   	<li style="background-color:transparent !important;">
			<center><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Loading...</center>
		</li>
	   </ul>
	</div>
</main>

<script>
	get_history();
	function get_history(){
		$.ajax({
	          url      : main_url+'front-end/Notification_Controller/get_notifications',
	          type     : 'GET',
	          dataType : 'JSON',
	          cache    : false,
	          success: function(data){
	             var notification;
	             var counter   = 0;
	             var count_new = 0;
	             var notification = "";
	             for(counter;counter<data.length;counter++)
	             {
	             	var nid  = encode(data[counter]['n_id']);
	             	var link = encode(data[counter]['n_link']);
	             	var image_link_split = data[counter]['profileimage'].split('/');
	             	var image = image_link_split[2]!=null&&image_link_split[2]!=''? image_link_split[2] : "no-image.png";
	             	var assemble_link    = image_link_split[0]+"/"+image_link_split[1]+"/"+image;
	             	notification += "<li class='collection-item avatar history_note'>";
	             	notification += "<a style='font-size:11px;' href='<?php echo base_url(); ?>front-end/Notification_Controller/seen/"+link+"/"+nid+"'>";
	             	notification += "<img src='<?php echo base_url(); ?>"+assemble_link+"'  class='circle'>";
	             	notification += "<span class='title' style='font-size:15px;'>"+data[counter]['name']+"</span>";
	             	notification += "<p class='truncate' style='font-size:13px;'>"+data[counter]['n_content']+"</p>";
	             	notification += "<span style='color:#9e9e9e;'>"+moment(data[counter]['n_created_at']).fromNow()+"</span>";
	             	notification += "</a>";
	             	notification += "</li>";
	             	count_new += 1;
	             }
	             if(count_new!=0)
	             {
	             	$('.new_notification_panel').show();
	             	$('.new_notification').html(notification);
	             }
	          }
	    });

		$.ajax({
	          url      : main_url+'front-end/Notification_Controller/get_notification_history',
	          type     : 'GET',
	          dataType : 'JSON',
	          cache    : false,
	          success: function(data){
	             var notification;
	             var counter   = 0;
	             var count_history = 0;
	             var notification = "";
	             for(counter;counter<data.length;counter++)
	             {
	             	var nid  = encode(data[counter]['n_id']);
	             	var link = encode(data[counter]['n_link']);
	             	var image_link_split = data[counter]['profileimage'].split('/');
	             	var image = image_link_split[2]!=null&&image_link_split[2]!=''? image_link_split[2] : "no-image.png";
	             	var assemble_link    = image_link_split[0]+"/"+image_link_split[1]+"/"+image;
	             	notification += "<li class='collection-item avatar history_note'>";
	             	notification += "<a style='font-size:11px;' href='<?php echo base_url(); ?>front-end/Notification_Controller/seen/"+link+"/"+nid+"'>";
	             	notification += "<img src='<?php echo base_url(); ?>"+assemble_link+"'  class='circle'>";
	             	notification += "<span class='title' style='font-size:15px;'>"+data[counter]['name']+"</span>";
	             	notification += "<p class='truncate' style='font-size:13px;'>"+data[counter]['n_content']+"</p>";
	             	notification += "<span style='color:#9e9e9e;'>"+moment(data[counter]['n_created_at']).fromNow()+"</span>";
	             	notification += "</a>";
	             	notification += "</li>";
	             	count_history += 1;
	             }
	             if(count_history!=0)
	             {
	             	$('.notification_history_list').html(notification);
	             }
	             else
	             {
	             	notification = '<li class="collection-item"  style="background-color:transparent !important;"><center><i class="fa fa-history"></i> No history</center></li>';
	             	$('.notification_history_list').html(notification);
	             }
	             setTimeout(function(){ get_history(); },5000);
	          }
	    });
	}
</script>