<section class="view intro-1 hm-black-strong">
    <div class="full-bg-img flex-center">
        <div class="container">
            <ul>
                <li>
                    <h1 class="h1-responsive font-bold wow fadeInDown" data-wow-delay="0.2s">On-the-Job-Training Policy</h1>
                </li>
                <li>
                    <p class="lead mt-4 mb-5 wow fadeInUp" data-wow-delay="0.5s"><?= $policy['op_content'] ?></p>
                </li>
            </ul>
        </div>
    </div>
</section>
