<nav class="navbar navbar-expand-lg navbar-dark fixed-top <?= is_null($this->uri->segment(1)) || $this->uri->segment(1) == 'Home' ? 'scrolling-navbar' : 'unscrolling-navbar' ?>">
    <div class="container">
        <a class="navbar-brand" href="Home"><img src="<?= base_url('resources/images/sti-logo.png') ?>" height="60px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?= is_null($this->uri->segment(1)) || $this->uri->segment(1) == 'Home' ? 'active' : '' ?>">
                    <a class="nav-link" href="Home">Home</a>
                </li>
                <li class="nav-item <?= $this->uri->segment(1) == 'Ojt_policy' ? 'active' : '' ?>">
                    <a class="nav-link" href="Ojt_policy">OJT Policy</a>
                </li>
                <li class="nav-item <?= $this->uri->segment(1) == 'Partner_companies' ? 'active' : '' ?>">
                    <a class="nav-link" href="Partner_companies">Partner Companies</a>
                </li>
                <li class="nav-item <?= $this->uri->segment(1) == 'Faqs' ? 'active' : '' ?>">
                    <a class="nav-link" href="Faqs">FAQ's</a>
                </li>
            </ul>
            <a class="login-link hoverable" data-toggle="modal" data-target="#modalLoginForm">Sign In</a>
        </div>
    </div>
</nav>
