<footer class="page-footer center-on-small-only mt-0">
    <!--Footer links-->
    <div class="container-fluid">
        <div class="row">
            <!--First column-->
            <div class="col-lg-3 col-md-6 ml-auto">
                <h5 class="title font-bold mt-3 mb-4">ABOUT US</h5>
                <p class="">
                    One of the best and reputable institutions. The contents of our page are the property of STI Novaliches and/or its partners, contributors or site administrators and are protected by STI and local copyright laws.
                </p>
            </div>
            <!--/.First column-->
            <hr class="w-100 clearfix d-sm-none">
            <!--Second column-->
            <div class="col-lg-2 col-md-6 ml-auto">
                <h5 class="title font-bold mt-3 mb-4">LINKS</h5>
                <ul>
                    <li><a href="https://www.sti.edu/" target="_blank">STI Official Website</a></li>
                    <li><a href="https://stinovaliches.edu.ph" target="_blank">Campus Official Website</a></li>
                    <li><a href="http://elms.sti.edu/" target="_blank">ELMS</a></li>
                </ul>
            </div>
            <!--/.Second column-->
            <hr class="w-100 clearfix d-sm-none">
            <!--Third column-->
            <div class="col-lg-3 col-md-6 ml-auto">
                <h5 class="title font-bold mt-3 mb-4">Find us on</h5>
                <ul>
                    <li>
                        <h2>
                            <a href="https://www.facebook.com/novaliches.sti.edu" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/stinovaliches" target="_blank"><i class="fa fa-twitter icon-4x"></i></a>
                        </h2>
                    </li>
                </ul>
            </div>
            <!--/.Third column-->
        </div>
    </div>
    <!--/.Footer links-->

    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container">
            <p class="text-center">
                 © <?= date('Y') ?> Copyright: STI Education Services Group, Inc.
            </p>
        </div>
    </div>
    <!--/.Copyright-->
</footer>
