<div class="modal fade" id="modalLoginForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content px-3">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-bold">Sign in</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-login">
                <div class="modal-body">
                    <div class="md-form">
                        <i class="fa fa-user prefix grey-text"></i>
                        <input type="text" name="uname" id="uname" class="form-control validate" required>
                        <label for="username">Username</label>
                    </div>

                    <div class="md-form">
                        <i class="fa fa-lock prefix grey-text"></i>
                        <input type="password" name="pword" id="pword" class="form-control validate" required>
                        <label for="password">Password</label>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button id="btn-login" class="btn btn-default" type="submit">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
