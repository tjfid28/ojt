<section class="view intro-1 hm-black-strong">
    <div class="full-bg-img flex-center">
        <div class="container">
            <ul>
                <li>
                    <h1 class="h1-responsive font-bold wow fadeInDown" data-wow-delay="0.2s">Partner Companies</h1>
                </li>
                <li>
                    <p class="lead mt-4 mb-5 wow fadeInUp" data-wow-delay="0.5s">Below is the list of STI partner companies</p>
                </li>
            </ul>
        </div>
    </div>
</section>

<!-- Main container -->
<div class="container">
    <section class="pt-5">
        <ul class="nav nav-tabs nav-justified">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#panel1" role="tab">ICT</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#panel2" role="tab">HM</a>
            </li>
        </ul>

        <div class="tab-content">
            <!--Panel 1-->
            <div class="tab-pane fade in show active" id="panel1" role="tabpanel">
                <br>

                <?php if (!$ict) : ?>
                    <p class="wow fadeIn text-center" data-wow-delay="0.3s">
                        No ICT Companies found.
                    </p>
                <?php else : ?>
                    <ul class="list-group">
                    <?php foreach ($ict as $key => $v) : ?>
                        <li class="list-group-item wow fadeIn" data-wow-delay="0.3s">
                            <div class="row">
                                <div class="col-md-2">
                                    <a href="<?= $v->co_link ?>" target="_blank">
                                        <img height="150" width="150" src="<?= base_url("resources/partner_companies/{$v->co_image_name}") ?>">
                                    </a>
                                </div>
                                <div class="col-md-10 verticalcenter">
                                    <p>
                                        <a href="<?= $v->co_link ?>" target="_blank">
                                            <h5><?= $v->co_name ?></h5>
                                        </a>
                                        <p class="text-justify"><?= $v->co_description; ?></p>
                                    </p>
                                </div>
                            </div>
                        </li>
                    <?php endforeach ?>
                    </ul>
                <?php endif ?>
            </div>
            <!--/.Panel 1-->
            <!--Panel 2-->
            <div class="tab-pane fade" id="panel2" role="tabpanel">
                <br>

                <?php if (!$hm) : ?>
                    <p class="wow fadeIn text-center" data-wow-delay="0.3s">
                        No HM Companies found.
                    </p>
                <?php else : ?>
                    <ul class="list-group">
                    <?php foreach ($hm as $key => $v) : ?>
                        <li class="list-group-item wow fadeIn" data-wow-delay="0.3s">
                            <div class="row">
                                <div class="col-md-2">
                                    <a href="<?= $v->co_link ?>" target="_blank">
                                        <img height="150" width="150" src="<?= base_url("resources/partner_companies/{$v->co_image_name}") ?>">
                                    </a>
                                </div>
                                <div class="col-md-10 verticalcenter">
                                    <p>
                                        <a href="<?= $v->co_link ?>" target="_blank">
                                            <h5><?= $v->co_name ?></h5>
                                        </a>
                                        <p class="text-justify"><?= $v->co_description; ?></p>
                                    </p>
                                </div>
                            </div>
                        </li>
                    <?php endforeach ?>
                    </ul>
                <?php endif ?>
            </div>
            <!--/.Panel 2-->
        </div>
    </section>
</div>
<!-- ./Main container -->

<div class="pt-5"></div>
