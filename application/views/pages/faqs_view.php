<section class="view intro-1">
    <div class="container pt-5">
        <div class="pt-5 divider-new wow fadeIn" data-wow-delay="0.2s">
            <h2 class="h2-responsive">Frequently Asked Questions</h2>
        </div>

        <?php if ($faqs) : ?>
            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                <?php foreach ($faqs as $key => $faq) : ?>
                    <div class="card wow fadeIn" data-wow-delay="0.5s">
                        <!-- Card header -->
                        <div class="card-header" role="tab" id="heading<?= $key; ?>">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $key; ?>" aria-expanded="false" aria-controls="collapse<?= $key; ?>">
                                <h5 class="mb-0">
                                    <?= $faq->fa_title; ?> <i class="fa fa-angle-down rotate-icon"></i>
                                </h5>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapse<?= $key; ?>" class="collapse" role="tabpanel" aria-labelledby="heading<?= $key; ?>">
                            <div class="card-body">
                                <?= $faq->fa_description; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        <?php else : ?>
            <h4 class="h4-responsive text-center wow fadeIn" data-wow-delay="0.5s">No Frequently Asked Questions found</h3>
        <?php endif ?>
    </div>
</section>
