<!--Carousel Wrapper-->
<div id="carousel-example-3" class="carousel slide carousel-fade white-text" data-ride="carousel">
    <!--Indicators-->
    <ol class="carousel-indicators">
        <?php for ($i=0; $i < count($slides); $i++) : ?>
            <li data-target="#carousel-example-3" data-slide-to="<?= $i ?>" class="<?= $i === 0 ? 'active' : '' ?>"></li>
        <?php endfor; ?>
    </ol>
    <!--/.Indicators-->

    <!--Slides-->
    <div class="carousel-inner" role="listbox">
        <?php foreach ($slides as $key => $slide) : ?>
            <div class="carousel-item view hm-black-light <?= $key === 0 ? 'active' : '' ?>" style="background-image: url(<?= base_url("resources/slide-images/{$slide->s_slug_name}"); ?>); background-repeat: no-repeat; background-size: cover;">

                <!-- Caption -->
                <div class="full-bg-img flex-center white-text">
                    <ul class="animated fadeInUp col-md-12">
                        <li>
                            <h1 class="h1-responsive flex-item font-bold"><?= $slide->s_caption; ?></h1>
                        </li>
                        <li>
                            <p class="flex-item"><?= $slide->s_description; ?></p>
                        </li>
                        </li>
                    </ul>
                </div>
                <!-- /.Caption -->
            </div>
        <?php endforeach; ?>
    </div>
    <!--/.Slides-->

    <!--Controls-->
    <a class="carousel-control-prev" href="#carousel-example-3" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel-example-3" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->

<!-- Main Container -->
<div class="container">
    <div class="divider-new wow fadeIn" data-wow-delay="0.2s">
        <h2 class="h2-responsive">Partner Companies</h2>
    </div>

    <!--Section: Partner Companies-->
    <section class="wow fadeIn" data-wow-delay="0.2s">
        <?php if (!$companies) : ?>
            <h4 class="text-center h4-responsive">No Partner Companies found</h4>
        <?php else : ?>
            <div class="row">
            <?php foreach ($companies as $key => $company) : ?>
                <div class="col-md-3 flex-center mt-2">
                    <a href="<?= $company->co_link ?>" target="_blank">
                        <img height="200" width="200" class="img-fluid hoverable" src="<?= base_url("resources/partner_companies/{$company->co_image_name}") ?>">
                    </a>
                </div>

                <?php if ($key === 3) : ?>
                    <?php break; ?>
                <?php endif; ?>
            <?php endforeach; ?>
            </div>

            <?php if (count($companies) > 4) : ?>
                <div class="mt-5 col-md-12 text-center">
                    <a class="btn btn-primary" href="Partner_companies">View All</a>
                </div>
            <?php endif ?>
        <?php endif ?>
    </section>
    <!--Section: Partner Companies-->

    <div class="divider-new wow fadeIn" data-wow-delay="0.2s">
        <h2 class="h2-responsive">Frequently Asked Questions</h2>
    </div>
    <!--Section: FAQ's-->
    <section class="wow fadeIn" data-wow-delay="0.2s">
        <?php if (!$faqs) : ?>
            <h4 class="text-center h4-responsive">No Frequently Asked Questions found</h4>
        <?php else : ?>
            <div class="list-group">
            <?php foreach ($faqs as $key => $faq) : ?>
                <!--List-->
                <a href="#!" class="list-group-item list-group-item-action flex-column align-items-start">
                    <div class="d-flex w-100 justify-content-between">
                      <h5 class="mb-1 blue-text"><?= $faq->fa_title ?></h5>
                    </div>
                    <p class="mb-1 text-justify"><?= $faq->fa_description ?></p>
                </a>
                <!--/.List-->

                <?php if ($key === 3) : ?>
                    <?php break; ?>
                <?php endif; ?>
            <?php endforeach; ?>
            <div class="list-group">

            <?php if (count($faqs) > 4) : ?>
                <a href="Faqs">View All</a>
            <?php endif ?>
        <?php endif; ?>
    </section>
    <!--Section: FAQ's-->
</div>

<div class="pt-5"></div>
<!--/ Main Container -->
