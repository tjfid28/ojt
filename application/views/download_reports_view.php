<style type="text/css">
	.Incomplete{
		background-color: #e57373 !important;
	}
	.Complete{
		background-color: #a5d6a7 !important;
	}
	.student_name{
		background-color: #f0f4c3 !important;
	}
	.preview_modal{
		background-color: #fff !important;
	}
	th{
		background-color: #e0e0e0 !important;
		font-size: 12px !important;
	}
</style>

<?php
    $this->db->select('concat(st_fname," ",st_lname) as name,st_lname,class_student_list.st_id,student.st_id,class_student_list.cla_id');
    $this->db->from('student_tbl as student');
    //$this->db->join('course_tbl as course', 'course.cou_id = student.cou_id','inner');
    $this->db->join('class_student_list_tbl as class_student_list', 'class_student_list.st_id = student.st_id', 'inner');
    $data = [
        'class_student_list.cla_id' => $cla_id,
    ];
    $this->db->where($data);
    $this->db->order_by('st_lname');
    $students = $this->db->get();
?>

<?php
    $this->db->select('req_name,req_id');
    $this->db->from('ojt_requirements_tbl');
    $this->db->order_by('req_id');
    $requirements = $this->db->get();
    echo "<table border='1' id='data_viewer' class='striped'>";
    echo "<thead>";
    echo "<th>Student Name</th>";
    foreach ($requirements->result() as $requirement) {
        echo "<th>".$requirement->req_name."</th>";
    }
    echo "</thead>";
    echo "<tbody>";
    foreach ($students->result() as $student) {
        $this->db->select('if(checklist.st_id is null,"Incomplete","Complete")as remark,requirement.req_id req_list_id');
        $this->db->from('ojt_requirements_tbl as requirement');
        $this->db->join('(select * from checklist_tbl where st_id = '.$student->st_id.") as checklist", 'checklist.req_id = requirement.req_id', 'left');
        $this->db->join('(select * from student_tbl where st_id = "'.$student->st_id.'") as student', 'student.st_id = checklist.st_id', 'left');
        $this->db->order_by('requirement.req_id');
        $checklist = $this->db->get();

        if ($type) {
            $remarks = array_column($checklist->result_array(), 'remark');
            $remarks = array_count_values($remarks);

            if (isset($remarks['Incomplete'])) {
                echo "<tr>";
                echo "<td class='student_name'>".$student->name."</td>";
                foreach ($checklist->result() as $check) {
                    echo "<td class='$check->remark'>".$check->remark."</td>";
                }
                echo "</tr>";
            }
        } else {
            echo "<tr>";
            echo "<td class='student_name'>".$student->name."</td>";
            foreach ($checklist->result() as $check) {
                echo "<td class='$check->remark'>".$check->remark."</td>";
            }
            echo "</tr>";
        }
    }
    echo "</tbody>";
    echo "<table>";
?>
