<main>
	<style>
	.checkbox-orange[type="checkbox"].filled-in:checked + label:after{
     border: 2px solid #0d47a1;
     background-color: #0d47a1;
	}
	</style>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5>SUB ADMINISTRATOR MANAGEMENT</h5>
		<hr>
		<div align="row">
			<div class="col s1 m12 l12" style="text-align:right;">
				<a href="<?= base_url('Administration/sub_administrator_management/bin') ?>" class="waves-effect waves-light btn">
		  		    <i class="material-icons left">delete_sweep</i>BIN
		  		</a>
				<a class="waves-effect waves-light btn blue darken-3" href="#add_sub_administrator"><i class="fa fa-plus-circle"></i> NEW SUB ADMINISTRATOR</a>
			</div>
		</div>
		<br>
		<div class="row">
	      <div class="col s12">
	      	<div class="card-panel">
				<div class="sub_admin_viewer" id="sub_admin_viewer" name="sub_admin_viewer">
					<center style="padding-top:50px;">

					</center>
				</div>
			</div>
		  </div>
		</div>
	</div>
</main>

<div id="add_sub_administrator" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> NEW SUB ADMINISTRATOR</h5>
      <br>
      <form class="form-add-sub-administrator" name="form-add-sub-administrator" id="form-add-sub-administrator">
      		<div class="row">
		        	<div class="row">
		        		<strong><i class="fa fa-key"></i> PERMISSIONS</strong>
			        	<div class="col s12 m12 l12">
			        		<br>
			        		<div class="col s12 m6 l6">
			        		  <strong>Basic Management</strong>
				        		<!-- <p>
							      <input type="checkbox" id="dashboard_permission" name="dashboard_permission" class="permission filled-in checkbox-orange"/>
							      <label for="dashboard_permission">Dashboard</label>
							    </p> -->
							    <p>
							      <input type="checkbox" id="news_permission" name="news_permission" class="permission filled-in checkbox-orange"/>
							      <label for="news_permission">News</label>
							    </p>
							    <p>
							      <input type="checkbox" id="class_list_permission" name="class_list_permission" class="permission filled-in checkbox-orange"/>
							      <label for="class_list_permission">Class list</label>
							    </p>
							    <p>
							      <input type="checkbox" id="slides_permission" name="slides_permission" class="permission filled-in checkbox-orange"/>
							      <label for="slides_permission">Slides</label>
							    </p>
							    <p>
							      <input type="checkbox" id="company_permission" name="company_permission" class="permission filled-in checkbox-orange"/>
							      <label for="company_permission">Partner Companies</label>
							    </p>
						    </div>
						    <div class="col s12 m6 l6">
						    <strong>OJT Management</strong>
							    <p>
							      <input type="checkbox" id="uploaded_documents_permission" name="uploaded_documents_permission" class="permission filled-in checkbox-orange"/>
							      <label for="uploaded_documents_permission">Uploaded Documents</label>
							    </p>
							    <p>
							      <input type="checkbox" id="ojt_requirements_permission" name="ojt_requirements_permission" class="permission filled-in checkbox-orange"/>
							      <label for="ojt_requirements_permission">OJT Requirements</label>
							    </p>
							    <p>
							      <input type="checkbox" id="downloadable_permission" name="downloadable_permission" class="permission filled-in checkbox-orange"/>
							      <label for="downloadable_permission">Downloadable Forms</label>
							    </p>
							    <p>
							      <input type="checkbox" id="ojt_policies_permission" name="ojt_policies_permission" class="permission filled-in checkbox-orange"/>
							      <label for="ojt_policies_permission">OJT Policies</label>
							    </p>
			        		</div>
						    <div class="col s12 m6 l6">
						    <br>
						    <strong>User Management</strong>
							    <p>
							      <input type="checkbox" id="student_permission" name="student_permission" class="permission filled-in checkbox-orange"/>
							      <label for="student_permission">Students</label>
							    </p>
							    <p>
							      <input type="checkbox" id="faculty_permission" name="faculty_permission" class="permission filled-in checkbox-orange"/>
							      <label for="faculty_permission">Faculty Members</label>
							    </p>
			        		</div>
			        		<div class="col s12 m6 l6">
						    <br>
						    <strong>School Management</strong>
							    <p>
							      <input type="checkbox" id="courses_permission" name="courses_permission" class="permission filled-in checkbox-orange"/>
							      <label for="courses_permission">Courses</label>
							    </p>
							    <p>
							      <input type="checkbox" id="departments_permission" name="departments_permission" class="permission filled-in checkbox-orange"/>
							      <label for="departments_permission">Departments</label>
							    </p>
							    <p>
							      <input type="checkbox" id="programs_permission" name="programs_permission" class="permission filled-in checkbox-orange"/>
							      <label for="programs_permission">Programs</label>
							    </p>
							    <p>
							      <input type="checkbox" id="sections_permission" name="sections_permission" class="permission filled-in checkbox-orange"/>
							      <label for="sections_permission">Sections</label>
							    </p>
			        		</div>
		        	</div>
		        </div>
	      		<div class="input-field col s12 m4 l4">
			          <input id="first_name" name="first_name" type="text" class="validate letters_only" required>
			          <label for="first_name">First Name</label>
		        </div>
		        <div class="input-field col s12 m4 l4">
			          <input id="middle_name" name="middle_name" type="text" class="validate letters_only" required>
			          <label for="middle_name">Middle Name</label>
		        </div>
		        <div class="input-field col s12 m4 l4">
			          <input id="last_name" name="last_name" type="text" class="validate letters_only" required>
			          <label for="last_name">Last Name</label>
		        </div>
		        <div class="input-field col s12 m6 l6">
			          <input id="uname" name="uname" type="text" class="validate" required>
			          <label for="uname">Username</label>
		        </div>
		        <div class="input-field col s12 m6 l6">
			          <input id="pword" name="pword" minlength="4" maxlength="15" type="password" class="validate" required>
			          <label for="pword">Password</label>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<div id="edit_sub_administrator" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-pencil"></i> EDIT SUB ADMINISTRATOR</h5>
      <br>
      <form class="form-edit-sub-administrator" name="form-edit-sub-administrator" id="form-edit-sub-administrator">
      		<div class="row">
      			<div class="row">
		        		<strong><i class="fa fa-key"></i> PERMISSIONS</strong>
			        	<div class="col s12 m12 l12">
			        		<br>
			        		<div class="col s12 m6 l6">
			        		  <strong>Basic Management</strong>
				        		<!-- <p>
							      <input type="checkbox" id="edit_dashboard_permission" name="edit_dashboard_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_dashboard_permission">Dashboard</label>
							    </p> -->
							    <p>
							      <input type="checkbox" id="edit_news_permission" name="edit_news_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_news_permission">News</label>
							    </p>
							    <p>
							      <input type="checkbox" id="edit_class_list_permission" name="edit_class_list_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_class_list_permission">Class list</label>
							    </p>
							    <p>
							      <input type="checkbox" id="edit_slides_permission" name="edit_slides_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_slides_permission">Slides</label>
							    </p>
							    <p>
							      <input type="checkbox" id="edit_company_permission" name="edit_company_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_company_permission">Partner Companies</label>
							    </p>
						    </div>
						    <div class="col s12 m6 l6">
						    <strong>OJT Management</strong>
							    <p>
							      <input type="checkbox" id="edit_uploaded_documents_permission" name="edit_uploaded_documents_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_uploaded_documents_permission">Uploaded Documents</label>
							    </p>
							    <p>
							      <input type="checkbox" id="edit_ojt_requirements_permission" name="edit_ojt_requirements_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_ojt_requirements_permission">OJT Requirements</label>
							    </p>
							    <p>
							      <input type="checkbox" id="edit_downloadable_permission" name="edit_downloadable_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_downloadable_permission">Downloadable Forms</label>
							    </p>
							    <p>
							      <input type="checkbox" id="edit_ojt_policies_permission" name="edit_ojt_policies_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_ojt_policies_permission">OJT Policies</label>
							    </p>
			        		</div>
						    <div class="col s12 m6 l6">
						    <br>
						    <strong>User Management</strong>
							    <p>
							      <input type="checkbox" id="edit_student_permission" name="edit_student_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_student_permission">Students</label>
							    </p>
							    <p>
							      <input type="checkbox" id="edit_faculty_permission" name="edit_faculty_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_faculty_permission">Faculty Members</label>
							    </p>
			        		</div>
			        		<div class="col s12 m6 l6">
						    <br>
						    <strong>School Management</strong>
							    <p>
							      <input type="checkbox" id="edit_courses_permission" name="edit_courses_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_courses_permission">Courses</label>
							    </p>
							    <p>
							      <input type="checkbox" id="edit_departments_permission" name="edit_departments_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_departments_permission">Departments</label>
							    </p>
							    <p>
							      <input type="checkbox" id="edit_programs_permission" name="edit_programs_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_programs_permission">Programs</label>
							    </p>
							    <p>
							      <input type="checkbox" id="edit_sections_permission" name="edit_sections_permission" class="permission filled-in checkbox-orange"/>
							      <label for="edit_sections_permission">Sections</label>
							    </p>
			        		</div>
		        	</div>
		        </div>
      			<input type="hidden" id="edit_id" name="edit_id" readonly>
	      		<div class="input-field col s12 m4 l4">
			          <input id="edit_first_name" name="edit_first_name" type="text" class="validate letters_only" placeholder="First Name" required>
			          <label for="edit_first_name">First Name</label>
		        </div>
		        <div class="input-field col s12 m4 l4">
			          <input id="edit_middle_name" name="edit_middle_name" type="text" class="validate letters_only" placeholder="Middle Name" required>
			          <label for="edit_middle_name">Middle Name</label>
		        </div>
		        <div class="input-field col s12 m4 l4">
			          <input id="edit_last_name" name="edit_last_name" type="text" class="validate letters_only" placeholder="Last Name" required>
			          <label for="edit_last_name">Last Name</label>
		        </div>
		        <div class="input-field col s12 m6 l6">
			          <input id="edit_uname" name="edit_uname" type="text" class="validate" placeholder="Username" required>
			          <label for="edit_uname">Username</label>
		        </div>
		        <div class="input-field col s12 m6 l6">
			          <input id="edit_pword" name="edit_pword" minlength="4" maxlength="15" type="password" class="validate" placeholder="Password" required>
			          <label for="edit_pword">Password</label>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3" id="edit" name="action"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/user_management/sub_administrator_management.js');?>"></script>
