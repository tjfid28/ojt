<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5>FACULTY MANAGEMENT</h5>
		<hr>
		<div align="row">
			<div class="col s1 m12 l12" style="text-align:right;">
                <a href="<?= base_url('Administration/faculty_management/bin') ?>" class="waves-effect waves-light btn">
                    <i class="material-icons left">delete_sweep</i>BIN
                </a>
				<a class="waves-effect waves-light btn blue darken-3" href="#add_faculty"><i class="fa fa-plus-circle"></i> NEW FACULTY MEMBER</a>
			</div>
		</div>
		<br>
		<div class="row">
	      <div class="col s12">
	      	<div class="card-panel">
				<div class="faculty_viewer" id="faculty_viewer" name="faculty_viewer">
					<center style="padding-top:50px;">

					</center>
				</div>
			</div>
		  </div>
		</div>
	</div>
</main>

<div id="add_faculty" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> NEW FACULTY MEMBER</h5>
      <br>
      <form class="form-add-faculty" name="form-add-faculty" id="form-add-faculty" enctype="multipart/form-data">
      		<div class="row">
      			<div class="file-field input-field">
				      <div class="btn blue darken-3 waves-effect waves-light">
					        <span>File</span>
					        <input type="file" form="form-add-faculty" id="fa_img" accept='.jpeg, .jpg, .png' name="fa_img" required>
				      </div>
				      <div class="file-path-wrapper">
				        	<input class="file-path validate" type="text" >
				      </div>
			    </div>
      			<div class="input-field col s12 m12 l12">
			          <input id="fa_number" name="fa_number" type="text" class="validate" required>
			          <label for="fa_number">Faculty Number/ID</label>
		        </div>
	      		<div class="input-field col s12 m4 l4">
			          <input id="first_name" name="first_name" type="text" class="validate letters_only" required>
			          <label for="first_name">First Name</label>
		        </div>
		        <div class="input-field col s12 m4 l4">
			          <input id="middle_name" name="middle_name" type="text" class="validate letters_only" required>
			          <label for="middle_name">Middle Name</label>
		        </div>
		        <div class="input-field col s12 m4 l4">
			          <input id="last_name" name="last_name" type="text" class="validate letters_only" required>
			          <label for="last_name">Last Name</label>
		        </div>
		        <div class="input-field col s12 m12 l12">
				    <select id="gender" name="gender" class="select" required>
					      <option disabled selected>Choose gender</option>
					      <option value="Male">Male</option>
					      <option value="Female">Female</option>
				    </select>
				    <label>Gender</label>
				</div>
				<div class="input-field col s12 m12 l12">
				    <select id="dept_id" name="dept_id" class="select" required>
					      <option value="" disabled selected>Choose department</option>
					      <?php foreach ($departments->result() as $department) {
    ?>
					      		<option value="<?php echo $department->dept_id ?>"><?php echo $department->dept_name; ?></option>
					      <?php
}
                          ?>
				    </select>
				    <label>Department</label>
				</div>
		        <div class="input-field col s12 m6 l6">
			          <input id="uname" name="uname" type="text" class="validate" required>
			          <label for="uname">Username</label>
		        </div>
		        <div class="input-field col s12 m6 l6">
			          <input id="pword" name="pword" minlength="4" maxlength="15" type="password" class="validate" required>
			          <label for="pword">Password</label>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<div id="edit_faculty" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> EDIT FACULTY MEMBER</h5>
      <br>
      <form class="form-edit-faculty" name="form-edit-faculty" id="form-edit-faculty" enctype="multipart/form-data">
      		<div class="row">
      			<input type="hidden" id="edit_id" name="edit_id" readonly>
      			<input type="hidden" id="edit_image_name" name="edit_image_name" readonly>
      			<div class="file-field input-field">
				      <div class="btn blue darken-3 waves-effect waves-light">
					        <span>File</span>
					        <input type="file" form="form-edit-faculty" id="edit_fa_img" accept='.jpeg, .jpg, .png' name="edit_fa_img">
				      </div>
				      <div class="file-path-wrapper">
				        	<input class="file-path validate" type="text" id="edit_fa_img_name" name="edit_fa_img_name">
				      </div>
			    </div>
      			<div class="input-field col s12 m12 l12">
			          <input id="edit_fa_number" name="edit_fa_number" placeholder="Faculty No/ID" type="text" class="validate" required>
			          <label for="fa_number">Faculty Number/ID</label>
		        </div>
	      		<div class="input-field col s12 m4 l4">
			          <input id="edit_first_name" name="edit_first_name" type="text" placeholder="First name" class="validate letters_only" required>
			          <label for="edit_first_name">First Name</label>
		        </div>
		        <div class="input-field col s12 m4 l4">
			          <input id="edit_middle_name" name="edit_middle_name" type="text" placeholder="Middle name" class="validate letters_only" required>
			          <label for="edit_middle_name">Middle Name</label>
		        </div>
		        <div class="input-field col s12 m4 l4">
			          <input id="edit_last_name" name="edit_last_name" type="text" placeholder="Last name" class="validate letters_only" required>
			          <label for="edit_last_name">Last Name</label>
		        </div>
		        <div class="input-field col s12 m12 l12">
				    <select id="edit_gender" name="edit_gender" required>
					      <option value="Male">Male</option>
					      <option value="Female">Female</option>
				    </select>
				    <label>Gender</label>
				</div>
				<div class="input-field col s12 m12 l12">
				    <select id="edit_dept_id" name="edit_dept_id" required>
					      <option value="" disabled selected>Choose department</option>
					      <?php foreach ($departments->result() as $department) {
                              ?>
					      		<option value="<?php echo $department->dept_id ?>"><?php echo $department->dept_name; ?></option>
					      <?php
                          }
                          ?>
				    </select>
				    <label>Department</label>
				</div>
		        <div class="input-field col s12 m6 l6">
			          <input id="edit_uname" name="edit_uname" type="text" placeholder="Username" class="validate" required>
			          <label for="edit_uname">Username</label>
		        </div>
		        <div class="input-field col s12 m6 l6">
			          <input id="edit_pword" name="edit_pword" minlength="4" maxlength="15" type="password" placeholder="Password" class="validate" required>
			          <label for="edit_pword">Password</label>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/user_management/faculty_management.js');?>"></script>
