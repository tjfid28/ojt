<main>
    <div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
        <h5>STUDENT MANAGEMENT</h5>
        <hr>
        <div align="row">
            <div class="col s1 m12 l12" style="text-align:right;">
                <a href="<?= base_url('Administration/student_management/bin') ?>" class="waves-effect waves-light btn">
                    <i class="material-icons left">delete_sweep</i>BIN
                </a>
                <a class="waves-effect waves-light btn blue darken-3" id="download_template"><i class="fa fa-download"></i> DOWNLOAD TEMPLATE</a>
                <a class="waves-effect waves-light btn blue darken-3" href="#excel"><i class="fa fa-upload"></i> IMPORT FROM EXCEL</a>
                <a class="waves-effect waves-light btn blue darken-3" href="#add_student"><i class="fa fa-plus-circle"></i> NEW STUDENT</a>
            </div>
        </div>
        <br>
        <div class="row">
          <div class="col s12">
            <div class="card-panel">
                <div class="student_viewer" id="student_viewer" name="student_viewer">
                    <center style="padding-top:50px;">

                    </center>
                </div>
            </div>
          </div>
        </div>
    </div>
</main>

<div id="add_student" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> NEW STUDENT</h5>
      <br>
      <form class="form-add-student" name="form-add-student" id="form-add-student" enctype="multipart/form-data">
            <div class="row">
                <div class="file-field input-field">
                      <div class="btn blue darken-3 waves-effect waves-light">
                            <span>File</span>
                            <input type="file" form="form-add-student" id="st_img" name="st_img" accept='.jpeg, .jpg, .png' required>
                      </div>
                      <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" >
                      </div>
                </div>
                <div class="input-field col s12 m12 l12">
                      <input id="st_number" name="st_number" type="text" class="validate" required>
                      <label for="st_number">Student Number/ID</label>
                </div>
                <div class="input-field col s12 m4 l4">
                      <input id="first_name" name="first_name" type="text" class="validate letters_only" required>
                      <label for="first_name">First Name</label>
                </div>
                <div class="input-field col s12 m4 l4">
                      <input id="middle_name" name="middle_name" type="text" class="validate letters_only" required>
                      <label for="middle_name">Middle Name</label>
                </div>
                <div class="input-field col s12 m4 l4">
                      <input id="last_name" name="last_name" type="text" class="validate letters_only" required>
                      <label for="last_name">Last Name</label>
                </div>
                <div class="input-field col s12 m12 l12">
                    <select id="gender" name="gender" class="select" required>
                          <option disabled selected>Choose gender</option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                    </select>
                    <label>Gender</label>
                </div>
                <div class="col s12 m12 l12">
                  <label>School year</label>
                    <div class="row">
                      <div class="col s5 m5 l5">
                        <input type="text" name="start_year" id="start_year" placeholder="<?= date('Y') ?>" required>
                      </div>
                      <div class="col s2 m2 l2" style="text-align:center;"><b style="line-height:40px;font-size:30px;">-</b></div>
                      <div class="col s5 m5 l5">
                        <input type="text" name="end_year" id="end_year" placeholder="<?= date('Y', strtotime('+1 year')) ?>" required>
                      </div>
                    </div>
                </div>
                <div class="input-field col s12 m12 l12">
                    <select id="cou_id" name="cou_id" class="select" required>
                          <option value="" disabled selected>Choose course</option>
                          <?php foreach ($courses->result() as $course) {
    ?>
                                <option value="<?php echo $course->cou_id ?>"><?php echo $course->cou_title; ?></option>
                          <?php
}
                          ?>
                    </select>
                    <label>Course</label>
                </div>
                <div class="input-field col s12 m12 l12">
                      <input id="email" name="email" type="email" class="validate" required>
                      <label for="email">Email</label>
                </div>
                <div class="input-field col s12 m12 l12">
                      <input id="mobile_no" name="mobile_no" type="text" class="validate" required>
                      <label for="mobile_no">Mobile No.</label>
                </div>
                <div class="input-field col s12 m6 l6">
                      <input id="uname" name="uname" type="text" class="validate" required>
                      <label for="uname">Username</label>
                </div>
                <div class="input-field col s12 m6 l6">
                      <input id="pword" name="pword" minlength="4" maxlength="15" type="password" class="validate" required>
                      <label for="pword">Password</label>
                </div>
            </div>
            <center>
                <button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
            </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<div id="edit_student" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> EDIT STUDENT</h5>
      <br>
      <form class="form-edit-student" name="form-edit-student" id="form-edit-student" enctype="multipart/form-data">
            <div class="row">
                <input type="hidden" id="edit_id" name="edit_id" readonly>
                <input type="hidden" id="edit_image_name" name="edit_image_name" readonly>
                <div class="file-field input-field">
                      <div class="btn blue darken-3 waves-effect waves-light">
                            <span>File</span>
                            <input type="file" form="form-edit-student" id="edit_st_img" accept='.jpeg, .jpg, .png' name="edit_st_img">
                      </div>
                      <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" id="edit_st_img_name" name="edit_st_img_name">
                      </div>
                </div>
                <div class="input-field col s12 m12 l12">
                      <input id="edit_st_number" name="edit_st_number" placeholder="Student No/ID" type="text" class="validate" required>
                      <label for="st_number">Student Number/ID</label>
                </div>
                <div class="input-field col s12 m4 l4">
                      <input id="edit_first_name" name="edit_first_name" type="text" placeholder="First name" class="validate letters_only" required>
                      <label for="edit_first_name">First Name</label>
                </div>
                <div class="input-field col s12 m4 l4">
                      <input id="edit_middle_name" name="edit_middle_name" type="text" placeholder="Middle name" class="validate letters_only" required>
                      <label for="edit_middle_name">Middle Name</label>
                </div>
                <div class="input-field col s12 m4 l4">
                      <input id="edit_last_name" name="edit_last_name" type="text" placeholder="Last name" class="validate letters_only" required>
                      <label for="edit_last_name">Last Name</label>
                </div>
                <div class="input-field col s12 m12 l12">
                    <select id="edit_gender" name="edit_gender" required>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                    </select>
                    <label>Gender</label>
                </div>
                <div class="col s12 m12 l12">
                  <label>School year</label>
                    <div class="row">
                      <div class="col s5 m5 l5">
                        <input type="text" name="edit_start_year" id="edit_start_year" placeholder="<?= date('Y') ?>" required>
                      </div>
                      <div class="col s2 m2 l2" style="text-align:center;"><b style="line-height:40px;font-size:30px;">-</b></div>
                      <div class="col s5 m5 l5">
                        <input type="text" name="edit_end_year" id="edit_end_year" placeholder="<?= date('Y', strtotime('+1 year')) ?>" required>
                      </div>
                    </div>
                </div>
                <div class="input-field col s12 m12 l12">
                    <select id="edit_cou_id" name="edit_cou_id" required>
                      <?php foreach ($courses->result() as $course) : ?>
                        <option value="<?php echo $course->cou_id ?>"><?php echo $course->cou_title; ?></option>
                      <?php endforeach; ?>
                    </select>
                    <label>Course</label>
                </div>
                <div class="input-field col s12 m12 l12">
                      <input id="edit_email" name="edit_email" type="text" placeholder="Email" class="validate" required>
                      <label for="edit_email">Email</label>
                </div>
                <div class="input-field col s12 m12 l12">
                      <input id="edit_mobile_no" name="edit_mobile_no" type="text" placeholder="Mobile No" class="validate" required>
                      <label for="edit_mobile_no">Mobile No</label>
                </div>
                <div class="input-field col s12 m6 l6">
                      <input id="edit_uname" name="edit_uname" type="text" placeholder="Username" class="validate" required>
                      <label for="edit_uname">Username</label>
                </div>
                <div class="input-field col s12 m6 l6">
                      <input id="edit_pword" name="edit_pword" minlength="4" maxlength="15" type="password" placeholder="Password" class="validate" required>
                      <label for="edit_pword">Password</label>
                </div>
            </div>
            <center>
                <button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
            </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<div id="excel" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-file-excel-o"></i> IMPORT EXCEL FILE</h5>
      <br>
      <form class="form-excel" name="form-excel" id="form-excel" enctype="multipart/form-data">
            <div class="row">
                <div class="file-field input-field">
                      <div class="btn blue darken-3 waves-effect waves-light">
                            <span><i class="fa fa-file-excel-o"></i> File</span>
                            <input type="file" form="form-excel" id="document" name="document" required>
                      </div>
                      <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" >
                      </div>
                </div>
            </div>
            <center>
                <button class="waves-effect waves-light btn blue darken-3" id="btn-upload"><i class="fa fa-upload"></i> IMPORT</button>
            </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/user_management/student_management.js');?>"></script>
