<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5>ADMINISTRATOR MANAGEMENT</h5>
		<hr>

		<?php if ($this->session->userdata('ad_id') == 0): ?>
			<div align="row">
				<div class="col s1 m12 l12" style="text-align:right;">
					<a href="<?= base_url('Administration/administrator_management/bin') ?>" class="waves-effect waves-light btn">
			  		    <i class="material-icons left">delete_sweep</i>BIN
			  		</a>
					<a class="waves-effect waves-light btn blue darken-3" href="#add_administrator"><i class="fa fa-plus-circle"></i> NEW ADMINISTRATOR</a>
				</div>
			</div>
		<?php endif ?>
		
		<br>
		<div class="row">
	      <div class="col s12">
	      	<div class="card-panel">
				<div class="admin_viewer" id="admin_viewer" name="admin_viewer">
					<center style="padding-top:50px;">
						
					</center>
				</div>
			</div>
		  </div>
		</div>
	</div>

	<input type="hidden" id="ad_id" value="<?= $this->session->userdata('ad_id'); ?>">
</main>

<?php if ($this->session->userdata('ad_id') == 0): ?>
<div id="add_administrator" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> NEW ADMINISTRATOR</h5>
      <br>
      <form class="form-add-administrator" name="form-add-administrator" id="form-add-administrator">
      		<div class="row">
	      		<div class="input-field col s12 m4 l4">
			          <input id="first_name" name="first_name" type="text" class="validate letters_only" required>
			          <label for="first_name">First Name</label>
		        </div>
		        <div class="input-field col s12 m4 l4">
			          <input id="middle_name" name="middle_name" type="text" class="validate letters_only" required>
			          <label for="middle_name">Middle Name</label>
		        </div>
		        <div class="input-field col s12 m4 l4">
			          <input id="last_name" name="last_name" type="text" class="validate letters_only" required>
			          <label for="last_name">Last Name</label>
		        </div>
		        <div class="input-field col s12 m6 l6">
			          <input id="uname" name="uname" type="text" class="validate" required>
			          <label for="uname">Username</label>
		        </div>
		        <div class="input-field col s12 m6 l6">
			          <input id="pword" name="pword" minlength="4" maxlength="15" type="password" class="validate" required>
			          <label for="pword">Password</label>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>
<?php endif; ?>

<div id="edit_administrator" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-pencil"></i> EDIT ADMINISTRATOR</h5>
      <br>
      <form class="form-edit-administrator" name="form-edit-administrator" id="form-edit-administrator">
      		<div class="row">
      			<input type="hidden" id="edit_id" name="edit_id" readonly>
	      		<div class="input-field col s12 m4 l4">
			          <input id="edit_first_name" name="edit_first_name" type="text" class="validate letters_only" placeholder="First Name" required>
			          <label for="edit_first_name">First Name</label>
		        </div>
		        <div class="input-field col s12 m4 l4">
			          <input id="edit_middle_name" name="edit_middle_name" type="text" class="validate letters_only" placeholder="Middle Name" required>
			          <label for="edit_middle_name">Middle Name</label>
		        </div>
		        <div class="input-field col s12 m4 l4">
			          <input id="edit_last_name" name="edit_last_name" type="text" class="validate letters_only" placeholder="Last Name" required>
			          <label for="edit_last_name">Last Name</label>
		        </div>
		        <div class="input-field col s12 m6 l6">
			          <input id="edit_uname" name="edit_uname" type="text" class="validate" placeholder="Username" required>
			          <label for="edit_uname">Username</label>
		        </div>
		        <div class="input-field col s12 m6 l6">
			          <input id="edit_pword" name="edit_pword" minlength="4" maxlength="15" type="password" class="validate" placeholder="Password" required>
			          <label for="edit_pword">Password</label>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3" id="edit" name="action"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/user_management/administrator_management.js');?>"></script>
