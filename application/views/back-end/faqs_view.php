<main>
  <div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
    <h5><i class="fa fa-question"></i> FREQUENTLY ASKED QUESTIONS</h5>

    <hr>

    <div align="row">
      <div class="col s1 m12 l12" style="text-align:right;">
        <a href="<?= base_url('Administration/faqs/bin') ?>" class="waves-effect waves-light btn">
            <i class="material-icons left">delete_sweep</i>Bin
        </a>
        <a class="waves-effect waves-light btn blue darken-3" href="#create_faq"><i class="fa fa-plus-circle"></i> ADD FAQ</a>
      </div>
    </div>

    <br>

    <div class="row">
      <div class="col s12">
        <div class="card-panel">
          <div class="faq_viewer" id="faq_viewer" name="faq_viewer">
            <center style="padding-top:50px;">

            </center>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>

<div id="create_faq" class="modal" style='width:80%;'>
    <div class="modal-content">
      <h5><i class="fa fa-building"></i> ADD FAQ</h5>
      <br>
      <form class="form-faq" name="form-faq" id="form-faq" >
            <div class="row">
                <div class="input-field col s12 m12 l12">
                      <input id="faq_title" name="faq_title" type="text" class="validate" required>
                      <label for="faq_title">FAQ Title</label>
                </div>
                <div class="input-field col s12 m12 l12">
                      <h5>Description</h5>
                      <textarea class='ckeditor' name='description' required></textarea>
                </div>
            </div>
            <center>
                <button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
            </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<div id="edit_faq" class="modal" style='width:80%;'>
    <div class="modal-content">
      <h5><i class="fa fa-building"></i> EDIT FAQ</h5>
      <br>
      <form class="edit-form-faq" name="edit-form-faq" id="edit-form-faq" >
            <div class="row">
                <input type='hidden' name='fa_id' class='fa_id' id='fa_id' required>
                <div class="input-field col s12 m12 l12 active">
                      <input id="edit_faq_title" name="edit_faq_title" type="text" class="validate" placeholder="Company Name" required>
                      <label for="edit_faq_title" class="active">Company Name</label>
                </div>
                <div class="input-field col s12 m12 l12">
                      <h5>Description</h5>
                      <textarea class='ckeditor edit_description' name='edit_description' id='edit_description' required></textarea>
                </div>
            </div>
            <center>
                <button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
            </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/faqs.js');?>"></script>
