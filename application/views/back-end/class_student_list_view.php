<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:50%;">
		<h5><i class="fa fa-list-alt"></i> <?php echo strtoupper($Class_Name); ?> STUDENT LISTS</h5>
		<hr>
		<div align="row">
			<div class="col s1 m12 l12" style="text-align:right;">
				<a class="waves-effect waves-light btn blue darken-3" href="#add_student"><i class="fa fa-plus-circle"></i> ADD STUDENT</a>
			</div>
		</div>
		<br>
		<div class="row">
	      <div class="col s12">
	      	<div class="card-panel">
				<div class="class_list_viewer" id="class_student_list_viewer" name="class_student_list_viewer">
					<center style="padding-top:50px;">

					</center>
				</div>
			</div>
	       </div>
		</div>
	</div>
	<?php $this->load->view('back_view'); ?>
</main>

<div id="add_student" class="modal">
    <div class="modal-content" style="height:90%;">
      <h5><i class="fa fa-plus-circle"></i> ADD STUDENT</h5>
      <br>
      <form class="form-add-student" name="form-add-student" id="form-add-student">
				<input type="hidden" name="cla_id" id="cla_id" value="<?= $class_id ?>">

	      		<div class="row">
	      			<div class="inputfield col s12 m12 l12">
	      				<label for="id_label_multiple">Select student for <?php echo strtoupper($Class_Name); ?>
						    <select id="st_id" name="st_id" multiple="multiple" class="student_list" style="width:100%;" data-placeholder="Student name" required>
							      <?php foreach ($students->result() as $student) {
    ?>
							      		<option value="<?php echo $student->student_real_id ?>"><?php echo $student->st_fname." ".$student->st_lname; ?></option>
							      <?php
}
                                  ?>
						    </select>
					    </label>
					</div>
		    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/class_student_list.js');?>"></script>
