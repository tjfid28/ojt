<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:10%;">
		<h5><i class="fa fa-file"></i> OJT Policy</h5>
		<hr>
		<div class="row">
			<div class="col s12">
				<div class="card-panel">
					<img src="<?php echo base_url('resources/images/');?><?php echo $policy['op_image']; ?>" style="width:100%;height:500px;object-fit:cover;">
					<form class="form-policy" name="form-policy" id="form-policy" enctype="multipart/form-data">
						<div class="file-field input-field">
					      <div class="btn waves-effect waves-light blue darken-3">
					        <span>Cover photo</span>
					        <input type="file" form="form-policy" id="policy_cover" name="policy_cover" accept='.jpeg, .jpg, .png' required >
					      </div>
					      <div class="file-path-wrapper">
					        <input class="file-path validate" type="text">
					      </div>
					    </div>
					    <div class="input-field">
					    	<textarea class="materialize-textarea validate" form="form-policy" id="ojt_policy_content" name="ojt_policy_content"><?php echo $policy['op_content']; ?></textarea>
					    	<label for="ojt_policy_content">Content</label>
					    </div>
						<br>
						<div class="right-align">
							<button class="btn waves-effect waves-light blue darken-3"><i class="fa fa-save"></i> SAVE</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</main>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/ojt_management/ojt_policy.js');?>"></script>
