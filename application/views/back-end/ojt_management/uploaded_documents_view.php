<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5><i class="fa fa-file-o"></i> UPLOADED DOCUMENTS</h5>
		<hr>
		<div>
			<p class="right-align">
		  		<a href="<?= base_url('Administration/uploaded_documents/bin') ?>" class="waves-effect waves-light btn">
		  		    <i class="material-icons left">delete_sweep</i>Bin
		  		</a>
		  	</p>

			<form>
				<div class="row">
					<div class="input-field col s12 m5 l5">
			          <input id="search" name="search" type="text" class="search">
			          <label for="search">SEARCH DOCUMENT & OWNER</label>
			        </div>
				</div>
			</form>
		</div>
		<div class="documents_viewer" id="documents_viewer" name="documents_viewer">
			<center style="padding-top:50px;">

			</center>
		</div>
	</div>
	<div class="fixed-action-btn">
	    <a class="btn-floating btn-large waves-effect waves-light btn blue accent-4">
	      <i class="large material-icons">menu</i>
	    </a>
	    <ul>
	      <li><a class="front-toggle btn-floating red tooltipped toggle"  data-position="left" data-delay="20" data-tooltip="Delete document" ><i class="material-icons">delete</i></a></li>
	      <!-- <li><a class="btn-floating blue tooltipped" href="#upload_document" data-position="left" data-delay="20" data-tooltip="Upload new document"><i class="material-icons">note_add</i></a></li> -->
	    </ul>
    </div>

</main>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/ojt_management/uploaded_documents.js');?>"></script>
