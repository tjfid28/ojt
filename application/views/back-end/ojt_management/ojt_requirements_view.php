<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5>OJT REQUIREMENTS</h5>
		<hr>
		<div align="row">
			<div class="col s1 m12 l12" style="text-align:right;">
				<a href="<?= base_url('Administration/ojt_requirements/bin') ?>" class="waves-effect waves-light btn">
                    <i class="material-icons left">delete_sweep</i>BIN
                </a>
				<a class="waves-effect waves-light btn blue darken-3" href="#add_requirement"><i class="fa fa-plus-circle"></i> NEW REQUIREMENT</a>
			</div>
		</div>
		<br>
		<div class="row">
	      <div class="col s12">
	      	<div class="card-panel">
				<div class="requirements_viewer" id="requirements_viewer" name="requirements_viewer">
					<center style="padding-top:50px;">

					</center>
				</div>
			</div>
	       </div>
		</div>
	</div>
</main>


<div id="add_requirement" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> NEW OJT REQUIREMENT</h5>
      <br>
      <form class="form-add-requirement" name="form-add-requirement" id="form-add-requirement">
      		<div class="row">
	      		<div class="input-field col s12 m12 l12">
			          <input id="req_name" name="req_name" type="text" class="validate letters_only" required>
			          <label for="req_name">Requirement Name</label>
		        </div>
		        <!-- <div class="input-field col s12 m6 l6">
				    <select id="cou_id" name="cou_id" class="select" required>
					      <option value="" disabled selected>Choose course</option>
					      <?php foreach ($courses->result() as $course) {
    ?>
					      		<option value="<?php echo $course->cou_id ?>"><?php echo $course->cou_title; ?></option>
					      <?php
}
                          ?>
				    </select>
				    <label>Course</label>
				</div>
				<div class="input-field col s12 m6 l6">
				    <select id="sub_id" name="sub_id" class="select" required>
					      <option value="" disabled selected>Choose subject</option>
					      <?php foreach ($subjects->result() as $subject) {
                              ?>
					      		<option value="<?php echo $subject->sub_id ?>"><?php echo $subject->sub_name; ?></option>
					      <?php
                          }
                          ?>
				    </select>
				    <label>Subject</label>
				</div> -->
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<div id="edit_requirement" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> EDIT OJT REQUIREMENT</h5>
      <br>
      <form class="form-edit-requirement" name="form-edit-requirement" id="form-edit-requirement">
      		<div class="row">
      			<input type="hidden" id="edit_id" name="edit_id" readonly>
	      		<div class="input-field col s12 m12 l12">
			          <input id="edit_req_name" name="edit_req_name" type="text" class="validate letters_only" Placeholder="Requirement Name" required>
			          <label for="edit_req_name">Requirement Name</label>
		        </div>
		        <!-- <div class="input-field col s12 m6 l6">
				    <select id="edit_cou_id" name="edit_cou_id" required>
					      <option value="" disabled selected>Choose course</option>
					      <?php foreach ($courses->result() as $course) {
                              ?>
					      		<option value="<?php echo $course->cou_id ?>"><?php echo $course->cou_title; ?></option>
					      <?php
                          }
                          ?>
				    </select>
				    <label>Course</label>
				</div>
				<div class="input-field col s12 m6 l6">
				    <select id="edit_sub_id" name="edit_sub_id" required>
					      <option value="" disabled selected>Choose subject</option>
					      <?php foreach ($subjects->result() as $subject) {
                              ?>
					      		<option value="<?php echo $subject->sub_id ?>"><?php echo $subject->sub_name; ?></option>
					      <?php
                          }
                          ?>
				    </select>
				    <label>Course</label>
				</div> -->
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/ojt_management/ojt_requirements.js');?>"></script>
