<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5><i class="fa fa-building"></i> PARTNER COMPANIES</h5>
		<hr>
		<div align="row">
			<div class="col s1 m12 l12" style="text-align:right;">
                <a href="<?= base_url('Administration/partner_companies/bin') ?>" class="waves-effect waves-light btn">
                    <i class="material-icons left">delete_sweep</i>Bin
                </a>
				<a class="waves-effect waves-light btn blue darken-3" href="#create_company"><i class="fa fa-plus-circle"></i> ADD COMPANY</a>
			</div>
		</div>
		<br>
		<div class="row">
	      <div class="col s12">
	      	<div class="card-panel">
				<div class="company_viewer" id="company_viewer" name="company_viewer">
					<center style="padding-top:50px;">

					</center>
				</div>
			 </div>
	       </div>
		</div>
	</div>
</main>

<div id="create_company" class="modal" style='width:80%;'>
    <div class="modal-content">
      <h5><i class="fa fa-building"></i> ADD COMPANY</h5>
      <br>
      <form class="form-company" name="form-company" id="form-company" enctype="multipart/form-data" >
      		<div class="row">
      			<div class="file-field input-field">
				      <div class="btn blue darken-3 waves-effect waves-light">
					        <span><i class="fa fa-file-image-o"></i> Company Logo</span>
					        <input type="file" form="form-company" id="company-image" name="company-image" accept='.jpeg, .jpg, .png' required>
				      </div>
				      <div class="file-path-wrapper">
				        	<input class="file-path validate" type="text" >
				      </div>
			    </div>
			    <div class="input-field col s12 m12 l12">
			          <input id="company_name" name="company_name" type="text" class="validate" required>
			          <label for="company_name">Company Name</label>
		        </div>
                <div class="input-field col s12 m12 l12">
                    <select id="company_type" name="company_type" required>
                        <option value="" disabled selected>Choose company type</option>
                        <option value="1">ICT</option>
                        <option value="2">HM</option>
                    </select>
                    <label for="company_type">Type</label>
                </div>
                <div class="input-field col s12 m12 l12">
                      <input id="company_link" name="company_link" type="url" class="validate">
                      <label for="company_link">Company Link</label>
                </div>
                <div class="input-field col s12 m12 l12">
                      <input id="company_email" name="company_email" type="email" class="validate">
                      <label for="company_email">Company Email</label>
                </div>
		        <div class="input-field col s12 m12 l12">
		        	  <h5>Description</h5>
			          <textarea class='ckeditor' name='description' required></textarea>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3" id="btn-upload"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<div id="edit_company" class="modal" style='width:80%;'>
    <div class="modal-content">
      <h5><i class="fa fa-building"></i> EDIT COMPANY</h5>
      <br>
      <form class="edit-form-company" name="edit-form-company" id="edit-form-company" enctype="multipart/form-data" >
      		<div class="row">
      			<input type='hidden' name='co_id' class='co_id' id='co_id' required>
      			<input type='hidden' name='edit_co_image' class='edit_co_image' id='edit_co_image' required>
      			<div class="file-field input-field">
				      <div class="btn blue darken-3 waves-effect waves-light">
					        <span><i class="fa fa-file-image-o"></i> Company Logo</span>
					        <input type="file" form="edit-form-company" id="edit_company-image" name="edit_company-image" accept='.jpeg, .jpg, .png' required>
				      </div>
				      <div class="file-path-wrapper">
				        	<input class="file-path validate" type="text" >
				      </div>
			    </div>
			    <div class="input-field col s12 m12 l12 active">
			          <input id="edit_name" name="edit_company_name" type="text" class="validate" placeholder="Company Name" required>
			          <label for="edit_name" class="active">Company Name</label>
		        </div>
                <div class="input-field col s12 m12 l12 active">
                    <select id="edit_type" name="edit_company_type" class="validate" required>
                        <option value="" disabled>Choose company type</option>
                        <option value="1">ICT</option>
                        <option value="2">HM</option>
                    </select>
                    <label for="edit_type">Type</label>
                </div>
                <div class="input-field col s12 m12 l12">
                      <input id="edit_company_link" name="edit_company_link" type="url" placeholder="Company Link" class="validate">
                      <label for="edit_company_link" class="active">Company Link</label>
                </div>
                <div class="input-field col s12 m12 l12">
                      <input id="edit_company_email" name="edit_company_email" type="email" placeholder="Company Email" class="validate">
                      <label for="edit_company_email" class="active">Company Email</label>
                </div>
		        <div class="input-field col s12 m12 l12">
		        	  <h5>Description</h5>
			          <textarea class='ckeditor edit_description' name='edit_description' id='edit_description' required></textarea>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3" id="btn-upload"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/partner_companies.js');?>"></script>
