<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:50%;">
		<h5><i class="fa fa-dashboard"></i> DASHBOARD</h5>
		<hr>
		<div class="row" style="text-align:center;">
			  <div class="col s12 m12 l12">
				  	<a href="<?php echo $_SESSION['account_type']==3 ? base_url('Administration/class_list') : '/ojt/Sub_Administration/class_list'?>">
				        <div class="card-panel blue-grey lighten-2 white-text hoverable">
					        <table class="white-text">
					        	<tr>
					        		<td rowspan="2"><span style="font-size:90px;"><i class="fa fa-list-alt"></i></span></td>
					        		<td> </td>
					        		<td style="text-align:right !important;"><b class="timer" style="font-size:40px;" data-to="<?php echo $cla_count; ?>" data-speed="300">0</b></td>
					        	</tr>
					        	<tr>
					        		<td> </td>
					        		<td style="text-align:right !important;"><span style="font-size:20px;">CLASS</span></td>
					        	</tr>
					        </table>
				        </div>
				    </a>
		      </div>
		      <div class="col s12 m5 l5">
			        <div class="card-panel blue darken-3 white-text hoverable">
			        	<a href="<?php echo $_SESSION['account_type']==3 ? base_url('Administration/student_management') : '/ojt/Sub_Administration/student_management'?>">
					        <table class="white-text responsive">
						        	<tr>
						        		<td rowspan="2"><span style="font-size:70px;"><i class="fa fa-group"></i></span></td>
						        		<td> </td>
						        		<td style="text-align:right !important;"><b class="timer" style="font-size:40px;" data-to="<?php echo $st_count; ?>" data-speed="300">0</b></td>
						        	</tr>
						        	<tr>
						        		<td> </td>
						        		<td style="text-align:right !important;"><span style="font-size:20px;">STUDENTS</span></td>
						        	</tr>
						    </table>
					    </a>
				    </div>
		      </div>
		      <div class="col s12 m7 l7">
			        <div class="card-panel red lighten-2 white-text hoverable">
			        	<a href="<?php echo $_SESSION['account_type']==3 ? base_url('Administration/faculty_management') : '/ojt/Sub_Administration/faculty_management'?>">
				        	 <table class="white-text">
						        	<tr>
						        		<td rowspan="2"><span style="font-size:90px;"><i class="fa fa-user"></i></span></td>
						        		<td> </td>
						        		<td style="text-align:right !important;"><b class="timer" style="font-size:40px;" data-to="<?php echo $fa_count; ?>" data-speed="300">0</b></td>
						        	</tr>
						        	<tr>
						        		<td> </td>
						        		<td style="text-align:right !important;"><span style="font-size:20px;">FACULTY MEMBERS</span></td>
						        	</tr>
						    </table>
					    </a>
			        </div>
		      </div>
	    </div>
	</div>
</main>

<script>
jQuery(function ($) {
	$('.timer').data('countToOptions', {
        formatter: function (value, options) {
          return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
        }
      });
      $('.timer').each(count);
      function count(options) {
        var $this = $(this);
        options = $.extend({}, options || {}, $this.data('countToOptions') || {});
        $this.countTo(options);
      }

});
</script>
