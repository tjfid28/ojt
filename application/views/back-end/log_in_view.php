<!DOCTYPE html>
<html>
<head>
	<title>STI COLLEGE NOVALICHES</title>
	  <link rel="icon" type="image/png" href="<?php echo base_url(); ?>resources/images/sti_logo.png" />
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/css/materialize.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/sweetalert.css');?>">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/master.css');?>">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

      <!--Import JS-->
      <span class="base_url" data-base-url="<?php echo base_url(); ?>"></span>
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	  <script src="<?php echo base_url('assets/js/plugins/sweetalert.min.js');?>"></script>
	  <script src="<?php echo base_url('assets/js/plugins/moment.js');?>"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script>

	  <!-- Defaults -->
	  <script type="text/javascript" src="<?php echo base_url('assets/js/home.js');?>"></script>
</head>
<body style="background-color:gray;">
	<main>
		<br>
		<div class="row">
			<div class="col s12 m12 l6 offset-l3">
					<div class="card">
						<div class="card-content">
							    <center>
						      		<h5 class="text-center"><i class="fa fa-sign-in"></i> ADMINISTRATION LOG IN</h5>
						        </center>
							  	<form id="form_login" class="form_login" name="form_login">
								      <div class="row">
										    <div class="input-field col s12 m12 l12">
										      <i class="material-icons prefix">person</i>
										      <input id="uname" name="uname" type="text" class="validate">
										      <label for="uname">Username</label>
										    </div>
										    <div class="input-field col s12 m12 l12">
										      <i class="material-icons prefix">lock</i>
										      <input id="pword" name="pword" type="password" class="validate">
										      <label for="pword">Password</label>
										    </div>
										    <br>
										    <center>
										    	<button class="waves-effect waves-light btn blue darken-4" id="btn_login" type="submit" >LOG IN</button>
										    	<a href="/ojt/" class="waves-effect waves-light btn blue darken-4" id="btn_login" type="submit" >GO TO HOME</a>
										    </center>
									  </div>
								</form>
						</div>
					</div>
			</div>
		</div>
	</main>
	<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/log_in.js');?>"></script>
</body>
</html>
