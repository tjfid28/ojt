<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5><i class="fa fa-film"></i> SLIDES</h5>
		<hr>
		<div align="row">
			<div class="col s1 m12 l12" style="text-align:right;">
                <a href="<?= base_url('Administration/slides_management/bin') ?>" class="waves-effect waves-light btn">
                    <i class="material-icons left">delete_sweep</i>Bin
                </a>
				<a class="waves-effect waves-light btn blue darken-3" href="#create_slide"><i class="fa fa-plus-circle"></i> ADD SLIDE</a>
			</div>
		</div>
		<br>
		<div class="row">
	      <div class="col s12">
	      	<div class="card-panel">
				<div class="slide_viewer" id="slide_viewer" name="slide_viewer">
					<center style="padding-top:50px;">

					</center>
				</div>
			 </div>
	       </div>
		</div>
	</div>
</main>

<div id="create_slide" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-file"></i> CREATE SLIDE</h5>
      <br>
      <form class="form-slide" name="form-slide" id="form-slide" enctype="multipart/form-data">
      		<div class="row">
      			<div class="file-field input-field">
				      <div class="btn blue darken-3 waves-effect waves-light">
					        <span><i class="fa fa-file-image-o"></i> Image</span>
					        <input type="file" form="form-slide" id="slide-image" name="slide-image" accept='.jpeg, .jpg, .png' required>
				      </div>
				      <div class="file-path-wrapper">
				        	<input class="file-path validate" type="text" >
				      </div>
			    </div>
			    <div class="input-field col s12 m6 l6">
			          <input id="caption" name="caption" type="text" class="validate" required>
			          <label for="caption">Caption</label>
		        </div>
		        <div class="input-field col s12 m6 l6">
			          <input id="description" name="description" type="text" class="validate" required>
			          <label for="description">Description</label>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3" id="btn-upload"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/slides.js');?>"></script>
