<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:50%;">
		<h5>CLASS STUDENT LISTS</h5>
		<hr>
		<div align="row">
			<div class="col s1 m12 l12" style="text-align:right;">
				<a class="waves-effect waves-light btn blue darken-3" href="#create_class"><i class="fa fa-plus-circle"></i> ADD STUDENT</a>
			</div>
		</div>
		<br>
		<div class="class_list_viewer" id="class_student_list_viewer" name="class_student_list_viewer">
			<center style="padding-top:50px;">
				
			</center>
		</div>
	</div>
</main>