<main>
    <div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:25%;">
        <h5><i class="fa fa-newspaper-o"></i> NEWS BIN</h5>

        <hr>

        <div class="row">
          <div class="col s12">
            <div class="card-panel">
              <div class="news_viewer" id="news_viewer" name="news_viewer">
                <center style="padding-top:50px;">

                </center>
              </div>
            </div>
          </div>
        </div>
    </div>
</main>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/news_bin.js');?>"></script>
