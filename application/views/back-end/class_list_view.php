<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5><i class="fa fa-list-alt"></i> CLASS LISTS</h5>
		<hr>
		<div align="row">
			<div class="col s12 m12 l12 right-align">
				<a href="<?= base_url('Administration/class_list/bin') ?>" class="waves-effect waves-light btn">
		  		    <i class="material-icons left">delete_sweep</i>BIN
		  		</a>
				<a class="waves-effect waves-light btn blue darken-3" href="#create_class">
					<i class="fa fa-plus-circle"></i> CREATE CLASS
				</a>
			</div>
		</div>
		<br>
		<div class="row">
	      <div class="col s12">
	      	<div class="card-panel">
				<div class="class_list_viewer" id="class_list_viewer" name="class_list_viewer">
					<center style="padding-top:50px;">

					</center>
				</div>
			</div>
		  </div>
		</div>
	</div>
</main>

<div id="create_class" class="modal">
    <div class="modal-content" style="height:90%;">
      <h5><i class="fa fa-plus-circle"></i> CREATE CLASS</h5>
      <br>
      <form class="form-create-class" name="form-create-class" id="form-create-class">
	      		<div class="row">
	      			<div class="input-field col s12 m4 l4">
					    <select id="sec_id" name="sec_id" class="select" required>
						      <option value="" disabled selected>Choose section</option>
						      <?php foreach ($sections->result() as $section) {
    ?>
						      		<option value="<?php echo $section->sec_id ?>" name="sec_<?php echo $section->sec_id ?>" id="sec_<?php echo $section->sec_id ?>"><?php echo $section->sec_name; ?></option>
						      <?php
}
                              ?>
					    </select>
					    <label>Section</label>
					</div>
	      			<div class="input-field col s12 m4 l4">
					    <select id="sub_id" name="sub_id" class="select" required>
						      <option value="" disabled selected>Choose subject</option>
						      <?php foreach ($subjects->result() as $subject) {
                                  ?>
						      		<option value="<?php echo $subject->sub_id ?>"><?php echo $subject->sub_name; ?></option>
						      <?php
                              }
                              ?>
					    </select>
					    <label>Subject</label>
					</div>
					<div class="input-field col s12 m4 l4">
					    <select id="cou_id" name="cou_id" class="select" required>
						      <option value="" disabled selected>Choose course</option>
						      <?php foreach ($courses->result() as $course) {
                                  ?>
						      		<option value="<?php echo $course->cou_id ?>"><?php echo $course->cou_title; ?></option>
						      <?php
                              }
                              ?>
					    </select>
					    <label>Course</label>
					</div>
					<div class="input-field col s12 m6 l6">
					    <select id="term" name="term" class="select" required>
						      <option value="" disabled selected>Choose Term</option>
						      <option value="1st">1st</option>
						      <option value="2nd">2nd</option>
					    </select>
					    <label>Term</label>
					</div>
					<div class="col s12 m6 l6">
						<label>School year</label>
					    <div class="row">
					    	<div class="col s5">
					    		<input type="text" name="start_year" id="start_year" placeholder="2016" required>
					    	</div>
					    	<div class="col s2" style="text-align:center;"><b style="line-height:40px;font-size:30px;">-</b></div>
					    	<div class="col s5">
					    		<input type="text" name="end_year" id="end_year" placeholder="2017" required>
					    	</div>
					    </div>
					</div>
					<div class="input-field col s12 m12 l12">
					    <select id="fa_id" name="fa_id" class="select" required>
						      <option value="" disabled selected>Choose faculty member</option>
						      <?php foreach ($faculty_members->result() as $faculty_member) {
                                  ?>
						      		<option value="<?php echo $faculty_member->fa_id ?>"><?php echo $faculty_member->fa_fname." ".$faculty_member->fa_lname; ?></option>
						      <?php
                              }
                              ?>
					    </select>
					    <label>Faculty member</label>
					</div>
		    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<div id="edit_class" class="modal">
    <div class="modal-content" style="height:90%;">
      <h5><i class="fa fa-plus-circle"></i> EDIT <span id="edit_sec_name" name="edit_sec_name"></span></h5>
      <br>
      <form class="form-edit-class" name="form-edit-class" id="form-edit-class">
      			<input type="hidden" id="edit_id" name="edit_id" readonly>
	      		<div class="row">
	      			<div class="input-field col s12 m6 l6">
					    <select id="edit_sub_id" name="edit_sub_id" required>
						      <option value="" disabled selected>Choose subject</option>
						      <?php foreach ($subjects->result() as $subject) {
                                  ?>
						      		<option value="<?php echo $subject->sub_id ?>"><?php echo $subject->sub_name; ?></option>
						      <?php
                              }
                              ?>
					    </select>
					    <label>Subject</label>
					</div>
					<div class="input-field col s12 m6 l6">
					    <select id="edit_cou_id" name="edit_cou_id" required>
						      <option value="" disabled selected>Choose course</option>
						      <?php foreach ($courses->result() as $course) {
                                  ?>
						      		<option value="<?php echo $course->cou_id ?>"><?php echo $course->cou_title; ?></option>
						      <?php
                              }
                              ?>
					    </select>
					    <label>Course</label>
					</div>
					<div class="input-field col s12 m6 l6">
					    <select id="edit_term" name="edit_term" class="select" required>
						      <option value="" disabled selected>Choose Term</option>
						      <option value="1st">1st</option>
						      <option value="2nd">2nd</option>
					    </select>
					    <label>Term</label>
					</div>
					<div class="col s12 m6 l6">
						<label>School year</label>
					    <div class="row">
					    	<div class="col s5">
					    		<input type="text" name="edit_start_year" id="edit_start_year" placeholder="2016" required>
					    	</div>
					    	<div class="col s2" style="text-align:center;"><b style="line-height:40px;font-size:30px;">-</b></div>
					    	<div class="col s5">
					    		<input type="text" name="edit_end_year" id="edit_end_year" placeholder="2017" required>
					    	</div>
					    </div>
					</div>
					<div class="input-field col s12 m6 l6">
					    <select id="edit_fa_id" name="edit_fa_id" required>
						      <option value="" disabled selected>Choose faculty member</option>
						      <?php foreach ($faculty_members->result() as $faculty_member) {
                                  ?>
						      		<option value="<?php echo $faculty_member->fa_id ?>"><?php echo $faculty_member->fa_fname." ".$faculty_member->fa_lname; ?></option>
						      <?php
                              }
                              ?>
					    </select>
					    <label>Faculty member</label>
					</div>
					<div class="input-field col s12 m6 l6">
					    <select id="edit_status" name="edit_status" class="select" required>
						      <option value="Open">Open</option>
						      <option value="Close">Close</option>
					    </select>
					    <label>Status</label>
					</div>
		    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/class_list.js');?>"></script>
