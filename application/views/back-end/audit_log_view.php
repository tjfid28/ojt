<main>
  <div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
    <h5><i class="fa fa-book"></i> AUDIT LOG</h5>
    <hr>
    <br>

    <div class="row">
      <div class="col s12">
        <div class="card-panel">
          <div class="audit_logs_viewer" id="audit_logs_viewer" name="audit_logs_viewer">
            <center style="padding-top:50px;">

            </center>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/audit_log.js');?>"></script>
