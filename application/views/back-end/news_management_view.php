<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5><i class="fa fa-newspaper-o"></i> NEWS</h5>

		<hr>

		<div class="row">
		  <div class="col s12 m12 l12">
		  	<p class="right-align">
		  		<a href="<?= base_url('Administration/news_management/bin') ?>" class="waves-effect waves-light btn">
		  		    <i class="material-icons left">delete_sweep</i>Bin
		  		</a>
		  	</p>

		    <div class="card-panel white">
		      <form id="form-news" class="form-news" name="form-news">
		        <div class="input-field">
		          <textarea id="news_field" name="news_field" form="form-news" class="materialize-textarea" required></textarea>
		          <label for="news_field"><i class="fa fa-edit"></i> Write news here</label>
		        </div>
		        <div class="right-align">
		        	<button type="submit" class="waves-effect waves-light btn blue darken-3"><i class="fa fa-newspaper-o"></i> POST</button>
		      	</div>
		      </form>
		    </div>
		  </div>
		</div>
		<div id="news_viewer" name="news_viewer" class="news_viewer" style="margin-left:10px;margin-right:10px;">
		</div>
	</div>
</main>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/news.js');?>"></script>
