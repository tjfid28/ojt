<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5>PROGRAM MANAGEMENT</h5>
		<hr>
		<div align="row">
			<div class="col s1 m12 l12" style="text-align:right;">
                <a href="<?= base_url('Administration/program_management/bin') ?>" class="waves-effect waves-light btn">
                    <i class="material-icons left">delete_sweep</i>BIN
                </a>
				<a class="waves-effect waves-light btn blue darken-3" href="#add_subject"><i class="fa fa-plus-circle"></i> NEW PROGRAM</a>
			</div>
		</div>
		<br>
		<div class="row">
	      <div class="col s12">
	        <div class="card-panel">
				<div class="section_viewer" id="section_viewer" name="section_viewer">
					<center style="padding-top:50px;">

					</center>
				</div>
			</div>
	    </div>
	</div>
</main>

<div id="add_subject" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> NEW PROGRAM</h5>
      <br>
      <form class="form-add-subject" name="form-add-subject" id="form-add-subject">
      		<div class="row">
	      		<div class="input-field col s12 m6 l6">
			          <input id="sub_name" name="sub_name" type="text" class="validate" required>
			          <label for="sub_name">Program Name</label>
		        </div>
		        <div class="input-field col s12 m3 l3">
			          <input id="sub_req_hours" name="sub_req_hours" min="1" type="number" class="validate" required>
			          <label for="sub_req_hours">Required Hours</label>
		        </div>
		        <div class="input-field col s12 m3 l3">
			          <input id="sub_unit" name="sub_unit" type="number" min="1" class="validate" required>
			          <label for="sub_unit">No of Units</label>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<div id="edit_subject" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> EDIT PROGRAM</h5>
      <br>
      <form class="form-edit-subject" name="form-edit-subject" id="form-edit-subject">
      		<div class="row">
      			<input type="hidden" id="edit_id" name="edit_id" readonly>
      			<div class="input-field col s12 m6 l6">
			          <input id="edit_sub_name" name="edit_sub_name" type="text" placeholder="Subject name" class="validate" required>
			          <label for="sub_name">Program Name</label>
		        </div>
		        <div class="input-field col s12 m3 l3">
			          <input id="edit_sub_req_hours" name="edit_sub_req_hours" min="1" placeholder="Required hours" type="number" class="validate" required>
			          <label for="edit_sub_req_hours">Required Hours</label>
		        </div>
		        <div class="input-field col s12 m3 l3">
			          <input id="edit_sub_unit" name="edit_sub_unit" type="number" min="1" placeholder="No. of units" class="validate" required>
			          <label for="edit_sub_unit">No of Units</label>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/school_management/subject_management.js');?>"></script>
