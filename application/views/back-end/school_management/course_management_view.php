<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5>COURSE MANAGEMENT</h5>
		<hr>
		<div align="row">
			<div class="col s1 m12 l12" style="text-align:right;">
                <a href="<?= base_url('Administration/course_management/bin') ?>" class="waves-effect waves-light btn">
                    <i class="material-icons left">delete_sweep</i>BIN
                </a>
				<a class="waves-effect waves-light btn blue darken-3" href="#add_course"><i class="fa fa-plus-circle"></i> NEW COURSE</a>
			</div>
		</div>
		<br>
		<div class="row">
	      <div class="col s12">
	      	<div class="card-panel">
				<div class="course_viewer" id="course_viewer" name="course_viewer">
					<center style="padding-top:50px;">

					</center>
				</div>
			</div>
		</div>
	</div>
</main>

<div id="add_course" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> NEW COURSE</h5>
      <br>
      <form class="form-add-coure" name="form-add-course" id="form-add-course">
      		<div class="row">
	      		<div class="input-field col s12 m12 l12">
			          <input id="cou_title" name="cou_title" type="text" class="validate letters_only" required>
			          <label for="cou_title">Course Title</label>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<div id="edit_course" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> EDIT COURSE</h5>
      <br>
      <form class="form-edit-course" name="form-edit-course" id="form-edit-course">
      		<div class="row">
      			<input type="hidden" id="edit_id" name="edit_id" readonly>
      			<div class="input-field col s12 m12 l12">
			          <input id="edit_cou_title" name="edit_cou_title" placeholder="Course Title" type="text" class="validate letters_only" required>
			          <label for="st_number">Department Title</label>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/school_management/course_management.js');?>"></script>
