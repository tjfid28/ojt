<main>
	<div class="container-fluid" style="margin-left:20px;margin-right:20px;padding-bottom:20%;">
		<h5>DEPARTMENT MANAGEMENT</h5>
		<hr>
		<div align="row">
			<div class="col s1 m12 l12" style="text-align:right;">
                <a href="<?= base_url('Administration/department_management/bin') ?>" class="waves-effect waves-light btn">
                    <i class="material-icons left">delete_sweep</i>BIN
                </a>
				<a class="waves-effect waves-light btn blue darken-3" href="#add_department"><i class="fa fa-plus-circle"></i> NEW DEPARTMENT</a>
			</div>
		</div>
		<br>
		<div class="row">
	      <div class="col s12">
	      	<div class="card-panel">
				<div class="department_viewer" id="department_viewer" name="department_viewer">
					<center style="padding-top:50px;">

					</center>
				</div>
		   	</div>
	       </div>
		</div>
	</div>
</main>

<div id="add_department" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> NEW DEPARTMENT</h5>
      <br>
      <form class="form-add-department" name="form-add-department" id="form-add-department">
      		<div class="row">
	      		<div class="input-field col s12 m12 l12">
			          <input id="dept_name" name="dept_name" type="text" class="validate letters_only" required>
			          <label for="dept_name">Department Name</label>
		        </div>
	    	</div>
	    	<center>
		       	<button type="submit" class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<div id="edit_department" class="modal">
    <div class="modal-content">
      <h5><i class="fa fa-plus-circle"></i> EDIT STUDENT</h5>
      <br>
      <form class="form-edit-department" name="form-edit-department" id="form-edit-department">
      		<div class="row">
      			<input type="hidden" id="edit_id" name="edit_id" readonly>
      			<div class="input-field col s12 m12 l12">
			          <input id="edit_dept_name" name="edit_dept_name" placeholder="Department Name" type="text" class="validate letters_only" required>
			          <label for="st_number">Department Name</label>
		        </div>
	    	</div>
	    	<center>
		       	<button class="waves-effect waves-light btn blue darken-3"><i class="fa fa-save"></i> SAVE</button>
		    </center>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/back-end/school_management/department_management.js');?>"></script>
