<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class AuditLog
{
    const INSERT_METHODS = [
        'insert', 'create', 'add', 'post', 'set'
    ];

    const UPDATE_METHODS = [
        'update', 'edit', 'save'
    ];

    const DELETE_METHODS = [
        'delete', 'remove', 'soft'
    ];

    const RESTORE_METHODS = ['restore'];

    private $all_methods = [];

    public function __construct()
    {
        $this->all_methods = array_merge(self::INSERT_METHODS, self::UPDATE_METHODS, self::DELETE_METHODS, self::RESTORE_METHODS);
    }

    public function log()
    {
        // We need an instance of CI as we will be using some CI classes
        $CI =& get_instance();

        if ($CI->session->userdata('account_type') == 3 && $this->match($this->all_methods, $CI->router->method)) {
            // Start off with the session stuff we know
            $data = array();
            $data['log_user_id'] = $CI->session->userdata('ad_id');

            // Next up, we want to know what page we're on, use the router class
            $data['log_module'] = ucfirst(trim(str_replace('_', ' ', current(explode('_Controller', $CI->router->class)))));

            if ($this->match(self::INSERT_METHODS, $CI->router->method)) {
                $data['log_action'] = 'INSERT';
            } elseif ($this->match(self::UPDATE_METHODS, $CI->router->method)) {
                $data['log_action'] = 'UPDATE';
            } elseif ($this->match(self::RESTORE_METHODS, $CI->router->method)) {
                $data['log_action'] = 'RESTORE';
            } else {
                if (strpos($CI->router->method, 'soft') !== false) {
                    $data['log_action'] = 'ARCHIVE';
                } else {
                    $data['log_action'] = 'DELETE';
                }
            }

            // And write it to the database
            $CI->db->insert('log_tbl', $data);
        }
    }

    private function match($needles, $haystack)
    {
        foreach ($needles as $needle) {
            if (strpos($haystack, $needle) !== false) {
                return true;
            }
        }
        return false;
    }
}
