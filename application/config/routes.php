<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|   example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|   https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|   $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|   $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|   $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
|       my-controller/my-method -> my_controller/my_method
*/

//My route list
$route['default_controller']   = 'Master_Controller/home';
$route['404_override']         = '';
$route['translate_uri_dashes'] = false;
$route['Home']                                                      = 'Master_Controller/home';
$route['Ojt_policy']                                                = 'Master_Controller/Ojt_policy';
$route['Partner_companies']                                         = 'Master_Controller/Partner_Company';
$route['Faqs']                                                      = 'Master_Controller/Faqs';
$route['Mobile_login']                                              = 'Master_Controller/mobile_login';
// Administration Routes
$route['Secret_log_in/log_in']                                      = 'Master_Controller/admin_log_in';
$route['Administration/dashboard']                                  = 'back-end/Administrator_Controller/dashboard';
$route['Administration/news_management']                            = 'back-end/Administrator_Controller/news_management';
$route['Administration/news_management/bin']                        = 'back-end/Administrator_Controller/news_bin';
//Class List
$route['Administration/class_list']                                 = 'back-end/Administrator_Controller/class_list';
$route['Administration/class_list/bin']                             = 'back-end/Administrator_Controller/class_list_bin';
$route['Administration/class_student_list/(:num)/(:num)/(:any)']    = 'back-end/Administrator_Controller/class_student_list/$1/$2/$3';
//User Management Routes
$route['Administration/administrator_management']                   = 'back-end/Administrator_Controller/administrator_management';
$route['Administration/administrator_management/bin']               = 'back-end/Administrator_Controller/administrator_management_bin';
$route['Administration/sub_administrator_management']               = 'back-end/Administrator_Controller/sub_administrator_management';
$route['Administration/sub_administrator_management/bin']           = 'back-end/Administrator_Controller/sub_administrator_management_bin';
$route['Administration/student_management']                         = 'back-end/Administrator_Controller/student_management';
$route['Administration/student_management/bin']                     = 'back-end/Administrator_Controller/student_management_bin';
$route['Administration/faculty_management']                         = 'back-end/Administrator_Controller/faculty_management';
$route['Administration/faculty_management/bin']                     = 'back-end/Administrator_Controller/faculty_management_bin';
//School Management Routes
$route['Administration/course_management']                          = 'back-end/Administrator_Controller/course_management';
$route['Administration/course_management/bin']                      = 'back-end/Administrator_Controller/course_management_bin';
$route['Administration/program_management']                         = 'back-end/Administrator_Controller/subject_management';
$route['Administration/program_management/bin']                     = 'back-end/Administrator_Controller/subject_management_bin';
$route['Administration/section_management']                         = 'back-end/Administrator_Controller/section_management';
$route['Administration/section_management/bin']                     = 'back-end/Administrator_Controller/section_management_bin';
$route['Administration/department_management']                      = 'back-end/Administrator_Controller/department_management';
$route['Administration/department_management/bin']                  = 'back-end/Administrator_Controller/department_management_bin';
//OJT Management Routes
$route['Administration/uploaded_documents']                         = 'back-end/Administrator_Controller/uploaded_documents';
$route['Administration/uploaded_documents/bin']                     = 'back-end/Administrator_Controller/uploaded_documents_bin';
$route['Administration/ojt_requirements']                           = 'back-end/Administrator_Controller/ojt_requirements';
$route['Administration/ojt_requirements/bin']                       = 'back-end/Administrator_Controller/ojt_requirements_bin';
$route['Administration/downloadable_forms']                         = 'back-end/Administrator_Controller/downloadable_forms';
$route['Administration/downloadable_forms/bin']                     = 'back-end/Administrator_Controller/downloadable_forms_bin';
$route['Administration/ojt_policy']                                 = 'back-end/Administrator_Controller/ojt_policy';
$route['Administration/slides_management']                          = 'back-end/Administrator_Controller/slides_management';
$route['Administration/slides_management/bin']                      = 'back-end/Administrator_Controller/slides_management_bin';
$route['Administration/partner_companies']                          = 'back-end/Administrator_Controller/partner_companies';
$route['Administration/partner_companies/bin']                      = 'back-end/Administrator_Controller/partner_companies_bin';
$route['Administration/faqs']                                       = 'back-end/Administrator_Controller/faqs';
$route['Administration/faqs/bin']                                   = 'back-end/Administrator_Controller/faqs_bin';
$route['Administration/audit_log']                                  = 'back-end/Administrator_Controller/audit_log';
// -- End of Administration
// Sub Administation
$route['Sub_Administration/dashboard']                                  = 'back-end/Sub_Administrator_Controller/dashboard';
$route['Sub_Administration/news_management']                            = 'back-end/Sub_Administrator_Controller/news_management';
//Class List
$route['Sub_Administration/class_list']                                 = 'back-end/Sub_Administrator_Controller/class_list';
$route['Sub_Administration/class_student_list/(:num)/(:num)/(:any)']    = 'back-end/Sub_Administrator_Controller/class_student_list/$1/$2/$3';
//User Management Routes
$route['Sub_Administration/student_management']                         = 'back-end/Sub_Administrator_Controller/student_management';
$route['Sub_Administration/faculty_management']                         = 'back-end/Sub_Administrator_Controller/faculty_management';
//School Management Routes
$route['Sub_Administration/course_management']                          = 'back-end/Sub_Administrator_Controller/course_management';
$route['Sub_Administration/program_management']                         = 'back-end/Sub_Administrator_Controller/subject_management';
$route['Sub_Administration/section_management']                         = 'back-end/Sub_Administrator_Controller/section_management';
$route['Sub_Administration/department_management']                      = 'back-end/Sub_Administrator_Controller/department_management';
//OJT Management Routes
$route['Sub_Administration/uploaded_documents']                         = 'back-end/Sub_Administrator_Controller/uploaded_documents';
$route['Sub_Administration/ojt_requirements']                           = 'back-end/Sub_Administrator_Controller/ojt_requirements';
$route['Sub_Administration/downloadable_forms']                         = 'back-end/Sub_Administrator_Controller/downloadable_forms';
$route['Sub_Administration/ojt_policy']                                 = 'back-end/Sub_Administrator_Controller/ojt_policy';
$route['Sub_Administration/slides_management']                          = 'back-end/Sub_Administrator_Controller/slides_management';
$route['Sub_Administration/partner_companies']                          = 'back-end/Sub_Administrator_Controller/partner_companies';
$route['Sub_Administration/audit_log']                                  = 'back-end/Sub_Administrator_Controller/audit_log';
$route['Sub_Administration/faqs']                                       = 'back-end/Sub_Administrator_Controller/faqs';
// -- End of Sub Administration
// Student Routes
$route['student/class_room']                                        = 'front-end/student/Student_Controller/class_room';
$route['student/(:any)/comments']                                   = 'front-end/student/Student_Controller/comment_room/$1';
$route['student/notifications']                                     = 'front-end/student/Student_Controller/notifications';
$route['student/profile']                                           = 'front-end/student/Student_Controller/profile';
$route['student/news']                                              = 'front-end/student/Student_Controller/news';
$route['student/my_documents']                                      = 'front-end/student/Student_Controller/my_documents';
$route['student/requirements_list']                                 = 'front-end/student/Student_Controller/requirements_list';
$route['student/downloadable_forms']                                = 'front-end/student/Student_Controller/downloadable_forms';
$route['student/companies']                                         = 'front-end/student/Student_Controller/companies';
$route['student/company/(:any)']                                    = 'front-end/student/Student_Controller/company/$1';
// -- End of Student
// Faculty Routes
$route['faculty/profile']                                           = 'front-end/faculty/Faculty_Controller/profile';
$route['faculty/news']                                              = 'front-end/faculty/Faculty_Controller/news';
$route['faculty/class_list']                                        = 'front-end/faculty/Faculty_Controller/class_list';
$route['faculty/(:any)/comments']                                   = 'front-end/faculty/Faculty_Controller/comment_room/$1';
$route['faculty/class_room/(:any)/(:any)']                          = 'front-end/faculty/Faculty_Controller/class_room/$1/$2';
$route['faculty/student_list/(:any)/(:any)']                        = 'front-end/faculty/Faculty_Controller/student_list/$1/$2';
$route['faculty/student_info/(:any)/(:any)']                        = 'front-end/faculty/Faculty_Controller/student_info/$1/$2';
$route['faculty/downloadable_forms']                                = 'front-end/faculty/Faculty_Controller/downloadable_forms';
$route['faculty/announcements']                                     = 'front-end/faculty/Faculty_Controller/announcements';
$route['faculty/notifications']                                     = 'front-end/faculty/Faculty_Controller/notifications';
// -- End of Faculty
