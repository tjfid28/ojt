<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Administrator_Management_Model extends CI_Model
{
    public function get_administrators()
    {
        return $this->db->get_where('admin_tbl', ['deleted_at' => null]);
    }

    public function get_bin()
    {
        return $this->db->get_where('admin_tbl', ['deleted_at IS NOT ' => null]);
    }

    public function add_administrators($uname, $pword, $fname, $mname, $lname)
    {
        $query = $this->db->get_where('admin_tbl', ['ad_uname' => $uname]);

        if ($query->num_rows() == 0) {
            $data = [
                'ad_uname' => $uname,
                'ad_pword' => $pword,
                'ad_fname' => $fname,
                'ad_mname' => $mname,
                'ad_lname' => $lname,
                'ad_created_at' => date("d-m-Y h:i:sa"),
                'ad_updated_at' => '',
            ];
            $this->db->insert('admin_tbl', $data);
        } else {
            echo "false";
        }
    }

    public function edit_administrators($uname, $pword, $fname, $mname, $lname, $id)
    {
        $query = $this->db->get_where('admin_tbl', ['ad_uname' => $uname, 'ad_id !=' => $id]);

        if ($query->num_rows() == 0) {
            $data = [
                'ad_uname' => $uname,
                'ad_pword' => $pword,
                'ad_fname' => $fname,
                'ad_mname' => $mname,
                'ad_lname' => $lname,
                'ad_updated_at'=> date("d-m-Y h:i:sa"),
            ];
            $this->db->update('admin_tbl', $data, ['ad_id' => $id]);
        } else {
            echo "false";
        }
    }

    public function delete_administrators($ad_id)
    {
        $this->db->delete("admin_tbl", ['ad_id' => $ad_id]);
    }

    public function softDelete($ad_id)
    {
        return $this->db->update(
            'admin_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['ad_id' => $ad_id]
        );
    }

    public function restore($ad_id)
    {
        return $this->db->update(
            'admin_tbl',
            ['deleted_at' => null],
            ['ad_id' => $ad_id]
        );
    }
}
