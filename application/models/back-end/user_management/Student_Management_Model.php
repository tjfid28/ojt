<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Student_Management_Model extends CI_Model
{
    public function get_students()
    {
        $this->db->join('course_tbl as course', 'course.cou_id = student.cou_id', 'inner');
        $this->db->order_by('st_lname');
        return $this->db->get_where('student_tbl as student', ['student.deleted_at' => null]);
    }

    public function get_bin()
    {
        $this->db->select('*, student.deleted_at as s_deleted_at');
        $this->db->join('course_tbl as course', 'course.cou_id = student.cou_id', 'inner');
        $this->db->order_by('st_lname');
        return $this->db->get_where('student_tbl as student', ['student.deleted_at IS NOT ' => null]);
    }

    public function add_student($uname, $pword, $fname, $mname, $lname, $cou_id, $gender, $year, $st_no, $email, $mobile_no, $st_img_content, $st_ext)
    {
        $query = $this->db->get_where('student_tbl', ['st_uname' => $uname, 'st_number' => $st_no]);

        if ($query->num_rows() == 0) {
            $root = getcwd();
            $get_Slug = rand(10, 99)."-".rand((int)10000000000, (int)99999999999).".".$st_ext;

            if (move_uploaded_file($st_img_content, $root."/resources/student_images/".basename($get_Slug))) {
                $data = [
                    'st_uname' => $uname,
                    'st_pword' => $pword,
                    'st_image_name' => $get_Slug,
                    'st_fname' => $fname,
                    'st_mname' => $mname,
                    'st_lname' => $lname,
                    'st_number' => $st_no,
                    'cou_id' => $cou_id,
                    'st_gender' => $gender,
                    'st_school_year' => $year,
                    'st_email' => $email,
                    'st_mobile_number' => $mobile_no,
                    'st_created_at' => date("d-m-Y h:i:sa"),
                    'st_updated_at' => ''
                ];

                $this->db->insert('student_tbl', $data);
            } else {
                echo "Error has been occured ! Please try again .";
            }
        } else {
            echo "false";
        }
    }

    public function import_student($student_data)
    {
        $this->db->insert('student_tbl', $student_data);
    }

    public function edit_student($uname, $pword, $fname, $mname, $lname, $cou_id, $gender, $year, $st_no, $email, $mobile_no, $st_img_content, $st_ext, $st_image_name, $old_st_image, $id)
    {
        $this->db->where('((st_pword="'.$pword.'" or st_number = "'.$st_no.'") and st_id!='.$id.' )');
        $query = $this->db->get('student_tbl');

        if ($query->num_rows() == 0) {
            $data = null;

            if ($st_image_name != null) {
                $root = getcwd();
                $get_Slug = rand(10, 99)."-".rand((int)10000000000, (int)99999999999).".".$st_ext;
                unlink($root."/resources/student_images/".$old_st_image);
                move_uploaded_file($st_img_content, $root."/resources/student_images/".basename($get_Slug));
                $data = [
                    'st_uname' => $uname,
                    'st_pword' => $pword,
                    'st_image_name'=> $get_Slug,
                    'st_fname' => $fname,
                    'st_mname' => $mname,
                    'st_lname' => $lname,
                    'st_number' => $st_no,
                    'st_email' => $email,
                    'st_mobile_number' => $mobile_no,
                    'cou_id' => $cou_id,
                    'st_gender' => $gender,
                    'st_school_year' => $year,
                    'st_updated_at' => date("d-m-Y h:i:sa")
                ];
            } else {
                $data = [
                    'st_uname' => $uname,
                    'st_pword' => $pword,
                    'st_fname' => $fname,
                    'st_mname' => $mname,
                    'st_lname' => $lname,
                    'st_number' => $st_no,
                    'st_email' => $email,
                    'st_mobile_number' => $mobile_no,
                    'cou_id' => $cou_id,
                    'st_gender' => $gender,
                    'st_school_year' => $year,
                    'st_updated_at'=> $date,
                ];
            }

            $this->db->update('student_tbl', $data, ['st_id' => $id]);
        } else {
            echo "false";
        }
    }

    public function delete_student($st_id, $st_img)
    {
        $root = getcwd();
        unlink($root."/resources/student_images/".$st_img);
        $this->db->delete("student_tbl", ['st_id' => $st_id]);
    }

    public function softDelete($st_id)
    {
        return $this->db->update(
            'student_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['st_id' => $st_id]
        );
    }

    public function restore($st_id)
    {
        return $this->db->update(
            'student_tbl',
            ['deleted_at' => null],
            ['st_id' => $st_id]
        );
    }
}
