<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Sub_Administrator_Management_Model extends CI_Model
{
    public function get_sub_administrators()
    {
        return $this->db->get_where('sub_admin_tbl', ['deleted_at' => null]);
    }

    public function get_bin()
    {
        return $this->db->get_where('sub_admin_tbl', ['deleted_at IS NOT ' => null]);
    }

    public function add_sub_administrators($data)
    {
        $query = $this->db->get_where('sub_admin_tbl', ['s_ad_uname' => $data['s_ad_uname']]);

        if ($query->num_rows() == 0) {
            $this->db->insert('sub_admin_tbl', $data);
        } else {
            echo "false";
        }
    }

    public function edit_sub_administrators($data, $id)
    {
        $query = $this->db->get_where('sub_admin_tbl', ['s_ad_uname' => $data['s_ad_uname'], 's_ad_id !=' => $id]);

        if ($query->num_rows() == 0) {
            $this->db->update('sub_admin_tbl', $data, ['s_ad_id' => $id]);
        } else {
            echo "false";
        }
    }

    public function delete_sub_administrators($s_ad_id)
    {
        $this->db->delete("sub_admin_tbl", ['s_ad_id' => $s_ad_id]);
    }

    public function softDelete($s_ad_id)
    {
        return $this->db->update(
            'sub_admin_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['s_ad_id' => $s_ad_id]
        );
    }

    public function restore($s_ad_id)
    {
        return $this->db->update(
            'sub_admin_tbl',
            ['deleted_at' => null],
            ['s_ad_id' => $s_ad_id]
        );
    }
}
