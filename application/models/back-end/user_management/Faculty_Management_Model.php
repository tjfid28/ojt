<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Faculty_Management_Model extends CI_Model
{
    public function get_faculty()
    {
        $this->db->join('department_tbl as department', 'department.dept_id = faculty.dept_id', 'inner');
        return $this->db->get_where('faculty_tbl as faculty', ['faculty.deleted_at' => null]);
    }

    public function get_bin()
    {
        $this->db->select('*, faculty.deleted_at as f_deleted_at');
        $this->db->join('department_tbl as department', 'department.dept_id = faculty.dept_id', 'inner');
        return $this->db->get_where('faculty_tbl as faculty', ['faculty.deleted_at IS NOT ' => null]);
    }

    public function add_faculty($data, $fa_img_content, $fa_ext)
    {
        $this->db->where('fa_uname', $data['fa_uname']);
        $this->db->or_where('fa_number', $data['fa_number']);
        $query = $this->db->get('faculty_tbl');

        if ($query->num_rows() == 0) {
            $root = getcwd();
            $get_Slug = rand(10, 99)."-".rand((int)10000000000, (int)99999999999).".".$fa_ext;

            if (move_uploaded_file($fa_img_content, $root."/resources/faculty_images/".basename($get_Slug))) {
                $data['fa_image_name'] = $get_Slug;
                $this->db->insert('faculty_tbl', $data);
            } else {
                echo "An error has occured! Please try again .";
            }
        } else {
            echo "false";
        }
    }

    public function edit_faculty($uname, $pword, $fname, $mname, $lname, $dept_id, $gender, $fa_no, $fa_img_content, $fa_ext, $fa_image_name, $old_fa_image, $id)
    {
        $this->db->select('fa_pword,fa_id,fa_number');
        $this->db->from('faculty_tbl as faculty');
        $this->db->where('((fa_pword = "'.$pword.'" or fa_number= "'.$fa_no.'") and fa_id!='.$id.' )');
        $query = $this->db->get();
        if ($query->num_rows()==0) {
            $data = null;
            $date=date("d-m-Y h:i:sa");

            if ($fa_image_name!=null) {
                $root = getcwd();
                $get_Slug = rand(10, 99)."-".rand((int)10000000000, (int)99999999999).".".$fa_ext;
                unlink($root."/resources/faculty_images/".$old_fa_image);
                move_uploaded_file($fa_img_content, $root."/resources/faculty_images/".basename($get_Slug));
                $data = [
                        'fa_uname'	   => $uname,
                        'fa_pword'	   => $pword,
                        'fa_image_name'=> $get_Slug,
                        'fa_fname'	   => $fname,
                        'fa_mname'	   => $mname,
                        'fa_lname'	   => $lname,
                        'fa_number'	   => $fa_no,
                        'dept_id'	   => $dept_id,
                        'fa_gender'	   => $gender,
                        'fa_updated_at'=> $date,
                    ];
            } else {
                $data = [
                        'fa_uname'	   => $uname,
                        'fa_pword'	   => $pword,
                        'fa_fname'	   => $fname,
                        'fa_mname'	   => $mname,
                        'fa_lname'	   => $lname,
                        'fa_number'	   => $fa_no,
                        'dept_id'	   => $dept_id,
                        'fa_gender'	   => $gender,
                        'fa_updated_at'=> $date,
                    ];
            }

            $this->db->where("fa_id=".$id);
            $this->db->update('faculty_tbl', $data);
        } else {
            echo "false";
        }
    }

    public function delete_faculty($fa_id, $fa_img)
    {
        $root = getcwd();
        unlink($root."/resources/faculty_images/".$fa_img);
        $this->db->where('fa_id', $fa_id);
        $this->db->delete("faculty_tbl");
    }

    public function softDelete($fa_id)
    {
        return $this->db->update(
            'faculty_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['fa_id' => $fa_id]
        );
    }

    public function restore($fa_id)
    {
        return $this->db->update(
            'faculty_tbl',
            ['deleted_at' => null],
            ['fa_id' => $fa_id]
        );
    }
}
