<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ojt_Policy_Model extends CI_Model
{
    public function get_policy()
    {
        $this->db->select('*');
        $this->db->from('ojt_policy_tbl');
        $query = $this->db->get();
        return $query->row_array();
    }

    public function save_policy($content, $img_content, $ext)
    {
        $root = getcwd();
        $date = date("d-m-Y h:i:sa");
        $data = null;
        if ($img_content!=null||$img_content!='') {
            $name = "policy_cover_".rand(100, 999).".".$ext;
            if ($_SESSION['policy_cover']!=null||$_SESSION['policy_cover']!='') {
                unlink($root."/resources/images/".$_SESSION['policy_cover']);
            }
            if (move_uploaded_file($img_content, $root."/resources/images/".basename($name))) {
                $data = [
                    'op_content'	 => $content,
                    'op_image'	 	 => $name,
                    'op_updated_at'  => $date,
                ];
            } else {
                echo "Error has been occured ! Please try again .";
            }
        } else {
            $data = [
                'op_content'	 => $content,
                'op_updated_at'  => $date,
            ];
        }
        $this->db->where('op_id', '1');
        $this->db->update('ojt_policy_tbl', $data);
        echo "Success";
    }
}
