<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ojt_Requirement_Model extends CI_Model
{
    public function get_requirements()
    {
        $this->db->order_by('req_name');
        return $this->db->get_where('ojt_requirements_tbl', ['deleted_at' => null]);
    }

    public function get_bin()
    {
        $this->db->order_by('req_name');
        return $this->db->get_where('ojt_requirements_tbl', ['deleted_at IS NOT ' => null]);
    }

    public function add_requirement($req_name, $sub_id, $cou_id)
    {
        date_default_timezone_set("Asia/Manila");
        $date=date("d-m-Y h:i:sa");
        $data = array(
                'req_name'	   	  => $req_name,
                'cou_id'	   	  => $cou_id,
                'sub_id'	   	  => $sub_id,
                'req_created_at'  => $date,
                'req_updated_at'  => '',
            );
        $this->db->insert('ojt_requirements_tbl', $data);
    }

    public function edit_requirement($req_name, $sub_id, $cou_id, $req_id)
    {
        date_default_timezone_set("Asia/Manila");
        $date=date("d-m-Y h:i:sa");
        $data = array(
                'req_name'	   	  => $req_name,
                'cou_id'	   	  => $cou_id,
                'sub_id'	   	  => $sub_id,
                'req_updated_at'  => $date,
            );
        $this->db->where("req_id=".$req_id);
        $this->db->update('ojt_requirements_tbl', $data);
    }

    public function delete_requirement($req_id)
    {
        $this->db->where('req_id', $req_id);
        $this->db->delete("ojt_requirements_tbl");
    }

    public function softDelete($req_id)
    {
        return $this->db->update(
            'ojt_requirements_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['req_id' => $req_id]
        );
    }

    public function restore($req_id)
    {
        return $this->db->update(
            'ojt_requirements_tbl',
            ['deleted_at' => null],
            ['req_id' => $req_id]
        );
    }
}
