<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Downloadable_Form_Model extends CI_Model
{
    public function get_forms()
    {
        $this->db->join('downloadable_forms_tbl as downloadable_forms', 'downloadable_forms.req_id = ojt_requirement.req_id', 'inner');
        return $this->db->get_where('ojt_requirements_tbl as ojt_requirement', ['downloadable_forms.deleted_at' => null]);
    }

    public function get_bin()
    {
        $this->db->join('downloadable_forms_tbl as downloadable_forms', 'downloadable_forms.req_id = ojt_requirement.req_id', 'inner');
        return $this->db->get_where('ojt_requirements_tbl as ojt_requirement', ['downloadable_forms.deleted_at IS NOT ' => null]);
    }

    public function add_form($df_content, $file_name, $req_id)
    {
        // Gather all required data
        $root = getcwd();
        $get_Slug = rand(100, 999)."-".rand((int)10000000000, (int)9999999999).".docx";
        $file_name = str_replace(" ", "_", $file_name);
        if (move_uploaded_file($df_content, $root."/resources/downloadable_forms/".basename($get_Slug))) {
            date_default_timezone_set("Asia/Manila");
            $date=date("d-m-Y h:i:sa");
            $data = array(
                    'df_file_name'	 => $file_name,
                    'df_slug'		 => $get_Slug,
                    'req_id'	   	 => $req_id,
                    'df_created_at'  => $date,
                    'df_updated_at'  => '',
                );
            $this->db->insert('downloadable_forms_tbl', $data);
            echo "Success";
        } else {
            echo "Error has been occured ! Please try again .";
        }
    }
    public function delete_form($df_id, $df_slug)
    {
        $root = getcwd();
        unlink($root."/resources/downloadable_forms/".$df_slug);
        $this->db->where('df_id', $df_id);
        $this->db->delete("downloadable_forms_tbl");
    }

    public function softDelete($df_id)
    {
        return $this->db->update(
            'downloadable_forms_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['df_id' => $df_id]
        );
    }

    public function restore($df_id)
    {
        return $this->db->update(
            'downloadable_forms_tbl',
            ['deleted_at' => null],
            ['df_id' => $df_id]
        );
    }
}
