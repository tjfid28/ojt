<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Uploaded_Documents_Model extends CI_Model
{
    public function get_documents($search)
    {
        $this->db->select('document.*,concat(st_fname," ",st_lname) as owner,st_number');
        $this->db->from('documents_tbl as document');
        $this->db->join('student_tbl as student', 'student.st_id = document.st_id', 'left');
        if (strcmp($search, "")!=0) {
            $this->db->where('d_file_name LIKE "%'.$search.'%" or concat(st_fname," ",st_lname) LIKE "%'.$search.'%" or concat(st_lname," ",st_fname) LIKE "%'.$search.'%" or st_number LIKE "%'.$search.'%"');
        }
        $this->db->order_by('d_created_at');
        $this->db->where('document.deleted_at', null);
        return $query = $this->db->get();
    }

    public function get_bin()
    {
        $this->db->select('document.*,concat(st_fname," ",st_lname) as owner,st_number');
        $this->db->from('documents_tbl as document');
        $this->db->join('student_tbl as student', 'student.st_id = document.st_id', 'left');
        $this->db->order_by('d_created_at');
        $this->db->where('document.deleted_at IS NOT ', null);
        return $query = $this->db->get();
    }

    public function delete_document($d_id, $d_slug)
    {
        $root = getcwd();
        unlink($root."/resources/uploaded_documents/".$d_slug);
        $this->db->where('d_id', $d_id);
        $this->db->delete("documents_tbl");
    }

    public function softDelete($d_id)
    {
        return $this->db->update(
            'documents_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['d_id' => $d_id]
        );
    }

    public function restore($d_id)
    {
        return $this->db->update(
            'documents_tbl',
            ['deleted_at' => null],
            ['d_id' => $d_id]
        );
    }

    public function getResume($d_id)
    {
        $root = getcwd();
        $this->db->where('d_id', $d_id);
        $fileName = $this->db->get("documents_tbl")->row()->d_slug;
        return $root . '/resources/uploaded_documents/' . $fileName;
    }
}
