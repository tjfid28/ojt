<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Slide_Model extends CI_Model
{
    public function get_slides()
    {
        return $this->db->get_where('slide_tbl', ['deleted_at' => null]);
    }

    public function get_bin()
    {
        return $this->db->get_where('slide_tbl', ['deleted_at IS NOT ' => null]);
    }

    public function create_slide($s_image_content, $s_image_name, $caption, $description)
    {
        // Gather all required data
        $root = getcwd();
        $get_Slug = $this->api->generate_slug($s_image_name);
        $s_image_name = str_replace(" ", "_", $s_image_name);
        if (move_uploaded_file($s_image_content, $root."/resources/slide-images/".basename($get_Slug))) {
            $date=date("d-m-Y h:i:sa");
            $data = [
                    's_slug_name'	 => $get_Slug,
                    's_caption'		 => $caption,
                    's_description'	 => $description,
                    's_created_at'   => $date,
                ];
            $this->db->insert('slide_tbl', $data);
            echo "Success";
        } else {
            echo "Error has been occured ! Please try again .";
        }
    }

    public function delete_slide($s_id, $slugname)
    {
        $root = getcwd();
        unlink($root."/resources/slide-images/".$slugname);
        $this->db->where('s_id', $s_id);
        $this->db->delete("slide_tbl");
    }

    public function softDelete($s_id)
    {
        return $this->db->update(
            'slide_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['s_id' => $s_id]
        );
    }

    public function restore($s_id)
    {
        return $this->db->update(
            'slide_tbl',
            ['deleted_at' => null],
            ['s_id' => $s_id]
        );
    }
}
