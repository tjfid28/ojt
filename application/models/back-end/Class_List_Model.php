<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Class_List_Model extends CI_Model
{
    public function get_classes()
    {
        $this->db->select('*,CONCAT(fa_fname," ",fa_lname)as adviser');
        $this->db->from('class_tbl as class');
        $this->db->join('section_tbl as section', 'section.sec_id = class.sec_id', 'inner');
        $this->db->join('course_tbl as course', 'course.cou_id = class.cou_id', 'inner');
        $this->db->join('subject_tbl as subject', 'subject.sub_id = class.sub_id', 'inner');
        $this->db->join('faculty_tbl as faculty', 'faculty.fa_id = class.fa_id', 'inner');
        $this->db->order_by('class.cla_id', 'desc');
        $this->db->where('class.deleted_at', null);
        return $query = $this->db->get();
    }

    public function get_bin()
    {
        $this->db->select('*,CONCAT(fa_fname," ",fa_lname)as adviser');
        $this->db->from('class_tbl as class');
        $this->db->join('section_tbl as section', 'section.sec_id = class.sec_id', 'inner');
        $this->db->join('course_tbl as course', 'course.cou_id = class.cou_id', 'inner');
        $this->db->join('subject_tbl as subject', 'subject.sub_id = class.sub_id', 'inner');
        $this->db->join('faculty_tbl as faculty', 'faculty.fa_id = class.fa_id', 'inner');
        $this->db->order_by('class.cla_id', 'desc');
        $this->db->where('class.deleted_at IS NOT ', null);
        return $query = $this->db->get();
    }

    public function create_class($sec_id, $sub_id, $cou_id, $fa_id, $term, $year)
    {
        date_default_timezone_set("Asia/Manila");
        $date=date("d-m-Y h:i:sa");
        $data = [
                'sec_id'	   => $sec_id,
                'sub_id'	   => $sub_id,
                'cou_id'	   => $cou_id,
                'fa_id'	   	   => $fa_id,
                'cla_term'	   => $term,
                'cla_year'     => $year,
                'cla_status'   => 'Open',
                'cla_created_at'   => $date,
                'cla_updated_at'   => '',
            ];
        $this->db->insert('class_tbl', $data);
    }

    public function edit_class($sub_id, $cou_id, $fa_id, $cla_id, $term, $year, $status)
    {
        date_default_timezone_set("Asia/Manila");
        $date=date("d-m-Y h:i:sa");
        $data = [
                'sub_id'	    => $sub_id,
                'cou_id'	    => $cou_id,
                'fa_id'	   	    => $fa_id,
                'cla_term'	   => $term,
                'cla_year'     => $year,
                'cla_status'   => $status,
                'cla_updated_at'=> $date,
            ];
        $this->db->where("cla_id=".$cla_id);
        $this->db->update('class_tbl', $data);
    }

    public function delete_class($cla_id)
    {
        $this->db->where('cla_id', $cla_id);
        $this->db->delete("class_tbl");
    }

    public function softDelete($cla_id)
    {
        return $this->db->update(
            'class_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['cla_id' => $cla_id]
        );
    }

    public function restore($cla_id)
    {
        return $this->db->update(
            'class_tbl',
            ['deleted_at' => null],
            ['cla_id' => $cla_id]
        );
    }
}
