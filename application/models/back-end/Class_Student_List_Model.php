<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

    class Class_Student_List_Model extends CI_Model
    {
        public function get_students($cou_id)
        {
            $this->db->select('*,student.st_id as student_real_id');
            $this->db->from('student_tbl as student');
            $this->db->join('class_student_list_tbl as class_student', 'student.st_id = class_student.st_id', 'left');
            $this->db->where("class_student.st_id is null and student.cou_id=".$cou_id);
            return $query = $this->db->get();
        }

        public function get_students_in_class()
        {
            $this->db->select('*');
            $this->db->from('class_student_list_tbl as class_student');
            $this->db->join('student_tbl as student', 'student.st_id = class_student.st_id', 'inner');
            $this->db->join('course_tbl as course', 'student.cou_id = course.cou_id', 'inner');
            $this->db->where('cla_id', $_SESSION['cla_id']);
            $this->db->order_by('cs_id', 'desc');
            return $query = $this->db->get();
        }

        public function add_student($st_id_container, $cla_id)
        {
            $st_id_container = explode(",", $st_id_container);
            date_default_timezone_set("Asia/Manila");
            $date=date("d-m-Y h:i:sa");
            $data = null;
            foreach ($st_id_container  as $get => $st_id) {
                $data = [
                'cla_id'	   => $cla_id,
                'st_id'		   => $st_id,
                'cs_added_at'  => $date,
                ];
                $this->db->insert('class_student_list_tbl', $data);
            }
        }

        public function delete_student($cs_id)
        {
            $this->db->where('cs_id', $cs_id);
            $this->db->delete("class_student_list_tbl");
        }
    }
