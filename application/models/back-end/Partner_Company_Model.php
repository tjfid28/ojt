<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Partner_Company_Model extends CI_Model
{
    public function get_companies($type = 0)
    {
        if ($type != 0) {
            $this->db->where('co_type', $type);
        }
        return $this->db->get_where('company_tbl', ['deleted_at' => null]);
    }

    public function get_bin()
    {
        return $this->db->get_where('company_tbl', ['deleted_at IS NOT ' => null]);
    }

    public function get_company($slug)
    {
        return $this->db->get_where('company_tbl', ['slug' => $slug]);
    }

    public function create_company($s_logo_content, $s_logo_name, $company_name, $slug, $description, $link, $email, $type)
    {
        // Gather all required data
        $root = getcwd();
        $get_Slug = $this->api->generate_slug($s_logo_name);
        $s_logo_name = str_replace(" ", "_", $s_logo_name);

        if (move_uploaded_file($s_logo_content, $root."/resources/partner_companies/".basename($get_Slug))) {
            $date = date("d-m-Y h:i:sa");
            $data = [
                'co_image_name' => $get_Slug,
                'co_name' => $company_name,
                'slug' => $slug,
                'co_email' => $email,
                'co_type' => $type,
                'co_link' => $link,
                'co_description' => $description,
                'co_created_at'  => $date,
            ];
            $this->db->insert('company_tbl', $data);
            echo "Success";
        } else {
            echo "Error has been occured ! Please try again .";
        }
    }

    public function edit_company($old_image, $co_id, $s_logo_content, $s_logo_name, $company_name, $description, $link, $email, $type)
    {
        // Gather all required data
        $root = getcwd();
        $get_Slug = $this->api->generate_slug($s_logo_name);
        $s_logo_name = str_replace(" ", "_", $s_logo_name);
        if (strcmp($s_logo_name, "")!=0&&$s_logo_name!=null) {
            unlink($root."/resources/partner_companies/".$old_image);
            move_uploaded_file($s_logo_content, $root."/resources/partner_companies/".basename($get_Slug));
        }

        $date = date("d-m-Y h:i:sa");
        $data = [
            'co_image_name' => $get_Slug,
            'co_name' => $company_name,
            'co_type' => $type,
            'co_description' => $description,
            'co_link' => $link,
            'co_email' => $email,
            'co_updated_at' => $date,
        ];

        $this->db->where('co_id', $co_id);
        $this->db->update('company_tbl', $data);
    }
    public function delete_company($co_id, $image_to_delete)
    {
        $root = getcwd();
        unlink($root."/resources/partner_companies/".$image_to_delete);
        $this->db->where('co_id', $co_id);
        $this->db->delete("company_tbl");
    }

    public function softDelete($co_id)
    {
        return $this->db->update(
            'company_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['co_id' => $co_id]
        );
    }

    public function restore($co_id)
    {
        return $this->db->update(
            'company_tbl',
            ['deleted_at' => null],
            ['co_id' => $co_id]
        );
    }
}
