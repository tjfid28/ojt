<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Subject_Management_Model extends CI_Model
{
    public function get_subjects()
    {
        return $this->db->get_where('subject_tbl', ['deleted_at' => null]);
    }

    public function get_bin()
    {
        return $this->db->get_where('subject_tbl', ['deleted_at IS NOT ' => null]);
    }

    public function add_subject($sub_name, $sub_req_hours, $sub_unit)
    {
        $this->db->select('sub_name');
        $this->db->from('subject_tbl');
        $this->db->where('sub_name', $sub_name);
        $query = $this->db->get();
        if ($query->num_rows()==0) {
            date_default_timezone_set("Asia/Manila");
            $date=date("d-m-Y h:i:sa");
            $data = [
                'sub_name'	   	   => $sub_name,
                'sub_req_hours'    => $sub_req_hours,
                'sub_unit'	       => $sub_unit,
                'sub_created_at'   => $date,
                'sub_updated_at'   => '',
            ];
            $this->db->insert('subject_tbl', $data);

            return true;
        } else {
            return "Subject ".$sub_name." already exist.";
        }
    }

    public function edit_subject($sub_name, $sub_req_hours, $sub_unit, $sub_id)
    {
        $this->db->select('sub_name,sub_id');
        $this->db->from('subject_tbl');
        $this->db->where('sub_name ="'.$sub_name.'" and sub_id!='.$sub_id);
        $query = $this->db->get();
        if ($query->num_rows()==0) {
            date_default_timezone_set("Asia/Manila");
            $date=date("d-m-Y h:i:sa");
            $data = [
                'sub_name'	   	   => $sub_name,
                'sub_req_hours'    => $sub_req_hours,
                'sub_unit'	       => $sub_unit,
                'sub_updated_at'   => $date,
            ];
            $this->db->where("sub_id=".$sub_id);
            $this->db->update('subject_tbl', $data);

            return true;
        } else {
            return "Subject ".$sub_name." already exist.";
        }
    }

    public function delete_subject($sub_id)
    {
        $this->db->where('sub_id', $sub_id);
        $this->db->delete("subject_tbl");

        return true;
    }

    public function softDelete($sub_id)
    {
        return $this->db->update(
            'subject_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['sub_id' => $sub_id]
        );
    }

    public function restore($sub_id)
    {
        return $this->db->update(
            'subject_tbl',
            ['deleted_at' => null],
            ['sub_id' => $sub_id]
        );
    }
}
