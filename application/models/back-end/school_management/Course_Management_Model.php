<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Course_Management_Model extends CI_Model
{
    public function get_courses()
    {
        return $this->db->get_where('course_tbl', ['deleted_at' => null]);
    }

    public function get_bin()
    {
        return $this->db->get_where('course_tbl', ['deleted_at IS NOT ' => null]);
    }

    public function add_course($cou_title)
    {
        $this->db->select('cou_title');
        $this->db->from('course_tbl');
        $this->db->where('cou_title', $cou_title);
        $query = $this->db->get();
        if ($query->num_rows()==0) {
            date_default_timezone_set("Asia/Manila");
            $date=date("d-m-Y h:i:sa");
            $data = [
                'cou_title'	   	   => $cou_title,
                'cou_created_at'   => $date,
                'cou_updated_at'   => '',
            ];
            $this->db->insert('course_tbl', $data);

            return true;
        } else {
            return "Course ".$cou_title." already exist.";
        }
    }

    public function edit_course($cou_title, $cou_id)
    {
        $this->db->select('cou_title');
        $this->db->from('course_tbl');
        $this->db->where('cou_title ="'.$cou_title.'" and cou_id!='.$cou_id);
        $query = $this->db->get();
        if ($query->num_rows()==0) {
            date_default_timezone_set("Asia/Manila");
            $date=date("d-m-Y h:i:sa");
            $data = [
                'cou_title'	     => $cou_title,
                'cou_updated_at' => $date,
            ];
            $this->db->where("cou_id=".$cou_id);
            $this->db->update('course_tbl', $data);

            return true;
        } else {
            return "Course ".$cou_title." already exist.";
        }
    }

    public function delete_course($cou_id)
    {
        $this->db->where('cou_id', $cou_id);
        $this->db->delete("course_tbl");
    }

    public function softDelete($cou_id)
    {
        return $this->db->update(
            'course_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['cou_id' => $cou_id]
        );
    }

    public function restore($cou_id)
    {
        return $this->db->update(
            'course_tbl',
            ['deleted_at' => null],
            ['cou_id' => $cou_id]
        );
    }
}
