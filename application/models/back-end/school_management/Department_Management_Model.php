<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Department_Management_Model extends CI_Model
{
    public function get_departments()
    {
        return $this->db->get_where('department_tbl', ['deleted_at' => null]);
    }

    public function get_bin()
    {
        return $this->db->get_where('department_tbl', ['deleted_at IS NOT ' => null]);
    }

    public function add_department($dept_name)
    {
        $query = $this->db->get_where('department_tbl', ['dept_name' => $dept_name]);

        if ($query->num_rows() == 0) {
            $data = [
                'dept_name' => $dept_name,
                'dept_created_at' => date("d-m-Y h:i:sa"),
                'dept_updated_at' => '',
            ];

            $this->db->insert('department_tbl', $data);
            return true;
        } else {
            return "Department ".$dept_name." already exist.";
        }
    }

    public function edit_department($dept_name, $dept_id)
    {
        $query = $this->db->get_where('department_tbl', ['dept_name' => $dept_name, 'dept_id !=' => $dept_id]);

        if ($query->num_rows() == 0) {
            $data = [
                'dept_name' => $dept_name,
                'dept_updated_at' => date("d-m-Y h:i:sa"),
            ];

            $this->db->update('department_tbl', $data, ['dept_id' => $dept_id]);
            return true;
        } else {
            return "Department ".$dept_name." already exist.";
        }
    }

    public function delete_department($dept_id)
    {
        $this->db->delete("department_tbl", ['dept_id' => $dept_id]);
    }

    public function softDelete($dept_id)
    {
        return $this->db->update(
            'department_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['dept_id' => $dept_id]
        );
    }

    public function restore($dept_id)
    {
        return $this->db->update(
            'department_tbl',
            ['deleted_at' => null],
            ['dept_id' => $dept_id]
        );
    }
}
