<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Section_Management_Model extends CI_Model
{
    public function get_sections()
    {
        return $this->db->get_where('section_tbl', ['deleted_at' => null]);
    }

    public function get_bin()
    {
        return $this->db->get_where('section_tbl', ['deleted_at IS NOT ' => null]);
    }

    public function get_sections_to_class()
    {
        $this->db->select('section.*');
        $this->db->from('section_tbl as section');
        $this->db->join('class_tbl as class', 'class.sec_id = section.sec_id', 'left');
        $this->db->where("class.sec_id is null");
        return $query = $this->db->get();
    }

    public function add_section($sec_name)
    {
        $this->db->select('sec_name');
        $this->db->from('section_tbl');
        $this->db->where('sec_name', $sec_name);
        $query = $this->db->get();
        if ($query->num_rows()==0) {
            date_default_timezone_set("Asia/Manila");
            $date=date("d-m-Y h:i:sa");
            $data = [
                'sec_name'	   => $sec_name,
                'sec_created_at'   => $date,
                'sec_updated_at'   => '',
            ];
            $this->db->insert('section_tbl', $data);

            return true;
        } else {
            return "Section ".$sec_name." already exist.";
        }
    }

    public function edit_section($sec_name, $sec_id)
    {
        $this->db->select('sec_name,sec_id');
        $this->db->from('section_tbl');
        $this->db->where('sec_name = "'.$sec_name.'" and sec_id!='.$sec_id);
        $query = $this->db->get();
        if ($query->num_rows()==0) {
            date_default_timezone_set("Asia/Manila");
            $date=date("d-m-Y h:i:sa");
            $data = [
                'sec_name'	      => $sec_name,
                'sec_updated_at' => $date,
            ];
            $this->db->where("sec_id=".$sec_id);
            $this->db->update('section_tbl', $data);

            return true;
        } else {
            return "Section ".$sec_name." already exist.";
        }
    }

    public function delete_section($sec_id)
    {
        $this->db->where('sec_id', $sec_id);
        $this->db->delete("section_tbl");
        return true;
    }

    public function softDelete($sec_id)
    {
        return $this->db->update(
            'section_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['sec_id' => $sec_id]
        );
    }

    public function restore($sec_id)
    {
        return $this->db->update(
            'section_tbl',
            ['deleted_at' => null],
            ['sec_id' => $sec_id]
        );
    }
}
