<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class News_Model extends CI_Model
{
    public function get_news()
    {
        $this->db->order_by('n_id', 'desc');
        return $this->db->get_where('news_tbl', ['deleted_at' => null]);
    }

    public function get_bin()
    {
        $this->db->order_by('n_id', 'desc');
        return $this->db->get_where('news_tbl', ['deleted_at IS NOT ' => null]);
    }

    public function post_news($post)
    {
        $date = date("Y-m-d H:i:s");
        $data = [
            'n_content' => $post,
            'n_date_posted' => $date
        ];
        $this->db->insert('news_tbl', $data);

        $this->db->select('st_id,fb_id');
        $this->db->from('student_tbl');
        $this->db->where('fb_id is not null and fb_id!=""');
        $students = $this->db->get();

        $this->api->batch_notification_to_fb(json_encode($students->result()));

        $this->db->select('fa_id,fb_id');
        $this->db->from('faculty_tbl');
        $this->db->where('fb_id is not null and fb_id!=""');
        $faculty = $this->db->get();

        $this->api->batch_notification_to_fb(json_encode($faculty->result()));

        return "Success";
    }

    public function delete_news($n_id)
    {
        $data = [
            'n_id' => $n_id
        ];
        $this->db->where($data);
        $this->db->delete("news_tbl");

        return "Success";
    }

    public function softDelete($n_id)
    {
        return $this->db->update(
            'news_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['n_id' => $n_id]
        );
    }

    public function restore($n_id)
    {
        return $this->db->update(
            'news_tbl',
            ['deleted_at' => null],
            ['n_id' => $n_id]
        );
    }
}
