<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Faqs_Model extends CI_Model
{
    public function get_faqs()
    {
        return $this->db->get_where('faq_tbl', ['deleted_at' => null]);
    }

    public function get_bin()
    {
        return $this->db->get_where('faq_tbl', ['deleted_at IS NOT ' => null]);
    }

    public function create_faq($fa_title, $description)
    {
        $date = date("d-m-Y h:i:sa");
        $data = [
            'fa_title' => $fa_title,
            'fa_description' => $description,
            'fa_created_at'  => $date
        ];
        $this->db->insert('faq_tbl', $data);
    }

    public function edit_faq($fa_id, $fa_title, $description)
    {
        $date = date("d-m-Y h:i:sa");
        $data = [
            'fa_title'        => $fa_title,
            'fa_description' => $description,
            'fa_updated_at'  => $date
        ];

        $this->db->where('fa_id', $fa_id);
        $this->db->update('faq_tbl', $data);
    }

    public function delete_faq($fa_id)
    {
        $this->db->where('fa_id', $fa_id);
        $this->db->delete("faq_tbl");
    }

    public function softDelete($fa_id)
    {
        return $this->db->update(
            'faq_tbl',
            ['deleted_at' => date("Y-m-d H:i:s")],
            ['fa_id' => $fa_id]
        );
    }

    public function restore($fa_id)
    {
        return $this->db->update(
            'faq_tbl',
            ['deleted_at' => null],
            ['fa_id' => $fa_id]
        );
    }
}
