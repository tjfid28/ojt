<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Audit_Log_Model extends CI_Model
{
    public function get_audit_logs()
    {
        $this->db->join('admin_tbl', 'admin_tbl.ad_id = log_tbl.log_user_id', 'inner');
        $this->db->order_by('log_id', 'desc');
        return $this->db->get('log_tbl');
    }
}
