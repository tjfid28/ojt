<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Profile_Model extends CI_Model{

		public function modify_student_access($st_id,$uname,$pword)
		{
			$this->db->select('st_pword,st_id');
	        $this->db->from('student_tbl as student');
	        $this->db->where('(st_pword="'.$pword.'" and st_id!='.$st_id.' )');
	        $query = $this->db->get();
	        if($query->num_rows()==0)
			{
				$data = array(
					'st_uname' 	=> $uname,
					'st_pword'  => $pword
				);
				$_SESSION['st_uname'] = $uname;
				$_SESSION['st_pword'] = $pword;
				$this->db->where('st_id',$st_id);
				$this->db->update('student_tbl',$data);
			}
			else
			{
				echo "false";
			}
		}

		public function modify_faculty_access($fa_id,$uname,$pword)
		{
			$this->db->select('fa_pword,fa_id');
	        $this->db->from('faculty_tbl as faculty');
	        $this->db->where('(fa_pword="'.$pword.'" and fa_id!='.$fa_id.' )');
	        $query = $this->db->get();
	        if($query->num_rows()==0)
			{
				$data = array(
					'fa_uname' 	=> $uname,
					'fa_pword'  => $pword
				);
				$_SESSION['fa_uname'] = $uname;
				$_SESSION['fa_pword'] = $pword;
				$this->db->where('fa_id',$fa_id);
				$this->db->update('faculty_tbl',$data);
			}
			else
			{
				echo "false";
			}
		}

	}
?>