<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Facebook_Model extends CI_Model
{
    public function store_fb_id($st_id, $fb_id)
    {
        $data = array(
            'fb_id' 	=> $fb_id,
        );
        $this->db->where('st_id', $st_id);
        $this->db->update('student_tbl', $data);
        return true;
    }

    public function store_fb_id_faculty($fa_id, $fb_id)
    {
        $data = array(
            'fb_id' 	=> $fb_id,
        );
        $this->db->where('fa_id', $fa_id);
        $this->db->update('faculty_tbl', $data);
        return true;
    }
}
