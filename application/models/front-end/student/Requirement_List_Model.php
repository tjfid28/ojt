<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Requirement_List_Model extends CI_Model
{
    public function get_requirements($st_id, $search = "")
    {
        $this->db->select('*');
        $this->db->from('student_requirements_tbl as student_requirement');
        $this->db->join('documents_tbl as document', 'document.d_id = student_requirement.d_id', 'inner');
        $this->db->join('ojt_requirements_tbl as requirement', 'requirement.req_id = student_requirement.req_id', 'inner');
        if (strcmp($search, "")==0) {
            $this->db->where('student_requirement.st_id', $st_id);
        } else {
            $this->db->where('student_requirement.st_id = '.$st_id.' and d_file_name LIKE "%'.$search.'%"');
        }
        return $query = $this->db->get();
    }

    public function upload_requirement($file_content, $file_name, $st_id, $req_id)
    {
        $root	  = getcwd();
        $get_Slug = $this->api->generate_slug($file_name);
        $file_name = $this->api->strip_spaces($file_name);
        if (move_uploaded_file($file_content, $root."/resources/uploaded_documents/".basename($get_Slug))) {
            $date=date("d-m-Y h:i:sa");
            $document = [
                'st_id'	   		 => $st_id,
                'd_file_name'	 => $file_name,
                'd_slug'		 => $get_Slug,
                'd_created_at'   => $date,
            ];
            $this->db->insert('documents_tbl', $document);
            $last_id = $this->db->insert_id();
            $requirement = [
                'st_id'	   		 => $st_id,
                'd_id'	 		 => $last_id,
                'req_id'		 => $req_id,
                'sr_status'		 => 'Pending',
                'sr_date_submitted' => $date
            ];
            $this->db->insert('student_requirements_tbl', $requirement);
            $this->api->system_notification($_SESSION['st_id'], $_SESSION['adviser_id'], "sent a requirement", base_url('faculty/student_info/').base64_encode(base64_encode($_SESSION['st_id']))."/".$_SESSION['st_lname'], 'Faculty');
            echo "Success";
        } else {
            echo "Error has been occured ! Please try again .";
        }
    }

    public function upload_from_documents($d_id, $st_id, $req_id)
    {
        $date=date("d-m-Y h:i:sa");
        $requirement = [
                'st_id'	   		 	=> $st_id,
                'd_id'	 		 	=> $d_id,
                'req_id'		 	=> $req_id,
                'sr_date_submitted' => $date
            ];
        $this->db->insert('student_requirements_tbl', $requirement);

        $this->api->system_notification($_SESSION['st_id'], $_SESSION['adviser_id'], "sent a requirement", base_url('faculty/student_info/').base64_encode(base64_encode($_SESSION['st_id']))."/".$_SESSION['st_lname'], 'Faculty');
    }

    public function remove_requirement($sr_id, $st_id)
    {
        $data = ['sr_id' => $sr_id , 'st_id' => $st_id];
        $this->db->where($data);
        $this->db->delete("student_requirements_tbl");
    }

    public function hasResume()
    {
        $where = ['st_id' => $_SESSION['st_id'], 'req_id' => 0];
        return $this->db->get_where('student_requirements_tbl', $where)->row();
    }
}
