<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
	
	class My_Documents_Model extends CI_Model{

		public function get_documents($st_id,$search = "")
		{
			$this->db->select('*');
	        $this->db->from('documents_tbl');
	        if(strcmp($search,"")==0)
	        {
	        	$this->db->where('st_id',$st_id);
	        }
	        else
	        {
	        	$this->db->where('st_id ='.$st_id.' and d_file_name LIKE "%'.$search.'%"');
	        }
	        $this->db->order_by('d_created_at');
	        return $query = $this->db->get();
		}

		public function upload_document($file_content,$file_name,$st_id)
		{
			$root	  = getcwd();
			$get_Slug = $this->api->generate_slug($file_name);
			$file_name = $this->api->strip_spaces($file_name);
			if(move_uploaded_file($file_content,$root."/resources/uploaded_documents/".basename($get_Slug)))
	        {
	        	
				date_default_timezone_set("Asia/Manila");
				$date=date("d-m-Y h:i:sa");
				$data = array(
					'st_id'	   		 => $st_id,
					'd_file_name'	 => $file_name,
					'd_slug'		 => $get_Slug,
				    'd_created_at'   => $date,
				);
				$this->db->insert('documents_tbl', $data); 
				echo "Success";
			}
			else
			{
				echo "Error has been occured ! Please try again .";
			}
		}

		public function delete_document($d_id,$d_slug)
		{
			$root = getcwd();
			unlink($root."/resources/uploaded_documents/".$d_slug);
			$this->db->where('d_id',$d_id);
			$this->db->delete("documents_tbl");
		}
	}
?>