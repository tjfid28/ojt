<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Class_List_Model extends CI_Model{

		public function get_classes()
		{
			$this->db->select('*,CONCAT(fa_fname," ",fa_lname)as adviser,count(student_list.cla_id)as number_of_students,class.cla_id as class_id');
	        $this->db->from('class_tbl as class');
	        $this->db->join('section_tbl as section', 'section.sec_id = class.sec_id','left');
	        $this->db->join('course_tbl as course', 'course.cou_id = class.cou_id','left');
	        $this->db->join('subject_tbl as subject', 'subject.sub_id = class.sub_id','left');
	        $this->db->join('faculty_tbl as faculty', 'faculty.fa_id = class.fa_id','left');
	        $this->db->join('class_student_list_tbl as student_list', 'student_list.cla_id = class.cla_id','left');
	        $this->db->where('class.fa_id ='.$_SESSION['fa_id']." and class.cla_status = 'Open'");
	        $this->db->group_by('class.cla_id');
	        return $query = $this->db->get();
		}

		public function get_class($cla_id)
		{
			$this->db->select('*,CONCAT(fa_fname," ",fa_lname)as adviser,count(*)as number_of_students');
	        $this->db->from('class_tbl as class');
	        $this->db->join('section_tbl as section', 'section.sec_id = class.sec_id','left');
	        $this->db->join('course_tbl as course', 'course.cou_id = class.cou_id','left');
	        $this->db->join('subject_tbl as subject', 'subject.sub_id = class.sub_id','left');
	        $this->db->join('faculty_tbl as faculty', 'faculty.fa_id = class.fa_id','left');
	        $this->db->join('class_student_list_tbl as student_list', 'student_list.cla_id = class.cla_id','left');
	        $data = array(
	        		'class.cla_id' => $cla_id,
	        		'class.fa_id'  => $_SESSION['fa_id']
	        	);
	        $this->db->where($data);
	        $this->db->group_by('class.cla_id');
	        $query = $this->db->get();
	        return $query->row_array();
		}

		public function upload_cover($file_content,$file_name,$cla_id)
		{
			$root	  = getcwd();
			$get_Slug = $this->api->generate_slug($file_name);
			$file_name = $this->api->strip_spaces($file_name);

			$this->db->select('*');
	        $this->db->from('class_tbl');
	        $this->db->where('cla_id',$cla_id);
	        $get_result = $this->db->get();
	        $class_data	= $get_result->row_array();

	        if($class_data['cla_slug']!=null||$class_data['cla_slug']!="")
	        {
	        	unlink($root."/resources/class_images/".$class_data['cla_slug']);
	        }

			if(move_uploaded_file($file_content,$root."/resources/class_images/".basename($get_Slug)))
	        {
				date_default_timezone_set("Asia/Manila");
				$date=date("d-m-Y h:i:sa");
				$data = array(
					'cla_image_name' => $file_name,
					'cla_slug'		 => $get_Slug,
				);
				$this->db->where("cla_id",$cla_id);
				$this->db->update('class_tbl',$data); 
				echo "Success";
			}
			else
			{
				echo "Error has been occured ! Please try again .";
			}
		}

	}
?>