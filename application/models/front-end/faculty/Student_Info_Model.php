<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Student_Info_Model extends CI_Model{

		public function get_requirements($st_id,$by_type)
		{
			$this->db->select('*');
			$this->db->from('student_requirements_tbl as student_requirement');
			$this->db->join('documents_tbl as document', 'student_requirement.d_id = document.d_id','inner');
			$this->db->join('ojt_requirements_tbl as requirement', 'requirement.req_id = student_requirement.req_id','inner');
			$get_by_type = array(
					'student_requirement.st_id' => $st_id,
					'requirement.req_id' 		=> $by_type
				);
			$get_all 	 = array(
					'student_requirement.st_id' => $st_id
				);
			$this->db->where($by_type == 0 ? $get_all : $get_by_type);
	        return $query = $this->db->get();
		}

		public function get_checklist()
		{
			$this->db->select('*,requirement.req_id req_list_id');
			$this->db->from('ojt_requirements_tbl as requirement');
			$this->db->join('(select * from checklist_tbl where st_id = '.$_SESSION['st_id'].") as checklist", 'checklist.req_id = requirement.req_id','left');
			$this->db->join('(select * from student_tbl where st_id = "'.$_SESSION['st_id'].'") as student','student.st_id = checklist.st_id','left');
			$this->db->order_by('requirement.req_id');
			return $query = $this->db->get();
		}

		public function checking($decision,$sr_id,$st_id,$req_id)
		{
			date_default_timezone_set("Asia/Manila");
			$date=date("d-m-Y h:i:sa");
			$decision = $decision == "Approve" ? "Approved" : "Rejected";
			$data = array(
			    'sr_status'	   	  => $decision,
			    'sr_date_checked' => $date,
			);
			$data_checker = array(
				'sr_id'	=> $sr_id,
				'st_id' => $st_id
			);
			$this->db->where($data_checker);
            $this->db->update('student_requirements_tbl', $data); 
            $this->api->system_notification($st_id,$_SESSION['fa_id'],$decision." your requirement",base_url('student/requirements_list'),'Student');
            return "Requirement has been ".$decision;
		}

		public function checklist($check,$st_id)
		{
			date_default_timezone_set("Asia/Manila");
			$date=date("d-m-Y h:i:sa");
			$data = array(
					'st_id' => $st_id,
					'req_id'=> $check,
				);
			$this->db->select('*');
			$this->db->from('checklist_tbl');
			$this->db->where($data);
			$query = $this->db->get();
			$result= $query->num_rows();
			if($result==0)
			{
				$array_date = array('cl_date_checked' => $date);
				$result = array_merge($data,$array_date);
				$this->db->insert('checklist_tbl', $result); 
				$this->api->system_notification($st_id,$_SESSION['fa_id'],"Checked a requirement from your checklist",base_url('student/requirements_list'),'Student');
			}
			else
			{
				$this->db->where($data);
            	$this->db->delete('checklist_tbl', $data);
            	$this->api->system_notification($st_id,$_SESSION['fa_id'],"Unchecked a requirement from your checklist",base_url('student/requirements_list'),'Student'); 
			}
		}
	}
?>