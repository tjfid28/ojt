<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Student_List_Model extends CI_Model{

		public function get_students($search = "")
		{
			$this->db->select('*');
	        $this->db->from('student_tbl as student');
	        $this->db->join('course_tbl as course', 'course.cou_id = student.cou_id','inner');
	        $this->db->join('class_student_list_tbl as class_student_list', 'class_student_list.st_id = student.st_id','inner');
	        if(strcmp($search,"")==0)
	        {
	        	$this->db->where('class_student_list.cla_id',$_SESSION['cla_id']);
	        }
	        else
	        {
	        	$this->db->where('class_student_list.cla_id = '.$_SESSION['cla_id'].' and (concat(st_fname," ",st_lname) LIKE "%'.$search.'%" or st_number LIKE "%'.$search.'%" or concat(st_lname," ",st_fname) LIKE "%'.$search.'%" )');
	        }
	        
	        $this->db->order_by('st_lname');
	        return $query = $this->db->get();
		}

		public function get_student_info()
		{
			$this->db->select('*');
	        $this->db->from('student_tbl as student');
	        $this->db->join('course_tbl as course', 'course.cou_id = student.cou_id','inner');
	        $this->db->join('class_student_list_tbl as class_student_list', 'class_student_list.st_id = student.st_id','inner');
	        $this->db->join('class_tbl as class' , 'class.cla_id = class_student_list.cla_id','inner');
	        $data = array(
	        		'student.st_id' => $_SESSION['st_id'],
	        	);
	        $this->db->where($data);
	        $this->db->order_by('st_lname');
	        $query = $this->db->get();
	        return $query->row_array() ;
		}

	}
?>