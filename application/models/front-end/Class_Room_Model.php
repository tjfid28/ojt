<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Class_Room_Model extends CI_Model
{
    public function get_class_announcements($cla_id)
    {
        $this->db->select('*,count(comment.cla_ann_id) as number_of_comments,announcement.cla_ann_id as class_announcement_id');
        $this->db->from('class_announcement_tbl as announcement');
        $this->db->join('faculty_tbl as faculty', 'faculty.fa_id = announcement.fa_id', 'inner');
        $this->db->join('announcement_comment_tbl as comment', 'comment.cla_ann_id = announcement.cla_ann_id', 'left');
        $this->db->where('cla_id', $cla_id);
        $this->db->group_by('announcement.cla_ann_id');
        $this->db->order_by('announcement.cla_ann_id', 'desc');
        // dd($this->db->get()->result());
        return $this->db->get();
    }

    public function get_comments($cla_ann_id)
    {
        $this->db->select('*,comment.st_id as student_id');
        $this->db->from('announcement_comment_tbl as comment');
        $this->db->join('faculty_tbl as faculty', 'comment.fa_id = faculty.fa_id', 'left');
        $this->db->join('student_tbl as student', 'student.st_id = comment.st_id', 'left');
        $this->db->where('cla_ann_id', $cla_ann_id);
        $this->db->order_by('ac_id', 'desc');
        return $this->db->get();
    }

    public function get_announcement($cla_ann_id)
    {
        $this->db->select('*');
        $this->db->from('class_announcement_tbl as announcement');
        $this->db->join('faculty_tbl as faculty', 'faculty.fa_id = announcement.fa_id', 'inner');
        $this->db->where('cla_ann_id', $cla_ann_id);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function post_announcement($data)
    {
        $this->db->insert('class_announcement_tbl', $data);

        $this->db->select('student.st_id,fb_id,class_student_list.cla_id,st_email,st_mobile_number');
        $this->db->from('student_tbl as student');
        $this->db->join('class_student_list_tbl as class_student_list', 'class_student_list.st_id = student.st_id', 'inner');
        $this->db->where('class_student_list.cla_id', $data['cla_id']);
        // $this->db->where('class_student_list.cla_id='.$cla_id.' and fb_id is not null and fb_id!=""');
        $this->db->group_by('student.st_id');

        $students = $this->db->get();
        $fbStudents = array_column($students->result_array(), 'fb_id');

        if (!empty($fbStudents)) {
            $this->api->batch_notification_to_fb(json_encode($fbStudents));
        }

        $this->load->library('sms');
        $attachment = $data['ann_attachment'] ?? null;

        foreach ($students->result() as $student) {
            $this->api->system_notification($student->st_id, $data['fa_id'], "Posted an announcement", base_url('student/class_room'), 'Student');
            $this->sendMail($student->st_email, $data['ann_post'], $attachment);
            $this->sms->send($data['ann_post'], $student->st_mobile_number);
        }

        return "Announcent has been posted";
    }

    public function delete_announcement($cla_ann_id, $fa_id, $attachment)
    {
        if ($attachment) {
            unlink(getcwd() . "/resources/announcement_files/{$attachment}");
        }

        $data = [
            'cla_ann_id' => $cla_ann_id,
            'fa_id' => $fa_id
        ];
        $this->db->delete("class_announcement_tbl", $data);
    }

    public function delete_comment($ac_id, $st_id)
    {
        $data = [
                'ac_id' => $ac_id,
                'st_id' => $st_id
            ];
        $this->db->where($data);
        $this->db->delete("announcement_comment_tbl");
    }

    public function delete_comment_faculty($ac_id, $fa_id)
    {
        $data = [
                'ac_id' => $ac_id,
                'fa_id' => $fa_id
            ];
        $this->db->where($data);
        $this->db->delete("announcement_comment_tbl");
    }

    public function post_comment_student($cla_ann_id, $comment_content, $st_id, $cla_id)
    {
        $data = [
            'st_id'             => $st_id,
            'ac_content'        => $comment_content,
            'cla_ann_id'        => $cla_ann_id,
            'ac_date_posted'    => date("Y-m-d H:i:s")
        ];
        $this->db->insert('announcement_comment_tbl', $data);
        $this->api->system_notification($_SESSION['st_id'], $_SESSION['note_fa_id'], "Commented on your post", base_url('faculty/').base64_encode(base64_encode($cla_ann_id))."/comments", 'Faculty');

        return "Comment has been saved";
    }

    public function post_comment_faculty($cla_ann_id, $comment_content, $fa_id, $cla_id)
    {
        $data = [
            'fa_id'             => $fa_id,
            'ac_content'        => $comment_content,
            'cla_ann_id'        => $cla_ann_id,
            'ac_date_posted'    => date("Y-m-d H:i:s")
        ];
        $this->db->insert('announcement_comment_tbl', $data);

        $this->db->select('student.st_id,fb_id,class_student_list.cla_id');
        $this->db->from('student_tbl as student');
        $this->db->join('class_student_list_tbl as class_student_list', 'class_student_list.st_id = student.st_id', 'inner');
        $this->db->where('class_student_list.cla_id='.$cla_id);
        $this->db->group_by('student.st_id');

        $students = $this->db->get();

        foreach ($students->result() as $student) {
            $this->api->system_notification($student->st_id, $fa_id, "Commented on a post", base_url('student/').base64_encode(base64_encode($cla_ann_id))."/comments", 'Student');
        }

        return "Comment has been saved";
    }

    protected function sendMail($to, $message, $attachment)
    {
        $this->load->library('email');
        $this->email->from('ojt.sti.novaliches@gmail.com', 'no-reply@ojt.novaliches.sti.edu');
        $this->email->to($to);

        $this->email->subject('Announcement');
        $this->email->message($message . "<p>Click <a href='". base_url('student/class_room') ."'>here</a></p>");

        if ($attachment) {
            $this->email->attach(getcwd() . "/resources/announcement_files/{$attachment}");
        }

        try {
            $this->email->send();
        } catch (Exception $e) {
            show_error($this->email->print_debugger());
        }
    }
}
