<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Notification_Model extends CI_Model{

		public function get_notifications()
		{
			$n_show_to = "";
			$condition = "";
			if($_SESSION['account_type']==2)
			{
				$this->db->select("notification.* ,faculty.fa_id,student.st_id,concat(student.st_fname,' ',student.st_lname) as name,concat('resources/student_images/',st_image_name) as profileimage");
				$n_show_to = "Faculty";
				$condition = array(
					'seen' 			=> 0,
					'n_show_to' 	=> $n_show_to,
					'faculty.fa_id'	=> $_SESSION['fa_id']
				);
			}
			if($_SESSION['account_type']==1)
			{
				$this->db->select("notification.* ,faculty.fa_id,student.st_id,concat(faculty.fa_fname,' ',faculty.fa_lname) as name,concat('resources/faculty_images/',fa_image_name) as profileimage ");
				$n_show_to = "Student";
				$condition = array(
					'seen' 			=> 0,
					'n_show_to' 	=> $n_show_to,
					'student.st_id'	=> $_SESSION['st_id']
				);
			}
			$this->db->from("notification_tbl as notification");
			$this->db->join("faculty_tbl as faculty","faculty.fa_id = notification.fa_id" , "left");
			$this->db->join("student_tbl as student","student.st_id = notification.st_id", "left");
			
			$this->db->where($condition);
			$this->db->order_by('n_id','desc');
			$query = $this->db->get();
			return $query;
		}

		public function get_notification_history()
		{
			
			$n_show_to = "";
			$condition = "";
			if($_SESSION['account_type']==2)
			{
				$this->db->select("notification.* ,faculty.fa_id,student.st_id,concat(student.st_fname,' ',student.st_lname) as name,concat('resources/student_images/',st_image_name) as profileimage");
				$n_show_to = "Faculty";
				$condition = array(
					'seen' 			=> 1,
					'n_show_to' 	=> $n_show_to,
					'faculty.fa_id'	=> $_SESSION['fa_id']
				);
			}
			if($_SESSION['account_type']==1)
			{
				$this->db->select("notification.* ,faculty.fa_id,student.st_id,concat(faculty.fa_fname,' ',faculty.fa_lname) as name,concat('resources/faculty_images/',fa_image_name) as profileimage ");
				$n_show_to = "Student";
				$condition = array(
					'seen' 			=> 1,
					'n_show_to' 	=> $n_show_to,
					'student.st_id'	=> $_SESSION['st_id']
				);
			}
			$this->db->from("notification_tbl as notification");
			$this->db->join("faculty_tbl as faculty","faculty.fa_id = notification.fa_id" , "left");
			$this->db->join("student_tbl as student","student.st_id = notification.st_id", "left");

			$this->db->where($condition);
			$this->db->order_by('n_id','desc');
			$query = $this->db->get();
			return $query;
		}

	}
?>