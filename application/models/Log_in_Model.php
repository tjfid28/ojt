<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

    class Log_in_Model extends CI_Model
    {
        public function getLoginData($uname, $pword)
        {
            $account_type = 1;
            $this->db->select('*');
            $log_in_credentials = null;
            $this->db->from("student_tbl as student");
            $this->db->join('course_tbl as course', 'student.cou_id = course.cou_id', 'left');
            $this->db->join('class_student_list_tbl as student_list', 'student.st_id = student_list.st_id', 'left');
            $this->db->join('class_tbl as class', 'class.cla_id = student_list.cla_id', 'left');
            $this->db->join('section_tbl as section', 'class.sec_id = section.sec_id', 'left');
            $log_in_credentials = [
                    'st_uname'	   => $uname,
                    'st_pword'	   => $pword,
                ];
            $this->db->where($log_in_credentials);
            $query = $this->db->get();
            $user_data=$query->row_array();

            if ($query->num_rows()==0) {
                $this->db->select('*');
                $this->db->from("faculty_tbl");
                $log_in_credentials = [
                        'fa_uname'	   => $uname,
                        'fa_pword'	   => $pword,
                    ];
                $this->db->where($log_in_credentials);
                $query = $this->db->get();
                $user_data=$query->row_array();
                $account_type = 2;
            }

            $this->load->library('session');

            if ($query->num_rows()==1) {
                if ($account_type==2) {
                    $Log_data = [
                        'fa_id'  		  => $user_data['fa_id'],
                        'fa_number'  	  => $user_data['fa_number'],
                        'fb_id'           => $user_data['fb_id'],
                        'fa_uname'        => $user_data['fa_uname'],
                        'fa_pword'        => $user_data['fa_pword'],
                        'fa_fname'        => $user_data['fa_fname'],
                        'fa_mname'        => $user_data['fa_mname'],
                        'fa_lname'  	  => $user_data['fa_lname'],
                        'fa_image_name'   => $user_data['fa_image_name'],
                        'account_type' 	  => 2,
                        'logged_in'       => true,
                    ];
                } else {
                    $Log_data = [
                        'st_id'  		  => $user_data['st_id'],
                        'st_number'  	  => $user_data['st_number'],
                        'st_email'       => $user_data['st_email'],
                        'fb_id'           => $user_data['fb_id'],
                        'st_uname'        => $user_data['st_uname'],
                        'st_pword'        => $user_data['st_pword'],
                        'st_fname'        => $user_data['st_fname'],
                        'st_mname'        => $user_data['st_mname'],
                        'st_lname'  	  => $user_data['st_lname'],
                        'st_image_name'   => $user_data['st_image_name']!=null&&$user_data['st_image_name']!="" ? $user_data['st_image_name'] : 'no-image.png',
                        'sec_name'	      => $user_data['sec_name'],
                        'cla_id'  	  	  => $user_data['cla_id'],
                        'adviser_id'  	  => $user_data['fa_id'],
                        'cla_slug'  	  => $user_data['cla_slug'],
                        'cou_title'   	  => $user_data['cou_title'],
                        'account_type' 	  => 1,
                        'logged_in'       => true,
                    ];
                    if ($user_data['cla_id']==null) {
                        return "No Class";
                    }
                }
                $this->session->set_userdata($Log_data);

                return "true";
            } else {
                return "false";
            }
        }

        public function getLoginSecretData($uname, $pword)
        {
            $account_type = 3;
            $this->db->select('*');
            $log_in_credentials = null;
            $this->db->from("admin_tbl");
            $log_in_credentials = [
                    'ad_uname'	   => $uname,
                    'ad_pword'	   => $pword,
                ];
            $this->db->where($log_in_credentials);
            $query = $this->db->get();
            $user_data=$query->row_array();

            if ($query->num_rows()==0) {
                $this->db->from("sub_admin_tbl");
                $log_in_credentials = [
                        's_ad_uname'	   => $uname,
                        's_ad_pword'	   => $pword,
                    ];
                $this->db->where($log_in_credentials);
                $query = $this->db->get();
                $user_data=$query->row_array();
                $account_type = 4;
            }

            $this->load->library('session');


            if ($query->num_rows()==1) {
                if ($account_type==3) {
                    $Log_data = [
                        'ad_id'  		  => $user_data['ad_id'],
                        'ad_uname'        => $user_data['ad_uname'],
                        'ad_pword'        => $user_data['ad_pword'],
                        'ad_fname'        => $user_data['ad_fname'],
                        'ad_mname'        => $user_data['ad_mname'],
                        'ad_lname'  	  => $user_data['ad_lname'],
                        'account_type' 	  => 3,
                        'logged_in'       => true,
                    ];
                } else {
                    $Log_data = [
                        's_ad_id'  		  => $user_data['s_ad_id'],
                        's_ad_uname'      => $user_data['s_ad_uname'],
                        's_ad_pword'      => $user_data['s_ad_pword'],
                        's_ad_fname'      => $user_data['s_ad_fname'],
                        's_ad_mname'      => $user_data['s_ad_mname'],
                        's_ad_lname'  	  => $user_data['s_ad_lname'],
                        's_class_list'	  => $user_data['s_class_list'],
                        's_news'	  	  => $user_data['s_news'],
                        's_sections'	  => $user_data['s_sections'],
                        's_class_list'	  => $user_data['s_class_list'],
                        's_courses'	  	  => $user_data['s_courses'],
                        's_departments'	  => $user_data['s_departments'],
                        's_programs'	  => $user_data['s_programs'],
                        's_faculty'	  	  => $user_data['s_faculty'],
                        's_student'	  	  => $user_data['s_student'],
                        's_uploaded_documents'	  => $user_data['s_uploaded_documents'],
                        's_ojt_requirement'	  	  => $user_data['s_ojt_requirement'],
                        's_downloadable_forms'	  => $user_data['s_downloadable_forms'],
                        's_ojt_policy'	  => $user_data['s_ojt_policy'],
                        's_company'	  	  => $user_data['s_company'],
                        's_slides'	  	  => $user_data['s_slides'],
                        'account_type'    => 4,
                        'logged_in'       => true,
                    ];
                }

                $this->session->set_userdata($Log_data);

                return "true";
            } else {
                return "false";
            }
        }
    }
