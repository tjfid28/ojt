<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_Controller extends CI_Controller
{
		public function __construct() 
	    { 
	            parent::__construct();
	            $this->load->helper('url'); 
	            $this->load->library('session');
	            $this->load->library('api');
	            $this->load->database();
	            
	    }
		public function modify_student_access()
		{
			$st_id	   = $_SESSION['st_id'];
			$uname 	   = $this->input->post('uname');
			$old_pword = $this->input->post('old_pword');
			$new_pword = $this->input->post('new_pword');

			if(strcmp($_SESSION['st_pword'],$old_pword)==0)
			{
				$this->load->model("front-end/Profile_Model");			
				$this->Profile_Model->modify_student_access($st_id,$uname,$new_pword);
			}
			else
			{
				echo "Old password does'nt match";
			}
		}
		public function modify_faculty_access()
		{
			$fa_id	   = $_SESSION['fa_id'];
			$uname 	   = $this->input->post('uname');
			$old_pword = $this->input->post('old_pword');
			$new_pword = $this->input->post('new_pword');

			if(strcmp($_SESSION['fa_pword'],$old_pword)==0)
			{
				$this->load->model("front-end/Profile_Model");			
				$this->Profile_Model->modify_faculty_access($fa_id,$uname,$new_pword);
			}
			else
			{
				echo "Old password does'nt match";
			}
		}

}
?>