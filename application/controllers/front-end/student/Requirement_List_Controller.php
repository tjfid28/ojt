<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Requirement_List_Controller extends CI_Controller
{
		public function __construct() 
	    { 
	            parent::__construct();
	            $this->load->helper('url'); 
	            $this->load->library('session');
	            $this->load->library('api');
	            $this->load->database();
	            
	    }
		public function get_requirements()
		{
			$search = $this->input->post('search');
			$this->load->model("front-end/student/Requirement_List_Model");			
			$result = $this->Requirement_List_Model->get_requirements($_SESSION['st_id'],$search);

			echo json_encode($result->result());
		}
		public function upload_requirement()
		{
			$file_content = $_FILES['requirement']['tmp_name'];
			$file_name	  = $_FILES['requirement']['name'];
			$req_id	      = $this->input->post('requirement_req_id');
			$st_id        = $_SESSION['st_id'];
			$filesize	  = filesize($_FILES['document']['tmp_name']);
			if($filesize<=20971520)
			{
				$this->load->model("front-end/student/Requirement_List_Model");			
				echo $result = $this->Requirement_List_Model->upload_requirement($file_content,$file_name,$st_id,$req_id);
			}
			else
			{
				echo false;
			}
		}

		public function upload_from_documents()
		{
			$d_id	      = $this->input->post('d_id');
			$req_id	      = $this->input->post('req_id');
			$st_id        = $_SESSION['st_id'];

			$this->load->model("front-end/student/Requirement_List_Model");			
			echo $result = $this->Requirement_List_Model->upload_from_documents($d_id,$st_id,$req_id);
		}

		public function remove_requirement()
		{
			$sr_id  = $this->input->post('sr_id');
			$st_id	= $_SESSION['st_id'];
			$this->load->model("front-end/student/Requirement_List_Model");				
			$this->Requirement_List_Model->remove_requirement($sr_id,$st_id);
		}

		public function download_document($slug_name,$file_name)
		{
		    $DLdata['slug_name'] = $slug_name;
		    $DLdata['file_name'] = $file_name;
		    $this->load->view("download_document_view",$DLdata);
		}


}
?>