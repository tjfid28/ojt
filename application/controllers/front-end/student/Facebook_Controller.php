<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook_Controller extends CI_Controller
{
		public function __construct() 
	    { 
	            parent::__construct();
	            $this->load->helper('url'); 
	            $this->load->library('session');
	            $this->load->library('api');
	            $this->load->database();
	            
	    }
		public function store_fb_id()
		{
			$fb_id = $this->input->post('fb_id');
			$st_id = $_SESSION['st_id'];
			$_SESSION['fb_id'] = $fb_id;
			$this->load->model("front-end/Facebook_Model");			
			$this->Facebook_Model->store_fb_id($st_id,$fb_id);
		}

		public function store_fb_id_faculty()
		{
			$fb_id = $this->input->post('fb_id');
			$fa_id = $_SESSION['fa_id'];
			$_SESSION['fb_id'] = $fb_id;
			$this->load->model("front-end/Facebook_Model");			
			$this->Facebook_Model->store_fb_id_faculty($fa_id,$fb_id);
		}
}
?>