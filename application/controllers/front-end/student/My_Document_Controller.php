<?php defined('BASEPATH') OR exit('No direct script access allowed');

class My_Document_Controller extends CI_Controller
{
		public function __construct() 
	    { 
	            parent::__construct();
	            $this->load->helper('url'); 
	            $this->load->library('session');
	            $this->load->library('api');
	            $this->load->database();
	            
	    }
		public function get_documents()
		{
			$search = $this->input->post('search');
			$this->load->model("front-end/student/My_Documents_Model");		
			$result = $this->My_Documents_Model->get_documents($_SESSION['st_id'],$search);

			echo json_encode($result->result());
		}
		public function upload_document()
		{
			$file_content = $_FILES['document']['tmp_name'];
			$file_name	  = $_FILES['document']['name'];
			$st_id        = $_SESSION['st_id'];
			$filesize	  = filesize($_FILES['document']['tmp_name']);
			if($filesize<=3145728)
			{
				$this->load->model("front-end/student/My_Documents_Model");			
				echo $result = $this->My_Documents_Model->upload_document($file_content,$file_name,$st_id);
			}
			else
			{
				echo false;
			}
		}

		public function delete_document()
		{
			$d_id   = $this->input->post('d_id');
			$d_slug = $this->input->post('slug_name');
			$this->load->model("front-end/student/My_Documents_Model");				
			$this->My_Documents_Model->delete_document($d_id,$d_slug);
		}

		public function download_document($slug_name,$file_name)
		{
		    $DLdata['slug_name'] = $slug_name;
		    $DLdata['file_name'] = $file_name;
		    $this->load->view("download_document_view",$DLdata);
		}


}
?>