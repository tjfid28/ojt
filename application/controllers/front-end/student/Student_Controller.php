<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Student_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function class_room()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==1) {
            $result['result']  = ['cla_id' => $_SESSION['cla_id'] , 'sec_name' => $_SESSION['sec_name'] , 'cla_slug' => $_SESSION['cla_slug']];
            $data['content'] = $this->load->view('front-end/class_room_view', $result, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function comment_room($cla_ann_id)
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==1) {
            $cla_ann_id = base64_decode(base64_decode($cla_ann_id));
            $this->load->model("front-end/Class_Room_Model");
            $_SESSION['cla_ann_id'] = $cla_ann_id;
            $result['cla_ann_id'] = $cla_ann_id;
            $result['class_announcement'] = $this->Class_Room_Model->get_announcement($cla_ann_id);
            $_SESSION['note_fa_id'] = $result['class_announcement']['fa_id'];
            if ($_SESSION['cla_id']==$result['class_announcement']['cla_id']) {
                $data['content'] = $this->load->view('front-end/comment_room_view', $result, true);
                $this->load->view('layout/master_view', $data);
            } else {
                header("location:/ojt");
            }
        } else {
            header("location:/ojt");
        }
    }

    public function notifications()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==1) {
            $data['content'] = $this->load->view('front-end/notification_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function profile()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==1) {
            $data['content'] = $this->load->view('front-end/student/profile_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function news()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==1) {
            $data['content'] = $this->load->view('front-end/news_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function my_documents()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==1) {
            $data['content'] = $this->load->view('front-end/student/my_documents_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function requirements_list()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==1) {
            $this->load->model("back-end/ojt_management/Ojt_Requirement_Model");
            $result['requirements'] = $this->Ojt_Requirement_Model->get_requirements();
            $this->load->model("front-end/student/My_Documents_Model");
            $result['documents'] = $this->My_Documents_Model->get_documents($_SESSION['st_id']);
            $this->load->model("front-end/faculty/Student_Info_Model");
            $result['checklists'] = $this->Student_Info_Model->get_checklist();
            $data['content'] = $this->load->view('front-end/student/requirements_list_view', $result, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function downloadable_forms()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==1) {
            $data['content'] = $this->load->view('front-end/student/downloadable_forms_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function companies()
    {
        if (isset($_SESSION['account_type']) && $_SESSION['account_type'] == 1) {
            $this->load->model("back-end/Partner_Company_Model");
            $data['companies'] = $this->Partner_Company_Model->get_companies()->result();
            $data['content'] = $this->load->view('front-end/student/companies_view', $data, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function company($slug)
    {
        if (isset($_SESSION['account_type']) && $_SESSION['account_type'] == 1) {
            $this->load->model("back-end/Partner_Company_Model");
            $this->load->model("front-end/student/Requirement_List_Model");
            $data['company'] = $this->Partner_Company_Model->get_company($slug)->row();
            $data['hasResume'] = $this->Requirement_List_Model->hasResume();
            $data['content'] = $this->load->view('front-end/student/single_company_view', $data, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function sendResume()
    {
        if (isset($_SESSION['account_type']) && $_SESSION['account_type'] == 1) {
            $this->load->library('email');
            $this->load->model("back-end/ojt_management/Uploaded_Documents_Model");
            $to = $this->input->post('co_email');
            $d_id = $this->input->post('d_id');
            $resume = $this->Uploaded_Documents_Model->getResume($d_id);
            $message = "Good Day, <p></p>Applying for intern. <p></p>Regards, <br>{$_SESSION['st_fname']} {$_SESSION['st_lname']}";

            $this->email->from($_SESSION['st_email'], $_SESSION['st_fname'] . " " . $_SESSION['st_lname']);
            $this->email->to($to);

            $this->email->subject('OJT Application');
            $this->email->message($message);
            $this->email->attach($resume, 'attachment', 'Resume');

            try {
                $this->email->send();
            } catch (Exception $e) {
                show_error($this->email->print_debugger());
            }
        } else {
            header("location:/ojt");
        }
    }
}
