<?php defined('BASEPATH') or exit('No direct script access allowed');

class Class_Room_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('api');
        $this->load->database();
    }

    public function get_class_announcements()
    {
        $this->load->model("front-end/Class_Room_Model");
        $result = $this->Class_Room_Model->get_class_announcements($_SESSION['cla_id']);

        echo json_encode($result->result());
    }

    public function get_comments()
    {
        $data_identifier = $this->input->post('data_identifier');
        $this->load->model("front-end/Class_Room_Model");
        $result = $this->Class_Room_Model->get_comments($data_identifier);

        echo json_encode($result->result());
    }

    public function post_announcement()
    {
        $data = [
            'fa_id' => $_SESSION['fa_id'],
            'ann_post' => htmlentities($this->input->post('announcement_field')),
            'cla_id' => $_SESSION['cla_id'],
            'ann_date_posted' => date("Y-m-d H:i:s")
        ];

        if ($_FILES['ann_attachment']['name'][0]) {
            $upload_path = 'resources/announcement_files';
            $filename = date('Y-m-d_H-i-s') . '.zip';
            $data['ann_attachment'] = $filename;
            $attachment = [];

            foreach ($_FILES['ann_attachment']['name'] as $key => $value) {
                $attachment["{$value}"] = file_get_contents($_FILES['ann_attachment']['tmp_name'][$key]);
            }

            $this->load->library('zip');
            $this->zip->add_data($attachment);
            $this->zip->archive("{$upload_path}/{$filename}");
        }

        $this->load->model("front-end/Class_Room_Model");
        echo $result = $this->Class_Room_Model->post_announcement($data);
    }

    public function delete_announcement()
    {
        $cla_ann_id = $this->input->post('cla_ann_id');
        $attachment = $this->input->post('ann_attachment');
        $fa_id = $_SESSION['fa_id'];
        $this->load->model("front-end/Class_Room_Model");
        echo $this->Class_Room_Model->delete_announcement($cla_ann_id, $fa_id, $attachment);
    }

    public function delete_comment()
    {
        $ac_id = $this->input->post('ac_id');
        if ($_SESSION['account_type']==1) {
            $st_id      = $_SESSION['st_id'];
            $this->load->model("front-end/Class_Room_Model");
            echo $this->Class_Room_Model->delete_comment($ac_id, $st_id);
        }
        if ($_SESSION['account_type']==2) {
            $fa_id      = $_SESSION['fa_id'];
            $this->load->model("front-end/Class_Room_Model");
            echo $this->Class_Room_Model->delete_comment_faculty($ac_id, $fa_id);
        }
    }

    public function post_comment($cla_ann_id)
    {
        $comment_content = htmlentities($this->input->post('comment_field'));
        $this->load->model("front-end/Class_Room_Model");
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==1) {
            $st_id = $_SESSION['st_id'];
            $cla_id = $_SESSION['cla_id'];
            echo $this->Class_Room_Model->post_comment_student($cla_ann_id, $comment_content, $st_id, $cla_id);
        }
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
            $this->db->select("cla_id,cla_ann_id");
            $this->db->from("class_announcement_tbl");
            $this->db->where('cla_ann_id', $cla_ann_id);
            $get_result = $this->db->get();
            $get_id = $get_result->row_array();
            $cla_id = $get_id['cla_id'];
            $fa_id  = $_SESSION['fa_id'];

            echo $this->Class_Room_Model->post_comment_faculty($cla_ann_id, $comment_content, $fa_id, $cla_id);
        }
    }
}
