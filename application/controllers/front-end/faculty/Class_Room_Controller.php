<?php defined('BASEPATH') or exit('No direct script access allowed');

class Class_Room_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('api');
        $this->load->database();
    }
    public function get_classes()
    {
        $this->load->model("front-end/faculty/Class_List_Model");
        $result = $this->Class_List_Model->get_classes();

        echo json_encode($result->result());
    }
}
