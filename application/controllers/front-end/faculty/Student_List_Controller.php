<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Student_List_Controller extends CI_Controller
{
		public function __construct() 
	    { 
	            parent::__construct();
	            $this->load->helper('url'); 
	            $this->load->library('session');
	            $this->load->library('api');
	            $this->load->database();
	            
	    }
	    public function get_students()
		{
			$search = $this->input->post('search');
			$this->load->model("front-end/faculty/Student_List_Model");			
			$result = $this->Student_List_Model->get_students($search);

			echo json_encode($result->result());
		}
		
}
?>