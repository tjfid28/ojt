<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Student_Info_Controller extends CI_Controller
{
		public function __construct() 
	    { 
	            parent::__construct();
	            $this->load->helper('url'); 
	            $this->load->library('session');
	            $this->load->library('api');
	            $this->load->database();
	            
	    }
	    public function get_requirements()
		{
			$this->load->model("front-end/faculty/Student_Info_Model");
			$st_id 	 = $_SESSION['st_id'];
			$by_type = $this->input->post('by_type');
			$result  = $this->Student_Info_Model->get_requirements($st_id,$by_type);

			echo json_encode($result->result());
		}
		
		public function checking()
		{
			$decision = $this->input->post('decision');
			$sr_id	  = $this->input->post('sr_id');
			$req_id	  = $this->input->post('req_id');
			$st_id 	  = $_SESSION['st_id'];
			$this->load->model("front-end/faculty/Student_Info_Model");
			echo $this->Student_Info_Model->checking($decision,$sr_id,$st_id,$req_id);
		}

		public function checklist()
		{
			$check = $this->input->post('check');
			$st_id 	 = $_SESSION['st_id'];
			$this->load->model("front-end/faculty/Student_Info_Model");
			echo $this->Student_Info_Model->checklist($check,$st_id);
		}
}
?>