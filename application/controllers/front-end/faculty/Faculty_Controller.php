<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Faculty_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('api');
        $this->load->database();
    }

    public function dashboard()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
            $data['content'] = $this->load->view('front-end/faculty/dashboard_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function profile()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
            $data['content'] = $this->load->view('front-end/faculty/profile_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function news()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
            $data['content'] = $this->load->view('front-end/news_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function class_list()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
            $data['content'] = $this->load->view('front-end/faculty/class_list_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function notifications()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
            $data['content'] = $this->load->view('front-end/notification_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }

    public function comment_room($cla_ann_id)
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
            $cla_ann_id = base64_decode(base64_decode($cla_ann_id));
            $this->load->model("front-end/Class_Room_Model");
            $result['cla_ann_id'] = $cla_ann_id;
            $_SESSION['cla_ann_id'] = $cla_ann_id;
            $result['class_announcement'] = $this->Class_Room_Model->get_announcement($cla_ann_id);

            if ($_SESSION['fa_id']==$result['class_announcement']['fa_id']) {
                $data['content'] = $this->load->view('front-end/comment_room_view', $result, true);
                $this->load->view('layout/master_view', $data);
            } else {
                header("location:/ojt");
            }
        } else {
            header("location:/ojt");
        }
    }

    public function class_room($cla_id, $section)
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
            $cla_id = base64_decode(base64_decode($cla_id));
            $this->load->model("front-end/faculty/Class_List_Model");
            $result['result']  = $this->Class_List_Model->get_class($cla_id);
            $_SESSION['cla_id']= $cla_id;

            if ($result['result']['fa_id']==$_SESSION['fa_id']) {
                $data['content']   = $this->load->view('front-end/class_room_view', $result, true);
                $this->load->view('layout/master_view', $data);
            } else {
                header("location:/ojt");
            }
        } else {
            header("location:/ojt");
        }
    }

    public function student_list($cla_id, $section)
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
            $cla_id = base64_decode(base64_decode($cla_id));
            $this->load->model("front-end/faculty/Class_List_Model");
            $_SESSION['cla_id']= $cla_id;
            $result['result']  = $this->Class_List_Model->get_class($cla_id);
            if ($result['result']['fa_id']==$_SESSION['fa_id']) {
                $data['content']   = $this->load->view('front-end/faculty/student_list_view', $result, true);
                $this->load->view('layout/master_view', $data);
            } else {
                header("location:/ojt");
            }
        } else {
            header("location:/ojt");
        }
    }

    public function student_info($st_id, $lname)
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
            $st_id = base64_decode(base64_decode($st_id));
            $this->load->model("back-end/ojt_management/Ojt_Requirement_Model");
            $result['requirements'] = $this->Ojt_Requirement_Model->get_requirements();
            $this->load->model("front-end/faculty/Student_List_Model");
            $_SESSION['st_id'] = $st_id;
            $result['result']  = $this->Student_List_Model->get_student_info();
            $this->load->model("front-end/faculty/Student_Info_Model");
            if ($result['result']['fa_id']==$_SESSION['fa_id']) {
                $result['checklists'] = $this->Student_Info_Model->get_checklist();
                $data['content']   = $this->load->view('front-end/faculty/student_info_view', $result, true);
                $this->load->view('layout/master_view', $data);
            } else {
                header("location:/ojt");
            }
        } else {
            header("location:/ojt");
        }
    }

    public function downloadable_forms()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==2) {
            $this->load->model("back-end/ojt_management/Ojt_Requirement_Model");
            $data['ojt_requirements'] = $this->Ojt_Requirement_Model->get_requirements();
            $data['content'] = $this->load->view('front-end/faculty/downloadable_forms_view', $data, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:/ojt");
        }
    }
}
