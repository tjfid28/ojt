<?php defined('BASEPATH') or exit('No direct script access allowed');

class Class_List_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('api');
        $this->load->library('excel');
        $this->load->database();
    }

    public function get_classes()
    {
        $this->load->model("front-end/faculty/Class_List_Model");
        $result = $this->Class_List_Model->get_classes();

        echo json_encode($result->result());
    }

    public function upload_cover()
    {
        $file_content = $_FILES['cover']['tmp_name'];
        $file_name    = $_FILES['cover']['name'];
        $cla_id       = $this->input->post('cla_id');

        $this->load->model("front-end/faculty/Class_List_Model");
        echo $result = $this->Class_List_Model->upload_cover($file_content, $file_name, $cla_id);
    }

    public function preview_report()
    {
        $data['cla_id'] = $this->input->post('cla_id');
        $data['type'] = $this->input->post('type');
        $this->load->view('download_reports_view', $data);
    }

    public function download_reports($cla_id, $type)
    {
        $objPHPExcel = new PHPExcel();

        // $objPHPExcel->getProperties()->setCreator();
        $objPHPExcel->setActiveSheetIndex(0);

        $this->db->select('concat(st_lname," ",st_fname,",",st_mname) as name,st_lname,class_student_list.st_id,student.st_id,class_student_list.cla_id,sec_name,section.sec_name,class.cla_term,class.cla_year');
        $this->db->from('student_tbl as student');
        $this->db->join('class_student_list_tbl as class_student_list', 'class_student_list.st_id = student.st_id', 'inner');
        $this->db->join('class_tbl as class', 'class.cla_id = class_student_list.cla_id', 'inner');
        $this->db->join('section_tbl as section', 'section.sec_id = class.sec_id', 'inner');
        $data = [
                    'class_student_list.cla_id' => $cla_id,
                ];
        $this->db->where($data);
        $this->db->order_by('st_lname');
        $students = $this->db->get();

        $this->db->select('req_name,req_id');
        $this->db->from('ojt_requirements_tbl');
        $this->db->order_by('req_id');
        $requirements = $this->db->get();

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('30');
        $objPHPExcel->getActiveSheet()->SetCellValue('A6', 'STUDENT NAME');
        $objPHPExcel->getActiveSheet()->getStyle("A6")->applyFromArray(
                    [
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => ['rgb' => "2196f3"]
                        ],
                        'borders' => [
                          'allborders' => [
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                          ]
                        ]
                    ]
                );
        $header_count = 1;

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ADVISER NAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', $_SESSION['fa_fname']." ".$_SESSION['fa_lname']);
        $objPHPExcel->getActiveSheet()->SetCellValue('A2', 'SECTION');
        $objPHPExcel->getActiveSheet()->SetCellValue('A3', 'TERM');
        $objPHPExcel->getActiveSheet()->SetCellValue('A4', 'SCHOOL YEAR');
        $objPHPExcel->getActiveSheet()->SetCellValue('A5', 'OJT MONITORING');

        $letter = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
        $number_of_requirements = 0;
        foreach ($requirements->result() as $requirement) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($letter[$header_count])->setWidth('4');
            $objPHPExcel->getActiveSheet()->SetCellValue($letter[$header_count]."6", strtoupper($requirement->req_name));
            $objPHPExcel->getActiveSheet()->getStyle($letter[$header_count]."6")->applyFromArray(
                    [
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => ['rgb' => "2196f3"]
                        ],
                        'borders' => [
                          'allborders' => [
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                          ]
                        ],
                    ]
                );
            $objPHPExcel->getActiveSheet()->getStyle($letter[$header_count]."6")->getAlignment()->setTextRotation(90);
            $header_count++;
            $number_of_requirements++;
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension($letter[$header_count])->setWidth('12');
        $objPHPExcel->getActiveSheet()->SetCellValue($letter[$header_count]."6", strtoupper("Final Remark"));
        $objPHPExcel->getActiveSheet()->getStyle($letter[$header_count]."6")->applyFromArray(
                    [
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => ['rgb' => "2196f3"]
                        ],
                        'borders' => [
                          'allborders' => [
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                          ],
                        ],
                    ]
                );

        $objPHPExcel->getActiveSheet()->mergeCells('B1:'.$letter[$header_count].'1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$letter[$header_count].'1')->applyFromArray(
                    [
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => ['rgb' => "2196f3"]
                        ],
                        'borders' => [
                          'allborders' => [
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                          ],
                        ],
                    ]
                );
        $objPHPExcel->getActiveSheet()->mergeCells('B2:'.$letter[$header_count].'2');
        $objPHPExcel->getActiveSheet()->getStyle('A2:'.$letter[$header_count].'2')->applyFromArray(
                    [
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => ['rgb' => "2196f3"]
                        ],
                        'borders' => [
                          'allborders' => [
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                          ],
                        ],
                    ]
                );
        $objPHPExcel->getActiveSheet()->mergeCells('B3:'.$letter[$header_count].'3');
        $objPHPExcel->getActiveSheet()->getStyle('A3:'.$letter[$header_count].'3')->applyFromArray(
                    [
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => ['rgb' => "2196f3"]
                        ],
                        'borders' => [
                          'allborders' => [
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                          ],
                        ],
                    ]
                );
        $objPHPExcel->getActiveSheet()->mergeCells('B4:'.$letter[$header_count].'4');
        $objPHPExcel->getActiveSheet()->getStyle('A4:'.$letter[$header_count].'4')->applyFromArray(
                    [
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => ['rgb' => "2196f3"]
                        ],
                        'borders' => [
                          'allborders' => [
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                          ],
                        ],
                    ]
                );
        $objPHPExcel->getActiveSheet()->mergeCells('A5:'.$letter[$header_count].'5');
        $objPHPExcel->getActiveSheet()->getStyle('A5:'.$letter[$header_count].'5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A5:'.$letter[$header_count].'5')->applyFromArray(
                    [
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => ['rgb' => "ffff00"]
                        ],
                        'borders' => [
                          'allborders' => [
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                          ],
                        ],
                    ]
                );

        $header_count = 1;
        $recordCount  = 7;
        $sec_name     = null;
        $s_name       = "";
        $term         = "";
        $year         = "";

        foreach ($students->result() as $student) {
            $s_name  = $student->sec_name;
            $term    = $student->cla_term;
            $year    = $student->cla_year;
            $number_of_checked_requirements = 0;
            $this->db->select('if(checklist.st_id is null,"Incomplete","Complete")as remark,requirement.req_id req_list_id');
            $this->db->from('ojt_requirements_tbl as requirement');
            $this->db->join('(select * from checklist_tbl where st_id = '.$student->st_id.") as checklist", 'checklist.req_id = requirement.req_id', 'left');
            $this->db->join('(select * from student_tbl where st_id = "'.$student->st_id.'") as student', 'student.st_id = checklist.st_id', 'left');
            $this->db->order_by('requirement.req_id');
            $checklist = $this->db->get();

            if ($type) {
                $remarks = array_column($checklist->result_array(), 'remark');
                $remarks = array_count_values($remarks);

                if (isset($remarks['Incomplete'])) {
                    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$recordCount, $student->name);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$recordCount)->applyFromArray(
                        [
                            'fill' => [
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => ['rgb' => "f4ff81"]
                            ],
                            'borders' => [
                              'allborders' => [
                                  'style' => PHPExcel_Style_Border::BORDER_THIN
                              ]
                            ]
                        ]
                    );

                    foreach ($checklist->result() as $check) {
                        if (strcmp($check->remark, 'Complete')==0) {
                            $number_of_checked_requirements++;
                        }
                        $marking = strcmp($check->remark, 'Complete') == 0 ?  '✓' : 'x';
                        $objPHPExcel->getActiveSheet()->SetCellValue($letter[$header_count].$recordCount, $marking);
                        $status_color = strcmp($check->remark, "Incomplete")!=0 ? "a5d6a7" : "ef9a9a";
                        $objPHPExcel->getActiveSheet()->getStyle($letter[$header_count].$recordCount)->applyFromArray(
                            [
                                'fill' => [
                                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                    'color' => ['rgb' => $status_color]
                                ], 'borders' => [
                                  'allborders' => [
                                      'style' => PHPExcel_Style_Border::BORDER_THIN
                                  ]
                                ],
                                'alignment' => [
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                ]
                            ]
                            );
                        $header_count++;
                    }

                    $final_mark = "";
                    $final_mark_color = "";
                    if ($number_of_checked_requirements==$number_of_requirements) {
                        $final_mark = "Completed";
                        $final_mark_color = "a5d6a7";
                    } else {
                        $final_mark = "Incomplete";
                        $final_mark_color = "ef9a9a";
                    }

                    $objPHPExcel->getActiveSheet()->SetCellValue($letter[$header_count].$recordCount, $final_mark);
                    $objPHPExcel->getActiveSheet()->getStyle($letter[$header_count].$recordCount)->applyFromArray(
                        [
                            'fill' => [
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => ['rgb' => $final_mark_color]
                            ], 'borders' => [
                              'allborders' => [
                                  'style' => PHPExcel_Style_Border::BORDER_THIN
                              ]
                            ],
                            'alignment' => [
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            ]
                        ]
                        );


                    $header_count=1;
                    $recordCount++;
                }
            } else {
                $objPHPExcel->getActiveSheet()->SetCellValue('A'.$recordCount, $student->name);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$recordCount)->applyFromArray(
                    [
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => ['rgb' => "f4ff81"]
                        ],
                        'borders' => [
                          'allborders' => [
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                          ]
                        ]
                    ]
                );

                foreach ($checklist->result() as $check) {
                    if (strcmp($check->remark, 'Complete')==0) {
                        $number_of_checked_requirements++;
                    }
                    $marking = strcmp($check->remark, 'Complete') == 0 ?  '✓' : 'x';
                    $objPHPExcel->getActiveSheet()->SetCellValue($letter[$header_count].$recordCount, $marking);
                    $status_color = strcmp($check->remark, "Incomplete")!=0 ? "a5d6a7" : "ef9a9a";
                    $objPHPExcel->getActiveSheet()->getStyle($letter[$header_count].$recordCount)->applyFromArray(
                        [
                            'fill' => [
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => ['rgb' => $status_color]
                            ], 'borders' => [
                              'allborders' => [
                                  'style' => PHPExcel_Style_Border::BORDER_THIN
                              ]
                            ],
                            'alignment' => [
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            ]
                        ]
                        );
                    $header_count++;
                }

                $final_mark = "";
                $final_mark_color = "";
                if ($number_of_checked_requirements==$number_of_requirements) {
                    $final_mark = "Completed";
                    $final_mark_color = "a5d6a7";
                } else {
                    $final_mark = "Incomplete";
                    $final_mark_color = "ef9a9a";
                }

                $objPHPExcel->getActiveSheet()->SetCellValue($letter[$header_count].$recordCount, $final_mark);
                $objPHPExcel->getActiveSheet()->getStyle($letter[$header_count].$recordCount)->applyFromArray(
                    [
                        'fill' => [
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => ['rgb' => $final_mark_color]
                        ], 'borders' => [
                          'allborders' => [
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                          ]
                        ],
                        'alignment' => [
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        ]
                    ]
                    );


                $header_count=1;
                $recordCount++;
            }
        }
        $objPHPExcel->getActiveSheet()->SetCellValue('B2', $s_name);
        $objPHPExcel->getActiveSheet()->SetCellValue('B3', $term);
        $objPHPExcel->getActiveSheet()->SetCellValue('B4', $year);
        $sec_name = $student->sec_name;

        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: Attachement;filename=".$sec_name.($type ? '_Incomplete' : '')."_Requirements_Report.xlsx");
        header("Cache-Control: max-age=0");

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $writer->save('php://output');
        exit;
    }
}
