<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_Controller extends CI_Controller
{
		public function __construct() 
	    { 
	            parent::__construct();
	            $this->load->helper('url'); 
	            $this->load->library('session');
	            $this->load->library('api');
	            $this->load->database();
	            
	    }

    	public function get_notifications(){
			$this->load->model("front-end/Notification_Model");		
			$result = $this->Notification_Model->get_notifications();

			echo json_encode($result->result());
		}

		public function get_notification_history(){
			$this->load->model("front-end/Notification_Model");		
			$result = $this->Notification_Model->get_notification_history();

			echo json_encode($result->result());
		}

		public function seen($link,$n_id){
			$n_id 	 = base64_decode(base64_decode($n_id));
			$forward = base64_decode(base64_decode($link));
			$this->api->seen($n_id);
			header("location:".$forward);
		}
	
}
?>