<?php defined('BASEPATH') or exit('No direct script access allowed');

class Slide_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('api');
        $this->load->database();
    }

    public function get_slides()
    {
        $this->load->model("back-end/Slide_Model");
        $result = $this->Slide_Model->get_slides();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/Slide_Model");
        $result = $this->Slide_Model->get_bin();

        echo json_encode($result->result());
    }

    public function create_slide()
    {
        $s_image_content = $_FILES['slide-image']['tmp_name'];
        $s_image_name    = $_FILES['slide-image']['name'];
        $caption       	 = strip_tags($this->input->post('caption'));
        $description     = strip_tags($this->input->post('description'));

        $this->load->model("back-end/Slide_Model");
        echo $result = $this->Slide_Model->create_slide($s_image_content, $s_image_name, $caption, $description);
    }

    public function delete_slide()
    {
        $s_id  		= $this->input->post('s_id');
        $slug_name  = $this->input->post('slug_name');
        $this->load->model("back-end/Slide_Model");
        $this->Slide_Model->delete_slide($s_id, $slug_name);
    }

    public function softDelete()
    {
        $s_id = $this->input->post('s_id');
        $this->load->model("back-end/Slide_Model");
        $this->Slide_Model->softDelete($s_id);
    }

    public function restore()
    {
        $s_id = $this->input->post('s_id');
        $this->load->model("back-end/Slide_Model");
        $this->Slide_Model->restore($s_id);
    }
}
