<?php defined('BASEPATH') or exit('No direct script access allowed');

class Partner_Company_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('api');
        $this->load->database();
    }

    public function get_companies()
    {
        $this->load->model("back-end/Partner_Company_Model");
        $result = $this->Partner_Company_Model->get_companies();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/Partner_Company_Model");
        $result = $this->Partner_Company_Model->get_bin();

        echo json_encode($result->result());
    }

    public function create_company()
    {
        $s_logo_content = $_FILES['company-image']['tmp_name'];
        $s_logo_name = $_FILES['company-image']['name'];
        $company_name = strip_tags($this->input->post('company_name'));
        $slug = url_title($company_name, 'dash', true);
        $description = strip_tags($this->input->post('description'));
        $link = strip_tags($this->input->post('company_link'));
        $email = strip_tags($this->input->post('company_email'));
        $type = strip_tags($this->input->post('company_type'));

        $this->load->model("back-end/Partner_Company_Model");
        $this->Partner_Company_Model->create_company($s_logo_content, $s_logo_name, $company_name, $slug, $description, $link, $email, $type);
    }

    public function edit_company()
    {
        $old_image = strip_tags($this->input->post('edit_co_image'));
        $co_id = strip_tags($this->input->post('co_id'));
        $s_logo_content = $_FILES['edit_company-image']['tmp_name'];
        $s_logo_name = $_FILES['edit_company-image']['name'];
        $company_name = strip_tags($this->input->post('edit_company_name'));
        $description = strip_tags($this->input->post('edit_description'));
        $link = strip_tags($this->input->post('edit_company_link'));
        $email = strip_tags($this->input->post('edit_company_email'));
        $type = strip_tags($this->input->post('edit_company_type'));

        $this->load->model("back-end/Partner_Company_Model");
        $this->Partner_Company_Model->edit_company($old_image, $co_id, $s_logo_content, $s_logo_name, $company_name, $description, $link, $email, $type);
    }

    public function delete_company()
    {
        $co_id  		  = $this->input->post('co_id');
        $image_to_delete  = $this->input->post('image_to_delete');
        $this->load->model("back-end/Partner_Company_Model");
        $this->Partner_Company_Model->delete_company($co_id, $image_to_delete);
    }

    public function softDelete()
    {
        $co_id = $this->input->post('co_id');
        $this->load->model("back-end/Partner_Company_Model");
        $this->Partner_Company_Model->softDelete($co_id);
    }

    public function restore()
    {
        $co_id = $this->input->post('co_id');
        $this->load->model("back-end/Partner_Company_Model");
        $this->Partner_Company_Model->restore($co_id);
    }
}
