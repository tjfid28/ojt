<?php defined('BASEPATH') or exit('No direct script access allowed');

class News_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('api');
        $this->load->database();
    }

    public function get_news()
    {
        $this->load->model("back-end/News_Model");
        $result = $this->News_Model->get_news();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/News_Model");
        $result = $this->News_Model->get_bin();

        echo json_encode($result->result());
    }

    public function post_news()
    {
        $post = $this->input->post('news_field');
        $this->load->model("back-end/News_Model");
        echo $result = $this->News_Model->post_news($post);
    }

    public function delete_news()
    {
        $n_id = $this->input->post('n_id');
        $this->load->model("back-end/News_Model");
        echo $result = $this->News_Model->delete_news($n_id);
    }

    public function softDelete()
    {
        $n_id = $this->input->post('n_id');
        $this->load->model("back-end/News_Model");
        echo $this->News_Model->softDelete($n_id);
    }

    public function restore()
    {
        $n_id = $this->input->post('n_id');
        $this->load->model("back-end/News_Model");
        echo $this->News_Model->restore($n_id);
    }
}
