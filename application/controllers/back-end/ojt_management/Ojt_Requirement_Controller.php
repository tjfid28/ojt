<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ojt_Requirement_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function get_requirements()
    {
        $this->load->model("back-end/ojt_management/Ojt_Requirement_Model");
        $result = $this->Ojt_Requirement_Model->get_requirements();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/ojt_management/Ojt_Requirement_Model");
        $result = $this->Ojt_Requirement_Model->get_bin();

        echo json_encode($result->result());
    }

    public function add_requirement()
    {
        $req_name  = strip_tags($this->input->post('req_name'));
        $sub_id    = strip_tags($this->input->post('sub_id'));
        $cou_id    = strip_tags($this->input->post('cou_id'));

        $this->load->model("back-end/ojt_management/Ojt_Requirement_Model");
        $this->Ojt_Requirement_Model->add_requirement($req_name, $sub_id, $cou_id);
    }

    public function edit_requirement()
    {
        $req_name  = strip_tags($this->input->post('edit_req_name'));
        $sub_id    = strip_tags($this->input->post('edit_sub_id'));
        $cou_id    = strip_tags($this->input->post('edit_cou_id'));
        $req_id    = strip_tags($this->input->post('edit_id'));

        $this->load->model("back-end/ojt_management/Ojt_Requirement_Model");
        $this->Ojt_Requirement_Model->edit_requirement($req_name, $sub_id, $cou_id, $req_id);
    }

    public function delete_requirement()
    {
        $req_id = $this->input->post('req_id');
        $this->load->model("back-end/ojt_management/Ojt_Requirement_Model");
        $this->Ojt_Requirement_Model->delete_requirement($req_id);
    }

    public function softDelete()
    {
        $req_id = $this->input->post('req_id');
        $this->load->model("back-end/ojt_management/Ojt_Requirement_Model");
        echo $this->Ojt_Requirement_Model->softDelete($req_id);
    }

    public function restore()
    {
        $req_id = $this->input->post('req_id');
        $this->load->model("back-end/ojt_management/Ojt_Requirement_Model");
        echo $this->Ojt_Requirement_Model->restore($req_id);
    }
}
