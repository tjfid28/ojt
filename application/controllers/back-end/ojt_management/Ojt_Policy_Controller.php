<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ojt_Policy_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('api');
        $this->load->database();
    }

    public function save_policy()
    {
        $img_content = $_FILES['policy_cover']['tmp_name'];
        $ext         = pathinfo($_FILES['policy_cover']['name'], PATHINFO_EXTENSION);
        $content     = $this->input->post('ojt_policy_content');

        $this->load->model("back-end/ojt_management/Ojt_Policy_Model");
        $this->Ojt_Policy_Model->save_policy($content, $img_content, $ext);
    }
}
