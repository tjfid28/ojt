<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Downloadable_Form_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function get_forms()
    {
        $this->load->model("back-end/ojt_management/Downloadable_Form_Model");
        $result = $this->Downloadable_Form_Model->get_forms();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/ojt_management/Downloadable_Form_Model");
        $result = $this->Downloadable_Form_Model->get_bin();

        echo json_encode($result->result());
    }

    public function add_form()
    {
        $df_content   = $_FILES['document']['tmp_name'];
        $file_name	  =	$_FILES['document']['name'];
        $req_id       = strip_tags($this->input->post('req_id'));

        $this->load->model("back-end/ojt_management/Downloadable_Form_Model");
        echo $result = $this->Downloadable_Form_Model->add_form($df_content, $file_name, $req_id);
    }

    public function delete_form()
    {
        $df_id   = $this->input->post('df_id');
        $df_slug = $this->input->post('slug_name');
        $this->load->model("back-end/ojt_management/Downloadable_Form_Model");
        $this->Downloadable_Form_Model->delete_form($df_id, $df_slug);
    }

    public function download_form($slug_name, $file_name)
    {
        $DLdata['slug_name'] = $slug_name;
        $DLdata['file_name'] = $file_name;
        $this->load->view("download_view", $DLdata);
    }

    public function softDelete()
    {
        $df_id = $this->input->post('df_id');
        $this->load->model("back-end/ojt_management/Downloadable_Form_Model");
        echo $this->Downloadable_Form_Model->softDelete($df_id);
    }

    public function restore()
    {
        $df_id = $this->input->post('df_id');
        $this->load->model("back-end/ojt_management/Downloadable_Form_Model");
        echo $this->Downloadable_Form_Model->restore($df_id);
    }
}
