<?php defined('BASEPATH') or exit('No direct script access allowed');

class Sub_Administrator_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('api');
        $this->load->database();
    }

    public function dashboard()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4) {
            $this->load->model("back-end/user_management/Student_Management_Model");
            $st_count  = $this->Student_Management_Model->get_students();
            $this->load->model("back-end/user_management/Faculty_Management_Model");
            $fa_count  = $this->Faculty_Management_Model->get_faculty();
            $this->load->model("back-end/Class_List_Model");
            $cla_count = $this->Class_List_Model->get_classes();
            $result['st_count']  = $st_count->num_rows();
            $result['fa_count']  = $fa_count->num_rows();
            $result['cla_count'] = $cla_count->num_rows();
            $data['content'] = $this->load->view('back-end/dashboard_view', $result, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function class_list()
    {
        if ((isset($_SESSION['account_type'])&&$_SESSION['account_type']==4)&&(isset($_SESSION['s_class_list'])&&$_SESSION['s_class_list']==1)) {
            $this->load->model("back-end/school_management/Section_Management_Model");
            $result['sections'] = $this->Section_Management_Model->get_sections_to_class();
            $this->load->model("back-end/school_management/Subject_Management_Model");
            $result['subjects'] = $this->Subject_Management_Model->get_subjects();
            $this->load->model("back-end/school_management/Course_Management_Model");
            $result['courses']  = $this->Course_Management_Model->get_courses();
            $this->load->model("back-end/user_management/Faculty_Management_Model");
            $result['faculty_members'] = $this->Faculty_Management_Model->get_faculty();

            // -- -- //
            $data['content'] = $this->load->view('back-end/class_list_view', $result, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function class_student_list($cla_id, $cou_id, $section)
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_class_list'])&&$_SESSION['s_class_list']==1)) {
            $result = null;
            $_SESSION['cla_id']   = $cla_id;
            $this->load->model("back-end/Class_Student_List_Model");
            $result['students']   = $this->Class_Student_List_Model->get_students($cou_id);
            $result['Class_Name'] = $section;
            $result['class_id'] = $cla_id;
            $data['content'] = $this->load->view('back-end/class_student_list_view', $result, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function student_management()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_student'])&&$_SESSION['s_student']==1)) {
            $this->load->model("back-end/school_management/Course_Management_Model");
            $course['courses'] = $this->Course_Management_Model->get_courses();
            $data['content']   = $this->load->view('back-end/user_management/student_management_view', $course, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function faculty_management()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_faculty'])&&$_SESSION['s_faculty']==1)) {
            $this->load->model("back-end/school_management/Department_Management_Model");
            $departments['departments'] = $this->Department_Management_Model->get_departments();
            $data['content']   = $this->load->view('back-end/user_management/faculty_management_view', $departments, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function department_management()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_departments'])&&$_SESSION['s_departments']==1)) {
            $data['content']   = $this->load->view('back-end/school_management/department_management_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function course_management()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_courses'])&&$_SESSION['s_courses']==1)) {
            $data['content']   = $this->load->view('back-end/school_management/course_management_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function section_management()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_sections'])&&$_SESSION['s_sections']==1)) {
            $data['content']   = $this->load->view('back-end/school_management/section_management_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function subject_management()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_programs'])&&$_SESSION['s_programs']==1)) {
            $data['content']   = $this->load->view('back-end/school_management/subject_management_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function uploaded_documents()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_uploaded_documents'])&&$_SESSION['s_uploaded_documents']==1)) {
            $data['content']   = $this->load->view('back-end/ojt_management/uploaded_documents_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function ojt_requirements()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_ojt_requirement'])&&$_SESSION['s_ojt_requirement']==1)) {
            $school_data = null;
            $this->load->model("back-end/school_management/Course_Management_Model");
            $school_data['courses'] = $this->Course_Management_Model->get_courses();
            $this->load->model("back-end/school_management/Subject_Management_Model");
            $school_data['subjects'] = $this->Subject_Management_Model->get_subjects();
            $data['content']   = $this->load->view('back-end/ojt_management/ojt_requirements_view', $school_data, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function downloadable_forms()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_downloadable_forms'])&&$_SESSION['s_downloadable_forms']==1)) {
            $ojt_data = null;
            $this->load->model("back-end/ojt_management/Ojt_Requirement_Model");
            $ojt_data['ojt_requirements'] = $this->Ojt_Requirement_Model->get_requirements();
            $data['content']   = $this->load->view('back-end/ojt_management/downloadable_forms_view', $ojt_data, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function ojt_policy()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_ojt_policy'])&&$_SESSION['s_ojt_policy']==1)) {
            $this->load->model("back-end/ojt_management/Ojt_Policy_Model");
            $policy 		   = $this->Ojt_Policy_Model->get_policy();
            $_SESSION['policy_cover'] = $policy['op_image'];
            $result['policy']  = $policy;
            $data['content']   = $this->load->view('back-end/ojt_management/ojt_policy_view', $result, true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function news_management()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_news'])&&$_SESSION['s_news']==1)) {
            $data['content']   = $this->load->view('back-end/news_management_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function partner_companies()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_company'])&&$_SESSION['s_company']==1)) {
            $data['content']   = $this->load->view('back-end/partner_companies_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function slides_management()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4&&(isset($_SESSION['s_slides'])&&$_SESSION['s_slides']==1)) {
            $data['content']   = $this->load->view('back-end/slides_management_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    /*public function audit_log()
    {
        if (isset($_SESSION['account_type'])&&$_SESSION['account_type']==4) {
            $data['content']   = $this->load->view('back-end/audit_log_view', '', true);
            $this->load->view('layout/master_view', $data);
        } else {
            header("location:".base_url());
        }
    }*/
}
