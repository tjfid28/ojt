<?php defined('BASEPATH') or exit('No direct script access allowed');

class Audit_Log_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function get_audit_logs()
    {
        $this->load->model("back-end/Audit_Log_Model");
        $result = $this->Audit_Log_Model->get_audit_logs();

        echo json_encode($result->result());
    }
}
