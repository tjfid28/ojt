<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Course_Management_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function get_courses()
    {
        $this->load->model("back-end/school_management/Course_Management_Model");
        $result = $this->Course_Management_Model->get_courses();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/school_management/Course_Management_Model");
        $result = $this->Course_Management_Model->get_bin();

        echo json_encode($result->result());
    }

    public function add_course()
    {
        $cou_title = strip_tags($this->input->post('cou_title'));

        $this->load->model("back-end/school_management/Course_Management_Model");
        echo $this->Course_Management_Model->add_course($cou_title);
    }

    public function edit_course()
    {
        $cou_title  = strip_tags($this->input->post('edit_cou_title'));
        $cou_id    = $this->input->post('edit_id');

        $this->load->model("back-end/school_management/Course_Management_Model");
        echo $this->Course_Management_Model->edit_course($cou_title, $cou_id);
    }

    public function delete_course()
    {
        $cou_id = $this->input->post('cou_id');
        $this->load->model("back-end/school_management/Course_Management_Model");
        $this->Course_Management_Model->delete_course($cou_id);
    }

    public function softDelete()
    {
        $cou_id = $this->input->post('cou_id');
        $this->load->model("back-end/school_management/Course_Management_Model");
        echo $this->Course_Management_Model->softDelete($cou_id);
    }

    public function restore()
    {
        $cou_id = $this->input->post('cou_id');
        $this->load->model("back-end/school_management/Course_Management_Model");
        echo $this->Course_Management_Model->restore($cou_id);
    }
}
