<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Section_Management_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function get_sections()
    {
        $this->load->model("back-end/school_management/Section_Management_Model");
        $result = $this->Section_Management_Model->get_sections();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/school_management/Section_Management_Model");
        $result = $this->Section_Management_Model->get_bin();

        echo json_encode($result->result());
    }

    public function add_section()
    {
        $sec_name = strip_tags($this->input->post('sec_name'));

        $this->load->model("back-end/school_management/Section_Management_Model");
        echo $this->Section_Management_Model->add_section($sec_name);
    }

    public function edit_section()
    {
        $sec_name  = strip_tags($this->input->post('edit_sec_name'));
        $sec_id    = $this->input->post('edit_id');

        $this->load->model("back-end/school_management/Section_Management_Model");
        echo $this->Section_Management_Model->edit_section($sec_name, $sec_id);
    }

    public function delete_section()
    {
        $sec_id = $this->input->post('sec_id');
        $this->load->model("back-end/school_management/Section_Management_Model");
        echo $this->Section_Management_Model->delete_section($sec_id);
    }

    public function softDelete()
    {
        $sec_id = $this->input->post('sec_id');
        $this->load->model("back-end/school_management/Section_Management_Model");
        echo $this->Section_Management_Model->softDelete($sec_id);
    }

    public function restore()
    {
        $sec_id = $this->input->post('sec_id');
        $this->load->model("back-end/school_management/Section_Management_Model");
        echo $this->Section_Management_Model->restore($sec_id);
    }
}
