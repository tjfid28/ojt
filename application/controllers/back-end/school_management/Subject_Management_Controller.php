<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Subject_Management_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function get_subjects()
    {
        $this->load->model("back-end/school_management/Subject_Management_Model");
        $result = $this->Subject_Management_Model->get_subjects();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/school_management/Subject_Management_Model");
        $result = $this->Subject_Management_Model->get_bin();

        echo json_encode($result->result());
    }

    public function add_subject()
    {
        $sub_name 	   = strip_tags($this->input->post('sub_name'));
        $sub_req_hours = strip_tags($this->input->post('sub_req_hours'));
        $sub_unit	   = strip_tags($this->input->post('sub_unit'));

        $this->load->model("back-end/school_management/Subject_Management_Model");
        echo $this->Subject_Management_Model->add_subject($sub_name, $sub_req_hours, $sub_unit);
    }

    public function edit_subject()
    {
        $sub_name 	   = strip_tags($this->input->post('edit_sub_name'));
        $sub_req_hours = strip_tags($this->input->post('edit_sub_req_hours'));
        $sub_unit	   = strip_tags($this->input->post('edit_sub_unit'));
        $sub_id	   	   = strip_tags($this->input->post('edit_id'));

        $this->load->model("back-end/school_management/Subject_Management_Model");
        echo $this->Subject_Management_Model->edit_subject($sub_name, $sub_req_hours, $sub_unit, $sub_id);
    }

    public function delete_subject()
    {
        $sub_id = $this->input->post('sub_id');
        $this->load->model("back-end/school_management/Subject_Management_Model");
        echo $this->Subject_Management_Model->delete_subject($sub_id);
    }

    public function softDelete()
    {
        $sub_id = $this->input->post('sub_id');
        $this->load->model("back-end/school_management/Subject_Management_Model");
        echo $this->Subject_Management_Model->softDelete($sub_id);
    }

    public function restore()
    {
        $sub_id = $this->input->post('sub_id');
        $this->load->model("back-end/school_management/Subject_Management_Model");
        echo $this->Subject_Management_Model->restore($sub_id);
    }
}
