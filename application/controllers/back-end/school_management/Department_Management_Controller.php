<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Department_Management_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function get_departments()
    {
        $this->load->model("back-end/school_management/Department_Management_Model");
        $result = $this->Department_Management_Model->get_departments();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/school_management/Department_Management_Model");
        $result = $this->Department_Management_Model->get_bin();

        echo json_encode($result->result());
    }

    public function add_department()
    {
        $dept_name = strip_tags($this->input->post('dept_name'));
        $this->load->model("back-end/school_management/Department_Management_Model");
        echo $this->Department_Management_Model->add_department($dept_name);
    }

    public function edit_department()
    {
        $dept_name  = strip_tags($this->input->post('edit_dept_name'));
        $dept_id    = $this->input->post('edit_id');

        $this->load->model("back-end/school_management/Department_Management_Model");
        echo $this->Department_Management_Model->edit_department($dept_name, $dept_id);
    }

    public function delete_department()
    {
        $dept_id = $this->input->post('dept_id');
        $this->load->model("back-end/school_management/Department_Management_Model");
        $this->Department_Management_Model->delete_department($dept_id);
    }

    public function softDelete()
    {
        $dept_id = $this->input->post('dept_id');
        $this->load->model("back-end/school_management/Department_Management_Model");
        echo $this->Department_Management_Model->softDelete($dept_id);
    }

    public function restore()
    {
        $dept_id = $this->input->post('dept_id');
        $this->load->model("back-end/school_management/Department_Management_Model");
        echo $this->Department_Management_Model->restore($dept_id);
    }
}
