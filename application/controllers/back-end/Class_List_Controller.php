<?php defined('BASEPATH') or exit('No direct script access allowed');

class Class_List_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        //$this->load->library('api');
        $this->load->database();
    }

    public function get_classes()
    {
        $this->load->model("back-end/Class_List_Model");
        $result = $this->Class_List_Model->get_classes();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/Class_List_Model");
        $result = $this->Class_List_Model->get_bin();

        echo json_encode($result->result());
    }

    public function create_class()
    {
        $sec_id = $this->input->post('sec_id');
        $sub_id = $this->input->post('sub_id');
        $cou_id = $this->input->post('cou_id');
        $fa_id = $this->input->post('fa_id');
        $term = $this->input->post('term');
        $year = $this->input->post('start_year')."-".$this->input->post('end_year');

        $this->load->model("back-end/Class_List_Model");
        $this->Class_List_Model->create_class($sec_id, $sub_id, $cou_id, $fa_id, $term, $year);
    }

    public function edit_class()
    {
        $sub_id = $this->input->post('edit_sub_id');
        $cou_id = $this->input->post('edit_cou_id');
        $fa_id  = $this->input->post('edit_fa_id');
        $cla_id = $this->input->post('edit_id');
        $term = $this->input->post('edit_term');
        $year = $this->input->post('edit_start_year')."-".$this->input->post('edit_end_year');
        $status = $this->input->post('edit_status');

        $this->load->model("back-end/Class_List_Model");
        $this->Class_List_Model->edit_class($sub_id, $cou_id, $fa_id, $cla_id, $term, $year, $status);
    }

    public function delete_class()
    {
        $cla_id = $this->input->post('cla_id');
        $this->load->model("back-end/Class_List_Model");
        $this->Class_List_Model->delete_class($cla_id);
    }

    public function softDelete()
    {
        $cla_id = $this->input->post('cla_id');
        $this->load->model("back-end/Class_List_Model");
        $this->Class_List_Model->softDelete($cla_id);
    }

    public function restore()
    {
        $cla_id = $this->input->post('cla_id');
        $this->load->model("back-end/Class_List_Model");
        $this->Class_List_Model->restore($cla_id);
    }
}
