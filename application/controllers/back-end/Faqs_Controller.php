<?php defined('BASEPATH') or exit('No direct script access allowed');

class Faqs_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('api');
        $this->load->database();
    }

    public function get_faqs()
    {
        $this->load->model("back-end/Faqs_Model");
        $result = $this->Faqs_Model->get_faqs();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/Faqs_Model");
        $result = $this->Faqs_Model->get_bin();

        echo json_encode($result->result());
    }

    public function create_faq()
    {
        $fa_title   = strip_tags($this->input->post('faq_title'));
        $description    = strip_tags($this->input->post('description'));
        $this->load->model("back-end/Faqs_Model");
        $this->Faqs_Model->create_faq($fa_title, $description);
    }

    public function edit_faq()
    {
        $fa_id = strip_tags($this->input->post('fa_id'));
        $fa_title = strip_tags($this->input->post('edit_faq_title'));
        $description = strip_tags($this->input->post('edit_description'));
        $this->load->model("back-end/Faqs_Model");
        $this->Faqs_Model->edit_faq($fa_id, $fa_title, $description);
    }

    public function delete_faq()
    {
        $fa_id = $this->input->post('fa_id');
        $this->load->model("back-end/Faqs_Model");
        $this->Faqs_Model->delete_faq($fa_id);
    }

    public function softDelete()
    {
        $fa_id = $this->input->post('fa_id');
        $this->load->model("back-end/Faqs_Model");
        $this->Faqs_Model->softDelete($fa_id);
    }

    public function restore()
    {
        $fa_id = $this->input->post('fa_id');
        $this->load->model("back-end/Faqs_Model");
        $this->Faqs_Model->restore($fa_id);
    }
}
