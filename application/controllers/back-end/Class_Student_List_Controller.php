<?php defined('BASEPATH') or exit('No direct script access allowed');

class Class_Student_List_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        //$this->load->library('api');
        $this->load->database();
    }

    public function get_students_in_class()
    {
        $this->load->model("back-end/Class_Student_List_Model");
        $result = $this->Class_Student_List_Model->get_students_in_class();

        echo json_encode($result->result());
    }

    public function add_student()
    {
        $st_id_container  = $this->input->post('st_id_container');
        $cla_id = $this->input->post('cla_id');
        $this->load->model("back-end/Class_Student_List_Model");
        $this->Class_Student_List_Model->add_student($st_id_container, $cla_id);
    }

    public function delete_student()
    {
        $cs_id = $this->input->post('cs_id');
        $this->load->model("back-end/Class_Student_List_Model");
        $this->Class_Student_List_Model->delete_student($cs_id);
    }
}
