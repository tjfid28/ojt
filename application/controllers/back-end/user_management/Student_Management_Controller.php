<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Student_Management_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('excel');
        $this->load->database();
    }

    public function filter($arr)
    {
        return !is_null($arr['fb_id']);
    }

    public function get_students()
    {
        $this->load->model("back-end/user_management/Student_Management_Model");
        $result = $this->Student_Management_Model->get_students();
        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/user_management/Student_Management_Model");
        $result = $this->Student_Management_Model->get_bin();
        echo json_encode($result->result());
    }

    public function add_student()
    {
        $st_img_content = $_FILES['st_img']['tmp_name'];
        $st_ext = pathinfo($_FILES['st_img']['name'], PATHINFO_EXTENSION);

        $uname  = $this->input->post('uname');
        $pword  = $this->input->post('pword');
        $fname  = $this->input->post('first_name');
        $mname  = $this->input->post('middle_name');
        $lname  = $this->input->post('last_name');
        $cou_id = $this->input->post('cou_id');
        $gender = $this->input->post('gender');
        $year = $this->input->post('start_year')."-".$this->input->post('end_year');
        $st_no  = $this->input->post('st_number');
        $email  = $this->input->post('email');
        $mobile_no  = $this->input->post('mobile_no');

        $this->load->model("back-end/user_management/Student_Management_Model");
        $this->Student_Management_Model->add_student($uname, $pword, $fname, $mname, $lname, $cou_id, $gender, $year, $st_no, $email, $mobile_no, $st_img_content, $st_ext);
    }

    public function import_student()
    {
        $root = getcwd();
        $date = date("d-m-Y h:i:sa");
        $excel_content = $_FILES['document']['tmp_name'];
        $st_ext = pathinfo($_FILES['document']['name'], PATHINFO_EXTENSION);

        if (strcmp($st_ext, 'xlsx') == 0 || strcmp($st_ext, 'csv') == 0) {
            move_uploaded_file($excel_content, $root."/resources/excel/".basename("student_list.".$st_ext));
            // $objReader = PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');    // For excel 2007
            //Set to read only
            $objReader->setReadDataOnly(true);
            //Load excel file
            $objPHPExcel  = $objReader->load($root.'/resources/excel/'."student_list.".$st_ext);
            $totalrows     = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);

            $this->load->model("back-end/school_management/Course_Management_Model");
            $result = $this->Course_Management_Model->get_courses();

            $success = 0;
            $failed  = 0;

            for ($i=2; $i<=$totalrows; $i++) {
                $cou_id = null;
                $Course = $objWorksheet->getCellByColumnAndRow(0, $i)->getValue();
                $StudentNo = $objWorksheet->getCellByColumnAndRow(1, $i)->getValue();
                $LastName = $objWorksheet->getCellByColumnAndRow(2, $i)->getValue();
                $FirstName = $objWorksheet->getCellByColumnAndRow(3, $i)->getValue();
                $MiddleName = $objWorksheet->getCellByColumnAndRow(4, $i)->getValue();
                $Email = $objWorksheet->getCellByColumnAndRow(5, $i)->getValue();
                $MobileNo = $objWorksheet->getCellByColumnAndRow(6, $i)->getValue();
                $Username = $objWorksheet->getCellByColumnAndRow(7, $i)->getValue();
                $Password = $objWorksheet->getCellByColumnAndRow(8, $i)->getValue();
                $SchoolYear = $objWorksheet->getCellByColumnAndRow(9, $i)->getValue();
                $Gender = $objWorksheet->getCellByColumnAndRow(10, $i)->getValue();

                $this->db->where('st_uname', $Username);
                $this->db->or_where('st_number', $StudentNo);
                $query = $this->db->get('student_tbl');

                if ((preg_match("/^[a-zA-Z\s]+$/", $FirstName) && preg_match('/^[a-zA-Z\s]+$/', $LastName) && preg_match('/^[a-zA-Z\s]+$/', $MiddleName)) && (($Course != null && strcmp($Course, "") != 0) && ($StudentNo != null && strcmp($StudentNo, "") !=0) && ($LastName != null && strcmp($LastName, "") !=0) && ($FirstName != null && strcmp($FirstName, "") !=0) && ($MiddleName !=null &&strcmp($MiddleName, "") !=0) && ($Username != null && strcmp($Username, "") !=0) && ($Password != null && strcmp($Password, "") != 0 && strlen($Password) >= 4 && strlen($Password) <= 15) && ($Gender != null && strcmp($Gender, "") != 0)) && ($Email != null && strcmp(trim($Email), "") != 0) && ($MobileNo != null && strcmp(trim($MobileNo), "") != 0 && strlen(trim($MobileNo)) == 12 && is_numeric($MobileNo)) && ($SchoolYear != null && strcmp($SchoolYear, "") !=0) && $query->num_rows() == 0) {
                    foreach ($result->result() as $course) {
                        if (strcmp(strtolower(trim($Course)), strtolower($course->cou_title))==0) {
                            $cou_id = $course->cou_id;
                        }
                    }

                    $student_data = [
                            'st_number' => $StudentNo,
                            'cou_id' => $cou_id,
                            'st_fname' => $FirstName,
                            'st_mname' => $MiddleName,
                            'st_lname' => $LastName,
                            'st_email' => $Email,
                            'st_mobile_number' => $MobileNo,
                            'st_uname' => $Username,
                            'st_pword' => $Password,
                            'st_school_year' => $SchoolYear,
                            'st_gender' => $Gender,
                            'st_created_at' => $date,
                        ];
                    $this->load->model("back-end/user_management/Student_Management_Model");
                    $this->Student_Management_Model->import_student($student_data);

                    $success+=1;
                } else {
                    $failed+=1;
                }
                // unlink($root .'/resources/excel/'.'student_list.'.$st_ext);
            }
            $total = $success + $failed;
            echo $success." out of ".$total." student records has been inserted";
        } else {
            echo "Error";
        }
    }

    public function edit_student()
    {
        $st_img_content = $_FILES['edit_st_img']['tmp_name'];
        $st_ext         = pathinfo($_FILES['edit_st_img']['name'], PATHINFO_EXTENSION);
        $st_image_name  = $this->input->post('edit_st_img_name');
        $old_st_image   = $this->input->post('edit_image_name');

        $uname = $this->input->post('edit_uname');
        $pword = $this->input->post('edit_pword');
        $fname = $this->input->post('edit_first_name');
        $mname = $this->input->post('edit_middle_name');
        $lname = $this->input->post('edit_last_name');
        $id    = $this->input->post('edit_id');
        $cou_id= $this->input->post('edit_cou_id');
        $gender= $this->input->post('edit_gender');
        $year = $this->input->post('edit_start_year')."-".$this->input->post('edit_end_year');
        $st_no = $this->input->post('edit_st_number');
        $email = $this->input->post('edit_email');
        $mobile_no = $this->input->post('edit_mobile_no');

        $this->load->model("back-end/user_management/Student_Management_Model");
        $this->Student_Management_Model->edit_student($uname, $pword, $fname, $mname, $lname, $cou_id, $gender, $year, $st_no, $email, $mobile_no, $st_img_content, $st_ext, $st_image_name, $old_st_image, $id);
    }

    public function download_template()
    {
        $this->load->helper('download');
        $this->load->helper('file');
        $root    = getcwd();
        $name    = "student_template.xlsx";
        $content = file_get_contents($root."/resources/excel/".$name);
        force_download($name, $content);
    }

    public function delete_student()
    {
        $st_id = $this->input->post('st_id');
        $st_img= $this->input->post('st_img');
        $this->load->model("back-end/user_management/Student_Management_Model");
        $this->Student_Management_Model->delete_student($st_id, $st_img);
    }

    public function softDelete()
    {
        $st_id = $this->input->post('st_id');
        $this->load->model("back-end/user_management/Student_Management_Model");
        echo $this->Student_Management_Model->softDelete($st_id);
    }

    public function restore()
    {
        $st_id = $this->input->post('st_id');
        $this->load->model("back-end/user_management/Student_Management_Model");
        echo $this->Student_Management_Model->restore($st_id);
    }
}
