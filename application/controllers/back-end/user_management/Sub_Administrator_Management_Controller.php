<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sub_Administrator_Management_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function get_sub_administrators()
    {
        $this->load->model("back-end/user_management/Sub_Administrator_Management_Model");
        $result = $this->Sub_Administrator_Management_Model->get_sub_administrators();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/user_management/Sub_Administrator_Management_Model");
        $result = $this->Sub_Administrator_Management_Model->get_bin();

        echo json_encode($result->result());
    }

    public function add_sub_administrators()
    {
        $this->data = [
            's_dashboard' => $this->input->post('dashboard_permission')=='on' ? 1 : 0,
            's_news' => $this->input->post('news_permission')=='on' ? 1 : 0,
            's_class_list' => $this->input->post('class_list_permission')=='on' ? 1 : 0,
            's_slides' => $this->input->post('slides_permission')=='on' ? 1 : 0,
            's_company' => $this->input->post('company_permission')=='on' ? 1 : 0,
            's_courses' => $this->input->post('courses_permission')=='on' ? 1 : 0,
            's_departments' => $this->input->post('departments_permission')=='on' ? 1 : 0,
            's_programs' => $this->input->post('programs_permission')=='on' ? 1 : 0,
            's_sections' => $this->input->post('sections_permission')=='on' ? 1 : 0,
            's_student' => $this->input->post('student_permission')=='on' ? 1 : 0,
            's_faculty' => $this->input->post('faculty_permission')=='on' ? 1 : 0,
            's_uploaded_documents' => $this->input->post('uploaded_documents_permission')=='on' ? 1 : 0,
            's_ojt_requirement'=> $this->input->post('ojt_requirements_permission')=='on' ? 1 : 0,
            's_downloadable_forms'=> $this->input->post('downloadable_permission')=='on' ? 1 : 0,
            's_ojt_policy' => $this->input->post('ojt_policies_permission')=='on' ? 1 : 0,
            's_ad_uname' => $this->input->post('uname'),
            's_ad_pword' => $this->input->post('pword'),
            's_ad_fname' => $this->input->post('first_name'),
            's_ad_mname' => $this->input->post('middle_name'),
            's_ad_lname' => $this->input->post('last_name'),
            's_ad_created_at' => date("d-m-Y h:i:sa"),
            's_ad_updated_at' => '',
        ];

        $this->load->model("back-end/user_management/Sub_Administrator_Management_Model");
        $this->Sub_Administrator_Management_Model->add_sub_administrators($this->data);
    }

    public function edit_sub_administrators()
    {
        $this->data = [
            's_dashboard' => $this->input->post('edit_dashboard_permission')=='on' ? 1 : 0,
            's_news' => $this->input->post('edit_news_permission')=='on' ? 1 : 0,
            's_class_list' => $this->input->post('edit_class_list_permission')=='on' ? 1 : 0,
            's_slides' => $this->input->post('edit_slides_permission')=='on' ? 1 : 0,
            's_company' => $this->input->post('edit_company_permission')=='on' ? 1 : 0,
            's_courses' => $this->input->post('edit_courses_permission')=='on' ? 1 : 0,
            's_departments' => $this->input->post('edit_departments_permission')=='on' ? 1 : 0,
            's_programs' => $this->input->post('edit_programs_permission')=='on' ? 1 : 0,
            's_sections' => $this->input->post('edit_sections_permission')=='on' ? 1 : 0,
            's_student' => $this->input->post('edit_student_permission')=='on' ? 1 : 0,
            's_faculty' => $this->input->post('edit_faculty_permission')=='on' ? 1 : 0,
            's_uploaded_documents' => $this->input->post('edit_uploaded_documents_permission')=='on' ? 1 : 0,
            's_ojt_requirement'=> $this->input->post('edit_ojt_requirements_permission')=='on' ? 1 : 0,
            's_downloadable_forms'=> $this->input->post('edit_downloadable_permission')=='on' ? 1 : 0,
            's_ojt_policy' => $this->input->post('edit_ojt_policies_permission')=='on' ? 1 : 0,
            's_ad_uname' => $this->input->post('edit_uname'),
            's_ad_pword' => $this->input->post('edit_pword'),
            's_ad_fname' => $this->input->post('edit_first_name'),
            's_ad_mname' => $this->input->post('edit_middle_name'),
            's_ad_lname' => $this->input->post('edit_last_name'),
            's_ad_updated_at' => date("d-m-Y h:i:sa")
        ];

        $id = $this->input->post('edit_id');
        $this->load->model("back-end/user_management/Sub_Administrator_Management_Model");
        $this->Sub_Administrator_Management_Model->edit_sub_administrators($this->data, $id);
    }

    public function delete_sub_administrators()
    {
        echo $s_ad_id = $this->input->post('s_ad_id');
        $this->load->model("back-end/user_management/Sub_Administrator_Management_Model");
        $this->Sub_Administrator_Management_Model->delete_sub_administrators($s_ad_id);
    }

    public function softDelete()
    {
        $s_ad_id = $this->input->post('s_ad_id');
        $this->load->model("back-end/user_management/Sub_Administrator_Management_Model");
        echo $this->Sub_Administrator_Management_Model->softDelete($s_ad_id);
    }

    public function restore()
    {
        $s_ad_id = $this->input->post('s_ad_id');
        $this->load->model("back-end/user_management/Sub_Administrator_Management_Model");
        echo $this->Sub_Administrator_Management_Model->restore($s_ad_id);
    }
}
