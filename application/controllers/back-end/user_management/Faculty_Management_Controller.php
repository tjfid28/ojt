<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Faculty_Management_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function get_faculty()
    {
        $this->load->model("back-end/user_management/Faculty_Management_Model");
        $result = $this->Faculty_Management_Model->get_faculty();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/user_management/Faculty_Management_Model");
        $result = $this->Faculty_Management_Model->get_bin();

        echo json_encode($result->result());
    }

    public function add_faculty()
    {
        $fa_img_content = $_FILES['fa_img']['tmp_name'];
        $fa_ext = pathinfo($_FILES['fa_img']['name'], PATHINFO_EXTENSION);

        $this->data = [
            'fa_uname'     => $this->input->post('uname'),
            'fa_pword'     => $this->input->post('pword'),
            'fa_fname'     => $this->input->post('first_name'),
            'fa_mname'     => $this->input->post('middle_name'),
            'fa_lname'     => $this->input->post('last_name'),
            'fa_number'    => $this->input->post('fa_number'),
            'dept_id'      => $this->input->post('dept_id'),
            'fa_gender'    => $this->input->post('gender'),
            'fa_created_at'   => date("d-m-Y h:i:sa"),
            'fa_updated_at'   => '',
        ];

        $this->load->model("back-end/user_management/Faculty_Management_Model");
        $this->Faculty_Management_Model->add_faculty($this->data, $fa_img_content, $fa_ext);
    }

    public function edit_faculty()
    {
        $fa_img_content = $_FILES['edit_fa_img']['tmp_name'];
        $fa_ext         = pathinfo($_FILES['edit_fa_img']['name'], PATHINFO_EXTENSION);
        $fa_image_name  = $this->input->post('edit_fa_img_name');
        $old_fa_image	= $this->input->post('edit_image_name');

        $uname  = $this->input->post('edit_uname');
        $pword  = $this->input->post('edit_pword');
        $fname  = $this->input->post('edit_first_name');
        $mname  = $this->input->post('edit_middle_name');
        $lname  = $this->input->post('edit_last_name');
        $id     = $this->input->post('edit_id');
        $dept_id= $this->input->post('edit_dept_id');
        $gender = $this->input->post('edit_gender');
        $fa_no  = $this->input->post('edit_fa_number');

        $this->load->model("back-end/user_management/Faculty_Management_Model");
        $this->Faculty_Management_Model->edit_faculty($uname, $pword, $fname, $mname, $lname, $dept_id, $gender, $fa_no, $fa_img_content, $fa_ext, $fa_image_name, $old_fa_image, $id);
    }

    public function delete_faculty()
    {
        $fa_id = $this->input->post('fa_id');
        $fa_img= $this->input->post('fa_img');
        $this->load->model("back-end/user_management/Faculty_Management_Model");
        $this->Faculty_Management_Model->delete_faculty($fa_id, $fa_img);
    }

    public function softDelete()
    {
        $fa_id = $this->input->post('fa_id');
        $this->load->model("back-end/user_management/Faculty_Management_Model");
        echo $this->Faculty_Management_Model->softDelete($fa_id);
    }

    public function restore()
    {
        $fa_id = $this->input->post('fa_id');
        $this->load->model("back-end/user_management/Faculty_Management_Model");
        echo $this->Faculty_Management_Model->restore($fa_id);
    }
}
