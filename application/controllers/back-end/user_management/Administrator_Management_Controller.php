<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Administrator_Management_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function get_administrators()
    {
        $this->load->model("back-end/user_management/Administrator_Management_Model");
        $result = $this->Administrator_Management_Model->get_administrators();

        echo json_encode($result->result());
    }

    public function get_bin()
    {
        $this->load->model("back-end/user_management/Administrator_Management_Model");
        $result = $this->Administrator_Management_Model->get_bin();

        echo json_encode($result->result());
    }

    public function add_administrators()
    {
        $uname = $this->input->post('uname');
        $pword = $this->input->post('pword');
        $fname = $this->input->post('first_name');
        $mname = $this->input->post('middle_name');
        $lname = $this->input->post('last_name');

        $this->load->model("back-end/user_management/Administrator_Management_Model");
        $this->Administrator_Management_Model->add_administrators($uname, $pword, $fname, $mname, $lname);
    }

    public function edit_administrators()
    {
        $uname = $this->input->post('edit_uname');
        $pword = $this->input->post('edit_pword');
        $fname = $this->input->post('edit_first_name');
        $mname = $this->input->post('edit_middle_name');
        $lname = $this->input->post('edit_last_name');
        $id    = $this->input->post('edit_id');

        $this->load->model("back-end/user_management/Administrator_Management_Model");
        $this->Administrator_Management_Model->edit_administrators($uname, $pword, $fname, $mname, $lname, $id);
    }

    public function delete_administrators()
    {
        echo $ad_id = $this->input->post('ad_id');
        $this->load->model("back-end/user_management/Administrator_Management_Model");
        $this->Administrator_Management_Model->delete_administrators($ad_id);
    }

    public function softDelete()
    {
        $ad_id = $this->input->post('ad_id');
        $this->load->model("back-end/user_management/Administrator_Management_Model");
        echo $this->Administrator_Management_Model->softDelete($ad_id);
    }

    public function restore()
    {
        $ad_id = $this->input->post('ad_id');
        $this->load->model("back-end/user_management/Administrator_Management_Model");
        echo $this->Administrator_Management_Model->restore($ad_id);
    }
}
