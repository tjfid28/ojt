<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_Controller extends CI_Controller
{
    protected $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->database();
    }

    public function mobile_login()
    {
        if (!isset($_SESSION['logged_in'])) {
            $this->load->view('front-end/home/partials/mobile_view_login');
        } else {
            header("location:".base_url('Home'));
        }
    }

    public function home()
    {
        if (!isset($_SESSION['logged_in'])) {
            $this->load->model("back-end/Slide_Model");
            $this->load->model("back-end/Partner_Company_Model");
            $this->load->model("back-end/Faqs_Model");

            $data['title'] = 'Home';
            $data['slides'] = $this->Slide_Model->get_slides()->result();
            $data['companies'] = $this->Partner_Company_Model->get_companies()->result();
            $data['faqs'] = $this->Faqs_Model->get_faqs()->result();

            $data['content'] = $this->load->view('pages/home_view', $data, true);
            $this->load->view('layout/front_master_view', $data);
        } else {
            if ($_SESSION['account_type']==4) {
                header("location:Sub_Administration/dashboard");
            } elseif ($_SESSION['account_type']==3) {
                header("location:Administration/dashboard");
            } elseif ($_SESSION['account_type']==2) {
                header("location:front-end/faculty/Faculty_Controller/class_list");
            } else {
                header("location:student/class_room");
            }
        }
    }

    public function Ojt_policy()
    {
        if (!isset($_SESSION['logged_in'])) {
            $this->load->model("back-end/ojt_management/Ojt_Policy_Model");
            $data['title'] = 'OJT Policy';
            $data['policy']  = $this->Ojt_Policy_Model->get_policy();
            $data['content'] = $this->load->view('pages/ojt_policy_view', $data, true);
            $this->load->view('layout/front_master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function Partner_Company()
    {
        if (!isset($_SESSION['logged_in'])) {
            $this->load->model("back-end/Partner_Company_Model");
            $data['ict'] = $this->Partner_Company_Model->get_companies(1)->result();
            $data['hm'] = $this->Partner_Company_Model->get_companies(2)->result();
            $data['content']   = $this->load->view('pages/partner_companies_view', $data, true);
            $this->load->view('layout/front_master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function Faqs()
    {
        if (!isset($_SESSION['logged_in'])) {
            $this->load->model("back-end/Faqs_Model");
            $data['faqs'] = $this->Faqs_Model->get_faqs()->result();
            $data['content']   = $this->load->view('pages/faqs_view', $data, true);
            $this->load->view('layout/front_master_view', $data);
        } else {
            header("location:".base_url());
        }
    }

    public function admin_log_in()
    {
        if (!isset($_SESSION['logged_in'])) {
            $this->load->view('back-end/log_in_view');
        } else {
            if ($_SESSION['account_type']==3) {
                header("location:".base_url('Administration/dashboard'));
            } elseif ($_SESSION['account_type']==2) {
                header("location:".base_url('Sub_Administration/dashboard'));
            } else {
                header("location:student/class_room");
            }
        }
    }

    public function Log_out()
    {
        $this->session->sess_destroy();
        $_SESSION['logged_in']=false;
        header("location:".base_url());
    }
}
