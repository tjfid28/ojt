<?php defined('BASEPATH') or exit('No direct script access allowed');

class Log_in_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        //$this->load->library('api');
        $this->load->database();
    }

    public function index()
    {
        echo "test";
    }

    public function Log_in_function()
    {
        $uname = $this->input->post('uname');
        $pword = $this->input->post('pword');
        $this->load->model("Log_in_Model");

        $result = $this->Log_in_Model->getLoginData($uname, $pword);

        if ($result=="true") {
            echo base_url("/Home");
        } else {
            echo $result;
        }
    }

    public function Log_in_secret_function()
    {
        $uname = $this->input->post('uname');
        $pword = $this->input->post('pword');
        $this->load->model("Log_in_Model");

        $result = $this->Log_in_Model->getLoginSecretData($uname, $pword);

        if ($result=="true") {
            echo base_url("/Home");
        } else {
            echo $result;
        }
    }
}
