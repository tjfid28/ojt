<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Readable var_dump with die
 *
 * Readable dump information about a variable
 *
 * @access  public
 * @param   mixed *
 */
if (! function_exists('dd')) {
    function dd($var)
    {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
        die();
    }
}
