-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2018 at 12:23 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ojt_management_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_tbl`
--

CREATE TABLE `admin_tbl` (
  `ad_id` int(20) NOT NULL,
  `ad_uname` varchar(300) NOT NULL,
  `ad_pword` varchar(300) NOT NULL,
  `ad_fname` varchar(300) NOT NULL,
  `ad_mname` varchar(300) NOT NULL,
  `ad_lname` varchar(300) NOT NULL,
  `ad_last_login` varchar(300) NOT NULL,
  `ad_created_at` varchar(300) DEFAULT NULL,
  `ad_updated_at` varchar(300) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ad_is_active` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_tbl`
--

INSERT INTO `admin_tbl` (`ad_id`, `ad_uname`, `ad_pword`, `ad_fname`, `ad_mname`, `ad_lname`, `ad_last_login`, `ad_created_at`, `ad_updated_at`, `deleted_at`, `ad_is_active`) VALUES
(0, 'admin', 'admin', 'Admin', 'Admin', 'Admin', '', '01-01-2018 12:00:00pm', '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `announcement_comment_tbl`
--

CREATE TABLE `announcement_comment_tbl` (
  `ac_id` int(100) NOT NULL,
  `st_id` int(20) NOT NULL,
  `fa_id` int(20) NOT NULL,
  `ac_content` text NOT NULL,
  `ac_date_posted` varchar(500) NOT NULL,
  `cla_ann_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checklist_tbl`
--

CREATE TABLE `checklist_tbl` (
  `cl_id` int(11) NOT NULL,
  `st_id` int(11) NOT NULL,
  `req_id` int(11) NOT NULL,
  `cl_date_checked` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class_announcement_tbl`
--

CREATE TABLE `class_announcement_tbl` (
  `cla_ann_id` int(20) NOT NULL,
  `cla_id` int(20) NOT NULL,
  `fa_id` int(20) NOT NULL,
  `ann_post` text NOT NULL,
  `ann_attachment` varchar(255) NOT NULL,
  `ann_date_posted` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class_student_list_tbl`
--

CREATE TABLE `class_student_list_tbl` (
  `cs_id` int(20) NOT NULL,
  `st_id` int(20) NOT NULL,
  `cla_id` int(20) NOT NULL,
  `cs_added_at` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_student_list_tbl`
--

INSERT INTO `class_student_list_tbl` (`cs_id`, `st_id`, `cla_id`, `cs_added_at`) VALUES
(1, 1, 1, '19-01-2018 03:04:01pm');

-- --------------------------------------------------------

--
-- Table structure for table `class_tbl`
--

CREATE TABLE `class_tbl` (
  `cla_id` int(20) NOT NULL,
  `sec_id` int(20) NOT NULL,
  `sub_id` int(20) NOT NULL,
  `cou_id` int(20) NOT NULL,
  `fa_id` int(20) NOT NULL,
  `cla_slug` varchar(500) NOT NULL,
  `cla_image_name` varchar(500) NOT NULL,
  `cla_term` varchar(400) NOT NULL,
  `cla_year` varchar(400) NOT NULL,
  `cla_status` varchar(200) NOT NULL,
  `cla_created_at` varchar(500) NOT NULL,
  `cla_updated_at` varchar(500) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_tbl`
--

INSERT INTO `class_tbl` (`cla_id`, `sec_id`, `sub_id`, `cou_id`, `fa_id`, `cla_slug`, `cla_image_name`, `cla_term`, `cla_year`, `cla_status`, `cla_created_at`, `cla_updated_at`, `deleted_at`) VALUES
(1, 2, 1, 1, 1, '', '', '2nd', '2017-2018', 'Open', '19-01-2018 03:03:49pm', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_tbl`
--

CREATE TABLE `company_tbl` (
  `co_id` int(11) NOT NULL,
  `co_name` varchar(500) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `co_type` tinyint(4) NOT NULL,
  `co_description` text NOT NULL,
  `co_link` varchar(255) DEFAULT NULL,
  `co_email` varchar(255) NOT NULL,
  `co_image_name` varchar(500) NOT NULL,
  `co_created_at` varchar(300) NOT NULL,
  `co_updated_at` varchar(300) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_tbl`
--

INSERT INTO `company_tbl` (`co_id`, `co_name`, `slug`, `co_type`, `co_description`, `co_link`, `co_email`, `co_image_name`, `co_created_at`, `co_updated_at`, `deleted_at`) VALUES
(1, 'Alorica', 'alorica', 1, 'We&rsquo;re a company that does one thing&mdash;we make lives better&hellip;one interaction at a time&mdash;for our clients, customers, colleagues and communities.\r\n', 'https://www.alorica.com/', 'jerwil.lapa29@gmail.com', '929-1298878494.jpg', '19-01-2018 02:51:44pm', '', NULL),
(2, 'Astoria Plaza', 'astoria-plaza', 2, 'Located in Ortigas Center, one of the Philippines&#39; progressive business and commercial districts, Astoria Plaza, a hotel in Pasig, offers you a private niche in the heart of the city.\r\n', 'https://www.astoriaplaza.com/', 'jerwil.lapa29@gmail.com', '575-1244714160.jpg', '19-01-2018 02:52:41pm', '', NULL),
(3, 'Action Labs IT Service Philippines Corporation', 'action-labs-it-service-philippines-corporation', 1, 'ActionLabs IT Services Phils. Corp. is an affiliate company of Worx Group of Companies founded in 1996. Our first Service Center was established in the heart of Gilmore. Additional centers were established in Olongapo, Makati, Angeles, Cagayan de Oro, Cebu and Davao.\r\n', 'http://www.actionlabs.com.ph/', 'jerwil.lapa29@gmail.com', '948-1320060343.jpg', '19-01-2018 02:54:14pm', '', NULL),
(4, 'Belmont Hotels & Resorts', 'belmont-hotels-resorts', 2, 'Belmont Hotel Manila is only a 5-minute walk away from Ninoy Aquino International Airport&rsquo;s Terminal 3 via the Runway Manila footbridge. Enjoy the scenic view of the the Manila skyline as you walk across to the hotel.\r\n', 'https://www.belmonthotelmanila.com/', 'jerwil.lapa29@gmail.com', '962-1246350831.jpg', '19-01-2018 02:55:26pm', '', NULL),
(5, 'Cabalen Management Inc.', 'cabalen-management-inc', 1, 'Cabalen is a group of casual - fine dining restaurants known for authentic Capampangan dishes and different Filipino specialties.\r\n', 'https://www.facebook.com/CabalenRestaurant/', '', '853-1309742197.jpg', '19-01-2018 02:57:45pm', '', NULL),
(6, 'City Garden Suites', 'city-garden-suites', 2, 'City Garden Suites, Hotel in Manila sits right in the heart of Manila as a benchmark of convenience and business-class comfort.\r\n', 'https://www.citygardensuites.com/', '', '153-1243812805.jpg', '19-01-2018 02:58:56pm', '', NULL),
(7, 'Diliman Educational Corporation', 'diliman-educational-corporation', 1, 'At Diliman Preparatory School, you don&#39;t have to worry about what tomorrow brings - because we make sure your child is ready for it.\r\n', 'http://www.dps.edu.ph/', '', '166-1249707194.jpg', '19-01-2018 03:00:43pm', '', NULL),
(8, 'Best Western', 'best-western', 2, 'International excellence and a charming local character blend together at Best Western Hotel La Corona Manila.&nbsp;\r\n', 'http://www.bestwesternhotelmanila.com/', '', '143-1267461520.jpg', '19-01-2018 03:01:53pm', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `course_tbl`
--

CREATE TABLE `course_tbl` (
  `cou_id` int(20) NOT NULL,
  `cou_title` varchar(500) NOT NULL,
  `cou_created_at` varchar(300) NOT NULL,
  `cou_updated_at` varchar(300) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_tbl`
--

INSERT INTO `course_tbl` (`cou_id`, `cou_title`, `cou_created_at`, `cou_updated_at`, `deleted_at`) VALUES
(1, 'Bachelor of Science in Information Technology', '19-01-2018 11:45:15am', '', NULL),
(2, 'Bachelor of Science in Computer Science', '19-01-2018 11:47:42am', '', NULL),
(3, 'Bachelor of Science in Computer Engineering', '19-01-2018 11:47:48am', '', NULL),
(4, 'Bachelor of Science in Accounting Technology', '19-01-2018 11:48:02am', '', NULL),
(5, 'Bachelor of Science in Business Management', '19-01-2018 11:48:16am', '', NULL),
(6, 'Bachelor of Science in Hotel and Restaurant Management', '19-01-2018 11:48:35am', '', NULL),
(7, 'Bachelor of Science in Tourism Management', '19-01-2018 11:48:59am', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `department_tbl`
--

CREATE TABLE `department_tbl` (
  `dept_id` int(20) NOT NULL,
  `dept_name` varchar(500) NOT NULL,
  `dept_created_at` varchar(300) NOT NULL,
  `dept_updated_at` varchar(300) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_tbl`
--

INSERT INTO `department_tbl` (`dept_id`, `dept_name`, `dept_created_at`, `dept_updated_at`, `deleted_at`) VALUES
(1, 'IT', '16-01-2018 02:38:38pm', '', NULL),
(2, 'Gen Ed', '19-01-2018 11:49:46am', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `documents_tbl`
--

CREATE TABLE `documents_tbl` (
  `d_id` int(20) NOT NULL,
  `st_id` int(20) NOT NULL,
  `d_file_name` varchar(500) NOT NULL,
  `d_slug` varchar(500) NOT NULL,
  `d_created_at` varchar(500) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `downloadable_forms_tbl`
--

CREATE TABLE `downloadable_forms_tbl` (
  `df_id` int(20) NOT NULL,
  `req_id` int(20) NOT NULL,
  `df_file_name` varchar(500) NOT NULL,
  `df_slug` varchar(300) NOT NULL,
  `df_created_at` varchar(300) NOT NULL,
  `df_updated_at` varchar(300) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `faculty_tbl`
--

CREATE TABLE `faculty_tbl` (
  `fa_id` int(20) NOT NULL,
  `fa_number` varchar(300) NOT NULL,
  `fb_id` varchar(500) NOT NULL,
  `dept_id` int(20) NOT NULL,
  `fa_uname` varchar(500) NOT NULL,
  `fa_pword` varchar(500) NOT NULL,
  `fa_image_name` varchar(500) NOT NULL,
  `fa_fname` varchar(500) NOT NULL,
  `fa_mname` varchar(500) NOT NULL,
  `fa_lname` varchar(500) NOT NULL,
  `fa_gender` varchar(300) NOT NULL,
  `fa_last_login` varchar(300) NOT NULL,
  `fa_created_at` varchar(300) NOT NULL,
  `fa_updated_at` varchar(300) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `fa_is_active` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty_tbl`
--

INSERT INTO `faculty_tbl` (`fa_id`, `fa_number`, `fb_id`, `dept_id`, `fa_uname`, `fa_pword`, `fa_image_name`, `fa_fname`, `fa_mname`, `fa_lname`, `fa_gender`, `fa_last_login`, `fa_created_at`, `fa_updated_at`, `deleted_at`, `fa_is_active`) VALUES
(1, '021-0001', '', 1, 'faculty', 'faculty', '39-1330609757.png', 'Faculty', 'Faculty', 'Faculty', 'Female', '', '19-01-2018 12:01:51pm', '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `faq_tbl`
--

CREATE TABLE `faq_tbl` (
  `fa_id` int(11) NOT NULL,
  `fa_title` varchar(255) NOT NULL,
  `fa_description` text NOT NULL,
  `fa_created_at` varchar(255) NOT NULL,
  `fa_updated_at` varchar(255) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq_tbl`
--

INSERT INTO `faq_tbl` (`fa_id`, `fa_title`, `fa_description`, `fa_created_at`, `fa_updated_at`, `deleted_at`) VALUES
(1, 'What is this website about?', 'It is an OJT Management System for all students of STI Novaliches enrolling the On-The-Job Training program.&nbsp;\r\n', '19-01-2018 03:03:16pm', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `log_tbl`
--

CREATE TABLE `log_tbl` (
  `log_id` int(11) NOT NULL,
  `log_user_id` int(11) NOT NULL,
  `log_module` varchar(255) NOT NULL,
  `log_action` varchar(255) NOT NULL,
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_tbl`
--

INSERT INTO `log_tbl` (`log_id`, `log_user_id`, `log_module`, `log_action`, `log_date`) VALUES
(1, 0, 'Uploaded Documents', 'ARCHIVE', '2018-02-12 11:16:36'),
(2, 0, 'Uploaded Documents', 'RESTORE', '2018-02-12 11:21:14'),
(3, 0, 'Uploaded Documents', 'ARCHIVE', '2018-02-12 11:21:27'),
(4, 0, 'Uploaded Documents', 'DELETE', '2018-02-12 11:22:00');

-- --------------------------------------------------------

--
-- Table structure for table `news_tbl`
--

CREATE TABLE `news_tbl` (
  `n_id` int(11) NOT NULL,
  `n_content` text NOT NULL,
  `n_date_posted` varchar(500) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification_tbl`
--

CREATE TABLE `notification_tbl` (
  `n_id` int(100) NOT NULL,
  `fa_id` int(11) NOT NULL,
  `st_id` int(11) NOT NULL,
  `n_content` text NOT NULL,
  `n_link` varchar(500) NOT NULL,
  `n_created_at` varchar(200) NOT NULL,
  `seen` varchar(100) NOT NULL,
  `n_show_to` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ojt_policy_tbl`
--

CREATE TABLE `ojt_policy_tbl` (
  `op_id` int(10) NOT NULL,
  `op_content` text NOT NULL,
  `op_image` varchar(500) NOT NULL,
  `op_updated_at` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ojt_policy_tbl`
--

INSERT INTO `ojt_policy_tbl` (`op_id`, `op_content`, `op_image`, `op_updated_at`) VALUES
(1, 'The On-the-Job-Training (OJT) course is designed to provide an opportunity for students to learn through experience by exposing them to an actual working environment in their chosen career field as part of their academic requirement.', 'policy_cover_752.jpg', '19-01-2018 02:45:38pm');

-- --------------------------------------------------------

--
-- Table structure for table `ojt_requirements_tbl`
--

CREATE TABLE `ojt_requirements_tbl` (
  `req_id` int(20) NOT NULL,
  `cou_id` int(20) NOT NULL,
  `sub_id` int(20) NOT NULL,
  `req_name` varchar(500) NOT NULL,
  `req_created_at` varchar(300) NOT NULL,
  `req_updated_at` varchar(300) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ojt_requirements_tbl`
--

INSERT INTO `ojt_requirements_tbl` (`req_id`, `cou_id`, `sub_id`, `req_name`, `req_created_at`, `req_updated_at`, `deleted_at`) VALUES
(0, 0, 0, 'Resume', '01-01-2018 12:00:00pm', '', NULL),
(1, 0, 0, 'Endorsement Letter receiving copy', '01-01-2018 12:00:00pm', '', NULL),
(2, 0, 0, 'Daily Time Record', '01-01-2018 12:00:00pm', '', NULL),
(3, 0, 0, 'OJT Training Plan', '01-01-2018 12:00:00pm', '', NULL),
(4, 0, 0, 'Parents Waiver', '01-01-2018 12:00:00pm', '', NULL),
(5, 0, 0, 'Confirmation Slip', '01-01-2018 12:00:00pm', '', NULL),
(6, 0, 0, 'Work Schedule', '01-01-2018 12:00:00pm', '', NULL),
(7, 0, 0, 'Certificate of Completion', '01-01-2018 12:00:00pm', '', NULL),
(8, 0, 0, 'Performance Appraisal Form', '01-01-2018 12:00:00pm', '', NULL),
(9, 0, 0, 'Integration Paper', '01-01-2018 12:00:00pm', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `section_tbl`
--

CREATE TABLE `section_tbl` (
  `sec_id` int(20) NOT NULL,
  `sec_name` varchar(300) NOT NULL,
  `sec_created_at` varchar(300) NOT NULL,
  `sec_updated_at` varchar(300) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section_tbl`
--

INSERT INTO `section_tbl` (`sec_id`, `sec_name`, `sec_created_at`, `sec_updated_at`, `deleted_at`) VALUES
(1, 'BS801A', '19-01-2018 11:50:36am', '', NULL),
(2, 'BT801M', '19-01-2018 11:50:42am', '', NULL),
(3, 'BSBT801P', '19-01-2018 11:50:49am', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slide_tbl`
--

CREATE TABLE `slide_tbl` (
  `s_id` int(11) NOT NULL,
  `s_slug_name` varchar(500) NOT NULL,
  `s_caption` varchar(500) NOT NULL,
  `s_description` text NOT NULL,
  `s_created_at` varchar(500) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide_tbl`
--

INSERT INTO `slide_tbl` (`s_id`, `s_slug_name`, `s_caption`, `s_description`, `s_created_at`, `deleted_at`) VALUES
(1, '654-1263393561.jpg', 'Your Teacher, Your Mentor', 'At STI, the training starts with having the right educator. They play a crucial role in the student’s journey by guiding them in their studies and shaping them to be well-rounded individuals.', '01-01-2018 12:00:00pm', NULL),
(2, '795-1383155210.jpg', 'Arts & Science', 'Bring Ideas to Life', '01-01-2018 12:00:00pm', NULL),
(3, '834-1313240879.jpg', 'ICT', 'Be the next Visionary', '01-01-2018 12:00:00pm', NULL),
(4, '608-1386499713.jpg', 'HRM', 'Savor your Opportunities', '01-01-2018 12:00:00pm', NULL),
(5, '326-1385088380.jpg', 'Engineering', 'Design Tomorrow\'s Technology', '01-01-2018 12:00:00pm', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_requirements_tbl`
--

CREATE TABLE `student_requirements_tbl` (
  `sr_id` int(20) NOT NULL,
  `st_id` int(20) NOT NULL,
  `d_id` int(20) NOT NULL,
  `req_id` int(20) NOT NULL,
  `sr_status` varchar(300) NOT NULL,
  `sr_date_checked` varchar(500) NOT NULL,
  `sr_date_submitted` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_tbl`
--

CREATE TABLE `student_tbl` (
  `st_id` int(20) NOT NULL,
  `cou_id` int(20) NOT NULL,
  `st_number` varchar(200) NOT NULL,
  `st_email` varchar(255) NOT NULL,
  `st_mobile_number` varchar(255) NOT NULL,
  `fb_id` varchar(500) NOT NULL,
  `st_uname` varchar(300) NOT NULL,
  `st_pword` varchar(300) NOT NULL,
  `st_image_name` varchar(500) NOT NULL,
  `st_fname` varchar(300) NOT NULL,
  `st_mname` varchar(300) NOT NULL,
  `st_lname` varchar(300) NOT NULL,
  `st_gender` varchar(200) NOT NULL,
  `st_school_year` varchar(255) NOT NULL,
  `st_last_login` varchar(300) NOT NULL,
  `st_created_at` varchar(300) NOT NULL,
  `st_updated_at` varchar(300) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `st_is_active` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_tbl`
--

INSERT INTO `student_tbl` (`st_id`, `cou_id`, `st_number`, `st_email`, `st_mobile_number`, `fb_id`, `st_uname`, `st_pword`, `st_image_name`, `st_fname`, `st_mname`, `st_lname`, `st_gender`, `st_school_year`, `st_last_login`, `st_created_at`, `st_updated_at`, `deleted_at`, `st_is_active`) VALUES
(1, 1, '021-2000-0000', 'john.doe@example.com', '639123456789', '', 'student', 'student', '71-1402196340.png', 'Student', 'Student', 'Student', 'Male', '2000-2001', '', '23-01-2018 02:24:44pm', '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `subject_tbl`
--

CREATE TABLE `subject_tbl` (
  `sub_id` int(20) NOT NULL,
  `sub_name` varchar(400) NOT NULL,
  `sub_req_hours` varchar(300) NOT NULL,
  `sub_unit` int(10) NOT NULL,
  `sub_created_at` varchar(300) NOT NULL,
  `sub_updated_at` varchar(300) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject_tbl`
--

INSERT INTO `subject_tbl` (`sub_id`, `sub_name`, `sub_req_hours`, `sub_unit`, `sub_created_at`, `sub_updated_at`, `deleted_at`) VALUES
(1, 'OJT', '364', 3, '19-01-2018 11:50:28am', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_admin_tbl`
--

CREATE TABLE `sub_admin_tbl` (
  `s_ad_id` int(20) NOT NULL,
  `s_ad_uname` varchar(300) NOT NULL,
  `s_ad_pword` varchar(300) NOT NULL,
  `s_ad_fname` varchar(300) NOT NULL,
  `s_ad_mname` varchar(300) NOT NULL,
  `s_ad_lname` varchar(300) NOT NULL,
  `s_dashboard` int(11) NOT NULL COMMENT 'permission',
  `s_news` int(11) NOT NULL COMMENT 'permission',
  `s_class_list` int(11) NOT NULL COMMENT 'permission',
  `s_sections` int(10) NOT NULL COMMENT 'permission',
  `s_courses` int(10) NOT NULL COMMENT 'permission',
  `s_departments` int(10) NOT NULL COMMENT 'permission',
  `s_programs` int(10) NOT NULL COMMENT 'permission',
  `s_faculty` int(11) NOT NULL COMMENT 'permission',
  `s_student` int(11) NOT NULL COMMENT 'permission',
  `s_uploaded_documents` int(11) NOT NULL COMMENT 'permission',
  `s_ojt_requirement` int(11) NOT NULL COMMENT 'permission',
  `s_downloadable_forms` int(11) NOT NULL COMMENT 'permission',
  `s_ojt_policy` int(11) NOT NULL COMMENT 'permission',
  `s_company` int(10) NOT NULL,
  `s_slides` int(11) NOT NULL COMMENT 'permission',
  `s_ad_last_login` varchar(300) NOT NULL,
  `s_ad_created_at` varchar(300) DEFAULT NULL,
  `s_ad_updated_at` varchar(300) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `s_ad_is_active` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_admin_tbl`
--

INSERT INTO `sub_admin_tbl` (`s_ad_id`, `s_ad_uname`, `s_ad_pword`, `s_ad_fname`, `s_ad_mname`, `s_ad_lname`, `s_dashboard`, `s_news`, `s_class_list`, `s_sections`, `s_courses`, `s_departments`, `s_programs`, `s_faculty`, `s_student`, `s_uploaded_documents`, `s_ojt_requirement`, `s_downloadable_forms`, `s_ojt_policy`, `s_company`, `s_slides`, `s_ad_last_login`, `s_ad_created_at`, `s_ad_updated_at`, `deleted_at`, `s_ad_is_active`) VALUES
(1, 'subadmin', 'subadmin', 'Subadmin', 'Subadmin', 'Subadmin', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '', '12-02-2018 05:54:19pm', '', NULL, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  ADD PRIMARY KEY (`ad_id`);

--
-- Indexes for table `announcement_comment_tbl`
--
ALTER TABLE `announcement_comment_tbl`
  ADD PRIMARY KEY (`ac_id`);

--
-- Indexes for table `checklist_tbl`
--
ALTER TABLE `checklist_tbl`
  ADD PRIMARY KEY (`cl_id`);

--
-- Indexes for table `class_announcement_tbl`
--
ALTER TABLE `class_announcement_tbl`
  ADD PRIMARY KEY (`cla_ann_id`);

--
-- Indexes for table `class_student_list_tbl`
--
ALTER TABLE `class_student_list_tbl`
  ADD PRIMARY KEY (`cs_id`);

--
-- Indexes for table `class_tbl`
--
ALTER TABLE `class_tbl`
  ADD PRIMARY KEY (`cla_id`);

--
-- Indexes for table `company_tbl`
--
ALTER TABLE `company_tbl`
  ADD PRIMARY KEY (`co_id`);

--
-- Indexes for table `course_tbl`
--
ALTER TABLE `course_tbl`
  ADD PRIMARY KEY (`cou_id`);

--
-- Indexes for table `department_tbl`
--
ALTER TABLE `department_tbl`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `documents_tbl`
--
ALTER TABLE `documents_tbl`
  ADD PRIMARY KEY (`d_id`);

--
-- Indexes for table `downloadable_forms_tbl`
--
ALTER TABLE `downloadable_forms_tbl`
  ADD PRIMARY KEY (`df_id`);

--
-- Indexes for table `faculty_tbl`
--
ALTER TABLE `faculty_tbl`
  ADD PRIMARY KEY (`fa_id`);

--
-- Indexes for table `faq_tbl`
--
ALTER TABLE `faq_tbl`
  ADD PRIMARY KEY (`fa_id`);

--
-- Indexes for table `log_tbl`
--
ALTER TABLE `log_tbl`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `news_tbl`
--
ALTER TABLE `news_tbl`
  ADD PRIMARY KEY (`n_id`);

--
-- Indexes for table `notification_tbl`
--
ALTER TABLE `notification_tbl`
  ADD PRIMARY KEY (`n_id`);

--
-- Indexes for table `ojt_policy_tbl`
--
ALTER TABLE `ojt_policy_tbl`
  ADD PRIMARY KEY (`op_id`);

--
-- Indexes for table `ojt_requirements_tbl`
--
ALTER TABLE `ojt_requirements_tbl`
  ADD PRIMARY KEY (`req_id`);

--
-- Indexes for table `section_tbl`
--
ALTER TABLE `section_tbl`
  ADD PRIMARY KEY (`sec_id`);

--
-- Indexes for table `slide_tbl`
--
ALTER TABLE `slide_tbl`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `student_requirements_tbl`
--
ALTER TABLE `student_requirements_tbl`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `student_tbl`
--
ALTER TABLE `student_tbl`
  ADD PRIMARY KEY (`st_id`);

--
-- Indexes for table `subject_tbl`
--
ALTER TABLE `subject_tbl`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `sub_admin_tbl`
--
ALTER TABLE `sub_admin_tbl`
  ADD PRIMARY KEY (`s_ad_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  MODIFY `ad_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcement_comment_tbl`
--
ALTER TABLE `announcement_comment_tbl`
  MODIFY `ac_id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `checklist_tbl`
--
ALTER TABLE `checklist_tbl`
  MODIFY `cl_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `class_announcement_tbl`
--
ALTER TABLE `class_announcement_tbl`
  MODIFY `cla_ann_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `class_student_list_tbl`
--
ALTER TABLE `class_student_list_tbl`
  MODIFY `cs_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `class_tbl`
--
ALTER TABLE `class_tbl`
  MODIFY `cla_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company_tbl`
--
ALTER TABLE `company_tbl`
  MODIFY `co_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `course_tbl`
--
ALTER TABLE `course_tbl`
  MODIFY `cou_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `department_tbl`
--
ALTER TABLE `department_tbl`
  MODIFY `dept_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `documents_tbl`
--
ALTER TABLE `documents_tbl`
  MODIFY `d_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `downloadable_forms_tbl`
--
ALTER TABLE `downloadable_forms_tbl`
  MODIFY `df_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faculty_tbl`
--
ALTER TABLE `faculty_tbl`
  MODIFY `fa_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `faq_tbl`
--
ALTER TABLE `faq_tbl`
  MODIFY `fa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `log_tbl`
--
ALTER TABLE `log_tbl`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `news_tbl`
--
ALTER TABLE `news_tbl`
  MODIFY `n_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_tbl`
--
ALTER TABLE `notification_tbl`
  MODIFY `n_id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ojt_policy_tbl`
--
ALTER TABLE `ojt_policy_tbl`
  MODIFY `op_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ojt_requirements_tbl`
--
ALTER TABLE `ojt_requirements_tbl`
  MODIFY `req_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `section_tbl`
--
ALTER TABLE `section_tbl`
  MODIFY `sec_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `slide_tbl`
--
ALTER TABLE `slide_tbl`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `student_requirements_tbl`
--
ALTER TABLE `student_requirements_tbl`
  MODIFY `sr_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_tbl`
--
ALTER TABLE `student_tbl`
  MODIFY `st_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subject_tbl`
--
ALTER TABLE `subject_tbl`
  MODIFY `sub_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sub_admin_tbl`
--
ALTER TABLE `sub_admin_tbl`
  MODIFY `s_ad_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
